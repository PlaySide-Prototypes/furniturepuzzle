﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EasyResources
{
    [System.Serializable]
    public class Resource
    {
		public List<UIBar> bars = new List<UIBar>();

		public bool maxOnInit;
		public float max = 100, min = 0;
		
		private float current;
		public float Current { get { return current; } }

        private readonly bool clampRange = true;

        public delegate void OnValueAdjusted();
        public OnValueAdjusted onValueAdusted;

		public delegate void OnEmpty();
		public OnEmpty onEmpty;

		public delegate void OnMax();
		public OnMax onMax;

		public float Percentage { get { return current / max; } }

		public bool IsEmpty { get { return current <= min; } }

		public Resource()
		{
			Initialise();
		}
		public Resource(Resource other)
		{
			max = other.max;
			min = other.min;
			current = other.current;
			bars = new List<UIBar>();
			foreach(UIBar b in other.bars)
			{
				AddBar(b);
			}
			Initialise();
		}

		public virtual void Initialise()
        {
			if(maxOnInit)
			{
				SetPercentage(100);
			}
			UpdateBars(true);
		}

		public void AddBar(UIBar bar)
		{
			bars.Add(bar);
			UpdateBars(true);
		}
		public void RemoveBar(UIBar bar)
		{
			bars.Remove(bar);
		}

		public void SetMax(float value)
		{
			max = value;
		}

		/// <summary>
		/// Adds the passed in value to the current value
		/// </summary>
		/// <param name="a_change"></param>
		public void AdjustValue(float change)
        {
			SetValue(current + change);
		}

        /// <summary>
        /// Sets the value of the bar to a specified value
        /// </summary>
        /// <param name="a_value"></param>
        public void SetValue(float value)
        {
            onValueAdusted?.Invoke();
			bool hitMax = value >= max && current < max;
			bool hitMin = value <= min && current > min;
			current = value;
			if (clampRange)
			{
				current = Mathf.Clamp(current, min, max);
			}
			if (hitMin)
			{
				onEmpty?.Invoke();
			}
			if(hitMax)
			{
				onMax?.Invoke();
			}
			UpdateBars();
		}

        /// <summary>
        /// Sets the value of the bar to a specified value
        /// </summary>
        /// <param name="a_value"></param>
        public void SetPercentage(float value)
        {
			SetValue(max * (value / 100));
          

        }

		public void UpdateBars(bool instant = false)
		{
			for (var i = bars.Count - 1; i > -1; i--)
			{
				if (bars[i] == null)
					bars.RemoveAt(i);
				else
					bars[i].UpdateBar(current,max, instant);
			}
		}
    }
}
