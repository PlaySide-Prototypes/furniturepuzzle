﻿using EasyResources;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBar : MonoBehaviour
{

	public Image backing;

	[Header("Fill")]
	public bool useFill = true;
	[DrawIf("useFill", true)]
	public Image fill;
	[DrawIf("useFill", true)]
	public float fillSpeed;

	[Header("Decay")]
	public bool useDecay;
	[DrawIf("useDecay", true)]
	public Image decay;
	[DrawIf("useDecay", true)]
	public float decaySpeed;
	[DrawIf("useDecay", true)]
	public float decayDelay;


	[Header("Text")]
	public bool useText;
	[DrawIf("useText", true)]
	public TMPro.TextMeshProUGUI amountText;
	public enum TextDisplayModes
	{
		Amount,
		CurrentMax,
		Percentage
	}
	[DrawIf("useText", true)]
	public TextDisplayModes textDisplayMode;

	private float targetPerect;
	private float startPercent;
	private float startdecayPercent;

	private float decayTimer = 0;
	private float decayDelayTimer = 0;
	private float slideTimer = 0;

	private bool sliding;
	private bool delaying;
	private bool decaying;

	public Transform followPoint;

	public Resource resource = null;
	private float destroyTimer;
	private float destroyTime;

	
	public void UpdateBar(float current, float max, bool instant = false)
	{
		UpdateBar(current / max, instant);
		UpdateText(current, max);

	}
	public void UpdateText(float current, float max)
	{
		switch (textDisplayMode)
		{
			case TextDisplayModes.Amount:
				amountText.text = current.ToString();
				break;
			case TextDisplayModes.CurrentMax:
				amountText.text = current + "/" + max;
				break;
			case TextDisplayModes.Percentage:
				amountText.text = ((current / max) * 100).ToString() + "%";
				break;
		}
	}
	public void UpdateBar(float percent, bool instant = false)
	{
		if (percent == 0 && fill.fillAmount == 0)
		{
			return;
		}
		if(instant)
		{
			fill.fillAmount = percent;
			decay.fillAmount = fill.fillAmount;
			sliding = false;
			decaying = false;
			delaying = false;
		}
		else 
		{
			if (useFill)
			{
				sliding = true;
				slideTimer = 0;
				startPercent = fill.fillAmount;
				targetPerect = percent;

			}
			if (useDecay)
			{
				decaying = false;
				delaying = true;
				decayDelayTimer = 0;
				decayTimer = 0;
				startdecayPercent = decay.fillAmount;
			}
		}
		
	}

	public void Initialise(Resource r = null, float destroyTime = -1)
	{
		this.destroyTime = destroyTime;
		this.resource = r;
		destroyTimer = destroyTime;
	}

	public void ResetDestroyTimer()
	{
		destroyTimer = destroyTime;
	}
	public void Update()
	{
		if(destroyTime != 0)
		{
			destroyTimer -= Time.deltaTime;
			if(destroyTimer <= 0)
			{
				if(resource != null)
				{
					if(resource.bars.Contains(this))
					{
						resource.RemoveBar(this);
					}
				}
				Destroy(this.gameObject);
			}
		}
		if (sliding)
		{
			
			slideTimer += Time.deltaTime;
			fill.fillAmount = Mathf.Lerp(startPercent, targetPerect, slideTimer / fillSpeed);
			
			if (slideTimer > fillSpeed)
			{
			
				slideTimer = 0;
				sliding = false;
			}
		}
		if (delaying)
		{
			if (decay != null)
			{
				decayDelayTimer += Time.deltaTime;
				if (decayDelayTimer >= decayDelay)
				{
					decaying = true;
					delaying = false;
				}
			}
		}
		if (decaying)
		{
			decay.fillAmount = Mathf.Lerp(startdecayPercent, targetPerect, decayTimer / decaySpeed);
			decayTimer += Time.deltaTime;
			if (decayTimer > decaySpeed)
			{
				decay.fillAmount = fill.fillAmount;
				decayTimer = 0;
				decaying = false;
			}
		}
		if(followPoint != null)
		{
			transform.position = Camera.main.WorldToScreenPoint(followPoint.position);
		}
	}


}
