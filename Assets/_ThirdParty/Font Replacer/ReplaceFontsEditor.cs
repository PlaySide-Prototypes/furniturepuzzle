﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(ReplaceFonts))]
public class ReplaceFontsEditor : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		if (GUILayout.Button("Replace All fonts"))
		{
			ReplaceFonts f = (ReplaceFonts)target;
			if (EditorUtility.DisplayDialog("Replace all Fonts in editor?",
			  "Are you sure you want to Replace", "Yes Replace", "No! Abort"))
			{
				f.Run();
			}
		}
	}
}
#endif