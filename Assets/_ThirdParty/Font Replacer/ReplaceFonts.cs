﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;

[ExecuteInEditMode]
public class ReplaceFonts : MonoBehaviour
{

	public Font fontToReplace;
	public TMPro.TMP_FontAsset fontAssetToReplace;

	public Font newFont;
	public TMPro.TMP_FontAsset newFontAsset;

	private int fontsReplaced;
   public void Run()
   {
		fontsReplaced = 0;
		if(newFont == null || newFontAsset == null)
		{
			Debug.LogError("The new font was not assigned please add one to the asset");
			return;
		}
		string[] guids = AssetDatabase.FindAssets("t:Prefab", new[] { "Assets" });

		foreach (string guid in guids)
		{


			string path = AssetDatabase.GUIDToAssetPath(guid);

			GameObject thisObject = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject;
			
			//EDITOR ASSET REPLACEMENT
			if(thisObject != null)
			{
			
				Text[] texts = thisObject.GetComponentsInChildren<Text>();
				foreach(Text t in texts)
				{
					if(t.font == fontToReplace)
					{
						Debug.Log("Replaced " + t.font + " with " + newFont);
						fontsReplaced++;
						t.font = newFont;
					}
					else if (t.font == null)
					{
						Debug.Log("font was null, setting to" + newFont);
						fontsReplaced++;
						t.font = newFont;
					}
				}

				TMPro.TextMeshProUGUI[] proTexts = thisObject.GetComponentsInChildren<TMPro.TextMeshProUGUI>();
				foreach(TMPro.TextMeshProUGUI t in proTexts)
				{
					
					if(t.font == fontToReplace)
					{
						Debug.Log("Replaced " + t.font + " with " + newFontAsset);
						fontsReplaced++;
						t.font = newFontAsset;
					}
					else if (t.font == null)
					{
						Debug.Log("font was null, setting to" + newFontAsset);
						fontsReplaced++;
						t.font = newFontAsset;
					}
				}
			}

			//SCENE REPLACEMENT
			Object[] sceneTexts = GameObject.FindObjectsOfTypeAll(typeof(Text));
			foreach (Text t in sceneTexts)
			{
		
				if (t.font == fontToReplace)
				{
					Debug.Log("Replaced " + t.font + " with " + newFont);
					fontsReplaced++;
					t.font = newFont;
				}
				else if (t.font == null)
				{
					Debug.Log("font was null, setting to" + newFont);
					fontsReplaced++;
					t.font = newFont;
				}
	
			}
			Object[] sceneTextsPro = GameObject.FindObjectsOfTypeAll(typeof(TMPro.TextMeshProUGUI));
			foreach (TMPro.TextMeshProUGUI t in sceneTextsPro)
			{

				if (t.font == fontAssetToReplace)
				{
					Debug.Log("Replaced " + t.font + " with " + newFontAsset);
					fontsReplaced++;
					t.font = newFontAsset;
				}
				else if (t.font == null)
				{
					Debug.Log("font was null, setting to" + newFontAsset);
					fontsReplaced++;
					t.font = newFontAsset;
				}
			}




		}
		Debug.Log("Complete: Replaced " + fontsReplaced + " fonts");
	}
}
#endif
