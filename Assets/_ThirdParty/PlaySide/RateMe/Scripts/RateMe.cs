﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(RateMe))]
public class RateMeEditor : Editor
{
	public override void OnInspectorGUI()
	{
		serializedObject.Update();

		SerializedProperty urlTypeProp = serializedObject.FindProperty("urlType");
		SerializedProperty gameID = serializedObject.FindProperty("appleGameID");

		EditorGUILayout.PropertyField(urlTypeProp);
		RateMe.URLType result = (RateMe.URLType)urlTypeProp.enumValueIndex;

		if (result == RateMe.URLType.Manual)
			EditorGUILayout.PropertyField(gameID);

		serializedObject.ApplyModifiedProperties();
	}

}
#endif



[Serializable]
public class iTunesJSON
{
	[System.Serializable]
	public class Rootobject
	{
		public int resultCount;
		public Result[] results;
	}

	[System.Serializable]
	public class Result
	{
		public object[] appletvScreenshotUrls;
		public string artworkUrl60;
		public string artworkUrl512;
		public string artworkUrl100;
		public string artistViewUrl;
		public bool isGameCenterEnabled;
		public string[] screenshotUrls;
		public string[] ipadScreenshotUrls;
		public string kind;
		public string[] features;
		public string[] supportedDevices;
		public object[] advisories;
		public float averageUserRatingForCurrentVersion;
		public string trackCensoredName;
		public string[] languageCodesISO2A;
		public string fileSizeBytes;
		public string sellerUrl;
		public string contentAdvisoryRating;
		public int userRatingCountForCurrentVersion;
		public string trackViewUrl;
		public string trackContentRating;
		public bool isVppDeviceBasedLicensingEnabled;
		public int primaryGenreId;
		public DateTime releaseDate;
		public string primaryGenreName;
		public string sellerName;
		public string[] genreIds;
		public string formattedPrice;
		public string currency;
		public string wrapperType;
		public string version;
		public int artistId;
		public string artistName;
		public string[] genres;
		public float price;
		public string description;
		public string bundleId;
		public int trackId;
		public string trackName;
		public string minimumOsVersion;
		public DateTime currentVersionReleaseDate;
		public string releaseNotes;
		public float averageUserRating;
		public int userRatingCount;
	}

}


public class RateMe : MonoBehaviour
{
	public enum URLType
	{
		Manual,
		AppleSearchAPI,
	}

	enum InitStates
	{
		Uninitialised,
		Initialising,
		Ready
	}

	string appID;
	string appIDPlayerPrefsKey = "Rate_AppID";


	InitStates initState = InitStates.Uninitialised;

	[SerializeField] URLType urlType = URLType.Manual;
	[SerializeField] string appleGameID = string.Empty;
	
	/// <summary>
	/// Fetches appID from iTunes search API using our bundle id.
	/// Refer to RateMe.isReady for whether or not initialisation has complete.
	/// </summary>
	public void InitRateMe()
	{
		if (initState != InitStates.Uninitialised)
			return;

		appID = Application.identifier;
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			if (urlType == URLType.Manual)
			{
				appID = appleGameID;
				initState = InitStates.Ready;
			}
			else
			{
				if (CompareVersionNum(11.0f))
				{
					initState = InitStates.Ready;
				}
				else
				{
					initState = InitStates.Initialising;
					//Start Coroutine on PanelManager because RateMe panel is turned off at initialisation time.
					PanelManager.Instance.StartCoroutine(GetAPPID());
				}
			}
		}
		else
		{
			initState = InitStates.Ready;
		}
	}

	/// <summary>
	/// Fetches from Apples lookup API to get the appID from our bundle ID to use as a store URL for page launching.
	/// </summary>
	/// <returns></returns>
	IEnumerator GetAPPID()
	{
		string cachedID = PlayerPrefs.GetString(appIDPlayerPrefsKey, "");
		if (!string.IsNullOrEmpty(cachedID))
		{
			appID = cachedID;
			initState = InitStates.Ready;
		}

		while (initState != InitStates.Ready)
		{
			WWW req = new WWW("http://itunes.apple.com/lookup?bundleId=" + Application.identifier);
			yield return req;


			if (string.IsNullOrEmpty(req.error))
			{
				string JSON = req.text;
				iTunesJSON.Rootobject resultClass = JsonUtility.FromJson<iTunesJSON.Rootobject>(JSON);

				Debug.Log("APPid = " + resultClass.results[0].trackId.ToString());
				appID = resultClass.results[0].trackId.ToString();

				PlayerPrefs.SetString(appIDPlayerPrefsKey, appID);

				initState = InitStates.Ready;
			}
			else
			{
				yield return new WaitForSeconds(30f);
			}
		}
	}


	/// <summary>
	/// Shows Rate Me popup, on iOS 11+ this will show the native rate popup, on all other platforms it will show a rate panel.
	/// </summary>
	/// <param name="customPanelAction">Leave null if you want to use the default popup.</param>
	/// <returns>true if a popup is shown, false if the system isn't ready to show a popup.</returns>
	public bool ShowRateMe()
	{
		if (initState == InitStates.Ready)
		{
#if UNITY_IOS
			if (CompareVersionNum(11.0f))
			{
				iOSReviewRequest.Request();
			}
			else
#endif
			{
				PopupWindowUI.PopupInfo popupInfo = new PopupMessageBase.PopupInfo(LocIDs.None, null, LocIDs.RateUs_Enjoying, new LocIDs[] { LocIDs.Confirm_Yes }, PopupWindowContainer.PopupIDs.Rate, new string[] { Application.productName });
				PanelManager.Instance.popupController.PopupOrQueue(popupInfo);
			}
		}
		else
		{
			if (initState == InitStates.Uninitialised)
				Debug.LogError("RateMe Panel Hasn't been Initialized, Call InitRateMe");
			else if (initState == InitStates.Initialising)
				Debug.LogWarning("RateMe Panel has not finished initializing!");
		}
		return initState == InitStates.Ready;
	}

	/// <summary>
	/// Opens the store page for the app.
	/// Called from a UI Button
	/// </summary>
	public void OpenAppStore()
	{
#if UNITY_IOS
	Application.OpenURL("itms-apps://itunes.apple.com/app/id" + appID);
#endif
#if UNITY_ANDROID
	Application.OpenURL ("market://details?id=" + appID);
#endif
	}

	/// <summary>
	/// Checks our current iOS version number against a target minimum version number
	/// </summary>
	/// <param name="_versionNum">Our target iOS version.</param>
	/// <returns>true if iOS version is equal to or higher than _versionNum.</returns>
	bool CompareVersionNum(float _versionNum)
	{

#if UNITY_IOS && !UNITY_EDITOR
	string systemVersionString = UnityEngine.iOS.Device.systemVersion;
	string[] versionNumSplit = systemVersionString.Split('.');
	float versionNum = float.Parse(versionNumSplit[0] + "." + versionNumSplit[1]);
	return versionNum > _versionNum;
#else
		return false;
#endif
	}
}

