﻿using UnityEngine;
using System.Collections;

public class ChildToTransform : MonoBehaviour {

	public bool maintainOffset;
	public Transform targetParent;

	void Awake ()
	{
		if(maintainOffset)
			transform.SetParent(targetParent, true);
		else
		{
			transform.SetParent(targetParent, false);
			transform.localPosition = Vector3.zero;
		}
	}
}
