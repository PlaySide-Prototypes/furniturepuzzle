﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StraightLine : MonoBehaviour
{
	public Transform pointContainer;
	public bool autoUpdate;
	public bool loop;
	public Vector3 offset;
	private LineRenderer lineRenderer;
	private Vector3[] points;

	void OnEnable()
	{
		UpdatePosition();
	}

	void Update()
	{
		if (autoUpdate)
			UpdatePosition();
	}


	public void UpdatePosition()
	{
		if(!pointContainer)
			pointContainer = transform;
			
		if(!lineRenderer)
			lineRenderer = GetComponent<LineRenderer>();

		Transform[] children = pointContainer.GetComponentsInChildren<Transform>();
		if(children.Length <=0)
			return;
		points = new Vector3[children.Length-1 + (loop?1:0)];

		#if UNITY_5_6_OR_NEWER
		lineRenderer.positionCount = points.Length;
		#else
		lineRenderer.numPositions = points.Length;
		#endif
		
		for(int i=1; i<children.Length; i++)
		{
			points[i-1] = children[i].position;
			points[i-1] += offset;
		}

		if(loop)
		{
			points[points.Length-1] = children[1].position;
			points[points.Length-1] += offset;
		}
		lineRenderer.SetPositions(points);
	}
}
