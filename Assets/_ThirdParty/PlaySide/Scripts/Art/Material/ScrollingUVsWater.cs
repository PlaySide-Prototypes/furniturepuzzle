using UnityEngine;

class ScrollingUVsWater : MonoBehaviour
{
    public Vector2 speed = Vector2.zero;
    private Material thisMaterial;
    private Vector2 offset;
	public float sinClampPercentage = 0f;
	public bool sinX = false;
	public bool sinY = false;

    void Awake()
    {
        Renderer renderer = GetComponent<Renderer>();
        if (renderer == null)
        {
            Debug.LogError("Could not find renderer for '" + gameObject.name + "', disabling ScrollingUVs");
            enabled = false;
        }
        else
        {
            thisMaterial = renderer.material;
            offset = thisMaterial.mainTextureOffset;
        }
    }

    void Update()
    {
        offset += speed * Time.deltaTime;

        Vector2 newUVs = offset;
        if (sinX) {
			newUVs.x = Mathf.Sin (offset.x) * sinClampPercentage;
		} 
		if (sinY) {
			newUVs.y = Mathf.Sin (offset.y) * sinClampPercentage;
		} 
        thisMaterial.SetTextureOffset("_MainTex", newUVs);

    }
}
