using UnityEngine;

class ScrollingUVs : MonoBehaviour
{
	[SerializeField] string uvName = string.Empty;
	[SerializeField] Vector2 speed = Vector2.zero;

	Material thisMaterial;
	Vector2 offset;
	bool uvSpecified;

	void Awake()
	{
		if (GetComponent<Renderer>() == null)
		{
			Debug.LogError("Could not find renderer for GameObject '"+name+"' (parent '"+transform.parent.name+"')");
			enabled = false;
		}
		else
		{
			thisMaterial = GetComponent<Renderer>().material;
			offset = thisMaterial.mainTextureOffset;
		}

		uvSpecified = !string.IsNullOrEmpty(uvName);
	}
	
	void Update()
	{
		float dTime = Time.deltaTime;

		// Add components separately (faster than Vector2 add)
		offset.x += speed.x * dTime;
		offset.y += speed.y * dTime;

		// Wrap into [0..1) range
		if (speed.x != 0.0f) { offset.x -= (int)(offset.x); }
		if (speed.y != 0.0f) { offset.y -= (int)(offset.y); }

		if (uvSpecified)
			thisMaterial.SetTextureOffset(uvName, offset);
		else
			thisMaterial.mainTextureOffset = offset;
	}
}
