using UnityEngine;
using UnityEngine.UI;

public class UIScrollingUV : MonoBehaviour
{
	#region Inspector variables
	[Header("Required")]
	public RawImage targetImage;
	public bool autoScroll;
	public Vector2 scrollRate;

	[Header("Used When 'Auto Scroll' is Disabled")]
	public Vector2 scrollValue;
	#endregion // Inspector variables

	Material targetMaterial;
	Rect newUVRect;

	void Awake()
	{
		if (targetImage == null)
		{
			Debug.LogError("UIScrollingUV on '" + PSUtils.Transforms.GetFullName(gameObject) + "' has no Target Image set!");
			enabled = false;
			return;
		}

		newUVRect = new Rect(targetImage.uvRect);
	}

	void FixedUpdate()
	{
		if (autoScroll)
			scrollValue = scrollRate * Time.fixedDeltaTime;

		newUVRect.x = targetImage.uvRect.x + scrollValue.x;
		newUVRect.x -= (int)(newUVRect.x);	// Clamp to [0..1)

		newUVRect.y = targetImage.uvRect.y + scrollValue.y;
		newUVRect.y -= (int)(newUVRect.y);	// Clamp to [0..1)

		targetImage.uvRect = newUVRect;
	}
}
