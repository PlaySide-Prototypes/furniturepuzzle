﻿using UnityEngine;
using System.Collections;

public class ScrollingUVGroup : MonoBehaviour {

	[SerializeField] Renderer[] targetRenderers;
	[SerializeField] string uvName = "_MainTex";
	[SerializeField] Vector2 speed = Vector2.zero;

	Material thisMaterial;
	Vector2 offset;

	void Awake()
	{
		if (targetRenderers[0] == null)
		{
			Debug.LogError("Could not find renderer for GameObject '"+name+"' (parent '"+transform.parent.name+"')");
			enabled = false;
		}
		else
		{
			thisMaterial = targetRenderers[0].material;

			for(int i=0; i< targetRenderers.Length; i++)
			{
				targetRenderers[i].material = thisMaterial;
			}
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		offset += speed * Time.deltaTime;

		// Wrap into [0..1) range
		if (speed.x != 0.0f) { offset.x -= (int)(offset.x); }
		if (speed.y != 0.0f) { offset.y -= (int)(offset.y); }

		thisMaterial.SetTextureOffset(uvName, offset);
	}
}
