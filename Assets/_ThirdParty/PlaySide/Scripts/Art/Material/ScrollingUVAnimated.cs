﻿using UnityEngine;
using System.Collections;

public class ScrollingUVAnimated : MonoBehaviour {

	[SerializeField] Renderer[] targetRenderers;
	[HideInInspector][SerializeField] Vector2 uvOffset = Vector2.zero;

	private Material thisMaterial;

	void Awake()
	{
		if (targetRenderers[0] == null)
		{
			Debug.LogError("Could not find renderer for GameObject '"+name+"' (parent '"+transform.parent.name+"')");
			enabled = false;
		}
		else
		{
			thisMaterial = targetRenderers[0].material;

			for(int i=0; i< targetRenderers.Length; i++)
			{
				targetRenderers[i].material = thisMaterial;
			}
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		thisMaterial.mainTextureOffset = uvOffset;
	}
}
