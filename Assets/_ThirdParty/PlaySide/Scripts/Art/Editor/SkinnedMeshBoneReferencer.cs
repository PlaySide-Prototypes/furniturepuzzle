﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class SkinnedMeshBoneReferencer : EditorWindow
{

	SkinnedMeshRenderer sourceSkinnedMesh;
	SkinnedMeshRenderer destinationSkinnedMesh;
	Transform			destinationRootBone;
	Transform[]			srcBones;
	Transform[]			destBones;
	Transform[]			destChildren;

	[MenuItem("PlaySide/Rig Tools/SkinnedMesh Bone References")]
	public static void CopyBoneReferences()
	{
		EditorWindow.GetWindow(typeof(SkinnedMeshBoneReferencer));
	}

	void OnGUI()
	{
		GUILayout.Label("Copy Bone References\n", EditorStyles.boldLabel);
		sourceSkinnedMesh = (SkinnedMeshRenderer)EditorGUILayout.ObjectField("Source Skinned Mesh:", sourceSkinnedMesh, typeof(SkinnedMeshRenderer), true);
		EditorGUILayout.Separator();
		destinationSkinnedMesh = (SkinnedMeshRenderer)EditorGUILayout.ObjectField("Destination Skinned Mesh:", destinationSkinnedMesh, typeof(SkinnedMeshRenderer), true);
		destinationRootBone = (Transform)EditorGUILayout.ObjectField("Destination Root Bone:", destinationRootBone, typeof(Transform), true);

		GUILayout.Label("\nSelect Source and Destination SkinnedMesh to copy from and to,\n along with the destination root bone", EditorStyles.label);

		if (sourceSkinnedMesh == null || destinationSkinnedMesh == null || destinationRootBone == null)
			return;
		else
		{
			if (srcBones == null)
				srcBones = sourceSkinnedMesh.bones;
			if (destBones == null)
				destBones = destinationSkinnedMesh.bones;

			if (GUILayout.Button("Copy Bone References"))
			{
				CopyBoneReferencesByName(sourceSkinnedMesh, destinationSkinnedMesh);
			}
			DrawBoneLists();
		}
	}

	void CopyBoneReferencesByName(SkinnedMeshRenderer src, SkinnedMeshRenderer dest)
	{
		destChildren = destinationRootBone.GetComponentsInChildren<Transform>(true);

		if(destBones.Length != srcBones.Length)
			destBones = new Transform[srcBones.Length];

		for (int i = 0; i < destBones.Length; i++)
		{
			foreach (Transform child in destChildren)
			{
				if (child.name == srcBones[i].name)
				{
					destBones[i] = child;
				}
			}
		}
		destinationSkinnedMesh.bones = destBones;
	}

	void DrawBoneLists()
	{
		//GUILayout.BeginArea(new Rect(100, 1400, 0,0));
		GUILayout.BeginHorizontal();

			// SOURCE
			GUILayout.BeginVertical();
				GUILayout.Label("Source", EditorStyles.label);
				if (srcBones != null)
				{
					foreach (Transform s in srcBones)
					{
						if(s != null)
							GUILayout.Label("-" + s.name, EditorStyles.label);
					}
				}
			GUILayout.EndVertical();

			// DESTINATION
			GUILayout.BeginVertical();
				GUILayout.Label("Dest", EditorStyles.label);
				if (destBones != null)
				{
					foreach (Transform d in destBones)
					{
						{
							if (d != null)
								GUILayout.Label("-" + ((d == null) ? "" : d.name), EditorStyles.label);
						}
					}
				}
			GUILayout.EndVertical();

		GUILayout.EndHorizontal();
		//GUILayout.EndArea();
	}
}