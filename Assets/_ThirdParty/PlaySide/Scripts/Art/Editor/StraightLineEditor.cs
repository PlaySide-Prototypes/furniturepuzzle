﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof( StraightLine))]
public class StraightLineEditor : Editor
{
	private StraightLine straightLine;

	public void OnEnable()
	{
		straightLine = (StraightLine)target;
	}

	public override void OnInspectorGUI()
	{
		EditorGUILayout.BeginHorizontal();
		if (GUILayout.Button("Update Line"))
			straightLine.UpdatePosition();
		EditorGUILayout.EndHorizontal();
		base.OnInspectorGUI();
	}
}