﻿using UnityEngine;

namespace Playside
{
    public abstract class Manager : MonoBehaviour
    {
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // *	Abstract Methods
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected abstract void Initialise();
        public abstract void Save();


        public abstract class ManagerBase<T> : Manager where T : ManagerBase<T>
        {
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            // *	Attributes Readers (Getters)
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            public static T Instance
            {
                get
                {
                    if (instance == null)
                    {
                        instance = GameObject.FindObjectOfType<T>();
                        if (instance == null)
                        {
                            Debug.LogError($"You are trying to call upon the instance of {typeof(T).Name}, but it does not exist in the scene. You'll need to add it manually first.");
                            return null;
                        }

                    #if UNITY_EDITOR
                        else if (Application.isPlaying)
                        {
                    #endif
                            // Only show error for auto setup if we are actually playing the game. This will signify race conditions that need to be resolved.
                            // However, if we are accessing Managers in Editor Mode, that's okay and should be supported.
                            Debug.LogError($"You are trying to call upon the instance of {typeof(T).Name} too early. It has not been initialised yet.");

                    #if UNITY_EDITOR
                        }
                    #endif

                        instance.Initialise();
                    }
                    return instance;
                }
            }

            public static bool HasInstance
            {
                get
                {
                    return instance != null;
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            // *	Variables
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            protected static T instance = null;

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            // *	Methods
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            protected virtual void Awake()
            {
                instance = (T)this;
                Initialise();
            }

            public void DontDestroy()
            {
                if ((transform.parent == null) && (this == instance))
                    DontDestroyOnLoad(instance);
            }
        }
    }
}
