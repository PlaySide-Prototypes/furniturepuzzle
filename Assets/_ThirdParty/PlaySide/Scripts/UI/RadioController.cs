﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioController : MonoBehaviour
{
	[SerializeField] RectTransform screenContainer;
	[SerializeField] int defaultIndex = 0;

	RadioMenu[] menus;

	int numMenus;
	int currentIndex;

	public int CurrentIndex(){ return currentIndex; }

	bool initalized = false;


	void Awake ()
	{
		RefreshRadios();
	}


	void Initialize()
	{
		currentIndex = -1;
	}

	void RefreshRadios()
	{
		if (!initalized)
			Initialize();

		menus = screenContainer.GetComponentsInChildren<RadioMenu>(true);
		numMenus = menus.Length;

		for(int i=0; i<numMenus; i++)
		{
			menus[i].Initialize(i==defaultIndex);
		}
		currentIndex = defaultIndex;
	}


	public void NextScreen() { SetScreen(currentIndex+1); }
	public void PreviousScreen() { SetScreen(currentIndex-1); }
//	public void SetScreen(float _index) { SetScreen(Mathf.RoundToInt(_index * numMenus)); }
	public void SetScreen(int _index)
	{
		if (currentIndex == _index || _index > numMenus)
			return;
		
		// Disable Previous memu, ignore initialized -1 index
		if(currentIndex > -1)
			menus[currentIndex].DeselectRadioMenu();

		// Set new index, ensure index is clamped to number of RadioMenus
		currentIndex =  Mathf.Clamp(_index, 0, numMenus-1);

		// Enable new current menu
		menus[currentIndex].SelectRadioMenu();
	}
}
