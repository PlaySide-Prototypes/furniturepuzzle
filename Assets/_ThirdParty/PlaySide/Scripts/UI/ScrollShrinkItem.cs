﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScrollShrinkItem : MonoBehaviour {

	public Animator anim;
	[HideInInspector] public RectTransform rectTransform;

	void Awake()
	{
		rectTransform = GetComponent<RectTransform>();
	}

	public void SetScale(float scale)
	{
		if (gameObject.activeSelf)
		{
			if(anim)
				anim.SetFloat("Scale", scale);
			else
				rectTransform.localScale = Vector3.one * scale;
		}
	}
}
