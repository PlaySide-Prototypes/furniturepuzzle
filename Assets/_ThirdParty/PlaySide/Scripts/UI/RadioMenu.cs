﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

[RequireComponent(typeof(Animator))]
public class RadioMenu : MonoBehaviour
{
	[SerializeField] Animator radioButtonAnimator;
	[SerializeField] string onAnim = "On";
	[SerializeField] string offAnim = "Off";
	public UnityEvent OnSelect;
	public UnityEvent OnDeselect;

	Animator anim;

	bool initialized = false;
	bool selected = false;
	bool awaitingDisable = false;


	// Called from RadioController
	public void Initialize(bool _selected)
	{
		if (initialized)
			return;
		
		anim = GetComponent<Animator>();
		selected = false;
		initialized = true;

		if (_selected)
			SelectRadioMenu();
		else
			DeselectRadioMenu();
	}


	// Called from RadioController
	public void SelectRadioMenu()
	{
		if (selected)
			return;
		
		OnSelect.Invoke();

		selected = true;
		awaitingDisable = false;
		gameObject.SetActive(true);

		if(radioButtonAnimator)
			radioButtonAnimator.SetBool("Selected", true);

		// Use animator if it exists, otherwise just GameObject enable/disable
		if(anim)
			anim.Play(onAnim, 0, 0.0f);
	}


	// Called from RadioController
	public void DeselectRadioMenu()
	{
		OnDeselect.Invoke();

		selected = false;
		awaitingDisable = true;

		if(radioButtonAnimator)
			radioButtonAnimator.SetBool("Selected", false);

		// Use animator if it exists, otherwise just GameObject enable/disable
		if(anim && gameObject.activeSelf)
			anim.Play(offAnim, 0, 0.0f);
		else
			gameObject.SetActive(false);
	}


	// Called from Animation Event
	public void DisableRadioMenu()
	{
		if (awaitingDisable)
		{
			gameObject.SetActive(false);
			awaitingDisable = false;
		}
		else if(anim)
			anim.Play(onAnim, 0, 0.0f);
	}
}
