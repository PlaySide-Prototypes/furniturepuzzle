﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum UIBackgroundIDs
{
	MainMenu,
	Characters
}

public class UI3DBackground : MonoBehaviour
{
	public static UI3DBackground Instance;
	[SerializeField] bool disableOnAwake;
	[SerializeField] Animator cameraAnimator;
	[SerializeField] GameObject backgroundContainer;

	[SerializeField] UnityEvent		onBGEnabled;
	[SerializeField] UnityEvent		onBGDisabled;

	int location = 0;
	public UIBackgroundIDs GetCurrentLocation()	{ return (UIBackgroundIDs)location;	}

	void Awake()
	{
		if(Instance == null)
			Instance = this;
		
		if (disableOnAwake)
			ToggleVisibility(false);
	}
	
	public void SetLocation (UIBackgroundIDs _id)
	{
		location = (int)_id;
		cameraAnimator.SetInteger("Location", location);

	}

	public void ToggleVisibility(bool _enabled)
	{
		backgroundContainer.SetActive(_enabled);

		if (_enabled)
			onBGEnabled.Invoke();
		else
			onBGDisabled.Invoke();
	}
}
