﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ScrollRect))]
public class ScrollInteractableDisabler : MonoBehaviour
{
	[SerializeField] float cutoffSpeed = 10.0f;
	[SerializeField] CanvasGroup[] targetCanvasGroups;

	ScrollRect scrollRect;
	float speed;
	bool isScrolling;


	void Awake ()
	{
		scrollRect = GetComponent<ScrollRect>();
    }

	void OnEnable()	{ isScrolling = false; }
	void OnDisable() { isScrolling = false;	}


	void Update()
    {
		speed = scrollRect.velocity.magnitude;

		if(speed > cutoffSpeed)
		{
			if (!isScrolling)
			{
				isScrolling = true;
				ToggleInteractable(false);
			}
		}
		else
		{
			if(isScrolling)
			{
				isScrolling = false;
				ToggleInteractable(true);
			}
		}
    }


	void ToggleInteractable(bool _interactable)
	{
		for(int i=0; i<targetCanvasGroups.Length; i++)
		{
			targetCanvasGroups[i].interactable = _interactable;
		}
	}
}
