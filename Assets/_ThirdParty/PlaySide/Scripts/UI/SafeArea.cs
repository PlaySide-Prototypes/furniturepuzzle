// Source: https://connect.unity.com/p/updating-your-gui-for-the-iphone-x-and-other-notched-devices

using UnityEngine;
using UnityEngine.UI;

namespace Crystal
{
    /// <summary>
    /// Safe area implementation for notched mobile devices. Usage:
    ///  (1) Add this component to the top level of the UI and link in any GUI Panels to adjust.
    ///  (2) If the panel uses a full screen background image, then create an immediate child and link to that instead, with all other elements childed below it.
    ///      This will allow the background image to stretch to the full extents of the screen behind the notch, which looks nicer.
    ///  (3) For other cases that use a mixture of full horizontal and vertical background stripes, use the Conform X & Y controls on separate elements as needed.
    /// </summary>
    public class SafeArea : MonoBehaviour
    {
        #region Simulations
        /// <summary>
        /// Simulation device that uses safe area due to a physical notch or software home bar. For use in Editor only.
        /// </summary>
        public enum SimDevice
        {
            /// <summary>
            /// Don't use a simulated safe area - GUI will be full screen as normal.
            /// </summary>
            None,
            /// <summary>
            /// Simulate the iPhone X and Xs (identical safe areas).
            /// </summary>
            iPhoneX,
            /// <summary>
            /// Simulate the iPhone Xs Max and XR (identical safe areas).
            /// </summary>
            iPhoneXsMax
        }

        /// <summary>
        /// Simulation mode for use in editor only. This can be edited at runtime to toggle between different safe areas.
        /// </summary>
		public static SimDevice sim = SimDevice.None;

        /// <summary>
        /// Normalised safe areas for iPhone X with Home indicator (ratios are identical to iPhone Xs). Absolute values:
        ///  PortraitU x=0, y=102, w=1125, h=2202 on full extents w=1125, h=2436;
        ///  PortraitD x=0, y=102, w=1125, h=2202 on full extents w=1125, h=2436 (not supported, remains in Portrait Up);
        ///  LandscapeL x=132, y=63, w=2172, h=1062 on full extents w=2436, h=1125;
        ///  LandscapeR x=132, y=63, w=2172, h=1062 on full extents w=2436, h=1125.
        ///  Aspect Ratio: ~19.5:9.
        /// </summary>
        Rect[] NSA_iPhoneX = new Rect[]
        {
            new Rect (0f, 102f / 2436f, 1f, 2202f / 2436f),  // Portrait
            new Rect (132f / 2436f, 63f / 1125f, 2172f / 2436f, 1062f / 1125f)  // Landscape
        };

        /// <summary>
        /// Normalised safe areas for iPhone Xs Max with Home indicator (ratios are identical to iPhone XR). Absolute values:
        ///  PortraitU x=0, y=102, w=1242, h=2454 on full extents w=1242, h=2688;
        ///  PortraitD x=0, y=102, w=1242, h=2454 on full extents w=1242, h=2688 (not supported, remains in Portrait Up);
        ///  LandscapeL x=132, y=63, w=2424, h=1179 on full extents w=2688, h=1242;
        ///  LandscapeR x=132, y=63, w=2424, h=1179 on full extents w=2688, h=1242.
        ///  Aspect Ratio: ~19.5:9.
        /// </summary>
        Rect[] NSA_iPhoneXsMax = new Rect[]
        {
            new Rect (0f, 102f / 2688f, 1f, 2454f / 2688f),  // Portrait
            new Rect (132f / 2688f, 63f / 1242f, 2424f / 2688f, 1179f / 1242f)  // Landscape
        };
		#endregion

		#region Inspector variables
		[Header("Hierarchy")]
		[SerializeField] RectTransform[] rectTransforms;
		[SerializeField] Canvas[] canvases;

		[Header("Settings")]
		[SerializeField] bool refreshEveryFrame = false;
		[SerializeField] bool adjustXAxis = true;  // Conform to screen safe area on X-axis (default true, disable to ignore)
		[SerializeField] bool adjustYAxis = true;  // Conform to screen safe area on Y-axis (default true, disable to ignore)

#if UNITY_EDITOR
		[Header("Unity Editor Debug Keys")]
		[SerializeField] KeyCode[] keyToHold = { KeyCode.LeftShift, KeyCode.RightShift };
		[SerializeField] KeyCode KeySafeArea = KeyCode.A;
#endif
		#endregion

		Rect lastSafeArea = new Rect(0, 0, 0, 0);

		public static bool isLandscape = false;
		public static bool isNarrowDevice = false;

		void Awake ()
        {
			if ((rectTransforms == null) || (rectTransforms.Length == 0))
            {
                Debug.LogError ("Cannot apply safe area - no RectTransforms set in Inspector");
                Destroy (gameObject);
            }
		}

		void Start ()
		{
			Refresh();
		}

		void Update ()
        {
#if UNITY_EDITOR
			// Debug Switch Devices in Editor
			if (Input.GetKeyDown(KeySafeArea))
			{
				if (keyToHold.Length == 0)
					ToggleSafeArea();
				else
				{
					for (int i = 0; i < keyToHold.Length; ++i)
					{
						if (Input.GetKey(keyToHold[i]))
							ToggleSafeArea();
					}
				}
			}
#endif
			if (refreshEveryFrame)
				Refresh();
        }

        void Refresh (bool _debugLog = false)
        {
            Rect safeArea = GetSafeArea ();

            if (safeArea != lastSafeArea)
                ApplySafeArea (safeArea, _debugLog);

			// Get the resolution of the screen
			float ratio = 1f;

			// Figure out the aspect ratio by dividing the largest sde by the smallest side (this allows for landscape & portrait mode)
			if (Screen.width > Screen.height)
			{
				ratio = (float)Screen.width / (float)Screen.height;
				isLandscape = true;
			}
			else
			{
				ratio = (float)Screen.height / (float)Screen.width;
				isLandscape = false;
			}

			isNarrowDevice = (ratio > 1.8f);
			
			for (int i = 0; i < canvases.Length; i++)
			{
				CanvasScaler canvasScaler = canvases[i].transform.GetComponent<CanvasScaler>();
				Vector2 refRes = canvasScaler.referenceResolution;

				if (isLandscape)
				{
					canvasScaler.matchWidthOrHeight = (isNarrowDevice ? 1.0f : 0.0f);
					canvasScaler.referenceResolution = (refRes.x > refRes.y) ? new Vector2(refRes.x, refRes.y) : new Vector2(refRes.y, refRes.x);
				}
				else
				{
					canvasScaler.matchWidthOrHeight = (isNarrowDevice ? 0.0f : 1.0f);
					canvasScaler.referenceResolution = (refRes.x < refRes.y) ? new Vector2(refRes.x, refRes.y) : new Vector2(refRes.y, refRes.x);
				}
			}
			Debug.Log("Landsapce" + isLandscape + ", Ratio: " + ratio.ToString("0.00") + ", Narrow: "+ isNarrowDevice);
		}

        Rect GetSafeArea ()
        {
            Rect safeArea = Screen.safeArea;

            if (Application.isEditor && sim != SimDevice.None)
            {
                Rect nsa = new Rect (0, 0, Screen.width, Screen.height);

                switch (sim)
                {
                    case SimDevice.iPhoneX:
                        if (Screen.height > Screen.width)  // Portrait
                            nsa = NSA_iPhoneX[0];
                        else  // Landscape
                            nsa = NSA_iPhoneX[1];
                        break;
                    case SimDevice.iPhoneXsMax:
                        if (Screen.height > Screen.width)  // Portrait
                            nsa = NSA_iPhoneXsMax[0];
                        else  // Landscape
                            nsa = NSA_iPhoneXsMax[1];
                        break;
                    default:
                        break;
                }

                safeArea = new Rect (Screen.width * nsa.x, Screen.height * nsa.y, Screen.width * nsa.width, Screen.height * nsa.height);
            }

            return safeArea;
        }

		void ApplySafeArea (Rect _safeArea, bool _debugLog)
        {
            lastSafeArea = _safeArea;

            // Ignore x-axis?
            if (!adjustXAxis)
            {
                _safeArea.x = 0;
                _safeArea.width = Screen.width;
            }

            // Ignore y-axis?
            if (!adjustYAxis)
            {
                _safeArea.y = 0;
                _safeArea.height = Screen.height;
            }

            // Convert safe area rectangle from absolute pixels to normalised anchor coordinates
            Vector2 anchorMin = _safeArea.position;
            Vector2 anchorMax = _safeArea.position + _safeArea.size;
            anchorMin.x /= Screen.width;
            anchorMin.y /= Screen.height;
            anchorMax.x /= Screen.width;
            anchorMax.y /= Screen.height;
			for (int i = 0; i < rectTransforms.Length; ++i)
			{
				rectTransforms[i].anchorMin = anchorMin;
				rectTransforms[i].anchorMax = anchorMax;
			}

			if (_debugLog)
				Debug.LogFormat("New safe area applied to '{0}': x={1}, y={2}, w={3}, h={4} on full extents w={5}, h={6}", name, _safeArea.x, _safeArea.y, _safeArea.width, _safeArea.height, Screen.width, Screen.height);
		}

		#region Editor Debug

#if UNITY_EDITOR
		SimDevice[] sims;
		int simIdx;

		/// <summary>
		/// Toggle the safe area simulation device.
		/// </summary>
		void ToggleSafeArea()
		{
			sims = (SimDevice[])System.Enum.GetValues(typeof(SimDevice));
			simIdx++;

			if (simIdx >= sims.Length)
				simIdx = 0;

			sim = sims[simIdx];
			Debug.Log("Switched to sim device '" + sims[simIdx] + "'");
			Refresh(true);
		}
#endif

		#endregion

	}
}
