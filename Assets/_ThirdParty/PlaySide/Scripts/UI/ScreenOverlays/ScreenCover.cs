﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class ScreenCover : MonoBehaviour
{
	public const float defaultFadeTime = 0.5f;
	public ScreenCoverIDs id;
	public Animator fadeAnim;
	public Image colorImage;
	public Renderer colorRenderer;
	public Action callbackWhenFaded;

	MaterialPropertyBlock matPropertyBlock;

	// private bool fading;

	public static ScreenCover[] Instances;

	void Awake()
	{
		if(Instances == null)
			Instances = new ScreenCover[(int)ScreenCoverIDs.Length];
		if(Instances[(int)id] != null)
			throw new UnityException("ScreenCover singleton already created!");
		Instances[(int)id] = this;
		hideComplete();

		if (colorRenderer != null)
		{
			matPropertyBlock = new MaterialPropertyBlock();
			colorRenderer.GetPropertyBlock(matPropertyBlock);
//			colorMaterial = colorRenderer.material;

		}
	}
		
	public void FadeOff(float duration, Color? fadeColor=null )
	{
		//if(fading)
		//	return;
		//fading = true;
		if(colorImage != null)
		{
			colorImage.enabled = true;
			colorImage.color = fadeColor ?? Color.black;
		}

		if (colorRenderer != null)
		{
			colorRenderer.enabled = true;
			matPropertyBlock.SetColor("_Color", fadeColor ?? Color.black);
			colorRenderer.SetPropertyBlock(matPropertyBlock);
		}
		
		fadeAnim.Play("FadeOff", 0, 0f);
		fadeAnim.speed = 1/duration;
		
	}

	public void CoverFadeOffComplete()
	{
		colorImage.enabled = false;
	}

	public void FadeOn(float duration, Color? fadeColor=null, Action endOfFadeCallback = null )
	{
		//if(fading)
		//	return;
		//fading = true;
		if(colorImage != null)
		{
			colorImage.enabled = true;
			colorImage.color = fadeColor ?? Color.black;
		}

		if (colorRenderer != null)
		{
			colorRenderer.enabled = true;
			matPropertyBlock.SetColor("_Color", fadeColor ?? Color.black);
			colorRenderer.SetPropertyBlock(matPropertyBlock);
		}

		fadeAnim.Play("FadeOn", 0, 0f);
		fadeAnim.speed = 1/duration;
	}

	public void Flash(float duration, Color? fadeColor=null, int variant=0 )
	{
		//if(fading)
		//	return;
		//fading = true;
		if(colorImage != null)
		{
			colorImage.enabled = true;
			colorImage.color = fadeColor ?? Color.black;
		}

		if (colorRenderer != null)
		{
			colorRenderer.enabled = true;
			matPropertyBlock.SetColor("_Color", fadeColor ?? Color.black);
			colorRenderer.SetPropertyBlock(matPropertyBlock);
		}
		
		fadeAnim.Play("Flash", 0, 0f);
		fadeAnim.speed = 1/duration;
	}

	public void hideComplete()
	{
		if(colorImage != null)
			colorImage.enabled = false;
		if (colorRenderer != null)
			colorRenderer.enabled = false;
		// fading = false;
	}

	public void CoverFadeOnComplete()
	{
		if (callbackWhenFaded != null)
		{
			callbackWhenFaded();
			callbackWhenFaded = null;
		}
	}
}
