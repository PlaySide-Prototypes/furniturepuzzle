﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
//using System.Collections.Generic;
using System.Text;

[RequireComponent(typeof(ScrollRect))]
public class ScrollShrink : MonoBehaviour
{
	public enum Directions
	{
		Horizontal,
		Vertical
	}

	public Directions direction = Directions.Horizontal;
	public Animator leftArrowAnim;
	public Animator rightArrowAnim;
	public float edgeShrinkProximity = 100.0f;
	public float minScale = 0f;
	public float maxScale = 1f;

	ScrollRect scrollRect;
	RectTransform rectTransform;
	RectTransform content;
	RectTransform viewport;

	private ScrollShrinkItem[] scrollItems;
	private bool showLeftArrow;
	private bool showRightArrow;

	float scale;
	float proxToLeft;
	float proxToRight;

	void Awake ()
	{
		Initialize();
	}

	void OnEnable()
	{
		Initialize();
	}

	public void Initialize()
	{
		scrollRect = GetComponent<ScrollRect>();
		content = scrollRect.content;
		viewport = scrollRect.viewport;

		if (direction == Directions.Horizontal)
		{
			content.pivot = new Vector2(0f, content.pivot.y);
			viewport.pivot = new Vector2(0.5f, viewport.pivot.y);
		}
		else
		{
			content.pivot = new Vector2(content.pivot.x, 1f);
			viewport.pivot = new Vector2(viewport.pivot.x, 0.5f);
		}

		scrollItems = content.GetComponentsInChildren<ScrollShrinkItem>(true); 
	}

	void Update ()
	{
		for (int i = 0; i < scrollItems.Length; i++)
		{
			// Position of item relative to viewport, add half viewport with for center snapping
			if (direction == Directions.Horizontal)
			{
				float halfVPWidth = viewport.rect.width * 0.5f;
				proxToLeft = (scrollItems[i].rectTransform.anchoredPosition.x + content.anchoredPosition.x) + halfVPWidth;
				proxToRight = -(scrollItems[i].rectTransform.anchoredPosition.x + content.anchoredPosition.x) + halfVPWidth;
			}
			else
			{
				float halfVPHeight = viewport.rect.height * 0.5f;
				proxToLeft = (scrollItems[i].rectTransform.anchoredPosition.y + content.anchoredPosition.y) + halfVPHeight;
				proxToRight = -(scrollItems[i].rectTransform.anchoredPosition.y + content.anchoredPosition.y) + halfVPHeight;
			}

			scale = Mathf.Clamp(Mathf.Min(proxToLeft, proxToRight), 0f, edgeShrinkProximity) / edgeShrinkProximity;
			scrollItems[i].rectTransform.localScale = Vector3.one * Mathf.Clamp(scale, minScale, maxScale);
		}

		// Todo check if content bounds are nearing edge of screen to hide arrows
		showLeftArrow = showRightArrow = false;

    	// Disable arrows if items are off the left or right side of the list
        if(leftArrowAnim)
		  leftArrowAnim.SetTrigger( (showLeftArrow)?"On":"Off" );
        if(rightArrowAnim)
		  rightArrowAnim.SetTrigger( (showRightArrow)?"On":"Off" );
	}
}
