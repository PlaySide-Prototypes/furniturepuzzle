﻿public enum PanelID {
	Popups,
 	HUD,
	MainMenu,
	Settings,
	LevelSelect,
	Store,
	Currency
}
