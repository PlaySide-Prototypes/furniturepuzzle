﻿using UnityEngine;

[RequireComponent(typeof(SuperButton))]
public class AndroidBackButton : MonoBehaviour
{
	[SerializeField] SuperButton superButton;

#if UNITY_EDITOR
	void OnValidate()
	{
		if (superButton == null)
			superButton = GetComponent<SuperButton>();
	}
#endif

	/// <summary> Called when object/script activates </summary>
	void Awake()
	{
#if UNITY_EDITOR
		if (superButton == null)
			throw new UnityException("SuperButton component set on '" + PSUtils.Transforms.GetFullName(transform) + "'");
#endif
	}

#if UNITY_EDITOR || UNITY_ANDROID
	/// <summary> Called once per frame </summary>
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape) && (superButton != null))
			superButton.OnClick();
	}
#endif
}
