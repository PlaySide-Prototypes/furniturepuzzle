﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(ScrollRect))]
public class HorizontalScrollSnap : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
	public enum ScrollStates { Idle, DragStart, Dragging, DragEnd, FreeMoving, Snapping }

	[SerializeField] RectTransform overrideContent;
	[SerializeField] float startSnapThresholdSpeed = 500.0f;
	[SerializeField] float snappingSpeed = 750.0f;
	[SerializeField] float targetedSnappingSpeed = 3000.0f;

	ScrollStates scrollState;

	ScrollRect scrollRect;

	RectTransform content;
	RectTransform viewport;

	RectTransform[] scrollItems;

	bool targetIndexOverriden;
	int contentChildCount;
	int snapIndex = 0;
	float snapDelta = 0.0f;

	Vector2 contentPos;
	Vector2 targetContentPos;
	Vector2 startScrollVelocity;

	public System.Action<int> OnValueChange;
	bool initialized = false;


	void Awake ()
	{
		Initialize();
		RefreshItemList();
	}


	void Initialize()
	{
		scrollState = ScrollStates.Idle;
		scrollRect = GetComponent<ScrollRect>();

		viewport = scrollRect.viewport;

		// Ensure content is anchored correctly
		content = scrollRect.content;
		content.anchorMax = content.anchorMin = new Vector2(0.5f, 0.5f);
		content.pivot = new Vector2(0, content.pivot.y);
		contentPos = content.anchoredPosition;

		initialized = true;
	}


	public void OnBeginDrag(PointerEventData data)
	{
		// Remove targetIndexOverride if user grabs scrollrect
		targetIndexOverriden = false;
		SetState(ScrollStates.DragStart);
	}


	public void OnDrag(PointerEventData data)
	{
		SetState(ScrollStates.Dragging);
	}


	public void OnEndDrag(PointerEventData data)
	{
		SetState(ScrollStates.DragEnd);
	}


	// Call if items are added or removed from the list
	public void RefreshItemList()
	{
		if (!initialized)
			Initialize();

		// Support a Content child container override in case UI hierarchy calls for additional transforms housing background art or something
		if (overrideContent)
			contentChildCount = overrideContent.childCount;
		else
			contentChildCount = scrollRect.content.childCount;
		
		scrollItems = new RectTransform[contentChildCount];

		for (int i = 0; i < contentChildCount; i++)
		{
			if (overrideContent)
				scrollItems[i] = overrideContent.GetChild(i).GetComponent<RectTransform>();
			else
				scrollItems[i] = scrollRect.content.GetChild(i).GetComponent<RectTransform>();
		}
	}


	// Return the currently centrally snapped item
	public RectTransform GetSnappedItem()
	{
		return scrollItems[snapIndex];
	}

	// Return the currently centrally snapped item
	public void PreviousItem()
	{
		if (snapIndex <= 0)
			return;

		snapIndex--;
		SetTargetItem(snapIndex);
	}


	// Return the currently centrally snapped item
	public void NextItem()
	{
		if (snapIndex == contentChildCount-1)
			return;
		
		snapIndex++;
		SetTargetItem(snapIndex);
	}


	void Update ()
	{
		switch(scrollState)
		{
			case ScrollStates.Idle:
			case ScrollStates.DragStart:
			case ScrollStates.Dragging:
				break;

			case ScrollStates.DragEnd:
				SetState(ScrollStates.FreeMoving);
				break;

			case ScrollStates.FreeMoving:
				UpdateFreeMoving();
				break;

			case ScrollStates.Snapping:
				UpdateSnapping();
				break;

			default:
				break;
		}
	}


	void SetState(ScrollStates _state)
	{
		switch(_state)
		{
			case ScrollStates.Idle:
			case ScrollStates.DragStart:
			case ScrollStates.Dragging:
			case ScrollStates.DragEnd:
			case ScrollStates.FreeMoving:
				break;

			case ScrollStates.Snapping:
				contentPos = content.anchoredPosition;
				scrollRect.velocity = Vector2.zero;
				if(!targetIndexOverriden)
					snapIndex = GetNearestItem();
				break;

			default:
				break;
		}
		scrollState = _state;
	}


	void UpdateFreeMoving()
	{
		// Check if velocity is low enough to start snapping
		if (Mathf.Abs(scrollRect.velocity.x) < startSnapThresholdSpeed)
		{
			SetState(ScrollStates.Snapping);
		}
	}


	// Move scrollrect content until target item is centered
	void UpdateSnapping()
	{
		// Snap faster if forced from code
		contentPos = Vector2.MoveTowards(contentPos, targetContentPos, (targetIndexOverriden ? targetedSnappingSpeed : snappingSpeed) * Time.deltaTime);
		content.anchoredPosition = contentPos;

		snapDelta = Mathf.Abs((scrollItems[snapIndex].anchoredPosition.x + content.anchoredPosition.x) - viewport.sizeDelta.x * viewport.pivot.x);

		if (snapDelta < 1.0f)
		{
			contentPos.x = content.anchoredPosition.x - ((scrollItems[snapIndex].anchoredPosition.x + content.anchoredPosition.x) - viewport.sizeDelta.x * viewport.pivot.x);
			content.anchoredPosition = contentPos;
			SetState(ScrollStates.Idle);
			targetIndexOverriden = false;
		}
	}


	// Manually set an index to jump to, moves with a custom snapping speed
	public void SetTargetItem(int _index)
	{		
		targetIndexOverriden = true;
		float delta = (scrollItems[_index].anchoredPosition.x + content.anchoredPosition.x) - viewport.sizeDelta.x * viewport.pivot.x;
		targetContentPos = new Vector2(contentPos.x-delta, contentPos.y);
		snapIndex = _index;
		SetState(ScrollStates.Snapping);
	}


	// Finds the nearest item in the list at current scroll content position
	int GetNearestItem()
	{
		int nearestIndex = 0;
		float lowestAbsDelta = content.sizeDelta.x;
		float lowestDelta = 0;
		float delta;
		float absDelta;

		for (int i=0; i<scrollItems.Length; i++)
		{
			// Position of item relative to viewport, add half viewport with for center snapping
			delta = (scrollItems[i].anchoredPosition.x + content.anchoredPosition.x) - viewport.sizeDelta.x * viewport.pivot.x;
			absDelta = Mathf.Abs(delta);

			// If closer than current closest to center of viewport
			if (absDelta < lowestAbsDelta)
			{
				lowestAbsDelta = absDelta;
				lowestDelta = delta;
				nearestIndex = i;
			}
		}

		// Target position for scroll content to move towards
		targetContentPos = new Vector2(contentPos.x-lowestDelta, contentPos.y);

		return nearestIndex;
	}

	// DEBUGGING
	/*GUIStyle guiStyle;
	void OnGUI()
	{
		guiStyle = new GUIStyle(GUI.skin.label);
		guiStyle.fontSize = 30;
		GUILayout.Label("State: "+ scrollState + "\nVelocity: "+scrollRect.velocity, guiStyle);
		GUILayout.Label("Snap: "+ snapIndex +" | "+ snapDelta, guiStyle);
	}*/
}
