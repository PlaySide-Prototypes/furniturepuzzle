﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI3DBackgroundNotifier : MonoBehaviour
{
	UIBackgroundIDs location;

//	public void SetLocation()
//	{
//		UI3DBackground.Inst.SetLocation(location);
//	}

	[EnumAction(typeof(UIBackgroundIDs))]
	public void SetLocation (int _location)
	{
		UI3DBackground.Instance.SetLocation((UIBackgroundIDs)_location);
	}
}
