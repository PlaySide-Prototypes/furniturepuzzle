﻿using UnityEngine;
using System.Collections.Generic;

/// <summary> Stand-alone FTUE, shown when a screen/hierarchy is enabled </summary>
/// <example>
/// 1. Put this on a Panel
/// 2. Add at least one FTUE (can have its own animations, switching etc)
/// 3. First time this component is enabled, the first FTUE will be displayed.
/// 4. If you add more than 1, the next one/s will be displayed next time/s the component is enabled.
/// 5. Each FTUE should call FTUEComplete() when it's finished, eg. from a dismiss button's event.
/// </example>
/// <remarks> Each FTUE can have its own sub-screens, switching animations, etc. </remarks>
public class MiniFTUE : MonoBehaviour
{
	#region Inspector variables

	[Header("Main FTUE Hierarchy")]
	[SerializeField] GameObject ftueHierarchy = null;

	[Header("1 or more FTUEs (next one plays whenever Component is (re)enabled)")]
	[SerializeField] GameObject[] ftues = null;

	#endregion // Inspector variables

	GameObject currentFTUE;

	/// <summary> Called when the object/script is enabled in the hierarchy </summary>
	void OnEnable()
	{
		if (ftueHierarchy != null)
		{
			// Check for opening the next subscreen in the sequence
			currentFTUE = null;
			for (int i = 0; i < ftues.Length; ++i)
			{
				GameObject ftue = ftues[i];
				if ((currentFTUE == null) && (PlayerPrefs.GetInt(GetPlayerPrefsKey(ftue)) == 0))
				{
					ftue.SetActive(true);
					currentFTUE = ftue;
				}
				else
					ftue.SetActive(false);
			}

			ftueHierarchy.SetActive(currentFTUE != null);
		}
	}

	/// <summary> Called from Button event etc, ie. when an FTUE screen is being closed </summary>
	public void MiniFTUEComplete()
	{
		if (currentFTUE == null)
			throw new UnityException("currentFTUEScreen is null - FTUE screen was not enabled");

		PlayerPrefs.SetInt(GetPlayerPrefsKey(currentFTUE), 1);
		PlayerPrefs.Save();

		ftueHierarchy.SetActive(false);
		currentFTUE.SetActive(false);
		currentFTUE = null;
	}

	/// <summary> Generates a unique(ish) PlayerPrefs key for the specified screen </summary>
	/// <param name="_ftue"> FTUE screen </param>
	/// <returns> The PlayerPrefs key </returns>
	string GetPlayerPrefsKey(GameObject _ftue)
	{
		return "MiniFTUE_Shown_" + PSUtils.Transforms.GetFullName(_ftue.transform);
	}

#if UNITY_EDITOR
	List<string> ftueNames = new List<string>();

	/// <summary> Called when component is loaded/changed in the Inspector </summary>
	void OnValidate()
	{
		if (ftues == null)
			return;

		// Ensure all subscreen names are unique
		ftueNames.Clear();
		for (int i = 0; i < ftues.Length; ++i)
		{
			GameObject ftue = ftues[i];
			while (ftueNames.Contains(ftue.name))
			{
				Debug.LogError("FTUE '" + ftue.name + "' name is not unique. Renaming one of them!");
				ftue.name += "_1";
			}
			ftueNames.Add(ftue.name);
		}
	}

	[ContextMenu("Clear Viewed From PlayerPrefs")]
	void ClearViewed()
	{
		for (int i = 0; i < ftues.Length; ++i)
			PlayerPrefs.DeleteKey(GetPlayerPrefsKey(ftues[i]));
		Debug.Log("MiniFTUE cleared from PlayerPrefs!");
	}
#endif
}
