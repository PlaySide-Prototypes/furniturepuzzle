﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public partial class PopupWindowContainer : MonoBehaviour
{
	[Header("Main")]
	public PopupIDs				popupId;					// Which popup this is
	public GameObject 			cancelButton;

	[Header("Title")]
	public Playside.UILocLabelText		titleText;			// Title

	[Header("Message + (optional) Image")]
	public Playside.UILocLabelText	messageText;		    // Message
	public Transform			imageContainer;				// Container for spawning image next to text

	[Header("Buttons")]
	public Playside.UILocLabelText[]	confirmButtonText;	// Confirm button's label
	public Playside.UILocLabelText		cancelButtonText;	// Cancel button's label
}
