using UnityEngine;
using System.Collections;

public class OpenPlatformSpecificURL : MonoBehaviour
{
	[SerializeField] string urlIOS;
	[SerializeField] string urlAndroid;

	public void OpenURL()
    {
		if (Application.platform == RuntimePlatform.IPhonePlayer)
	        Application.OpenURL(urlIOS);
		else
			Application.OpenURL(urlAndroid);
    }
}
