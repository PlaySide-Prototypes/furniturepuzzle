﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ColorVariantCategories
{
	GemColorTypes,
	RarityColorTypes
}

public enum BubbleColorTypes
{
	Blue,
	Purple,
	Red,
	Orange,
	Yellow,
	Green,
}

public enum RarityColorTypes
{
	Common,
	Rare,
	Epic
}
