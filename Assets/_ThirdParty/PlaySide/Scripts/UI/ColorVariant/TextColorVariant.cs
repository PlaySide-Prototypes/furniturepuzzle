﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TextColorVariant : ColorVariant
{
	Text text;
	int currentVariant;


	void Awake ()
	{
		text = GetComponent<Text>();
		initialized = true;
	}


	public override void RefreshColor(int _variant)
	{
		if (!initialized)
			return;
		
		text.color = variants[_variant].myColor;
	}
}
