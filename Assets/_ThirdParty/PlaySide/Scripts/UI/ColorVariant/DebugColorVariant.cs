﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugColorVariant : MonoBehaviour
{
	public BubbleColorTypes globalBubbleColor = BubbleColorTypes.Blue;
	public RarityColorTypes globalRarityColor = RarityColorTypes.Common;
}
