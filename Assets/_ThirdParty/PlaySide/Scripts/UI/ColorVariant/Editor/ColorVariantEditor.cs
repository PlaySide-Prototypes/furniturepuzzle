﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;
using System.Reflection;

[CustomEditor(typeof(ColorVariant), true)]
public class ColorVariantEditor : Editor
{

	ColorVariant colorVariant;
	Type enumType;
	int numEnumValues;


	void OnEnable ()
	{
		colorVariant = (ColorVariant)target;

		RefreshEnumInfo();
	}


	void RefreshEnumInfo()
	{
		if(colorVariant.variants == null)
			colorVariant.variants = new ColorVariant.Variant[0];

		colorVariant.categoryIndex = (int)colorVariant.enumType;

		// Add tintable categories
		switch(colorVariant.enumType)
		{
			case ColorVariantCategories.GemColorTypes:
				numEnumValues = System.Enum.GetValues(typeof(BubbleColorTypes)).Length;
				break;

			case ColorVariantCategories.RarityColorTypes:
				numEnumValues = System.Enum.GetValues(typeof(RarityColorTypes)).Length;
				break;
			default:
				numEnumValues = 0;
				break;
		}

		ColorVariant.Variant[] newVariants = new ColorVariant.Variant[numEnumValues];

		// Try and maintain previous variants when switching color type enums
		for(int i=0; i<numEnumValues; i++)
		{
			newVariants[i].colorIndex = i;

			if (i > colorVariant.variants.Length - 1)
				newVariants[i].myColor = Color.white;
			else
				newVariants[i] = colorVariant.variants[i];
		}

		colorVariant.variants = newVariants;
	}


	public override void OnInspectorGUI ()
	{
		EditorGUI.BeginChangeCheck();
		colorVariant.enumType = (ColorVariantCategories)EditorGUILayout.EnumPopup("Type", colorVariant.enumType);
		if(EditorGUI.EndChangeCheck())
		{
			RefreshEnumInfo();
		}

		for(int i=0; i<numEnumValues; i++)
		{
			switch(colorVariant.enumType)
			{
				case ColorVariantCategories.GemColorTypes:
					colorVariant.variants[i].myColor = EditorGUILayout.ColorField(((BubbleColorTypes)i).ToString(), colorVariant.variants[i].myColor);
					break;

				case ColorVariantCategories.RarityColorTypes:
					colorVariant.variants[i].myColor = EditorGUILayout.ColorField(((RarityColorTypes)i).ToString(), colorVariant.variants[i].myColor);
					break;
				default:
					break;
			}
		}
	}
}
