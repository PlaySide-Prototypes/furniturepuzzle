﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;
using System.Reflection;


[CustomEditor(typeof(DebugColorVariant))]
public class DebugColorVariantEditor : Editor
{
	DebugColorVariant debugColorVariant;

	void OnEnable ()
	{
		debugColorVariant = (DebugColorVariant)target;
		SetDebugColor();
	}

	public override void OnInspectorGUI ()
	{
		EditorGUI.BeginChangeCheck();
		debugColorVariant.globalBubbleColor = (BubbleColorTypes)EditorGUILayout.EnumPopup("Type", debugColorVariant.globalBubbleColor);
		debugColorVariant.globalRarityColor = (RarityColorTypes)EditorGUILayout.EnumPopup("Type", debugColorVariant.globalRarityColor);
		if(EditorGUI.EndChangeCheck())
			SetDebugColor();
	}

	void SetDebugColor()
	{
		// Minus 1 because for whatevre reason bubble asset starts enum index at 1 instead of 0
		if (Application.isPlaying)
		{
			ColorVariantManager.SetColorIndex((int)ColorVariantCategories.GemColorTypes, (int)debugColorVariant.globalBubbleColor);
			ColorVariantManager.SetColorIndex((int)ColorVariantCategories.RarityColorTypes, (int)debugColorVariant.globalRarityColor);
		}
	}
}
