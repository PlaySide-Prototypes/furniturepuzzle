﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class ColorVariant : MonoBehaviour
{
	[Serializable]
	public struct Variant
	{
		public int colorIndex;
		public Color myColor;
	}

	public ColorVariantCategories enumType;
	public Variant[] variants;

	public bool initialized = false;
	public int categoryIndex;
	bool registered = false;


    public abstract void RefreshColor(int _variant);

	void OnEnable()
	{
		Register();
		RefreshColor(ColorVariantManager.GetColorIndex(categoryIndex));
	}


	void OnDestroy()
	{
		ColorVariantManager.UnregisterColorVariant(categoryIndex, this);
	}


	void Register()
	{
		if (registered)
			return;
		
		ColorVariantManager.RegisterColorVariant(categoryIndex, this);
		registered = true;
	}
}
