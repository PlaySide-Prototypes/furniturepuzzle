﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RawImage))]
public class RawImageColorVariant : ColorVariant
{
	RawImage rawImage;
	int currentVariant;


	void Awake ()
	{
		rawImage = GetComponent<RawImage>();
		initialized = true;
	}


	public override void RefreshColor(int _variant)
	{
		if (!initialized)
			return;

		rawImage.color = variants[_variant].myColor;
	}
}
