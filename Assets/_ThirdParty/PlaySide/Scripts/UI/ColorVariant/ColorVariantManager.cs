﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ColorVariantManager
{
	// List of ColorVariant components, array index per ColorVariantCateogry
	static List<ColorVariant>[] allColorVariants;

	// Current color index per category
	static int[] variantTypeIndices;
	static bool initialized = false;


	public static void Initialize()
	{
		if (initialized)
			return;


		// Iniitalize ColorVariant component lists per ColorVariantCategory
		int variantCategoryCount = System.Enum.GetValues(typeof(ColorVariantCategories)).Length;
		allColorVariants = new List<ColorVariant>[variantCategoryCount];
		for (int i=0; i<variantCategoryCount; i++)
		{
			allColorVariants[i] = new List<ColorVariant>();
		}

		//
		variantTypeIndices = new int[variantCategoryCount];

		initialized = true;
	}


	// Set the current global color index value of an ColorVariantType
	public static void SetColorIndex(int _categoryIndex, int _colorEnumIndex)
	{
		Initialize();

		if (variantTypeIndices[_categoryIndex] != _colorEnumIndex)
		{
			variantTypeIndices[_categoryIndex] = _colorEnumIndex;
			RefreshAll(_categoryIndex);
		}
	}


	// Get the current global color index value of an ColorVariantType
	public static int GetColorIndex(int _categoryIndex)
	{
		Initialize();
		return variantTypeIndices[_categoryIndex];
	}


	// Refresh all color variants of a ColorVariantType
	public static void RefreshAll(int _categoryIndex)
	{
		Initialize();

		for(int i=0; i<allColorVariants[_categoryIndex].Count; i++)
		{
			allColorVariants[_categoryIndex][i].RefreshColor(variantTypeIndices[(int)allColorVariants[_categoryIndex][i].enumType]);
		}
	}


	// Register a new ColorVariant component
	public static void RegisterColorVariant(int _categoryIndex, ColorVariant _colorVariant)
	{
		Initialize();
		allColorVariants[_categoryIndex].Add(_colorVariant);
	}


	// Unregister a removed/destroyed ColorVariant component
	public static void UnregisterColorVariant(int _categoryIndex, ColorVariant _colorVariant)
	{
		Initialize();
		allColorVariants[_categoryIndex].Remove(_colorVariant);
	}
}
