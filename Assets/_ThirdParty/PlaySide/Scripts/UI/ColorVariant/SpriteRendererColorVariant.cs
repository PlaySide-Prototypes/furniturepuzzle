﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteRendererColorVariant : ColorVariant
{
	SpriteRenderer spriteRenderer;
	int currentVariant;


	void Awake ()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		initialized = true;
	}


	public override void RefreshColor(int _variant)
	{
		if (!initialized)
			return;
		
		spriteRenderer.color = variants[_variant].myColor;
	}
}
