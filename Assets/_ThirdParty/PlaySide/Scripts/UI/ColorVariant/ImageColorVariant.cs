﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ImageColorVariant : ColorVariant
{
	Image image;
	int currentVariant;


	void Awake ()
	{
		image = GetComponent<Image>();
		initialized = true;
	}


	public override void RefreshColor(int _variant)
	{
		if (!initialized)
			return;
		
		image.color = variants[_variant].myColor;
	}
}
