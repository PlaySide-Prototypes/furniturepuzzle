using System.Collections.Generic;
using UnityEngine;

public class RecycleStack
{
	Stack<GameObject> pool = new Stack<GameObject>();

	public void Recycle(GameObject gameObj)
	{
		gameObj.SetActive(false);
		pool.Push(gameObj);
	}

	public GameObject RetrieveOrCreate(GameObject prefab, bool setActive = true)
	{
		GameObject gameObj = (pool.Count > 0) ? pool.Pop() : GameObject.Instantiate(prefab);

		if (setActive)
			gameObj.SetActive(true);

		return gameObj;
	}

	public void Empty()
	{
		while (pool.Count > 0)
			GameObject.Destroy(pool.Pop());
	}

	public void Clear()
	{
		pool.Clear();
	}
}
