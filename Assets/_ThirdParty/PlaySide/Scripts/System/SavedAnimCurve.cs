﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "TestCurve", menuName = "Resources/Curves", order = 1)]
public class SavedAnimCurve : ScriptableObject
{
	public AnimationCurve curve;
	internal float averageValue = 0.0f;

	public float Evaluate(float time)
	{
		return curve.Evaluate(time);
	}

	public bool IsTimePastEnd(float time)
	{
		return time > curve.keys[curve.length - 1].time;
	}

#if UNITY_EDITOR
	public void OnValidate()
	{
		//UpdateAverageValue();
	}

	public void UpdateAverageValue()
	{
		float endTime = curve.keys[curve.keys.Length - 1].time;
		float totalValue = 0.0f;
		int samplesTaken = 0;
		for (float i = 0.0f; i < endTime; i += 0.01f)
		{
			totalValue += curve.Evaluate(i);
			++samplesTaken;
		}

		SerializedObject serializedObject = new SerializedObject(this);
		serializedObject.Update();
		serializedObject.FindProperty("averageValue").floatValue = totalValue / samplesTaken;
		serializedObject.ApplyModifiedProperties();
	}

	public void ScaleValues()
	{

	}
#endif
}

#if UNITY_EDITOR
[CustomEditor(typeof(SavedAnimCurve))]
public class SavedAnimCurveEditor : Editor
{
	float multiplier = 1.0f;

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		if (GUILayout.Button("Normalize"))
		{
			SavedAnimCurve curveTarget = (SavedAnimCurve)target;
			float highestValue = Mathf.NegativeInfinity;
			float lowestValue = Mathf.Infinity;
			for (int i = 0; i < curveTarget.curve.length; ++i)
			{
				if (curveTarget.curve.keys[i].value > highestValue)
					highestValue = curveTarget.curve.keys[i].value;
				if (curveTarget.curve.keys[i].value < lowestValue)
					lowestValue = curveTarget.curve.keys[i].value;
			}

			Keyframe[] savedKeys = curveTarget.curve.keys;
			for (int i = 0; i < savedKeys.Length; ++i)
			{
				serializedObject.Update();

				Keyframe oldFrame = savedKeys[i];
				float lerpValue = (oldFrame.value - lowestValue) / (highestValue - lowestValue);
				float newValue = Mathf.Lerp(0, 1, lerpValue);

				curveTarget.curve.RemoveKey(i);
				curveTarget.curve.AddKey(new Keyframe(oldFrame.time, newValue, oldFrame.inTangent, oldFrame.outTangent, oldFrame.inWeight, oldFrame.outWeight));

				serializedObject.ApplyModifiedProperties();
			}
		}

		// Moves the curve so that the first point is at the origin.
		if (GUILayout.Button("Zero"))
		{
			SavedAnimCurve curveTarget = (SavedAnimCurve)target;
			Keyframe[] savedKeys = curveTarget.curve.keys;
			float timeDiff = savedKeys[0].time;
			float valueDiff = savedKeys[0].value;

			serializedObject.Update();
			for (int i = 0; i < savedKeys.Length; ++i)
			{
				Keyframe oldFrame = savedKeys[i];
				curveTarget.curve.RemoveKey(i);
				curveTarget.curve.AddKey(new Keyframe(oldFrame.time - timeDiff, oldFrame.value - valueDiff, oldFrame.inTangent, oldFrame.outTangent, oldFrame.inWeight, oldFrame.outWeight));
			}
			serializedObject.ApplyModifiedProperties();
		}

		GUILayout.BeginHorizontal();
		multiplier = EditorGUILayout.FloatField(multiplier);
		if (GUILayout.Button("Scale Curve Values"))
		{
			SavedAnimCurve curveTarget = (SavedAnimCurve)target;

			serializedObject.Update();
			Undo.RecordObject(curveTarget, "Scale curve");

			Keyframe[] savedKeys = curveTarget.curve.keys;
			for (int i = 0; i < savedKeys.Length; ++i)
			{
				Keyframe oldFrame = savedKeys[i];
				curveTarget.curve.RemoveKey(i);
				curveTarget.curve.AddKey(new Keyframe(oldFrame.time, oldFrame.value * multiplier, oldFrame.inTangent, oldFrame.outTangent, oldFrame.inWeight, oldFrame.outWeight));
			}

			serializedObject.ApplyModifiedProperties();
		}
		GUILayout.EndHorizontal();


		if (GUILayout.Button("Convert from Maya FOV to Unity FOV"))
		{
			SavedAnimCurve curveTarget = (SavedAnimCurve)target;

			serializedObject.Update();
			Undo.RecordObject(curveTarget, "Maya to Unity FOV");

			Keyframe[] savedKeys = curveTarget.curve.keys;
			for (int i = 0; i < savedKeys.Length; ++i)
			{
				Keyframe oldFrame = savedKeys[i];
				curveTarget.curve.RemoveKey(i);
				float angleOfView = 21.946f / (2.0f * oldFrame.value);
				float verticalFOV = 2.0f * Mathf.Atan(angleOfView * (1920 / 1080));
				verticalFOV *= Mathf.Rad2Deg;

				curveTarget.curve.AddKey(new Keyframe(oldFrame.time, verticalFOV, oldFrame.inTangent, oldFrame.outTangent, oldFrame.inWeight, oldFrame.outWeight));
			}

			serializedObject.ApplyModifiedProperties();
		}
	}
}

#endif
