﻿using UnityEngine;

public class AspectRatio
{
	public enum Ratios
	{
		Undefined,

		Landscape_4_3,
		Landscape_16_10,
		Landscape_16_9,
		Landscape_185_9,	// 18.5:9 (Galaxy S8/S9)
		Landscape_195_9,	// 19.46:9 (iPhone X)

		Portrait_4_3,
		Portrait_16_10,
		Portrait_16_9,
		Portrait_185_9,		// 9:18.5 (Galaxy S8/S9)
		Portrait_195_9,		// 9:19.46 (iPhone X)
	}

	static Ratios _ratio = Ratios.Undefined;
	public static float _fraction;

	/// <summary> Name of the aspect ratio </summary>
	public static Ratios Ratio
	{
		get
		{
			if (_ratio == 0)
				Calculate();

			return _ratio;
		}
	}

	/// <summary> Actual aspect fraction, eg. 1.333 for (landscape) 4:3, 1.666 for 16:10, 0.75 for (portrait) 3:4, etc </summary>
	public static float Fraction
	{
		get
		{
			if (_fraction == 0)
				Calculate();

			return _fraction;
		}
	}

	/// <summary> Calculates the fraction + aspect ratio </summary>
	static void Calculate()
	{
		if (Screen.width > Screen.height)
		{
			// Landscape
			_fraction = (float)Screen.width / Screen.height;
			if (_fraction < (4f / 3f) + 0.1f)
				_ratio = Ratios.Landscape_4_3;
			else if (_fraction < (16f / 10f) + 0.1f)
				_ratio = Ratios.Landscape_16_10;
			else if (_fraction < (16f / 9f) + 0.1f)
				_ratio = Ratios.Landscape_16_9;
			else if (_fraction < (2960f / 1440f) + 0.1f)
				_ratio = Ratios.Landscape_185_9;
			else if (_fraction < (2436f / 1125f) + 0.1f)
				_ratio = Ratios.Landscape_195_9;
		}
		else
		{
			// Portrait
			_fraction = (float)Screen.width / Screen.height;
			if (_fraction > (3f / 4f) - 0.01f)
				_ratio = Ratios.Portrait_4_3;
			else if (_fraction > (10f / 16f) - 0.01f)
				_ratio = Ratios.Portrait_16_10;
			else if (_fraction > (9f / 16f) - 0.01f)
				_ratio = Ratios.Portrait_16_9;
			else if (_fraction > (1440f / 2960f) - 0.01f)
				_ratio = Ratios.Portrait_185_9;
			else if (_fraction > (1125f / 2436f) - 0.01f)
				_ratio = Ratios.Portrait_195_9;
		}
	}
}
