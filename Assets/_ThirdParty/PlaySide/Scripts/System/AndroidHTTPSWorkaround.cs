﻿using UnityEngine;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

public sealed class AndroidHTTPSWorkaround : MonoBehaviour
{
#if UNITY_ANDROID
	void Awake()
	{
		// Debug.Log("Installing handling for Android HTTPS issue.");
		System.Net.ServicePointManager.ServerCertificateValidationCallback =
		delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
		{
			// Debug.Log("Handled Android HTTPS issue");
			return true;
		};
	}
#endif
}