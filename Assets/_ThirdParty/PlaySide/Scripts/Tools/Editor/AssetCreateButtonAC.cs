﻿// REFERENCE HERE: https://docs.unity3d.com/ScriptReference/Animations.AnimatorController.html

using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using System.Collections;

public class AssetCreateButtonAC : MonoBehaviour
{
	[MenuItem("Assets/PlaySide UI/Create Button Controller", false, 1)]
	static void CreateAnimatorController()
	{
		string selectedFolder = AssetDatabase.GetAssetPath(Selection.activeObject) + "/";
		string acName = Selection.activeObject.name;
		string acPath = selectedFolder + "AC_" + acName + ".controller";
		AnimatorController controller;

		// Add CONTROLLER
		if (AssetDatabase.LoadAssetAtPath(acPath, typeof(AnimatorController)) == null)
			controller = UnityEditor.Animations.AnimatorController.CreateAnimatorControllerAtPath(acPath);
		else
		{
			Debug.LogWarning("Failed to create Button AnimatorController, one already exists: " + acPath);
			controller = (AnimatorController)AssetDatabase.LoadAssetAtPath(acPath, typeof(AnimatorController));
			return;
		}

		// ------------------------------
		// Add CONTROLLER DATA
		// ------------------------------
		// Add STATEMACHINES
		AnimatorStateMachine rootStateMachine = controller.layers[0].stateMachine;

		// Add PARAMETERS
		controller.AddParameter("Inactive", AnimatorControllerParameterType.Bool);
		controller.AddParameter("Pressed", AnimatorControllerParameterType.Trigger);
		controller.AddParameter("Highlighted", AnimatorControllerParameterType.Trigger);
		controller.AddParameter("Normal", AnimatorControllerParameterType.Trigger);
		controller.AddParameter("Disabled", AnimatorControllerParameterType.Trigger);

		// Add STATES
		AnimatorState stateOnIdle = rootStateMachine.AddState("OnIdle");
		AnimatorState statePressed = rootStateMachine.AddState("Pressed");
		AnimatorState stateInactive = rootStateMachine.AddState("Inactive");

		// Add CLIPS
		AnimationClip clipOnIdle = CreateAnimClip(selectedFolder, acName, "OnIdle", true);
		AnimationClip clipPressed = CreateAnimClip(selectedFolder, acName, "Pressed", false);
		AnimationClip clipInactive = CreateAnimClip(selectedFolder, acName, "Inactive", true);

		// Add TRANSITIONS
		AnimatorStateTransition trans_OnIdle_To_Pressed = 		stateOnIdle.AddTransition(statePressed, false);
		AnimatorStateTransition trans_Pressed_To_OnIdle =		statePressed.AddTransition(stateOnIdle, true);

		AnimatorStateTransition trans_Any_To_Inactive = 		rootStateMachine.AddAnyStateTransition(stateInactive);
		AnimatorStateTransition trans_Inactive_To_OnIdle = 		stateInactive.AddTransition(statePressed, true);
		AnimatorStateTransition trans_Inactive_To_Pressed = 	stateInactive.AddTransition(stateOnIdle, true);


		trans_OnIdle_To_Pressed.exitTime = trans_Pressed_To_OnIdle.exitTime = 0.99f;
		trans_Any_To_Inactive.duration = trans_OnIdle_To_Pressed.duration = trans_Pressed_To_OnIdle.duration = 0.01f;

		trans_Inactive_To_OnIdle.hasExitTime = false;
		trans_Inactive_To_Pressed.hasExitTime = false;
		trans_Inactive_To_OnIdle.duration = trans_Inactive_To_Pressed.duration = 0.01f;

		trans_OnIdle_To_Pressed.AddCondition(UnityEditor.Animations.AnimatorConditionMode.If, 1, "Pressed");
		trans_Any_To_Inactive.AddCondition(UnityEditor.Animations.AnimatorConditionMode.If, 1, "Inactive");
		trans_Inactive_To_OnIdle.AddCondition(UnityEditor.Animations.AnimatorConditionMode.IfNot, 1, "Inactive");
		trans_Inactive_To_Pressed.AddCondition(UnityEditor.Animations.AnimatorConditionMode.IfNot, 1, "Inactive");


		// ------------------------------
		// Add ANIMATION DATA
		// ------------------------------
		// OnIdle
		clipOnIdle.SetCurve("", typeof(CanvasGroup), "m_Alpha", AnimationCurve.Linear(0f, 1f, 1f, 1f));
		clipOnIdle.SetCurve("Content", typeof(Transform), "m_LocalScale.x", AnimationCurve.Linear(0f, 1f, 1f, 1f));
		clipOnIdle.SetCurve("Content", typeof(Transform), "m_LocalScale.y", AnimationCurve.Linear(0f, 1f, 1f, 1f));
		clipOnIdle.SetCurve("Content", typeof(Transform), "m_LocalScale.z", AnimationCurve.Linear(0f, 1f, 1f, 1f));


		// Pressed
		AnimationCurve curvePressedScale = AnimationCurve.Linear(0f, 1f, 0.5f, 1f);
		Keyframe keyPressedScale0 = new Keyframe();
		keyPressedScale0.time = 0.0833f;
		keyPressedScale0.value = 0.95f;
		Keyframe keyPressedScale1 = new Keyframe();
		keyPressedScale1.time = 0.1666f;
		keyPressedScale1.value = 1.025f;
		Keyframe keyPressedScale2 = new Keyframe();
		keyPressedScale2.time = 0.25f;
		keyPressedScale2.value = 1f;

		curvePressedScale.AddKey(keyPressedScale0);
		curvePressedScale.AddKey(keyPressedScale1);
		curvePressedScale.AddKey(keyPressedScale2);

		clipPressed.SetCurve("", typeof(CanvasGroup), "m_Alpha", AnimationCurve.Linear(0f, 1f, 0.5f, 1f));
		clipPressed.SetCurve("Content", typeof(Transform), "m_LocalScale.x", curvePressedScale);
		clipPressed.SetCurve("Content", typeof(Transform), "m_LocalScale.y", curvePressedScale);
		clipPressed.SetCurve("Content", typeof(Transform), "m_LocalScale.z", curvePressedScale);


		// Inactive
		clipInactive.SetCurve("", typeof(CanvasGroup), "m_Alpha", AnimationCurve.Linear(0f, 0.5f, 1f, 0.5f));
		clipInactive.SetCurve("Content", typeof(Transform), "m_LocalScale.x", AnimationCurve.Linear(0f, 1f, 1f, 1f));
		clipInactive.SetCurve("Content", typeof(Transform), "m_LocalScale.y", AnimationCurve.Linear(0f, 1f, 1f, 1f));
		clipInactive.SetCurve("Content", typeof(Transform), "m_LocalScale.z", AnimationCurve.Linear(0f, 1f, 1f, 1f));


		// Assign Clips
		stateOnIdle.motion = 	clipOnIdle;
		statePressed.motion = 	clipPressed;
		stateInactive.motion = 	clipInactive;
	}


	// Create new Animation Clip asset in directory
	static AnimationClip CreateAnimClip(string _path, string _acName, string _clipName, bool _loop)
	{
		AnimationClip newClip = new AnimationClip();
		string clipPath = _path + "ANM_" + _acName + "_" + _clipName + ".anim";

		if (_loop)
		{
			AnimationClipSettings settings = AnimationUtility.GetAnimationClipSettings(newClip);
			settings.loopTime = true;
			AnimationUtility.SetAnimationClipSettings(newClip, settings); 
		}

		// Only create asset if non exists
		if (AssetDatabase.LoadAssetAtPath(clipPath, typeof(AnimationClip)) == null)
			AssetDatabase.CreateAsset(newClip, clipPath);

		return newClip;
	}


	// Add Animation Event
	static void CreateAnimationEvent(AnimationClip _clip, string _event, float _timePosition)
	{
		AnimationEvent[] evts = new AnimationEvent[1];
		evts[0] = new AnimationEvent();
		evts[0].time = _timePosition;
		evts[0].functionName = _event;

		AnimationUtility.SetAnimationEvents(_clip, evts);
	}
}
