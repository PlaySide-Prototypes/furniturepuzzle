﻿using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using System.Collections.Generic;

public class AnimationAssistant : EditorWindow
{
	static EditorWindow thisWindow;
	static Rect animationWindowRect;
	static Rect helperWindowRect;
	Vector2 windowDimensions;

	[MenuItem("Window/Animation Assistant", false, 0)]//, false, 1)]
	public static void RebuildPrefabFromFBX()
	{
		thisWindow = EditorWindow.GetWindow(typeof(AnimationAssistant));

		//thisWindow.titleContent = new GUIContent("Animation Assistant");
		animationWindowRect = AnimationWindowHelper.GetOpenAnimationWindowRect();
		Vector2 windowSize = new Vector2(200f, animationWindowRect.height);
		thisWindow.minSize = windowSize;
		thisWindow.maxSize = windowSize;
		thisWindow.ShowUtility();
	}

	struct KeyProperty
	{
		public string transformName;
		public string propertyName;
		public string path;
		public string newPath;
		public System.Type type;
		public bool modified;
	}

	struct ClipInfo
	{
		public AnimationClip clip;
		public string assetPath;
		public bool currentClip;
	}

//	bool connectedToAnimation = false;
	bool initialized = false;
	bool showWindowContents = true;
	bool awaitingPathRename;
	bool showMissingBindings = true;

	AnimatorController 	animatorController;
	AnimationClip currentClip;
	AnimationClip[] clips;
	bool currentClipLoopTime;

	// Renaming path
	string currentRenamedPath;
	int renameIndex;

	// Clip data
	KeyProperty[] currentClipKeyProperties;
	KeyProperty[] keyProperties;
	Dictionary<int, bool> missingPropertyIndices = new Dictionary<int, bool>();
//	ClipInfo[] clipInfos;
	int propertyIndex;
	
	// GUI
	Vector2 scrollPos;
	Rect bgRect;
	Texture2D bgTexture;

	Color offWhite = new Color(.5f, .5f, .5f, 1f);
//	Color lightBlue = new Color(.7f, .8f, 1f, 1f);
	Color darkGrey = new Color(.2f, .2f, .2f, 1f);



	void OnEnable()
	{
		initialized = true;
		animatorController = AnimationWindowHelper.GetAnimationWindowCurrentController();
		currentClip = AnimationWindowHelper.GetAnimationWindowCurrentClip();
		RefreshDetails();

		bgTexture = new Texture2D(1, 1);
		bgTexture.SetPixel(0,0,Color.white);
		bgTexture.Apply();
	}

	public void OnInspectorUpdate()
	{
		AnimationWindowHelper.ForceOpenAnimationWindowRepaint();
//		connectedToAnimation = AnimationWindowHelper.IsAnimationWindowOpen();
		PositionWindow();
		Repaint();
	}

	void PositionWindow()
	{
		// Ensure window doesn't take focus since it steals t off the actual Animation Window
		thisWindow = this;//EditorWindow.GetWindow(typeof(AnimationAssistant), false, "Animation Assistant", false);
		
		animationWindowRect = AnimationWindowHelper.GetOpenAnimationWindowRect();
		windowDimensions = new Vector2(showWindowContents ? 200f : 25f, animationWindowRect.height);

		thisWindow.minSize = windowDimensions;
		thisWindow.maxSize = windowDimensions;

		helperWindowRect = new Rect(animationWindowRect.x - windowDimensions.x, animationWindowRect.y + 5f, windowDimensions.x, windowDimensions.y);
		thisWindow.position = helperWindowRect;

		bgRect = new Rect(0, 0, windowDimensions.x, windowDimensions.y);
	}

	void OnGUI()
	{
		if (initialized)
		{
			GUI.skin.box.normal.background = bgTexture;
			GUI.color = darkGrey;//connectedToAnimation ? darkGrey : Color.red; 
			GUI.Box(bgRect, GUIContent.none);
			GUI.color = Color.white; 

			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button(EditorGUIUtility.FindTexture("d_animationvisibilitytoggleon"), GUILayout.Width(25), GUILayout.Height(25)))
			{
				showWindowContents = !showWindowContents;

				PositionWindow();
			}
			GUILayout.Space(5);
			GUILayout.EndHorizontal();

			if (!showWindowContents)
				return;

			if(!AnimationWindowHelper.IsAnimationWindowOpen())
			{
				GUILayout.Label("No Animation Window Open");
				return;
			}

			// Update animator controller if selection has changed
			if (animatorController == null || animatorController != AnimationWindowHelper.GetAnimationWindowCurrentController())
			{
				animatorController = AnimationWindowHelper.GetAnimationWindowCurrentController();
				currentClip = AnimationWindowHelper.GetAnimationWindowCurrentClip();
				RefreshDetails();
			}
			else if (currentClip == null || currentClip != AnimationWindowHelper.GetAnimationWindowCurrentClip())
			{
				currentClip = AnimationWindowHelper.GetAnimationWindowCurrentClip();
				RefreshDetails();
			}
			if (animatorController == null || currentClip == null)
				return;
			
			GUILayout.BeginHorizontal();
			GUILayout.Space(5);
			if (GUILayout.Button("Select Animator"))
				AnimationWindowHelper.PrintMethods();
				//Selection.activeObject = animatorController;
			GUILayout.Space(5);
			GUILayout.EndHorizontal();

			EditorGUILayout.ObjectField("", animatorController, typeof(AnimatorController), true);

			GUILayout.Label("");
			GUILayout.Label(currentClip.name);

			EditorGUI.BeginChangeCheck();
			currentClipLoopTime = EditorGUILayout.ToggleLeft("Loop Clip", currentClipLoopTime);
			if(EditorGUI.EndChangeCheck())
			{
				AnimationClipSettings settings = AnimationUtility.GetAnimationClipSettings(currentClip);
				settings.loopTime = currentClipLoopTime;
				AnimationUtility.SetAnimationClipSettings(currentClip, settings); 
			}


			GUILayout.Label("");

			showMissingBindings = EditorGUILayout.Foldout(showMissingBindings, "Missing Bindings");

			if (animatorController == null)
				return;
			else
			{
				if (showMissingBindings && keyProperties != null && currentClipKeyProperties != null)
				{
					// Table contents
					scrollPos = EditorGUILayout.BeginScrollView(scrollPos, false, false, GUILayout.Width(windowDimensions.x));//GUILayout.Height((float)(highestChildCount+1)*20f));
					DrawAnimPropertyList();
					EditorGUILayout.EndScrollView();
				}
			}
		}
	}


	// Draw propreties and their paths
	void DrawAnimPropertyList()
	{
		GUILayout.BeginHorizontal();
		GUILayout.BeginVertical();
		EditorGUI.indentLevel++;

		for(int i=0; i<keyProperties.Length; i++)
		{
			//if (!ArrayUtility.Contains(currentClipKeyProperties, keyProperties[i]))
			if(missingPropertyIndices[i])
			{
				GUILayout.BeginHorizontal();
				if (GUILayout.Button("+", GUILayout.Width(20f)))
					AddKeyProperty(keyProperties[i]);
				GUILayout.BeginVertical();
				EditorGUILayout.LabelField(keyProperties[i].propertyName);
				GUI.color = offWhite;
				EditorGUILayout.LabelField(keyProperties[i].path);
				GUI.color = Color.white;
				GUILayout.EndVertical();
				GUILayout.EndHorizontal();
			}
		}

		EditorGUI.indentLevel--;
		GUILayout.EndVertical();
		GUILayout.EndHorizontal();
	}


	// Retrive curve data from all clips in animator controller
	void RefreshDetails()
	{
		if (animatorController == null)
			return;

		List<KeyProperty> tmpCurrentClipPropreties = new List<KeyProperty>();
		List<KeyProperty> tmpPropreties = new List<KeyProperty>();
		List<ClipInfo> tmpClips = new List<ClipInfo>();
		missingPropertyIndices = new Dictionary<int, bool>();

		AnimationClipSettings settings = AnimationUtility.GetAnimationClipSettings(currentClip);
		currentClipLoopTime = settings.loopTime;
		AnimationUtility.SetAnimationClipSettings(currentClip, settings); 

		// Get Clips
		foreach (AnimatorControllerLayer layer in animatorController.layers)
		{
			ChildAnimatorState[] rootStateMachine = layer.stateMachine.states;
			foreach (ChildAnimatorState s in rootStateMachine)
			{
				if (s.state != null && s.state.motion != null)
				{
					ClipInfo info = new ClipInfo();

					info.assetPath = AssetDatabase.GetAssetPath(s.state.motion);
					info.clip = AssetDatabase.LoadAssetAtPath(info.assetPath, typeof(AnimationClip)) as AnimationClip;
					info.currentClip = info.clip == AnimationWindowHelper.GetAnimationWindowCurrentClip();

					if(!tmpClips.Contains(info))
						tmpClips.Add(info);
				}
			}
		}
//		clipInfos = tmpClips.ToArray();


		if(currentClip)
		{
			KeyProperty kp = new KeyProperty();
			EditorCurveBinding[] bindings =  AnimationUtility.GetCurveBindings(currentClip);

			foreach (EditorCurveBinding b in bindings)
			{
				kp.propertyName = b.propertyName;
				kp.path = b.path;
				kp.newPath = b.path;
				kp.type = b.type;
				tmpCurrentClipPropreties.Add(kp);
			}
		}
		currentClipKeyProperties = tmpCurrentClipPropreties.ToArray();


		// Get Key Properties
		foreach (ClipInfo c in tmpClips)
		{
			if (c.clip != null)
			{
				KeyProperty kp = new KeyProperty();
				EditorCurveBinding[] bindings =  AnimationUtility.GetCurveBindings(c.clip); 

				foreach (EditorCurveBinding b in bindings)
				{
					bool duplicateBinding = false;
					kp.propertyName = b.propertyName;
					kp.path = b.path;
					kp.newPath = b.path;
					kp.type = b.type;

					// Check for duplicate propreties between clips
					for(int i=0; i<tmpPropreties.Count; i++)
					{
						if (tmpPropreties.Contains(kp))
						{
							duplicateBinding = true;
							break;
						}
					}

					if(!duplicateBinding)
						tmpPropreties.Add(kp);
				}
			}
			keyProperties = tmpPropreties.ToArray();
		}

		// Check for missing propreties in current clip
		for(int i=0; i<keyProperties.Length; i++)
		{
			missingPropertyIndices[i] = !tmpCurrentClipPropreties.Contains(keyProperties[i]);
		}
	}


	// Add KeyProperty missing from current clip
	void AddKeyProperty(KeyProperty _kp)
	{
		currentClip.SetCurve(_kp.path, _kp.type, _kp.propertyName, AnimationCurve.Linear(0f, 1f, 1f, 1f));
		AnimationWindowHelper.RefreshOpenAnimationWindow();
		RefreshDetails();
	}
}