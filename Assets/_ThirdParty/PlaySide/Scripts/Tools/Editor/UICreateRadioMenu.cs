﻿// REFERENCE HERE: https://docs.unity3d.com/ScriptReference/Animations.AnimatorController.html

using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using UnityEditor.Animations;
using System.Collections;

public class UICreateRadioMenu : MonoBehaviour
{
	[MenuItem("GameObject/PlaySide UI/Create Radio Menu", false, 0)]
	private static void CreateRadioMenu()
	{
		GameObject selectedGO = Selection.activeGameObject;
		Transform selectedTrans = selectedGO.transform; 

		// ----------------------
		// ROOT RectTransform
		// ----------------------
		GameObject panelGameObject = new GameObject("RadioMenu");
		RectTransform rectTransRoot = panelGameObject.AddComponent(typeof(RectTransform)) as RectTransform;
		rectTransRoot.SetParent(selectedTrans, false);
		rectTransRoot.anchorMin = new Vector2(0, 0);
		rectTransRoot.anchorMax = new Vector2(1, 1);
		rectTransRoot.sizeDelta = new Vector2(0, 0);

		// _---------------------
		// Content
		// _---------------------
		GameObject goContent = null;
		Transform transContent =  rectTransRoot.Find("Content");

		if (transContent == null)
		{
			goContent = new GameObject("Content");
			transContent = goContent.transform;
		}
		else
			goContent = transContent.gameObject;

		RectTransform rectTransContent = transContent.GetComponent<RectTransform>();

		if (rectTransContent == null)
		{
			rectTransContent = goContent.AddComponent(typeof(RectTransform)) as RectTransform;
			rectTransContent.SetParent(rectTransRoot, false);
		}
		rectTransContent.anchorMin = new Vector2(0, 0);
		rectTransContent.anchorMax = new Vector2(1, 1);
		rectTransContent.sizeDelta = new Vector2(0, 0);

		// ----------------------
		// Animator
		// ----------------------
		Animator anim = panelGameObject.GetComponent<Animator>();
		if(anim == null)
			anim = panelGameObject.AddComponent(typeof(Animator)) as Animator;

		if(anim != null)
		{
			string[] controllerPaths = AssetDatabase.FindAssets("AC_RadioMenu_Generic");
			if (controllerPaths.Length > 0)
			{
				string pathBtnController = AssetDatabase.GUIDToAssetPath(controllerPaths[0]);
				AnimatorController controller = AssetDatabase.LoadAssetAtPath(pathBtnController, typeof(AnimatorController)) as AnimatorController; 
				UnityEditor.Animations.AnimatorController.SetAnimatorController(anim, controller);
			}
		}

		// ----------------------
		// Radio Menu
		// ----------------------
		RadioMenu radioMenu = panelGameObject.GetComponent<RadioMenu>();
		if (radioMenu == null)
			radioMenu  = panelGameObject.AddComponent(typeof(RadioMenu)) as RadioMenu;

		// ----------------------
		// Canvas GRoup
		// ----------------------
		CanvasGroup canvasGroup = panelGameObject.GetComponent<CanvasGroup>();
		if (canvasGroup == null)
			canvasGroup  = panelGameObject.AddComponent(typeof(CanvasGroup)) as CanvasGroup;

		Selection.activeGameObject = panelGameObject;
	}
}
