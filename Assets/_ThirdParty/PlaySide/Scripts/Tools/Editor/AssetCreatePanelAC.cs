﻿// REFERENCE HERE: https://docs.unity3d.com/ScriptReference/Animations.AnimatorController.html

using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using System.Collections;

public class AssetCreatePanelAC : MonoBehaviour
{
	[MenuItem("Assets/PlaySide UI/Create Panel Controller", false, 1)]
	static void CreateAnimatorController()
	{
		string selectedFolder = AssetDatabase.GetAssetPath(Selection.activeObject) + "/";
		string acName = Selection.activeObject.name;
		string acPath = selectedFolder + "AC_" + acName + ".controller";
		AnimatorController controller;

		// Add CONTROLLER
		if (AssetDatabase.LoadAssetAtPath(acPath, typeof(AnimatorController)) == null)
			controller = UnityEditor.Animations.AnimatorController.CreateAnimatorControllerAtPath(acPath);
		else
		{
			Debug.LogWarning("Failed to create Panel AnimatorController, one AnimatorController already exists: " + acPath);
			controller = (AnimatorController)AssetDatabase.LoadAssetAtPath(acPath, typeof(AnimatorController));
			return;
		}

		// ------------------------------
		// Add CONTROLLER DATA
		// ------------------------------
		// Add STATEMACHINES
		AnimatorStateMachine rootStateMachine = controller.layers[0].stateMachine;

		// Add STATES
		AnimatorState stateStart = rootStateMachine.AddState("Start");
		AnimatorState stateOn = rootStateMachine.AddState("On");
		AnimatorState stateOnIdle = rootStateMachine.AddState("OnIdle");
		AnimatorState stateOff = rootStateMachine.AddState("Off");
		AnimatorState stateOffIdle = rootStateMachine.AddState("OffIdle");

		// Add CLIPS
		AnimationClip clipStart = CreateAnimClip(selectedFolder, acName, "Start", false);
		AnimationClip clipOn = CreateAnimClip(selectedFolder, acName, "On", false);
		AnimationClip clipOnIdle = CreateAnimClip(selectedFolder, acName, "OnIdle", true);
		AnimationClip clipOff = CreateAnimClip(selectedFolder, acName, "Off", false);
		AnimationClip clipOffIdle = CreateAnimClip(selectedFolder, acName, "OffIdle", true);

		// Add TRANSITIONS
		AnimatorStateTransition trans_On_To_OnIdle = stateOn.AddTransition(stateOnIdle, true);
		AnimatorStateTransition trans_Off_To_OffIdle = stateOff.AddTransition(stateOffIdle, true);
		trans_On_To_OnIdle.exitTime = trans_Off_To_OffIdle.exitTime = 0.99f;
		trans_On_To_OnIdle.duration = trans_Off_To_OffIdle.duration = 0.01f;


		// ------------------------------
		// Add ANIMATION DATA
		// ------------------------------
		// Start
		clipStart.SetCurve("", typeof(CanvasGroup), "m_Alpha", 				AnimationCurve.Linear(0f, 0f, 1f, 0f));
		clipStart.SetCurve("", typeof(CanvasGroup), "m_Interactable", 		AnimationCurve.Linear(0f, 0f, 1f, 0f));
		clipStart.SetCurve("", typeof(CanvasGroup), "m_BlocksRaycasts", 	AnimationCurve.Linear(0f, 0f, 1f, 0f));

		// On
		clipOn.SetCurve("", typeof(CanvasGroup), "m_Alpha", 				AnimationCurve.Linear(0f, 0f, 0.5f, 1f));
		clipOn.SetCurve("", typeof(CanvasGroup), "m_Interactable", 			AnimationCurve.Linear(0f, 0f, 0.5f, 0f));
		clipOn.SetCurve("", typeof(CanvasGroup), "m_BlocksRaycasts", 		AnimationCurve.Linear(0f, 0f, 0.5f, 0f));

		// OnIdle
		clipOnIdle.SetCurve("", typeof(CanvasGroup), "m_Alpha", 			AnimationCurve.Linear(0f, 1f, 1f, 1f));
		clipOnIdle.SetCurve("", typeof(CanvasGroup), "m_Interactable", 		AnimationCurve.Linear(0f, 1f, 1f, 1f));
		clipOnIdle.SetCurve("", typeof(CanvasGroup), "m_BlocksRaycasts", 	AnimationCurve.Linear(0f, 1f, 1f, 1f));

		// Off
		clipOff.SetCurve("", typeof(CanvasGroup), "m_Alpha", 				AnimationCurve.Linear(0f, 1f, 0.25f, 0f));
		clipOff.SetCurve("", typeof(CanvasGroup), "m_Interactable", 		AnimationCurve.Linear(0f, 0f, 0.25f, 0f));
		clipOff.SetCurve("", typeof(CanvasGroup), "m_BlocksRaycasts", 		AnimationCurve.Linear(0f, 0f, 0.25f, 0f));

		// OffIdle
		clipOffIdle.SetCurve("", typeof(CanvasGroup), "m_Alpha", 			AnimationCurve.Linear(0f, 0f, 1f, 0f));
		clipOffIdle.SetCurve("", typeof(CanvasGroup), "m_Interactable", 	AnimationCurve.Linear(0f, 0f, 1f, 0f));
		clipOffIdle.SetCurve("", typeof(CanvasGroup), "m_BlocksRaycasts", 	AnimationCurve.Linear(0f, 0f, 1f, 0f));

		// Animation Events
		CreateAnimationEvent(clipStart, "OnStartupComplete", 	0.01666f);
		CreateAnimationEvent(clipOn, "TransitionComplete", 		0.48333f);
		CreateAnimationEvent(clipOff, "TransitionComplete", 	0.23333f);
		CreateAnimationEvent(clipOffIdle, "DisablePanel", 		0.01666f);

		// Assign Clips
		stateStart.motion = 	clipStart;
		stateOn.motion = 		clipOn;
		stateOnIdle.motion = 	clipOnIdle;
		stateOff.motion = 		clipOff;	
		stateOffIdle.motion =	clipOffIdle;
	}


	// Create new Animation Clip asset in directory
	static AnimationClip CreateAnimClip(string _path, string _acName, string _clipName, bool _loop)
	{
		AnimationClip newClip = new AnimationClip();
		string clipPath = _path + "ANM_" + _acName + "_" + _clipName + ".anim";

		if (_loop)
		{
			AnimationClipSettings settings = AnimationUtility.GetAnimationClipSettings(newClip);
			settings.loopTime = true;
			AnimationUtility.SetAnimationClipSettings(newClip, settings); 
		}

		// Only create asset if non exists
		if (AssetDatabase.LoadAssetAtPath(clipPath, typeof(AnimationClip)) == null)
			AssetDatabase.CreateAsset(newClip, clipPath);

		return newClip;
	}


	// Add Animation Event
	static void CreateAnimationEvent(AnimationClip _clip, string _event, float _timePosition)
	{
		AnimationEvent[] evts = new AnimationEvent[1];
		evts[0] = new AnimationEvent();
		evts[0].time = _timePosition;
		evts[0].functionName = _event;

		AnimationUtility.SetAnimationEvents(_clip, evts);
	}
}
