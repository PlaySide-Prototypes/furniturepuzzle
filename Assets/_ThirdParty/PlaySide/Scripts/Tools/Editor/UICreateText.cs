﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class UICreateText : MonoBehaviour
{
	[MenuItem("GameObject/PlaySide UI/Create Text", false, 0)]
	private static void CreateText()
	{
		GameObject selectedGO = Selection.activeGameObject;
		Transform selectedTrans = selectedGO.transform;

		// ROOT RectTransform
		GameObject panelGameObject = new GameObject("Txt");
		RectTransform rectTransRoot = panelGameObject.AddComponent(typeof(RectTransform)) as RectTransform;
		rectTransRoot.SetParent(selectedTrans, false);
		rectTransRoot.sizeDelta = new Vector2(300, 150);

		// Text
		Text txt = panelGameObject.GetComponent<Text>();
		if (txt == null)
			txt = panelGameObject.AddComponent(typeof(Text)) as Text;
		txt.text = "New Text";

        // Text
        Playside.UILocLabelText label = panelGameObject.GetComponent<Playside.UILocLabelText>();
		if (label == null)
			label = panelGameObject.AddComponent(typeof(Playside.UILocLabelText)) as Playside.UILocLabelText;

		Selection.activeGameObject = panelGameObject;
	}

	[MenuItem("GameObject/PlaySide UI/Create Text Mesh", false, 0)]
	private static void CreateTextMesh()
	{
		GameObject selectedGO = Selection.activeGameObject;
		Transform selectedTrans = selectedGO.transform;

		// ROOT RectTransform
		GameObject panelGameObject = new GameObject("Txt Mesh");
		RectTransform rectTransRoot = panelGameObject.AddComponent(typeof(RectTransform)) as RectTransform;
		rectTransRoot.SetParent(selectedTrans, false);
		rectTransRoot.sizeDelta = new Vector2(300, 150);

		// Text
		TMPro.TextMeshProUGUI txt = panelGameObject.GetComponent<TMPro.TextMeshProUGUI>();
		if (txt == null)
			txt = panelGameObject.AddComponent(typeof(TMPro.TextMeshProUGUI)) as TMPro.TextMeshProUGUI;
		txt.text = "New Text";

        // Text
        Playside.UILocLabelTextMesh label = panelGameObject.GetComponent<Playside.UILocLabelTextMesh>();
		if (label == null)
			label = panelGameObject.AddComponent(typeof(Playside.UILocLabelTextMesh)) as Playside.UILocLabelTextMesh;

		Selection.activeGameObject = panelGameObject;
	}
}
