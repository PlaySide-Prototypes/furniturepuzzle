﻿using System.IO;
using System;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class RebuildFBXPrefab : EditorWindow
{
	static EditorWindow thisWindow;

	static Vector2 windowDimensions = new Vector2(1350, 620);
	[MenuItem("PlaySide/Tools/Rebuild Prefab (FBX)", false, 0)]
	[MenuItem("Assets/PlaySide Tools/Rebuild Prefab (FBX)", false, 0)]
	public static void RebuildPrefabFromFBX()
	{
		thisWindow = EditorWindow.GetWindow(typeof(RebuildFBXPrefab));
		thisWindow.minSize = windowDimensions;
		thisWindow.maxSize = windowDimensions;
	}

	struct ChildInfo
	{
		public Transform trans;
		public int depth;
		public bool isAnimatorChild;
		public bool isAnimator;
		public bool isSkinnedMesh;
		public bool isMesh;
		public bool isNewTransform;
		public bool cousinExists;
		public string transformName;
		public string animRelPath;
		public string parentPath;
		public string fullPath;
	}

	struct SkinnedMeshPair
	{
		public SkinnedMeshRenderer srcSkinnedMesh;
		public SkinnedMeshRenderer dstSkinnedMesh;
		public Transform dstTransform;
		public string dstAnimRelPath;
		public string dstRootBonePath;
	}

	bool initialized = false;
	bool showNonAnimatorTransforms = false;
	bool showOnlyRenderers = false;
	bool reparentMaintainWorldPosition = true;
//	bool showOnlyBrokenItems = false;


	// Multiple animators
	int animatorSelected = 0;
	Animator[] destinationChildAnimators;
	string[] prefabAnimatorNames;


	// Source and Destination hierarchies
	ChildInfo[] srcChildInfos;
	ChildInfo[] dstChildInfos;
	SkinnedMeshPair[] skinnedMeshPairs;
	int infoIndex;

	GameObject srcFBX;
	GameObject dstPrefab;
	GameObject instantiatedPrefab;
	Transform prefabRoot;
	Transform fbxRoot;
	Transform animatorTransform;

	int prefabChildCount;
	int fbxChildCount;
	int highestChildCount;

	int prefabRendererCount;
	int fbxRendererCount;
	int highestRendererCount;

	string prefabAnimatorPath;
	bool isPrefabRootAnimator;

	// Renaming path
	bool awaitingTransformRename;
	bool awaitingPathRename;
	string renameTransformName;
	string renameNewParentPath;
	ChildInfo renameChildInfo;
	List<string> newTransformPaths;

	//Search
	string fbxSearchText;
	string prefabSearchText;
	bool fbxSearchNameOnly;
	bool prefabSearchNameOnly;

	// GUI
	Vector2 scrollPosPrefab;
	Vector2 scrollPosFBX;
	Rect leftInputAreaRect;
	Rect rightInputAreaRect;
	Rect leftTableAreaRect;
	Rect rightTableAreaRect;
	Rect leftTableBGRect;
	Rect rightTableBGRect;
	Rect bottomNavArea;
	Texture2D bgTexture; 

	Color transformNameColor;
	Color whiteColor = Color.white;
	Color lightBlueColor = new Color(.85f, .85f, 1f, 1f);

	// Pro Colors
	Color proBGColor = new Color(.22f, .22f, .22f, 1f);
	Color proTableBGColor = new Color(.3f, .3f, .3f, 1f);
	Color proTableBG2Color = new Color(.2f, .2f, .2f, 1f);
	Color proRedColor = new Color(0.45f, 0.05f, 0.05f, 1f);
	Color proLightRedColor = new Color(1.0f, 0.05f, 0.05f, 1f);

	// Personal Colors
	Color freeBGColor = new Color(.76f, .76f, .76f, 1f);
	Color freeTableBGColor = new Color(.6f, .6f, .6f, 1f);
	Color freeTableBG2Color = new Color(.5f, .5f, .5f, 1f);
	Color freeRedColor = new Color(0.5f, 0.33f, 0.33f, 1f);
	Color freeLightRedColor = new Color(1.0f, 0.33f, 0.33f, 1f);

	float column1Width = 300f;
	float column3Width = 285f;

	bool isPro = false;

	void OnEnable()
	{
		isPro = EditorGUIUtility.isProSkin;//Application.HasProLicense();
		bgTexture = new Texture2D(1, 1);
		bgTexture.SetPixel(0,0,whiteColor);
		bgTexture.Apply();

		bottomNavArea = new Rect(0, windowDimensions.y - 30f, windowDimensions.x, 30f);
		newTransformPaths = new List<string>();

		if(AssetDatabase.GetAssetPath(Selection.activeObject).Contains(".fbx"))
			srcFBX = Selection.activeObject as GameObject;
		else
			dstPrefab = Selection.activeObject as GameObject;

		RefreshDetails();
		initialized = true;
		awaitingTransformRename = awaitingPathRename = false;
		instantiatedPrefab = null;

		fbxSearchText = "";
		prefabSearchText = "";
		fbxSearchNameOnly = false;
		prefabSearchNameOnly = false;
	}


	void OnGUI()
	{
		if (initialized)
		{
			Event e = Event.current;
			if (e.keyCode == KeyCode.Return && renameChildInfo.trans != null)
			{
				if (awaitingPathRename)
					ReparentTransform();
				else if (awaitingTransformRename)
					RenameTransform();
			}

			GUILayout.BeginHorizontal(GUILayout.Height(50f));
			GUILayout.FlexibleSpace();
			GUILayout.Label("REBUILD FBX Prefab\n", EditorStyles.boldLabel);
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();

			// FBX and Prefab inputs
			DrawInputs();

			if (srcFBX != null && dstPrefab != null)
			{
				GUI.skin.box.normal.background = bgTexture;
				GUI.color = isPro ? proTableBGColor : freeTableBGColor; 
				GUI.Box(leftTableBGRect, GUIContent.none);
				GUI.color = isPro ? proTableBG2Color : freeTableBG2Color; 
				GUI.Box(rightTableBGRect, GUIContent.none);
				GUI.color = whiteColor; 

				// Padding below table column label text
				EditorGUILayout.LabelField("", GUILayout.Height(50f));

				// Table contents
				scrollPosPrefab = EditorGUILayout.BeginScrollView(scrollPosPrefab, false, false, GUILayout.Width(windowDimensions.x), GUILayout.Height(windowDimensions.y - 140f));//GUILayout.Height((float)(highestChildCount+1)*20f));

				// This Horizontal layout controls the height of the scrollable table area
				GUILayout.BeginHorizontal(GUILayout.Height((float)( (showOnlyRenderers ? highestRendererCount : highestChildCount)) * 22f));
				DrawHierarchies();
				GUILayout.EndHorizontal();

				EditorGUILayout.EndScrollView();
				EditorGUILayout.LabelField("", GUILayout.Height(80f));

				// Buttons
				GUI.color = isPro ? proBGColor : freeBGColor; 
				GUI.Box(bottomNavArea, GUIContent.none);
				GUI.color = whiteColor;
				GUILayout.BeginArea(new Rect(0, windowDimensions.y - 25f, windowDimensions.x, 30f));
				GUILayout.BeginHorizontal();
				GUILayout.Space(20f);

				showNonAnimatorTransforms = GUILayout.Toggle(showNonAnimatorTransforms, " Show Non-Animator Transforms");
				showOnlyRenderers = GUILayout.Toggle(showOnlyRenderers, " Show Only Renderers");
				reparentMaintainWorldPosition = GUILayout.Toggle(reparentMaintainWorldPosition, " Reparent Maintaining World Position");

				GUILayout.FlexibleSpace();

				if (GUILayout.Button("Fix Meshes and Skinning", GUILayout.Width(300f)))
					RebuildPrefab();

				GUILayout.Space(20f);
				//GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
				GUILayout.EndArea();
			}

			GUILayout.Space(20f);
		}
	}

	// ========================================================
	// DRAW GUI
	// ========================================================
	void RefreshDetails()
	{
		highestChildCount = 1;
		highestRendererCount = 1;

		if (dstPrefab && srcFBX)
		{
			// Prefab Animators
			FindChildAnimators();
			Animator animator = destinationChildAnimators[animatorSelected];
			animatorTransform = null;
			if (animator)
			{
				animatorTransform = animator.transform;

				if (animatorTransform == prefabRoot)
					prefabAnimatorPath = "";
				else
					prefabAnimatorPath = AnimationUtility.CalculateTransformPath(animatorTransform, prefabRoot)+"/";
			}

			isPrefabRootAnimator = (dstPrefab.GetComponent<Animator>() != null);

			// Child Count
			prefabChildCount = dstPrefab.GetComponentsInChildren<Transform>(true).Length;
			fbxChildCount = srcFBX.GetComponentsInChildren<Transform>(true).Length;
			highestChildCount = (fbxChildCount > prefabChildCount) ? fbxChildCount : prefabChildCount;

			// Renderer Count
			prefabRendererCount = dstPrefab.GetComponentsInChildren<Renderer>(true).Length;
			fbxRendererCount = srcFBX.GetComponentsInChildren<Renderer>(true).Length;
			highestRendererCount = (fbxRendererCount > prefabRendererCount) ? fbxRendererCount : prefabRendererCount;

			// FBX
			infoIndex = 0;
			fbxRoot = PrefabUtility.FindPrefabRoot(srcFBX).transform;
			srcChildInfos = new ChildInfo[srcFBX.GetComponentsInChildren<Transform>(true).Length-1];
			srcChildInfos = GetChildPaths(srcFBX.transform, srcChildInfos, fbxRoot, 1, true);

			// PREFAB
			infoIndex = 0;
			prefabRoot = PrefabUtility.FindPrefabRoot(dstPrefab).transform;
			dstChildInfos = new ChildInfo[dstPrefab.GetComponentsInChildren<Transform>(true).Length-1];
			dstChildInfos = GetChildPaths(dstPrefab.transform, dstChildInfos, prefabRoot, 1, false);

			FindSkinnedMeshRendererPairs();
		}

		// Top input rects
		leftInputAreaRect = new Rect(10, 30, 650, 350);
		rightInputAreaRect = new Rect(670, 30, 650, 350);

		// Table Rects
		leftTableBGRect = new Rect(10, 110, 650, windowDimensions.y-120f);
		rightTableBGRect = new Rect(670, 110, 650, windowDimensions.y-120f);
		leftTableAreaRect = new Rect(10, 20, 650, (float)( (showOnlyRenderers ? highestRendererCount : highestChildCount))*22f);
		rightTableAreaRect = new Rect(670, 20, 650, (float)( (showOnlyRenderers ? highestRendererCount : highestChildCount))*22f);

		this.Repaint();
	}


	// Draw input fields
	void DrawInputs()
	{
		// LEFT AREA
		//GUILayout.BeginHorizontal();
		GUILayout.BeginArea(leftInputAreaRect);
		GUILayout.BeginVertical();

		// Source FBX Object Field
		EditorGUI.BeginChangeCheck();
		srcFBX = (GameObject)EditorGUILayout.ObjectField("Source FBX", srcFBX, typeof(GameObject), false);
		if (EditorGUI.EndChangeCheck())
		{
			if (Path.GetExtension(AssetDatabase.GetAssetPath(srcFBX)) != ".fbx")
				srcFBX = null;
			RefreshDetails();
			// TODO: Dirty, clean this up so it doesn't need to be called twice here
			RefreshDetails();
		}
		GUILayout.Space(18f);

		// Searchbar FBX
		GUILayout.BeginHorizontal();
		{
			EditorGUI.BeginChangeCheck();
			fbxSearchText = GUILayout.TextField(fbxSearchText, GUI.skin.FindStyle("ToolbarSeachTextField"));
			if (EditorGUI.EndChangeCheck())
				scrollPosPrefab = new Vector2(scrollPosPrefab.x, 0);
			
			if (GUILayout.Button("", GUI.skin.FindStyle("ToolbarSeachCancelButton")))
			{
				// Remove focus if cleared
				fbxSearchText = "";
				GUI.FocusControl(null);
			}
			fbxSearchNameOnly = GUILayout.Toggle(fbxSearchNameOnly, " Search Name Only");
		}
		GUILayout.EndHorizontal();
		EditorGUILayout.LabelField("", GUILayout.Height(5));
		// >>>> Searchbar FBX end

		// Src FBX Table Titles
		GUILayout.BeginHorizontal();
		GUILayout.Label("FBX",				EditorStyles.boldLabel, 	GUILayout.Width(column1Width));
		GUILayout.Label("Relative Path", 	EditorStyles.boldLabel, 	GUILayout.Width(column3Width));
		GUI.color = lightBlueColor;
		if (GUILayout.Button(EditorGUIUtility.ObjectContent(null, typeof(Material)).image, GUILayout.Width(20f), GUILayout.Height(20f)))
			Debug.Log("Copy all");
		if (GUILayout.Button("+", GUILayout.Width(20f), GUILayout.Height(20f)))
			CopyAllMissingTransformsToPrefab();
		GUI.color = whiteColor;
		GUILayout.EndHorizontal();

		GUILayout.EndVertical();
		GUILayout.EndArea();
		//GUILayout.EndHorizontal();

		// Right Area
		GUILayout.BeginArea(rightInputAreaRect);
		GUILayout.BeginVertical();

		// Destination Prefab Object Field
		EditorGUI.BeginChangeCheck();
		dstPrefab = (GameObject)EditorGUILayout.ObjectField("Destination Prefab", dstPrefab, typeof(GameObject), false);
		if (EditorGUI.EndChangeCheck())
		{
			GameObject root = PrefabUtility.FindPrefabRoot(dstPrefab);
			if (root != dstPrefab)
				dstPrefab = null;
			RefreshDetails();
			RefreshDetails(); // TODO: Dirty, clean this up so it doesn't need to be called twice here
		}

		if (srcFBX != null && dstPrefab != null)
		{
			EditorGUI.BeginChangeCheck();
			animatorSelected = EditorGUILayout.Popup("Animator Hierarchy", animatorSelected, prefabAnimatorNames);
			if (EditorGUI.EndChangeCheck())
			{
				RefreshDetails();
			}
		}
		else
			GUILayout.Label("");

		// Searchbar PREFAB
		GUILayout.BeginHorizontal();
		{
			EditorGUI.BeginChangeCheck();
			prefabSearchText = GUILayout.TextField(prefabSearchText, GUI.skin.FindStyle("ToolbarSeachTextField"));
			if (EditorGUI.EndChangeCheck())
				scrollPosPrefab = new Vector2(scrollPosPrefab.x, 0);
			
			if (GUILayout.Button("", GUI.skin.FindStyle("ToolbarSeachCancelButton")))
			{
				// Remove focus if cleared
				prefabSearchText = "";
				GUI.FocusControl(null);
			}
			prefabSearchNameOnly = GUILayout.Toggle(prefabSearchNameOnly, " Search Name Only");
		}
		GUILayout.EndHorizontal();
		EditorGUILayout.LabelField("", GUILayout.Height(5));
		// >>>> Searchbar PREFAB end

		// Destination Prefab Table Titles
		GUILayout.BeginHorizontal();
		GUILayout.Label("Prefab", 			EditorStyles.boldLabel, 	GUILayout.Width(column1Width));
		GUILayout.Label("Relative Path", 	EditorStyles.boldLabel, 	GUILayout.Width(column3Width));
		GUILayout.Label(EditorGUIUtility.ObjectContent(null, typeof(Transform)).image,GUILayout.Width(20f), GUILayout.Height(20f));
		GUILayout.EndHorizontal();

		GUILayout.EndVertical();
		GUILayout.EndArea();
	}

	// Draw Transform hierarchy table
	void DrawHierarchies()
	{
		// BEGIN Detail Table
		// This Horizontal layout controls the height of the left and right table contents clipping
		GUILayout.BeginHorizontal(GUILayout.Width(1360), GUILayout.Height((float)( (showOnlyRenderers ? highestRendererCount : highestChildCount) + 2)*22f));

		// LEFT AREA
		GUILayout.BeginArea(leftTableAreaRect);
		GUILayout.BeginVertical();
		GUILayout.BeginVertical();
		if(!isPrefabRootAnimator)
			GUILayout.Label("", EditorStyles.label);

		// Draw FBX root icon and colour manually
		GUILayout.BeginHorizontal();
		GUILayout.Label(EditorGUIUtility.ObjectContent(null, typeof(Animator)).image, GUILayout.Width(20f), GUILayout.Height(20f));
		GUI.contentColor = Color.green;
		GUILayout.Label((srcFBX!=null)?srcFBX.transform.name:"Root", EditorStyles.label);
		GUI.contentColor = whiteColor;
		GUILayout.EndHorizontal();

		if (srcFBX && dstPrefab && srcChildInfos!=null)
			DrawChildHierarchy(srcChildInfos, true);
		GUILayout.EndVertical();
		GUILayout.EndVertical();
		GUILayout.EndArea();

		// RIGHT AREA
		GUILayout.BeginArea(rightTableAreaRect);
		GUILayout.BeginVertical();
		GUILayout.BeginVertical();

		// Draw Prefab root icon and colour manually IF it is also an Animator
		if (isPrefabRootAnimator)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label(EditorGUIUtility.ObjectContent(null, typeof(Animator)).image, GUILayout.Width(20f), GUILayout.Height(20f));
			GUI.contentColor = Color.green;
			GUILayout.Label("Root", EditorStyles.label);
			GUI.contentColor = whiteColor;
			GUILayout.EndHorizontal();
		}
		else
			GUILayout.Label("Root", EditorStyles.label);

		if (dstPrefab && srcFBX && dstChildInfos != null)
		{
			EditorGUI.BeginChangeCheck();
			DrawChildHierarchy(dstChildInfos, false);
			if (EditorGUI.EndChangeCheck())
			{
				RefreshDetails();
				RefreshDetails(); // TODO: Dirty, clean this up so it doesn't need to be called twice here
			}
		}
		GUILayout.EndVertical();
		GUILayout.EndVertical();
		GUILayout.EndArea();

		// END Detail Table
		GUILayout.EndHorizontal();
	}


	ChildInfo[] GetChildPaths(Transform _parent, ChildInfo[] _infos, Transform _root, int _depth, bool _isSource)
	{
		int childCount = _parent.childCount;

		// Store paths, transform, depth info to use when drawing GUI tables
		for (int i = 0; i < childCount; i++)
		{
			Transform child = _parent.GetChild(i);
			if (child != _parent)
			{
				string hierarchyPath = AnimationUtility.CalculateTransformPath(child, _root);
				string parentPath = AnimationUtility.CalculateTransformPath(_parent, _root);
				string animRelPath = parentPath + (parentPath == "" ? "" : "/") + child.name;

				if (prefabAnimatorPath != "")
					animRelPath = animRelPath.Replace(prefabAnimatorPath, "");

				// Flag transform created in this editing session
				foreach (string p in newTransformPaths)
				{
					if (p == hierarchyPath)
						_infos[infoIndex].isNewTransform = true;
				}

				_infos[infoIndex].transformName = child.name;
				_infos[infoIndex].fullPath = hierarchyPath;
				_infos[infoIndex].animRelPath = animRelPath;
				_infos[infoIndex].parentPath = parentPath;
				_infos[infoIndex].isAnimator = child.GetComponent<Animator>();
				_infos[infoIndex].isAnimatorChild = (hierarchyPath.Contains(prefabAnimatorPath) || _infos[infoIndex].isAnimator);
				_infos[infoIndex].isSkinnedMesh = child.GetComponent<SkinnedMeshRenderer>();
				_infos[infoIndex].isMesh = child.GetComponent<MeshRenderer>();
				_infos[infoIndex].trans = child;
				_infos[infoIndex].depth = _depth;

				// Check this last since i've lazily just used the information assigned above
				// Force empty paths to true
				if (!_isSource)
					_infos[infoIndex].cousinExists = true;
				else
					_infos[infoIndex].cousinExists = (_isSource) ? IsTransformInPrefab(_infos[infoIndex]) : false;

				// Debug.Log("T: "+child.name+"\nHierarchyPath: "+hierarchyPath+"\nParentPath: "+parentPath+"\nanimRelPath: "+_infos[infoIndex].animRelPath);

				infoIndex++;

				// Recurse for children, increment depth for children to get proper hierarchy indentation
				if (_parent.GetChild(i).childCount != 0)
					_infos = GetChildPaths(child, _infos, _root, _depth + 1, _isSource);
			}
		}
		return _infos;
	}

	void FindSkinnedMeshRendererPairs()
	{
		List<SkinnedMeshPair> pairList = new List<SkinnedMeshPair>();
		SkinnedMeshRenderer[] srcSMRs = srcFBX.GetComponentsInChildren<SkinnedMeshRenderer>(true);
		SkinnedMeshRenderer[] dstSMRs = dstPrefab.GetComponentsInChildren<SkinnedMeshRenderer>(true);


		for(int i=0; i<srcSMRs.Length; i++)
		{
			for(int k=0; k<dstSMRs.Length; k++)
			{
				
				string srcPath = AnimationUtility.CalculateTransformPath(srcSMRs[i].transform, srcFBX.transform);
				string dstPath = AnimationUtility.CalculateTransformPath(dstSMRs[k].transform, dstPrefab.transform.Find(prefabAnimatorPath));

				if(srcPath == dstPath)
				{
					SkinnedMeshPair pair = new SkinnedMeshPair();
					pair.srcSkinnedMesh = srcSMRs[i];
					pair.dstSkinnedMesh = dstSMRs[k];
					pair.dstAnimRelPath = dstPath;
					pair.dstTransform = dstSMRs[k].transform;
					pair.dstRootBonePath = AnimationUtility.CalculateTransformPath(srcSMRs[i].rootBone, srcFBX.transform);
					pairList.Add(pair);
					break;
				}
			}
		}

		skinnedMeshPairs = pairList.ToArray();
	}

	void FindChildAnimators()
	{
		destinationChildAnimators = dstPrefab.GetComponentsInChildren<Animator>(true);
		prefabAnimatorNames = new string[destinationChildAnimators.Length];

		for(int i=0; i<destinationChildAnimators.Length; i++)
		{
			prefabAnimatorNames[i] = destinationChildAnimators[i].transform.name;
		}
	}


	// Draw child details GUI
	void DrawChildHierarchy(ChildInfo[] _infos, bool _isSource)
	{
		for (int i = 0; i < _infos.Length; i++)
		{
			if (_infos[i].trans)
			{
				// SEARCH FILTERING
				string searchText = (_isSource) ? fbxSearchText : prefabSearchText;
				string searchableText = ((_isSource && fbxSearchNameOnly) || (!_isSource && prefabSearchNameOnly)) ? _infos[i].transformName: _infos[i].fullPath;
				
				if (!_infos[i].isAnimator && !string.IsNullOrEmpty(searchText) && !(searchableText.ToLower().Contains(searchText.ToLower())))
						continue;

				// Only draw renderers if toggled on
				if (showOnlyRenderers && !(_infos[i].isSkinnedMesh || _infos[i].isMesh) && !(_infos[i].trans == animatorTransform))
					continue;

				string controlName = ((_isSource) ? "src_" : "") + "path_"+ i.ToString();
				transformNameColor = whiteColor;
				Transform srcCompanionTransform = null;
				Transform dstCompanionTransform = null;
				bool isAddedTransform = false;
				bool transformMoved = false;

				// Find cousin transforms in FBX and Prefab
				srcCompanionTransform = srcFBX.transform.Find(_infos[i].animRelPath);
				dstCompanionTransform = dstPrefab.transform.Find(prefabAnimatorPath+_infos[i].animRelPath);

				// Check what color to draw table element as, and which elements should be draw in the element
				if (!_isSource)
				{
					// Check if transform in destination prefab has changed relative to FBX
					if (srcCompanionTransform != null && dstCompanionTransform != null)
					{
						transformMoved = ( srcCompanionTransform.localPosition != dstCompanionTransform.localPosition
							|| srcCompanionTransform.localRotation != dstCompanionTransform.localRotation
							|| srcCompanionTransform.localScale != dstCompanionTransform.localScale
						);
					}

					// Colorize Name (pro only)
					if (_infos[i].isAnimatorChild)
					{
						if (_infos[i].isAnimator)
							transformNameColor = Color.green; // Green = Animator
						else
							transformNameColor = whiteColor;
					}
					else
					{
						transformNameColor = Color.grey; // Grey = Absent from FBX
						isAddedTransform = true;
					}
				}

				// Skip this item if it's not part of the FBX
				if ((!showNonAnimatorTransforms && isAddedTransform))
					continue;

				// ------------------------------------------------------------
				// BEGIN Draw table item
				// ------------------------------------------------------------
				Rect cellRect = EditorGUILayout.BeginHorizontal();
				GUI.contentColor = transformNameColor;

				// Highlight missing transforms red
				if ( ((srcCompanionTransform == null && !_infos[i].isAnimator) && _infos[i].isAnimatorChild ) || (dstCompanionTransform == null && _isSource))
				{
					GUI.color = isPro ? proRedColor : freeRedColor;
					GUI.Box(cellRect, GUIContent.none);
					GUI.color = whiteColor;
				}

				// Draw component type icons
				if (_isSource || _infos[i].isAnimatorChild)
				{
					if(_infos[i].isAnimator)
						GUILayout.Label(EditorGUIUtility.ObjectContent(null, typeof(Animator)).image, GUILayout.Width(20f), GUILayout.Height(20f));
					else if(_infos[i].isSkinnedMesh)
						GUILayout.Label(EditorGUIUtility.ObjectContent(null, typeof(SkinnedMeshRenderer)).image, GUILayout.Width(20f), GUILayout.Height(20f));
					else if(_infos[i].isMesh)
						GUILayout.Label(EditorGUIUtility.ObjectContent(null, typeof(MeshRenderer)).image, GUILayout.Width(20f), GUILayout.Height(20f));
					else
						GUILayout.Label("", GUILayout.Width(20f), GUILayout.Height(20f));
				}

				// Indent name based on hierarchy depth
				GUILayout.Space((_infos[i].depth-1) * 10f);

				// Toggle visibility of Non-FBX transforms in prefab
				if ( !(!showNonAnimatorTransforms && isAddedTransform))
				{
					if (_isSource)
						GUILayout.Label(_infos[i].trans.name, EditorStyles.label);
					else
					{
						// Begin check for editing transform NAME
						if(_infos[i].isAnimatorChild)
						{
							EditorGUI.BeginChangeCheck();
							_infos[i].transformName = EditorGUILayout.TextField(_infos[i].transformName, GUILayout.Width(150));
							if (EditorGUI.EndChangeCheck())
							{
								renameChildInfo = _infos[i];
								renameTransformName = _infos[i].transformName;
								awaitingTransformRename = true;
							}
						}
						else
							GUILayout.Label(_infos[i].trans.name, EditorStyles.label, GUILayout.Width(150));
					}

					GUI.contentColor = whiteColor;
					GUILayout.FlexibleSpace();

					// FBX drawing
					if (_isSource)
					{
						EditorGUILayout.TextField(_infos[i].parentPath, GUILayout.MaxWidth(300f));
						bool isInPrefab = _infos[i].cousinExists;////IsTransformInPrefab(_infos[i]);

						// Draw Button for: Reassign Material from FBX if mismatched
						if ((_infos[i].isSkinnedMesh || _infos[i].isMesh) && isInPrefab && !IsMaterialsMatch(_infos[i]))
						{
							if (GUILayout.Button(EditorGUIUtility.ObjectContent(null, typeof(Material)).image, GUILayout.Width(20f), GUILayout.Height(20f)))
								ReassignMaterials(_infos[i].animRelPath);
						}
						else
							GUILayout.Label("", GUILayout.Width(20f), GUILayout.Height(20f));

						// Draw Button for: Add missing FBX Transform, ignore Animator root transform
						if (!_infos[i].isAnimator)
						{
							if (!isInPrefab)
							{
								if (GUILayout.Button("+", GUILayout.Width(20f)))
									CopySingleMissingTransformToPrefab(_infos[i].trans, _infos[i].fullPath);
							}
							else
								GUILayout.Label("", GUILayout.Width(20f), GUILayout.Height(20f));
						}
						else
							GUILayout.Label("", GUILayout.Width(20f), GUILayout.Height(20f));
					}
					// Destination Prefab drawing
					else if (_infos[i].isAnimatorChild)
					{
						// Parent Path
						if (_infos[i].isAnimator)
							EditorGUILayout.LabelField("", GUILayout.Width(300f));
						else
						{
							EditorGUI.BeginChangeCheck();
							GUI.SetNextControlName(controlName);
							_infos[i].parentPath = EditorGUILayout.TextField(_infos[i].parentPath, GUILayout.MaxWidth(300f));
							if (EditorGUI.EndChangeCheck())
							{
								renameChildInfo = _infos[i];
								renameNewParentPath = _infos[i].parentPath;
								awaitingPathRename = true;
							}
						}

						// Begin check for editing transform PATH
						//EditorGUILayout.Toggle(transformMoved, GUILayout.Width(20f));
						EditorGUILayout.LabelField(transformMoved ? " * " : "   ", GUILayout.Width(20f));

						// Copy Anim Relative Path to clipboard
						if (!_infos[i].isAnimator)
						{
							if (GUILayout.Button("C", GUILayout.Width(20f)))
							{
								EditorGUIUtility.systemCopyBuffer = _infos[i].parentPath + "/" + _infos[i].transformName;
								EditorGUI.FocusTextInControl(controlName);
							}
							GUI.contentColor = whiteColor;
						}
						else
							GUILayout.Label("", GUILayout.Width(20f), GUILayout.Height(20f));

						// Delete Transform button, ignore Animator root transform
						if (!_infos[i].isAnimator)
						{
							GUI.backgroundColor = (isPro) ? proLightRedColor : freeLightRedColor;
							if (GUILayout.Button("X", GUILayout.Width(20f)))
								DeleteTransform(_infos[i]);
							GUI.backgroundColor = whiteColor;
							GUI.contentColor = whiteColor;
						}
						else
							GUILayout.Label("", GUILayout.Width(20f), GUILayout.Height(20f));
					}
				}
				GUI.contentColor = whiteColor;

				GUILayout.Space(10f);
				EditorGUILayout.EndHorizontal();
			}
		}
	}


	// ========================================================
	// MODIFY PREFAB
	// ========================================================

	// Alter or create new parent for last edited child transform, called when user hits 'Return'
	void ReparentTransform()
	{
		// Get current path since childinfo path may have already changed
		string currentParentPath = AnimationUtility.CalculateTransformPath(renameChildInfo.trans.parent, dstPrefab.transform);
		string targetParentPath = renameNewParentPath.Replace("/"+renameChildInfo.transformName, "");
		CreateInstance();

		// Reparent if path has changed
		if(currentParentPath != targetParentPath)
			Reparent(instantiatedPrefab.transform.Find(renameChildInfo.fullPath), targetParentPath);

		ClearInstances();
		RefreshDetails();

		renameChildInfo.trans = null;
		awaitingPathRename = false;
	}


	// Copy all missing transforms in FBX over to PREFAB
	void CopyAllMissingTransformsToPrefab()
	{
		// Create scene copy so we can manipulate transform parents
		CreateInstance();

		// Copy using scene instanced prefab
		for(int i=0; i< srcChildInfos.Length; i++)
		{
			if (!srcChildInfos[i].cousinExists)
				CopyFBXTransformToPrefab(srcChildInfos[i].trans, srcChildInfos[i].fullPath);
		}

		ClearInstances();
		RefreshDetails();
		RebuildPrefab();
	}

	// Copy single target missing transform in FBX over to PREFAB
	void CopySingleMissingTransformToPrefab(Transform _transform, string _path)
	{
		// Create scene copy so we can manipulate transform parents
		CreateInstance();

		// Copy using scene instanced prefab
		CopyFBXTransformToPrefab(_transform, _path);

		ClearInstances();
		RefreshDetails();
		RebuildPrefab();
	}


	void CopyFBXTransformToPrefab(Transform _transform, string _path)
	{
		// Clone FBX transform and move to prefab hierarchy
		GameObject go =  Instantiate(_transform.gameObject);
		go.name = _transform.name;

		string parentPath = prefabAnimatorPath + _path;
		parentPath = parentPath.Replace("/"+_transform.name, "");

		Reparent(go.transform, parentPath);
	}


	// Using scene instance of prefab, reparent transform to target parent path (create hierarchy if none exist)
	// Maintains world position
	void Reparent(Transform _child, string _parentPath)
	{
		Transform child = _child; // Target child inside instantiated prefab
		Transform newParent = instantiatedPrefab.transform.Find(_parentPath); // Target parent inside instantiated prefab

		if (child)
		{
			// Check if parent exists in prefab, if not then create hierarchy to match _parentPath
			if (newParent)
				child.SetParent(newParent, reparentMaintainWorldPosition);
			else
			{
				string[] separators = {"/"};
				string[] splitPath = _parentPath.Split(separators, StringSplitOptions.RemoveEmptyEntries);
				string newParentPath = "";
				Transform prevNewTransform = instantiatedPrefab.transform;
				Transform newTransform;

				// Iterate through each transform in _parentPath, create any missing transforms
				for(int i=0; i<splitPath.Length; i++)
				{
					// Don't add a slash for first entry
					newParentPath += ((i>0)?"/":"") + splitPath[i];
					newTransform = instantiatedPrefab.transform.Find(newParentPath);

					// Create new transform if none exist, add to list of user created transforms
					if (newTransform == null)
					{
						GameObject newGO = new GameObject();
						newGO.name = splitPath[i];
						newGO.transform.SetParent(prevNewTransform, false);

						prevNewTransform = newGO.transform;
						child.SetParent(prevNewTransform, reparentMaintainWorldPosition);

						newTransformPaths.Add(newParentPath);
					}
					else
						prevNewTransform = newTransform;
				}
			}
		}
	}


	// Rename last edited child transform, called when user hits 'Return'
	void RenameTransform()
	{
		renameChildInfo.transformName = renameChildInfo.trans.name = renameTransformName;
		RefreshDetails();
		RefreshDetails();
		renameChildInfo.trans = null;
		awaitingTransformRename = false;
	}

	// Repair and Rebuild Prefab from FBX
	void RebuildPrefab()
	{
		FixTransforms();
		FixMeshRenderers();
		FixSkinnedMeshes();
		ReassignSkinWeights();
		AssignRootBones();
		ClearInstances();
	}


	// Reassign any missing Meshes from MeshRenderers
	void FixMeshRenderers()
	{
		foreach (ChildInfo c in srcChildInfos)
		{
			foreach(ChildInfo cDest in dstChildInfos)
			{
				if (c.isMesh && cDest.isMesh && c.trans != null && c.animRelPath == cDest.animRelPath)
				{
					MeshFilter srcMeshFilter = c.trans.GetComponent<MeshFilter>();
					MeshFilter dstMeshFilter = cDest.trans.GetComponent<MeshFilter>();
					if(dstMeshFilter.sharedMesh != srcMeshFilter.sharedMesh)
						dstMeshFilter.sharedMesh = srcMeshFilter.sharedMesh;
				}
			}
		}
	}


	// Reassign any missing Meshes from SkinnedMeshRenderers
	void FixSkinnedMeshes()
	{
		foreach (ChildInfo c in srcChildInfos)
		{
			foreach(ChildInfo cDest in dstChildInfos)
			{
				if (c.isSkinnedMesh && cDest.isSkinnedMesh && c.trans != null && c.animRelPath == cDest.animRelPath)
				{
					SkinnedMeshRenderer srcSkinnedMesh = c.trans.GetComponent<SkinnedMeshRenderer>();
					SkinnedMeshRenderer dstSkinnedMesh = cDest.trans.GetComponent<SkinnedMeshRenderer>();
					if(dstSkinnedMesh.sharedMesh != srcSkinnedMesh.sharedMesh)
						dstSkinnedMesh.sharedMesh = srcSkinnedMesh.sharedMesh;
				}
			}
		}
	}


	bool IsMaterialsMatch(ChildInfo _info)
	{
		Renderer srcRenderer = _info.trans.GetComponent<Renderer>();
		Renderer dstRenderer = null;
		Transform dstTrans = dstPrefab.transform.Find(prefabAnimatorPath+_info.animRelPath);

		if (dstTrans)
			dstRenderer = dstTrans.GetComponent<Renderer>();
		else
			return false;
		// Check if one has more materials than the other
		if (srcRenderer.sharedMaterials.Length != dstRenderer.sharedMaterials.Length)
			return false;

		// Check if any materials are out of order or missing
		for (int i=0; i<srcRenderer.sharedMaterials.Length; i++)
		{
			if (srcRenderer.sharedMaterials[i] != dstRenderer.sharedMaterials[i])
				return false;
		}

		return true;
	}


	// Copy all materials from source FBX to destination Prefab
	void ReassignMaterials(string _animRelativePath)
	{
		Renderer srcRenderer = srcFBX.transform.Find(_animRelativePath).GetComponent<Renderer>();
		Renderer dstRenderer = dstPrefab.transform.Find(prefabAnimatorPath+_animRelativePath).GetComponent<Renderer>();

		dstRenderer.sharedMaterials = srcRenderer.sharedMaterials;
	}


	bool IsTransformInPrefab(ChildInfo _info)
	{
		return (dstPrefab.transform.Find(prefabAnimatorPath+_info.animRelPath) != null);
	}


	// Matches transform pos/rot/scale to source FBX
	void FixTransforms()
	{
		foreach (ChildInfo c in srcChildInfos)
		{
			foreach(ChildInfo cDest in dstChildInfos)
			{
				if (c.trans != null && c.animRelPath == cDest.animRelPath)
				{
					cDest.trans.localPosition = c.trans.localPosition;
					cDest.trans.localRotation = c.trans.localRotation;
					cDest.trans.localScale = c.trans.localScale;
				}
			}
		}
	}


	// Transfers influenced bones from Source FBX
	void ReassignSkinWeights()
	{
		if (skinnedMeshPairs == null)
			return;

		for (int i=0; i<skinnedMeshPairs.Length; i++)
		{
			if (skinnedMeshPairs[i].srcSkinnedMesh == null || skinnedMeshPairs[i].dstSkinnedMesh == null)
				return;
			skinnedMeshPairs[i].dstSkinnedMesh.bones = TransferBoneList(skinnedMeshPairs[i].srcSkinnedMesh, skinnedMeshPairs[i].dstSkinnedMesh); //destBones;
		}
	}

	// Assigns source relative rootbone to skinnedmeshes
	void AssignRootBones()
	{
		for (int i=0; i<skinnedMeshPairs.Length; i++)
		{
			if (skinnedMeshPairs[i].srcSkinnedMesh == null || skinnedMeshPairs[i].dstSkinnedMesh == null)
				return;
			skinnedMeshPairs[i].dstSkinnedMesh.rootBone = dstPrefab.transform.Find(prefabAnimatorPath+skinnedMeshPairs[i].dstRootBonePath);
		}
	}



	// Transfer bone list from Source to Destination skinned mesh
	Transform[] TransferBoneList(SkinnedMeshRenderer _srcSkinnedMesh, SkinnedMeshRenderer _dstSkinnedMesh)
	{
		int numSrcBones = _srcSkinnedMesh.bones.Length;
		Transform[] destBones = new Transform[numSrcBones];

		for (int i=0; i < destBones.Length; i++)
		{
			string srcBonePath = AnimationUtility.CalculateTransformPath(_srcSkinnedMesh.bones[i], srcFBX.transform);
			destBones[i] = dstPrefab.transform.Find(prefabAnimatorPath+srcBonePath);
		}

		return destBones;
	}


	void DeleteTransform(ChildInfo _info)
	{
		if (EditorUtility.DisplayDialog("Delete Transform?",
			"Are you sure you want to delete\n\n" + _info.transformName +"  (Children: "+_info.trans.GetComponentsInChildren<Transform>(true).Length+")",
			"Delete", "Cancel"))
		{
			// Create scene copy so we can manipulate transform parents
			CreateInstance();
			Transform target = instantiatedPrefab.transform.Find(_info.fullPath);
			Undo.DestroyObjectImmediate(target.gameObject);
			ClearInstances();

			RefreshDetails();
		}
	}


	// Create temporary instance of Destination prefab
	void CreateInstance()
	{
		if (instantiatedPrefab == null)
		{
			instantiatedPrefab = Instantiate(dstPrefab);
			instantiatedPrefab = PrefabUtility.ConnectGameObjectToPrefab(instantiatedPrefab, dstPrefab);
		}
	}


	// Clear temporary instance of Destination prefab
	void ClearInstances()
	{
		if (instantiatedPrefab)
		{
			PrefabUtility.ReplacePrefab(instantiatedPrefab, PrefabUtility.GetCorrespondingObjectFromSource(instantiatedPrefab), ReplacePrefabOptions.ConnectToPrefab);
			DestroyImmediate(instantiatedPrefab);
			instantiatedPrefab = null;
		}
	}
}
