﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UILayoutGroupFitter : MonoBehaviour
{

	[MenuItem ("CONTEXT/HorizontalOrVerticalLayoutGroup/Layout Preferred Fit")]
	static void PreferredFit (MenuCommand command) {
		HorizontalOrVerticalLayoutGroup layoutGroup = (HorizontalOrVerticalLayoutGroup)command.context;

		layoutGroup.childControlWidth = true;
		layoutGroup.childControlHeight = true;
		layoutGroup.childForceExpandWidth = false;
		layoutGroup.childForceExpandHeight = false;

		ContentSizeFitter csf = layoutGroup.transform.GetComponent<ContentSizeFitter>();
		if (csf == null)
			csf = layoutGroup.gameObject.AddComponent(typeof(ContentSizeFitter)) as ContentSizeFitter;
		csf.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
		csf.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
	}

}
