﻿using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using System.Collections.Generic;

public class RepathAnimationClips : EditorWindow
{
	static EditorWindow thisWindow;

	static Vector2 windowDimensions = new Vector2(800, 600);
	[MenuItem("Assets/PlaySide Tools/Repath Animation Clips", false, 0)]//, false, 1)]
	public static void RebuildPrefabFromFBX()
	{
		thisWindow = EditorWindow.GetWindow(typeof(RepathAnimationClips));
		thisWindow.minSize = windowDimensions;
		thisWindow.maxSize = windowDimensions;
	}

	struct KeyProperty
	{
		public string transformName;
		public string propertyName;
		public string path;
		public string newPath;
		public System.Type type;
		public int occuranceCount;
		public bool modified;
	}

	struct ClipInfo
	{
		public AnimationClip clip;
		public string assetPath;
	}

	bool initialized = false;
	bool awaitingPathRename;

	AnimatorController 	animatorController;
	AnimationClip[] clips;


	// Renaming path
	string currentRenamedPath;
	int renameIndex;

	// Clip data
//	KeyProperty[] allKeyProperties;
	KeyProperty[] keyProperties;
	ClipInfo[] clipInfos;
	int propertyIndex;

	// GUI
	Vector2 scrollPos;
	Rect leftInputAreaRect;
	Rect rightInputAreaRect;
//	Rect leftTableAreaRect;
//	Rect rightTableAreaRect;
	Rect leftTableBGRect;
	Rect rightTableBGRect;
	Rect bottomNavArea;
	Texture2D bgTexture;

	Color transformNameColor;
//	Color defaultBGColor = new Color(.21f, .21f, .21f, 1f);
	Color darkGrey = new Color(.2f, .2f, .2f, 1f);
	Color lightGrey = new Color(.3f, .3f, .3f, 1f);
	Color offWhite = new Color(.5f, .5f, .5f, 1f);
//	Color lightBlue = new Color(.7f, .8f, 1f, 1f);
	Color lightBlueBG = new Color(.2f, .25f, .35f, 1f);


	void OnEnable()
	{
		animatorController = Selection.activeObject as AnimatorController;

		bgTexture = new Texture2D(1, 1);
		bgTexture.SetPixel(0,0,Color.white);
		bgTexture.Apply();

		bottomNavArea = new Rect(0, windowDimensions.y - 30f, windowDimensions.x, 30f);

		if (AssetDatabase.GetAssetPath(Selection.activeObject).Contains(".controller"))
		{
			animatorController = Selection.activeObject as AnimatorController;
			if(animatorController)
				RefreshDetails();
		}
		awaitingPathRename = false;
		initialized = true;
	}


	void OnGUI()
	{
		if (initialized)
		{
			Event e = Event.current;
			if (e.keyCode == KeyCode.Return && animatorController != null)
			{
				if (awaitingPathRename)
					RenamePath();
			}

			GUI.skin.box.normal.background = bgTexture;
			GUI.color = lightGrey; 
			GUI.Box(leftTableBGRect, GUIContent.none);
			GUI.color = darkGrey; 
			GUI.Box(rightTableBGRect, GUIContent.none);
			GUI.color = Color.white; 

			// Title
			GUILayout.BeginHorizontal(GUILayout.Height(30));
				GUILayout.FlexibleSpace();
				GUILayout.Label("REPATH ANIMATION CLIPS", EditorStyles.boldLabel);
				GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();

			// Animator Controller
			EditorGUI.BeginChangeCheck();
			animatorController = (AnimatorController)EditorGUILayout.ObjectField("Animator Controller:", animatorController, typeof(AnimatorController), true);
			if (EditorGUI.EndChangeCheck())
			{
				if(animatorController)
					RefreshDetails();
			}
			
			EditorGUILayout.LabelField("", GUILayout.Height(5));

			GUILayout.BeginHorizontal();
			GUILayout.Label("Properties\n", EditorStyles.boldLabel, GUILayout.Width(windowDimensions.x*0.25f));
			GUILayout.Label("Path\n", EditorStyles.boldLabel, GUILayout.Width(windowDimensions.x*0.75f));
			GUILayout.EndHorizontal();


			if (animatorController == null)
				return;
			else
			{
				// Table contents
				scrollPos = EditorGUILayout.BeginScrollView(scrollPos, false, false, GUILayout.Width(windowDimensions.x), GUILayout.Height(windowDimensions.y - 120f));//GUILayout.Height((float)(highestChildCount+1)*20f));
				DrawAnimPropertyList();
				EditorGUILayout.EndScrollView();
			}

			// Buttons
			GUILayout.BeginArea(new Rect(0, windowDimensions.y - 25f, windowDimensions.x, 30f));

			GUI.color = lightBlueBG; 
			GUI.Box(bottomNavArea, GUIContent.none);
			GUI.color = Color.white;

			GUILayout.BeginHorizontal();
				GUILayout.Space(20f);
				GUILayout.FlexibleSpace();
					if (GUILayout.Button("Repath", GUILayout.Width(300f)))
						RepathClips();
				GUILayout.FlexibleSpace();
				GUILayout.Space(20f);
			GUILayout.EndHorizontal();

			GUILayout.EndArea();
		}
	}


	// Draw propreties and their paths
	void DrawAnimPropertyList()
	{
		GUILayout.BeginHorizontal();
		GUILayout.BeginVertical();

		for(int i=0; i<keyProperties.Length; i++)
		{
			// OLD Property path
			GUILayout.BeginVertical();
			GUILayout.BeginHorizontal();
			GUILayout.Space(5);

			if (GUILayout.Button("X", GUILayout.Width(20)))
			{
				DeleteKeyProperty(i);
				return;
			}

			// Property name
			if(keyProperties[i].modified)
				GUI.color = Color.yellow;
			GUILayout.Label(((keyProperties[i].occuranceCount>0) ? "("+keyProperties[i].occuranceCount.ToString()+") " : "   ") + keyProperties[i].propertyName, EditorStyles.boldLabel, GUILayout.Width(windowDimensions.x*0.25f - 20));
			GUI.color = Color.white;

			// New Path
			EditorGUI.BeginChangeCheck();
			keyProperties[i].newPath = GUILayout.TextField(keyProperties[i].newPath, GUILayout.Width(windowDimensions.x*0.7f));
			if (EditorGUI.EndChangeCheck())
			{
				currentRenamedPath = keyProperties[i].newPath;
				renameIndex = i;
				awaitingPathRename = true;
			}

			GUILayout.Space(10);
			GUILayout.EndHorizontal();


			// NEW Property path
			GUILayout.BeginHorizontal();
			GUILayout.Space(10);

			// Empty spacer below name
			GUILayout.Label(" ", GUILayout.Width(windowDimensions.x*0.25f));

			// OLD Name
			GUI.color = offWhite;
			GUILayout.Label(keyProperties[i].path, GUILayout.Width(windowDimensions.x*0.7f));
			GUI.color = Color.white;

			GUILayout.Space(10);
			GUILayout.EndHorizontal();
			GUILayout.EndVertical();
			GUILayout.Label("", GUILayout.Height(5));
		}

		GUILayout.EndVertical();
		GUILayout.EndHorizontal();
	}


	// Retrive curve data from all clips in animator controller
	void RefreshDetails()
	{
//		List<KeyProperty> tmpAllKeyProperties = new List<KeyProperty>();
		List<KeyProperty> tmpPropreties = new List<KeyProperty>();
		List<ClipInfo> tmpClips = new List<ClipInfo>();

		// Get Clips
		foreach (AnimatorControllerLayer layer in animatorController.layers)
		{
			ChildAnimatorState[] rootStateMachine = layer.stateMachine.states;
			foreach (ChildAnimatorState s in rootStateMachine)
			{
				if (s.state != null && s.state.motion != null)
				{
					ClipInfo info = new ClipInfo();

					info.assetPath = AssetDatabase.GetAssetPath(s.state.motion);
					info.clip = AssetDatabase.LoadAssetAtPath(info.assetPath, typeof(AnimationClip)) as AnimationClip;
//					info.propertyPaths = "";

					if(!tmpClips.Contains(info))
						tmpClips.Add(info);
				}
			}
		}
		clipInfos = tmpClips.ToArray();


		// Get Key Properties
		foreach (ClipInfo c in tmpClips)
		{
			if (c.clip != null)
			{
				KeyProperty kp = new KeyProperty();
				EditorCurveBinding[] bindings =  AnimationUtility.GetCurveBindings(c.clip); 

				foreach (EditorCurveBinding b in bindings)
				{
					bool duplicateBinding = false;
					kp.propertyName = b.propertyName;
					kp.path = b.path;
					kp.newPath = b.path;
					kp.occuranceCount = 0;
					kp.type = b.type;

					// Check for duplicate propreties between clips
					for(int i=0; i<tmpPropreties.Count; i++)
					{
						if (tmpPropreties[i].type == kp.type && tmpPropreties[i].propertyName == kp.propertyName && tmpPropreties[i].path == kp.path)
						{
							KeyProperty v = tmpPropreties[i];
							v.occuranceCount++;
							tmpPropreties[i] = v;
							duplicateBinding = true;
							break;
						}
					}

					if(!duplicateBinding)
						tmpPropreties.Add(kp);

//					tmpAllKeyProperties.Add(kp);
				}
			}
		}

		keyProperties = tmpPropreties.ToArray();
//		allKeyProperties = tmpAllKeyProperties.ToArray();

		// Table Rects
		leftTableBGRect = new Rect(0, 90, windowDimensions.x*0.25f, windowDimensions.y-120f);
		rightTableBGRect = new Rect(windowDimensions.x*0.25f, 90, windowDimensions.x*0.75f, windowDimensions.y-120f);
//		leftTableAreaRect = new Rect(15, 0, 650, (float)(keyProperties.Length + 3)*22f);
//		rightTableAreaRect = new Rect(670, 0, 650, (float)(keyProperties.Length + 3)*22f);
	}


	// Remove target curve
	void DeleteKeyProperty(int _index)
	{
		string propertyName = keyProperties[_index].propertyName;
		string[] componentExtensions = new string[]{".x", ".y", ".z", ".w"};

		foreach(string s in componentExtensions)
		{
			if (propertyName.Substring(propertyName.Length - 2) == s)
			{
				Debug.Log("Extension " + s + " found in " + propertyName);
				propertyName = propertyName.Remove(propertyName.Length - 2, 2);
			}
		}

		foreach (ClipInfo c in clipInfos)
		{
			if (c.clip != null)
				c.clip.SetCurve(keyProperties[_index].path, keyProperties[_index].type, propertyName, null);
		}

		RefreshDetails();
	}


	void RenamePath()
	{
		// Remove any slashes on the end of paths
		if (currentRenamedPath.Substring(Mathf.Clamp(currentRenamedPath.Length - 1, 0, currentRenamedPath.Length)) == "/")
			currentRenamedPath = currentRenamedPath.TrimEnd("/".ToCharArray());
		
		keyProperties[renameIndex].newPath = currentRenamedPath;
		keyProperties[renameIndex].modified = keyProperties[renameIndex].path != keyProperties[renameIndex].newPath;
		awaitingPathRename = false;
	}


	void RepathClips()
	{
		// Get Key Properties
		foreach (ClipInfo c in clipInfos)
		{
			if (c.clip != null)
			{
				EditorCurveBinding[] bindings =  AnimationUtility.GetCurveBindings(c.clip); 

				for(int i=0; i<keyProperties.Length; i++)
				{
					for(int b=0; b<bindings.Length; b++)
					{
						AnimationCurve curve = AnimationUtility.GetEditorCurve(c.clip, bindings[b]);
						bool bindingRenamed = false;
						int renamedPropertyIndex = -1;

						// Check if property already exists
						if (keyProperties[i].propertyName == bindings[b].propertyName && keyProperties[i].newPath != bindings[b].path && keyProperties[i].path == bindings[b].path && keyProperties[i].type == bindings[b].type)
						{
							bindingRenamed = true;
							renamedPropertyIndex = i;
						}

						// TODO check if property has multiple comonents and repath them all

						// Add new property if renamed and missing
						if(bindingRenamed && renamedPropertyIndex > -1)
							RepathCurve(c.clip, keyProperties[renamedPropertyIndex].type,  keyProperties[renamedPropertyIndex].path, keyProperties[renamedPropertyIndex].newPath, keyProperties[renamedPropertyIndex].propertyName, curve);
					}
				}
			}
		}
		RefreshDetails();
	}

	// Move a clip c
	void RepathCurve(AnimationClip _clip, System.Type _type,  string _currentPath, string _newPath, string _propertyName, AnimationCurve _curve, int _componentCount=0)
	{
		Debug.Log(_clip.name + ": Adding " + _type.ToString() +" - "+ _propertyName +": "+ _currentPath +" >> "+_newPath+"");
		
		// Add new curve
		_clip.SetCurve(_newPath, _type, _propertyName, _curve);

		// Remove old curve
		_clip.SetCurve(_currentPath, _type, _propertyName, null);
	}
}
