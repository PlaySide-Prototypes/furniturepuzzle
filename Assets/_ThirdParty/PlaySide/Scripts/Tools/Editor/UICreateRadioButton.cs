﻿// REFERENCE HERE: https://docs.unity3d.com/ScriptReference/Animations.AnimatorController.html

using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using UnityEditor.Animations;
using System.Collections;

public class UICreateRadioButton : MonoBehaviour
{
	[MenuItem("GameObject/PlaySide UI/Create Radio Button", false, 0)]
	private static void CreateRadioButton()
	{
		GameObject selectedGO = Selection.activeGameObject;
		Transform selectedTrans = selectedGO.transform;

		// ----------------------
		// ROOT RectTransform
		// ----------------------
		GameObject panelGameObject = new GameObject("Btn Radio");
		RectTransform rectTransRoot = panelGameObject.AddComponent(typeof(RectTransform)) as RectTransform;
		rectTransRoot.SetParent(selectedTrans, false);
		rectTransRoot.sizeDelta = new Vector2(300, 150);


		Animator ac = panelGameObject.GetComponent<Animator>();
		if(ac == null)
			ac = panelGameObject.AddComponent(typeof(Animator)) as Animator;

		if(ac != null)
		{
			string[] controllerPaths = AssetDatabase.FindAssets("AC_Btn_Radio_Generic");
			if (controllerPaths.Length > 0)
			{
				string pathBtnController = AssetDatabase.GUIDToAssetPath(controllerPaths[0]);
				AnimatorController controller = AssetDatabase.LoadAssetAtPath(pathBtnController, typeof(AnimatorController)) as AnimatorController; 
				UnityEditor.Animations.AnimatorController.SetAnimatorController(ac, controller);
			}
		}

		// _---------------------
		// Button Image
		// _---------------------
		Image imgButton = panelGameObject.GetComponent<Image>();
		if (imgButton == null)
			imgButton  = panelGameObject.AddComponent(typeof(Image)) as Image;
		imgButton.color = new Color(0, 0, 0, 0);


		// _---------------------
		// Button
		// _---------------------
		Button btn = panelGameObject.GetComponent<Button>();
		if (btn == null)
			btn  = panelGameObject.AddComponent(typeof(Button)) as Button;
		btn.transition = Selectable.Transition.Animation;


		// _---------------------
		// Content
		// _---------------------
		GameObject goContent = null;
		Transform transContent =  rectTransRoot.Find("Content");

		if (transContent == null)
		{
			goContent = new GameObject("Content");
			transContent = goContent.transform;
		}
		else
			goContent = transContent.gameObject;
		
		RectTransform rectTransContent = transContent.GetComponent<RectTransform>();

		if (rectTransContent == null)
		{
			rectTransContent = goContent.AddComponent(typeof(RectTransform)) as RectTransform;
			rectTransContent.SetParent(rectTransRoot, false);
		}
		rectTransContent.anchorMin = new Vector2(0, 0);
		rectTransContent.anchorMax = new Vector2(1, 1);
		rectTransContent.sizeDelta = new Vector2(0, 0);

		// _---------------------
		// Image BG
		// _---------------------
		GameObject goImgBG = null;
		Transform transImgBG =  rectTransContent.Find("Img BG");

		if (transImgBG == null)
		{
			goImgBG = new GameObject("Img BG");
			transImgBG = goImgBG.transform;
		}
		else
			goImgBG = transImgBG.gameObject;
		
		RectTransform rectTransImgBG = transImgBG.GetComponent<RectTransform>();

		if (rectTransImgBG == null)
		{
			rectTransImgBG = goImgBG.AddComponent(typeof(RectTransform)) as RectTransform;
			rectTransImgBG.SetParent(rectTransContent, false);
		}

		Image imgBG = rectTransImgBG.GetComponent<Image>();
		if (imgBG == null)
			imgBG  = goImgBG.AddComponent(typeof(Image)) as Image;
		imgBG.color = new Color(1, 1, 1, 1);

		rectTransImgBG.anchorMin = new Vector2(0, 0);
		rectTransImgBG.anchorMax = new Vector2(1, 1);
		rectTransImgBG.sizeDelta = new Vector2(0, 0);

		Selection.activeGameObject = panelGameObject;
	}
}
