﻿// Source https://forum.unity.com/threads/animation-window-preview-animation-with-specific-start-and-end-frames.467892/#post-3045523

using System.Collections;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using System.Reflection;
using System;

public class AnimationWindowHelper
{
	static System.Type animationWindowType = null;
	static UnityEngine.Object openAnimationWindow;

	public static bool IsAnimationWindowOpen()
	{
		return (openAnimationWindow != null);
	}

	static System.Type GetAnimationWindowType()
	{
		if (animationWindowType == null)
		{
			animationWindowType = System.Type.GetType("UnityEditor.AnimationWindow,UnityEditor");
		}
		return animationWindowType;
	}

	static UnityEngine.Object GetOpenAnimationWindow()
	{
		if (openAnimationWindow == null)
		{
			//Debug.Log("Found " + (openAnimationWindow).name);
			UnityEngine.Object[] openAnimationWindows = Resources.FindObjectsOfTypeAll(GetAnimationWindowType());
			if (openAnimationWindows.Length > 0)
				openAnimationWindow = openAnimationWindows[0];
			return openAnimationWindow;
		}
		return openAnimationWindow;
	}

	public static Rect GetOpenAnimationWindowRect()
	{
		return ((EditorWindow)GetOpenAnimationWindow()).position;
	}

	public static void ForceOpenAnimationWindowRepaint()
	{
		((EditorWindow)GetOpenAnimationWindow()).Repaint();
	}

	public static void RepaintOpenAnimationWindow()
	{
		UnityEngine.Object w = GetOpenAnimationWindow();
		if (w != null)
		{
			(w as EditorWindow).Repaint();
		}
	}

	public static void RefreshOpenAnimationWindow()
	{
		UnityEngine.Object w = GetOpenAnimationWindow();
		if (w != null)
		{
			BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance;
			FieldInfo animEditor = GetAnimationWindowType().GetField("m_AnimEditor", flags);

			Type animEditorType = animEditor.FieldType;
			System.Object animEditorObject = animEditor.GetValue(w);
			FieldInfo animWindowState = animEditorType.GetField("m_State", flags);
			Type windowStateType = animWindowState.FieldType;

			windowStateType.InvokeMember("ForceRefresh", BindingFlags.InvokeMethod | BindingFlags.Public, null, animWindowState.GetValue(animEditorObject), null);
		}
	}

	public static void PrintMethods()
	{
		UnityEngine.Object w = GetOpenAnimationWindow();
		if (w != null)
		{
			BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance;
			FieldInfo animEditor = GetAnimationWindowType().GetField("m_AnimEditor", flags);

			Type animEditorType = animEditor.FieldType;
//			System.Object animEditorObject = animEditor.GetValue(w);
			FieldInfo animWindowState = animEditorType.GetField("m_State", flags);
			Type windowStateType = animWindowState.FieldType;

			Debug.Log("Methods");
			MethodInfo[] methods = windowStateType.GetMethods(BindingFlags.Public | BindingFlags.Instance);
			Debug.Log("Methods : " + methods.Length);
			for (int i = 0; i < methods.Length; i++)
			{
				MethodInfo currentInfo = methods[i];
				Debug.Log(currentInfo.ToString());
			}
		}
	}


	public static AnimationClip GetAnimationWindowCurrentClip()
	{
		UnityEngine.Object w = GetOpenAnimationWindow();
		if (w != null)
		{
			BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance;
			FieldInfo animEditor = GetAnimationWindowType().GetField("m_AnimEditor", flags);

			Type animEditorType = animEditor.FieldType;
			System.Object animEditorObject = animEditor.GetValue(w);
			FieldInfo animWindowState = animEditorType.GetField("m_State", flags);
			Type windowStateType = animWindowState.FieldType;
			System.Object windowInst = animWindowState.GetValue(animEditorObject);

			// NEW
			MethodInfo mi = windowStateType.GetMethod("get_activeAnimationClip", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
			System.Object clip = mi.Invoke(windowInst, null);

			// OLD
			//System.Object clip = windowStateType.InvokeMember("get_activeAnimationClip", BindingFlags.InvokeMethod | BindingFlags.Public, null, animWindowState.GetValue(animEditorObject), null);

			return (AnimationClip)clip;
		}

		return null;
	}


	public static AnimatorController GetAnimationWindowCurrentController()
	{
		UnityEngine.Object w = GetOpenAnimationWindow();
		if (w != null)
		{
			BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance;
			FieldInfo animEditor = GetAnimationWindowType().GetField("m_AnimEditor", flags);

			Type animEditorType = animEditor.FieldType;
			System.Object animEditorObject = animEditor.GetValue(w);
			FieldInfo animWindowState = animEditorType.GetField("m_State", flags);
			Type windowStateType = animWindowState.FieldType;
			System.Object windowInst = animWindowState.GetValue(animEditorObject);

			// NEW
			MethodInfo mi = windowStateType.GetMethod("get_activeAnimationPlayer", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
			System.Object controllerObj = mi.Invoke(windowInst, null);

			// OLD
			//System.Object controllerObj = (windowStateType).InvokeMember("get_activeAnimationPlayer", BindingFlags.InvokeMethod | BindingFlags.Public | BindingFlags.Instance, null, windowInst, null);

			// Early return if not selecting an Animatable hierarchy
			if (controllerObj == null)
				return null;

			// Get the actual Animator Controller in the project rather than runtime one
			AnimatorController controller = null;
			string acPath = AssetDatabase.GetAssetPath(((Animator)controllerObj).runtimeAnimatorController);
			if (AssetDatabase.LoadAssetAtPath(acPath, typeof(AnimatorController)) != null)
				controller = (AnimatorController)AssetDatabase.LoadAssetAtPath(acPath, typeof(AnimatorController));
			
			return controller;
		}

		return null;
	}
}
