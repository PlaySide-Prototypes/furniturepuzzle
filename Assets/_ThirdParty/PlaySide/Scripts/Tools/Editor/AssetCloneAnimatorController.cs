﻿// REFERENCE HERE: https://docs.unity3d.com/ScriptReference/Animations.AnimatorController.html
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using System.Collections;

public class AssetCloneAnimatorController : EditorWindow
{
	const string toolVersion = "1.0";

	static EditorWindow thisWindow;
	static Vector2 minWindowSize = new Vector2(300f, 225f);

	[MenuItem("Assets/PlaySide Tools/Clone AnimatorController", false, 0)]
	public static void OpenCreateAnimatorController()
	{
		if (thisWindow == null)
			thisWindow = (EditorWindow)EditorWindow.GetWindow(typeof(AssetCloneAnimatorController), true, "Clone Animator "+ toolVersion);

		thisWindow.minSize = minWindowSize;
	}

	// Note that we pass the same path, and also pass "true" to the second argument.
	[MenuItem("Assets/PlaySide Tools/Clone AnimatorController", true)]
	private static bool NewMenuOptionValidation()
	{
		// This returns true when the selected object is a Texture2D (the menu item will be disabled otherwise).
		return ((Selection.activeObject != null) && (Selection.activeObject.GetType() == typeof(AnimatorController)));
	}


	AnimatorController 	srcController;
	AnimatorController 	dstController;
	string 				srcControllerName;
	string 				dstControllerName;

	GUIStyle richTextStyle;


	void OnEnable()
	{
		RefreshTargetControllerDetails();
	}

	void OnGUI()
	{
		// Close Clone window if srcController reference is lost
		if (srcController == null)
			thisWindow.Close();

		richTextStyle = new GUIStyle(GUI.skin.label);
		richTextStyle.richText = true;

		// Source Animator Controller
		//srcController = (AnimatorController)EditorGUILayout.ObjectField("Animator Controller:", srcController, typeof(AnimatorController), false);
		EditorGUILayout.LabelField("<b>Animator Controller:</b> " + srcControllerName, richTextStyle);

		//EditorGUILayout.LabelField("Source Name \t"+ srcControllerName);
		EditorGUILayout.Separator();

		// Destination Animator Controller
		EditorGUI.BeginChangeCheck();
		{
			dstControllerName = EditorGUILayout.TextField("Clone Name", dstControllerName);
		}
		if (EditorGUI.EndChangeCheck())
		{
			// Strip '_AC' from destination name if it was included
			dstControllerName = dstControllerName.Replace("AC_", "");
			dstControllerName = dstControllerName.Replace("ac_", "");
		}

		EditorGUILayout.Separator();

		bool invalidDestName = string.IsNullOrEmpty(dstControllerName);

		// Clone Button
		if(invalidDestName)
			GUI.color = Color.red;

		if (GUILayout.Button((invalidDestName ? "Enter Clone Name" : "Clone Animator")) && (srcController != null) && (!invalidDestName))
			CloneAnimatorController();

		if(invalidDestName)
			GUI.color = Color.white;

		DrawBoneLists();
	}

	void DrawBoneLists()
	{
		//GUILayout.BeginArea(new Rect(100, 1400, 0,0));
		GUILayout.BeginHorizontal();

		// SOURCE
		GUILayout.BeginVertical();
		GUILayout.Label("\nContents:\n", EditorStyles.label);


		if (srcController != null)
		{
			EditorGUI.indentLevel++;
			foreach (AnimatorControllerLayer layer in srcController.layers)
			{
				ChildAnimatorState[] rootStateMachine = layer.stateMachine.states;
				foreach (ChildAnimatorState s in rootStateMachine)
				{
					if (s.state != null && s.state.motion != null)
						GUILayout.Label("<b>"+s.state.name + ":</b> \t\t" + s.state.motion.name, richTextStyle);
				}
			}
			EditorGUI.indentLevel--;
		}
		GUILayout.EndVertical();

		GUILayout.EndHorizontal();
		//GUILayout.EndArea();
	}


	void RefreshTargetControllerDetails()
	{
		//if(srcController == null)
			srcController = Selection.activeObject as AnimatorController;

		// Don't allow inclusiong of AC_ in name since we auto-add this
		if (srcController != null)
		{
			srcControllerName = srcController.name;
			srcControllerName = srcControllerName.Replace("AC_", "");
			srcControllerName = srcControllerName.Replace("ac_", "");
		}
	}

	void CloneAnimatorController()
	{
		string pathSrcController = AssetDatabase.GetAssetPath(srcController);
		string pathParentDir = Path.GetDirectoryName(Path.GetDirectoryName(pathSrcController));
		string pathControllerFolder = pathParentDir + "/" + dstControllerName;
		AssetDatabase.CreateFolder(pathParentDir, dstControllerName);


		// ------------------------------
		// Clone CONTROLLER
		// ------------------------------
		string pathNewController = pathControllerFolder +"/AC_"+ dstControllerName + ".controller";
		AssetDatabase.CopyAsset(pathSrcController, pathNewController);
		dstController = AssetDatabase.LoadAssetAtPath(pathNewController, typeof(AnimatorController)) as AnimatorController;


		// ------------------------------
		// Clone ANIMATION CLIPS
		// ------------------------------

		foreach (AnimatorControllerLayer layer in dstController.layers)
		{
			ChildAnimatorState[] rootStateMachine = layer.stateMachine.states;
			foreach (ChildAnimatorState s in rootStateMachine)
			{
				if (s.state != null && s.state.motion != null)
				{
					string pathAnimClip = AssetDatabase.GetAssetPath(s.state.motion);

					string newClipName;
					if(s.state.motion.name.Contains(srcControllerName))
						newClipName = s.state.motion.name.Replace(srcControllerName, dstControllerName);
					else
						newClipName = s.state.motion.name +"_"+ dstControllerName;
					
					string pathNewClip = pathParentDir + "/" + dstControllerName + "/" + newClipName + ".anim";

					// Clone clip if none exist
					AnimationClip clonedClip = AssetDatabase.LoadAssetAtPath(pathNewClip, typeof(AnimationClip)) as AnimationClip;
					if (clonedClip == null)
					{
						AssetDatabase.CopyAsset(pathAnimClip, pathNewClip);
						clonedClip = AssetDatabase.LoadAssetAtPath(pathNewClip, typeof(AnimationClip)) as AnimationClip;
					}

					// Assign Cloned clip to cloned animator state
					s.state.motion = clonedClip;
				}
			}
		}
		Debug.Log("Clone Animator Controller: "+ pathNewController);
		SelectProjectObject(pathNewController);
	}

	/// <summary> Select Localisation CSV in project view</summary>
	static void SelectProjectObject(string _path)
	{
		UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath(_path, typeof(UnityEngine.Object));
		Selection.activeObject = obj;
		EditorGUIUtility.PingObject(obj);
	}
}
