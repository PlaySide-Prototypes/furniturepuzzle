using UnityEngine;
using System;

public class Screenshot : MonoBehaviour
{
	[SerializeField] KeyCode[] keyToHold = new KeyCode[] { KeyCode.LeftShift, KeyCode.RightShift };
	[SerializeField] KeyCode keyToPress = KeyCode.S;
	[SerializeField] [Range(1, 16)] int sizeMultiplier = 1;

#if !UNITY_EDITOR
	void OnEnable()
	{
		this.enabled = false;
	}
#endif

	void Update()
	{
		if (Input.GetKeyDown(keyToPress))
		{
			if (keyToHold.Length == 0)
				CaptureScreenshot();
			else for (int i = 0; i < keyToHold.Length; ++i)
			{
				if (Input.GetKey(keyToHold[i]))
					CaptureScreenshot();
			}
		}
	}

	public void CaptureScreenshot()
	{
		string path = "Screenshot_";
		DateTime now = DateTime.Now;
		path += now.ToString("yyyyMMdd_HHmmss");
		path += ".png";

		if (!Application.isEditor)
			path = System.IO.Path.Combine(Application.persistentDataPath, path);

		Debug.Log("Screenshot saving to: " + path);

		ScreenCapture.CaptureScreenshot(path, sizeMultiplier);
	}
}
