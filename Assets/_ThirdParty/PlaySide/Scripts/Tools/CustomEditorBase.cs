﻿//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//             Custom Editor Base Class
//             Author: Christopher Allport
//             Date Created: November 11, 2014 
//             Last Updated: August 10, 2020
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  Description:
//
//		This Script is a base class for all New Custom Editors. It adds some 
//			additional functionality that should be readily available in all
//			Custom Editor scripts.
//
//    A custom editor is used to add additional functionality to the Unity 
//		inspector when dealing with the aforementioned class data.
//
//	  This includes the addition of adding in buttons or calling a method when a 
//		value is changed.
//	  Most importantly, a custom editor is used to make the inspector more 
//		readable and easier to edit.
//
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

// Turn this on if you are having trouble finding the script in the project files. It'll draw the option as part of the header, same as Unity default behaviour.
// // // // #define RENDER_SCRIPT_FILE_HEADER


#if UNITY_EDITOR
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
#endif


/// <summary>
/// Any method marked with this Attribute is automatically called when the custom inspector is rendered.
/// </summary>
[System.Diagnostics.Conditional("UNITY_EDITOR")]
[System.AttributeUsage(System.AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
public sealed class InspectorRegion : System.Attribute
{
    public string DisplayLabel { get; private set; }
    public int DrawOrder { get; private set; }

    /// <summary>
    /// Any method inside of a class inheriting from the CustomEditorBase class tagged with this attribute will be automatically invoked during inspector render.
    /// </summary>
    /// 
    /// <param name="displayLabel">
    /// The header text to display above the block of contents rendered by the invoking method. Leave as empty if no display label is desired.
    /// </param>
    /// 
    /// <param name="drawOrder">
    /// The order in which this method should be invoked compared to all other methods marked with InspectorRegion. 
    /// If left as -1, method will be invoked in sequential order as listed in your CustomEditor class.
    /// </param>
	public InspectorRegion(string displayLabel = "", int drawOrder = -1)
    {
        this.DisplayLabel = displayLabel;
        this.DrawOrder = drawOrder;
    }
}

/// <summary>
/// A field marked with this attribute will not be shown in the inspector by default. The field will become your responsibility to show in the inspector.
/// This attribute is basically the exact same attribute as "HideInInspector", except this one makes more sense to someone else reading the code.
/// </summary>
[System.Diagnostics.Conditional("UNITY_EDITOR")]
[System.AttributeUsage(System.AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
public sealed class HandledInCustomInspector : System.Attribute
{
}


#if UNITY_EDITOR
namespace Playside
{
    public class CustomEditorBase<K> : Editor          // K for Klass
                             where K : UnityEngine.Object
    {
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* CONST VALUES
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected const BindingFlags REFLECTION_SEARCH_FLAGS = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance;
        protected const float TextLayoutPadding = 50.0f;

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	*{} Class Declarations
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected delegate void CallbackMethod();
        protected delegate void ArrayElementDrawAction(int index);
        protected delegate int ArraySorter<T>(T val1, T val2);
        protected delegate List<string> AllDropdownOptionsGetter();

        protected enum BooleanState
        {
            TRUE,
            FALSE
        }

        protected enum ButtonAlignment
        {
            /// <summary> Inspector Window will show Button from LeftMost side to RightMost side. </summary>
            FullWidth,
            /// <summary> Inspector Window will show Button in center, with gaps from the leftmost and rightmost sides of the window. </summary>
            Centered,
            /// <summary> Inspector Window will show Button in from leftmost side until dead center. </summary>
            Left,
            /// <summary> Inspector Window will show Button in from dead center until rightmost side. </summary>
            Right
        }

        protected enum SimpleTextAlignment
        {
            /// <summary> Text will appear in the centre of the window. </summary>
            Centered,
            /// <summary> Text will appear on the left side of the window. </summary>
            Left,
            /// <summary> Text will appear on the right side of the window. </summary>
            Right
        }

        protected enum PlusMinusButtons
        {
            PlusPressed,
            MinusPressed,
            None
        }

        protected enum FieldModificationResult
        {
            NoChange,
            Modified
        }

        protected enum ArrayModificationResult
        {
            NoChange,
            ModifiedValue,
            SwappedItems,
            RemovedLastElement,
            AddedNewElement,
        }

        private struct SerialisableFieldData
        {
            public string header;
            public FieldInfo fieldInfo;
            public GUIContent label;
            public SerializedProperty serialisedProperty;
        }

        private struct InspectorRegionData
        {
            public MethodInfo methodInfo;
            public InspectorRegion regionInfo;
        }

        private struct WrappedStringInfo
        {
            public float editorWidthAtCalcTime;
            public float maxLineSize;
            public List<string> wrappedTextLines;
        }

        protected class ButtonsInfo
        {
            public string buttonName;
            public CallbackMethod onClicked;
            public string tooltip = null;
        }

        protected class EditorIndentation : System.IDisposable
        {
            private int m_indentCount;

            public EditorIndentation()
            {
                m_indentCount = 1;
                EditorGUI.indentLevel += m_indentCount;
            }

            public EditorIndentation(int indentCount)
            {
                m_indentCount = indentCount;
                EditorGUI.indentLevel += m_indentCount;
            }

            public void Dispose()
            {
                EditorGUI.indentLevel -= m_indentCount;
            }
        }

        protected struct SetEditorIndentation : System.IDisposable
        {
            private int m_priorIndent;

            public SetEditorIndentation(int newIndentLevel)
            {
                m_priorIndent = EditorGUI.indentLevel;
                EditorGUI.indentLevel = newIndentLevel;
            }

            public void Dispose()
            {
                EditorGUI.indentLevel = m_priorIndent;
            }
        }

        protected class GUIDisable : System.IDisposable
        {
            public GUIDisable()
            {
                GUI.enabled = false;
            }

            public void Dispose()
            {
                GUI.enabled = true;
            }
        }

        protected class FieldModificationResultClamp
        {
            public FieldModificationResult modificationResult
            {
                get
                {
                    return _modificationResult;
                }
                set
                {
                    if (_modificationResult != FieldModificationResult.Modified)
                    {
                        _modificationResult = value;
                    }
                }
            }

            private FieldModificationResult _modificationResult = FieldModificationResult.NoChange;

            public static implicit operator FieldModificationResultClamp(FieldModificationResult result)
            {
                FieldModificationResultClamp clamp = new FieldModificationResultClamp();
                clamp.modificationResult = result;
                return clamp;
            }
            public static implicit operator FieldModificationResult(FieldModificationResultClamp clamp)
            {
                return clamp.modificationResult;
            }
        }

        protected class ArrayTypeDef<T>
        {
            public T[] storedArray = null;
            public List<T> storedList = null;

            public int Count { get { return storedArray != null ? storedArray.Length : storedList.Count; } }
            public int Length { get { return storedArray != null ? storedArray.Length : storedList.Count; } }

            public T this[int index]
            {
                get
                {
                    return storedArray != null ? storedArray[index] : storedList[index];
                }
                set
                {
                    if (storedArray != null)
                        storedArray[index] = value;
                    else
                        storedList[index] = value;
                }
            }

            private ArrayTypeDef(T[] array)
            {
                storedArray = array;
            }
            private ArrayTypeDef(List<T> list)
            {
                storedList = list;
            }
            public void Sort(ArraySorter<T> sorter)
            {
                if (storedList != null)
                {
                    storedList.Sort((T val1, T val2) => sorter.Invoke(val1, val2));
                }
                else
                {
                    System.Array.Sort(storedArray, (T val1, T val2) => sorter.Invoke(val1, val2));
                }
            }

            public static implicit operator ArrayTypeDef<T>(T[] array)
            {
                return new ArrayTypeDef<T>(array);
            }
            public static implicit operator ArrayTypeDef<T>(List<T> list)
            {
                return new ArrayTypeDef<T>(list);
            }
        }

        protected class ArraySorterInfo<T>
        {
            public string buttonLabel;
            public ArraySorter<T> sorterFunc;
            public string tooltip = null;
        }

        protected class DrawArrayParameters<T>
        {
            public bool isArrayResizeable = true;
            public bool canReorderArray = false;
            public bool addSpacesBetweenElements = false;
            public ArrayElementDrawAction drawElementCallback = null;
            public CallbackMethod onArraySorted = null;
            public ArraySorter<T> alphabeticalComparer = null;

            public ArraySorterInfo<T> otherSorter1 = null;
            public ArraySorterInfo<T> otherSorter2 = null;
            public ArraySorterInfo<T> otherSorter3 = null;
        }

        protected class DropdownChoicesData
        {
            public int currentSelectionIndex;
            public string[] allChoices;
            public string[] choicesUsedByOtherElements;
            public string[] remainingSelectableChoices;
            public int[] remainingChoicesIndexToAllChoicesIndex;
        }

        protected class DropdownChoicesParameters
        {
            /// <summary> The object that this Data is being pulled from. Pass in the Array Element. </summary>
            public object owningObject;
            /// <summary> The index of the array which is making use of this Param List. If you don't have one. Pass in as zero. </summary>
            public int arrayIndex;
            /// <summary> Delegate to gets all of the dropdown options available as a string. </summary>
            public AllDropdownOptionsGetter allDropdownOptionsGetter;
            /// <summary> Delegate to get a list of all array items that currently have assigned elements found in the dropdown list. </summary>
            public AllDropdownOptionsGetter allUsedDropdownOptionsGetter;
            /// <summary> Forces the Dropdown Box Choices backend to refresh/recalculated chosen boxes, etc. This can be intensive, so only refresh when necessary. </summary>
            public bool forceRefesh = false;
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	*. Protected Static Variables
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected static string sm_sOpenPathDirectory = @"C:\";

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	*- Private Instance Variables
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        private float? m_editorWindowContextWidth = null;
        private AudioSource m_activeAudioSource = null;

        private LocManager.LanguageStrings[] m_locLanguageStrings = null;

        private Dictionary<string, bool> m_foldoutBooleans = new Dictionary<string, bool>();
        private Dictionary<object, bool> m_foldoutOwners = new Dictionary<object, bool>();

        private Dictionary<string, Dictionary<System.Type, string[]>> m_alphabetisedEnumTypes = new Dictionary<string, Dictionary<System.Type, string[]>>();
        private Dictionary<object, DropdownChoicesData[]> m_cachedDropdownChoicesWithUsedElementsRemoved = new Dictionary<object, DropdownChoicesData[]>();
        private Dictionary<string, WrappedStringInfo> m_calculatedWrappedStrings = new Dictionary<string, WrappedStringInfo>();

        /// <summary> The width of each ASCII Text Character. </summary>
        private List<float> m_textCharacterWidths = new List<float>();

        /// <summary> Fields that will be serialised and used during gameplay. </summary>
        private List<SerialisableFieldData> m_serializableGameplayRelatedFields = new List<SerialisableFieldData>();

        /// <summary> Fields that will be serialised and used during Editor Mode Only ( #if UNITY_EDITOR ). </summary>
        private List<SerialisableFieldData> m_serializableEditorOnlyFields = new List<SerialisableFieldData>();

        /// <summary> Methods to call automatically to render inspector GUI. </summary>
        private List<InspectorRegionData> m_sortedInspectorRegions = new List<InspectorRegionData>();

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	*+ Attr_Readers
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected K Target { get { return target as K; } }
        protected virtual string ScriptDescription { get { return ""; } }
        protected virtual int DefaultInspectorGameplayFieldsRegionOrder { get { return 1; } }
        protected virtual int DefaultInspectorEditorFieldsRegionOrder { get { return DefaultInspectorGameplayFieldsRegionOrder + 1; } }

#if RENDER_SCRIPT_FILE_HEADER
    protected SerializedProperty OwningScriptProperty { get; private set; }
#endif

        protected long FrameCountInInspector { get; private set; } = 0;
        protected bool IsProgressBarWindowActive { get; private set; } = false;

        protected float StartDrawValueXPos { get { return EditorWindowContextWidth * 0.42f; } } // Position which the value should start being Drawn From (updates with Inspector length)
        protected float DrawValueWidth { get { return EditorWindowContextWidth * 0.58f; } } // Width of the editable value (updates with Inspector Length)

        protected virtual Color MainFontColour { get { return new Color32(61, 84, 47, 255); } }
        protected virtual Color SecondaryFontColour { get { return new Color32(137, 107, 47, 255); } }

        protected Color NormalFontColour { get { return new Color(0.000f, 0.000f, 0.000f, 1.000f); } }                  // Everything is fine			=> BLACK COLOUR
        protected Color ImportantObjectMissingFontColour { get { return new Color(1.000f, 0.000f, 0.000f, 0.750f); } }  // Missing Object is Required	=> RED COLOUR
        protected Color ObjectMissingFontColour { get { return new Color(0.627f, 0.121f, 0.729f, 0.750f); } }           // Missing Object				=> PURPLE COLOUR

        protected bool IsUsingUnityProSkin { get { return EditorGUIUtility.isProSkin; } }

        protected float EditorWindowContextWidth
        {
            get
            {
                if (m_editorWindowContextWidth.HasValue == false)
                {
                    m_editorWindowContextWidth = (float)typeof(EditorGUIUtility).GetProperty("contextWidth", BindingFlags.NonPublic | BindingFlags.Static).GetValue(null, null);
                }
                return m_editorWindowContextWidth.Value;
            }
        }

        /// <summary> Gets the Start X Position of the Drag And Drop portion of a Unity Object Reference (EditorGUILayout) that is drawn into the Inspector. </summary>
        protected float XPositionOfUnityDrawObjectWithLabel
        {
            get { return EditorWindowContextWidth * 0.4205f; }
        }

        protected Vector3? SceneViewMouseViewportPosition
        {
            get
            {
                if (Event.current == null)
                {
                    return null;
                }

                SceneView sceneView = SceneView.currentDrawingSceneView;
                if (sceneView == null)
                {
                    sceneView = SceneView.lastActiveSceneView;
                    if (sceneView == null)
                    {
                        return null;
                    }
                }

                Vector3 mousePosition = Event.current.mousePosition;
                mousePosition.y = sceneView.camera.pixelHeight - mousePosition.y;
                mousePosition = sceneView.camera.ScreenToViewportPoint(mousePosition);
                return mousePosition;
            }
        }

        protected AudioSource ActiveAudioSource
        {
            get
            {
                if (m_activeAudioSource == null)
                {
                    m_activeAudioSource = GameObject.FindObjectOfType<AudioSource>();
                }

                return m_activeAudioSource;
            }
        }

        protected LocManager.LanguageStrings[] LanguageStrings
        {
            get
            {
                if (m_locLanguageStrings == null)
                {
                    m_locLanguageStrings = LocManager.GetLanguageStrings();
                }
                return m_locLanguageStrings;
            }
        }


        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* Unity Method: On Enabled
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        private void OnEnable()
        {
            FrameCountInInspector = 0;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* Unity Method: On Inspector GUI
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        public sealed override void OnInspectorGUI()
        {
            if (FrameCountInInspector == 0)
            {
                // Initialise being done here instead of OnEnable because it uses GUI functionality.
                //  GUI functions do not work in the constructor or OnEnable. So we do Initialise here instead.
                Initialise();
            }

            // Only needs to be updated when called, once per frame.
            m_editorWindowContextWidth = null;

            // Get it away from the top of the inspector so it's easier to read!
            AddSpaces(2);

            // If we have a script description. Draw it.
            DrawScriptDescription();

            // Draw Script reference
#if RENDER_SCRIPT_FILE_HEADER
            GUI.enabled = false;
            DrawSerialisedObjectOptions("Script:", OwningScriptProperty);
            GUI.enabled = true;
            AddSpaces(2);
#endif

            // Draw each Inspector Region that has been assigned.
            foreach (InspectorRegionData inspectorRegion in m_sortedInspectorRegions)
            {
                DrawNewInspectorRegion(inspectorRegion);
            }

            // Reserialise Script Instance if things have been changed.
            if (GUI.changed)
            {
                OnGUIChanged();
                SetDirty();
                if (Application.isPlaying == false)
                {
                    UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(UnityEngine.SceneManagement.SceneManager.GetActiveScene());
                }
            }

            // Next Frame.
            ++FrameCountInInspector;
        }

        protected virtual void OnDefaultInspectorElementsRendered()
        {
        }

        protected virtual void OnEditorInspectorElementsRendered()
        {
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Initialise
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        private void Initialise()
        {
            // Link back to the File that owns this script. Will be rendered as the first field for this cutom inspector. Same as Unity's default.
#if RENDER_SCRIPT_FILE_HEADER
            SerializedProperty iterator = serializedObject.GetIterator();
            for (bool enterChildren = true; iterator.NextVisible(enterChildren); enterChildren = false)
            {
                if (iterator.propertyPath.Equals("m_Script"))
                {
                    OwningScriptProperty = iterator;
                    break;
                }
            }
#endif

            // Find out the width of each text character we can use (this can be used later to dynamically alter where labels and editable fields are located)
            BuildCharacterWidthsList();

            // Determine which fields are serializable. Display them if 'DrawDefaultInspector' is invoked
            BuildFieldInfoList();

            // Determines which Functions/Methods will be called to display everything used in the Custom Inspector.
            BuildInspectorRegionsList();

            OnInitialised();
        }

        protected virtual void OnInitialised()
        {
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Build Character Widths List
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        private void BuildCharacterWidthsList()
        {
            m_textCharacterWidths.Clear();
            m_textCharacterWidths.Add(0.0f); // Null Terminator
            for (int c = 1; c < 256; ++c)
            {
                string s = System.Char.ConvertFromUtf32(c);
                Vector2 textDimensions = GUI.skin.label.CalcSize(new GUIContent(s));
                float charWidth = textDimensions.x;
                m_textCharacterWidths.Add(charWidth);
            }
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Build Field Info List
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        private void BuildFieldInfoList()
        {
            m_serializableGameplayRelatedFields.Clear();
            m_serializableEditorOnlyFields.Clear();

            System.Type currentClassType = Target.GetType();
            while (currentClassType != typeof(UnityEngine.Object))
            {
                FieldInfo[] allFields = currentClassType.GetFields(REFLECTION_SEARCH_FLAGS);
                string queuedHeader = null;
                foreach (FieldInfo fieldInfo in allFields)
                {
                    HeaderAttribute headerAttribute = fieldInfo.GetCustomAttribute<HeaderAttribute>();
                    if (headerAttribute != null)
                    {
                        queuedHeader = headerAttribute.header;
                    }

                    if (fieldInfo.IsLiteral || fieldInfo.IsInitOnly)
                    {
                        // Consts and Static fields not applicable.
                        continue;
                    }

                    // Make sure we are not adding fields more than once. This happens when looping through base classes which have the same private variable names (legal).
                    bool canAddField = true;
                    foreach (SerialisableFieldData s in m_serializableGameplayRelatedFields)
                    {
                        if (s.fieldInfo.Name.Equals(fieldInfo.Name))
                        {
                            canAddField = false;
                            break;
                        }
                    }
                    foreach (SerialisableFieldData s in m_serializableEditorOnlyFields)
                    {
                        if (s.fieldInfo.Name.Equals(fieldInfo.Name))
                        {
                            canAddField = false;
                            break;
                        }
                    }
                    if (canAddField == false)
                    {
                        continue;
                    }

                    if (fieldInfo.IsPublic == false)
                    {
                        // If not public, determine if it has the [SerializableField] attribute.
                        SerializeField attrSerializeField = fieldInfo.GetCustomAttribute<SerializeField>();
                        if (attrSerializeField == null)
                        {
                            continue;
                        }
                    }

                    // Remove if [HideInInspector]
                    HideInInspector attrHideInInspector = fieldInfo.GetCustomAttribute<HideInInspector>();
                    if (attrHideInInspector != null)
                    {
                        continue;
                    }

                    // Delegates cannot be serialised, Except for Unity Events
                    if (fieldInfo.FieldType != typeof(UnityEngine.Events.UnityEvent) && fieldInfo.FieldType.IsSubclassOf(typeof(UnityEngine.Events.UnityEvent)) == false)
                    {
                        if (fieldInfo.FieldType == typeof(System.Action) || fieldInfo.FieldType.IsSubclassOf(typeof(System.Action))
                            || fieldInfo.FieldType == typeof(System.Delegate) || fieldInfo.FieldType.IsSubclassOf(typeof(System.Delegate)))
                        {
                            continue;
                        }
                    }

                    SerializedProperty serialisedProperty = serializedObject.FindProperty(fieldInfo.Name);
                    if (serialisedProperty == null)
                    {
                        System.NonSerializedAttribute nonSerialisedAttr = fieldInfo.GetCustomAttribute<System.NonSerializedAttribute>();
                        if (nonSerialisedAttr != null)
                        {
                            // Marked for Non-Serialisation
                            continue;
                        }
                        System.SerializableAttribute serialisableAttr = fieldInfo.FieldType.GetCustomAttribute<System.SerializableAttribute>();
                        if (serialisableAttr == null)
                        {
                            Debug.LogError($"{Target.GetType().Name}: {fieldInfo.Name} has been marked for Inspector. However, {fieldInfo.FieldType.Name} is missing the [System.Serializable] tag.");
                        }
                        else
                        {
                            if (fieldInfo.FieldType.IsArray)
                            {
                                System.Type elementType = fieldInfo.FieldType.GetElementType();
                                serialisableAttr = elementType.GetCustomAttribute<System.SerializableAttribute>();
                                if (serialisableAttr == null)
                                {
                                    Debug.LogError($"{Target.GetType().Name}: The array \"{fieldInfo.Name}\" has been marked for Inspector. However, the referenced class type \"{elementType.Name}\" is missing the [System.Serializable] tag.");
                                }
                                else
                                {
                                    Debug.LogError($"{Target.GetType().Name}: The array \"{fieldInfo.Name}\" has been marked for Inspector. However, the referenced class type \"{elementType.Name}\" cannot be serialised. Are you directly referencing a Generic Class Type?");
                                }
                            }
                            else
                            {
                                Debug.LogError($"{Target.GetType().Name}: {fieldInfo.Name} has been marked for Inspector. However, this field cannot be serialised. Are you directly referencing a generic class type?");
                            }
                        }
                        continue;
                    }

                    // Should remove this field from the default fields to show? (Check it for serialisation issues first)
                    HandledInCustomInspector customInspAttr = fieldInfo.GetCustomAttribute<HandledInCustomInspector>();
                    if (customInspAttr != null)
                    {
                        continue;
                    }

                    // Get Display Name: Replace ' m_ ' prefix in Variable Name
                    string variableName = Regex.Replace(fieldInfo.Name, "^m?_", "", RegexOptions.IgnoreCase);
                    string replacedEditorVariableName = Regex.Replace(variableName, "^editor", "", RegexOptions.IgnoreCase);
                    bool isEditorVariable = variableName.Equals(replacedEditorVariableName) == false;

                    string displayName;
#if UNITY_2019_1_OR_NEWER
                    InspectorNameAttribute inspNameAttr = fieldInfo.GetCustomAttribute<InspectorNameAttribute>();
                    if (inspNameAttr != null)
                    {
                        displayName = inspNameAttr.displayName.TrimEnd(':') + ':';
                    }
                    else
#endif
                    {
                        displayName = string.IsNullOrEmpty(replacedEditorVariableName) ? $"Unknown Variable:" : $"{char.ToUpper(replacedEditorVariableName[0])}";
                        int varNameLength = replacedEditorVariableName.Length;
                        for (int i = 1; i < varNameLength; ++i)
                        {
                            if (char.IsUpper(replacedEditorVariableName[i]))
                            {
                                // Capital Letter => Space, then letter. Unless there are two capital letters in a row. In which case leave them next to one another.
                                if (char.IsUpper(replacedEditorVariableName[i - 1]) == false)
                                {
                                    displayName += $" {replacedEditorVariableName[i]}";
                                    continue;
                                }
                            }
                            if (replacedEditorVariableName[i] == '_')
                            {
                                // Underscore => Replace with space
                                displayName += ' ';
                                continue;
                            }

                            displayName += replacedEditorVariableName[i];
                        }
                        displayName += ":";
                    }


                    GUIContent label;
                    TooltipAttribute tooltipAttr = fieldInfo.GetCustomAttribute<TooltipAttribute>();
                    if (tooltipAttr != null && string.IsNullOrEmpty(tooltipAttr.tooltip) == false)
                    {
                        label = new GUIContent(displayName, tooltipAttr.tooltip);
                    }
                    else
                    {
                        label = new GUIContent(displayName);
                    }

                    SerialisableFieldData fieldData = new SerialisableFieldData()
                    {
                        header = queuedHeader,
                        label = label,
                        fieldInfo = fieldInfo,
                        serialisedProperty = serialisedProperty,
                    };

                    queuedHeader = null;

                    if (isEditorVariable)
                    {
                        m_serializableEditorOnlyFields.Add(fieldData);
                    }
                    else
                    {
                        m_serializableGameplayRelatedFields.Add(fieldData);
                    }
                }

                currentClassType = currentClassType.BaseType;
            }
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Build Inspector Regions List
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        private void BuildInspectorRegionsList()
        {
            m_sortedInspectorRegions.Clear();

            // Custom Inspector Regions determined by the overriding class.
            MethodInfo[] allMethods = this.GetType().GetMethods(REFLECTION_SEARCH_FLAGS);
            foreach (MethodInfo methodInfo in allMethods)
            {
                InspectorRegion inspRegion = methodInfo.GetCustomAttribute<InspectorRegion>();
                if (inspRegion == null)
                {
                    continue;
                }

                InspectorRegionData inspRegionData = new InspectorRegionData()
                {
                    methodInfo = methodInfo,
                    regionInfo = inspRegion
                };

                m_sortedInspectorRegions.Add(inspRegionData);
            }

            if (m_serializableGameplayRelatedFields.Count > 0)
            {
                // Game Related Variables To Draw By Default. Only need to identify this area as "Gameplay Fields" if there are Editor Only fields which will also be shown.
                string displayLabel = m_serializableEditorOnlyFields.Count > 0 ? "~Gameplay Fields~." : "";
                InspectorRegionData defaultInspRegionData = new InspectorRegionData()
                {
                    methodInfo = typeof(CustomEditorBase<K>).GetMethod(nameof(DrawGameplayFields), REFLECTION_SEARCH_FLAGS),
                    regionInfo = new InspectorRegion(displayLabel: displayLabel, drawOrder: DefaultInspectorGameplayFieldsRegionOrder)
                };

                m_sortedInspectorRegions.Add(defaultInspRegionData);
            }

            if (m_serializableEditorOnlyFields.Count > 0)
            {
                // Editor Related Variables To Draw By Default
                InspectorRegionData editorInspRegionData = new InspectorRegionData()
                {
                    methodInfo = typeof(CustomEditorBase<K>).GetMethod(nameof(DrawEditorOnlyFields), REFLECTION_SEARCH_FLAGS),
                    regionInfo = new InspectorRegion(displayLabel: "~Editor Only Fields~ (compiled out during build).", drawOrder: DefaultInspectorEditorFieldsRegionOrder)
                };

                m_sortedInspectorRegions.Add(editorInspRegionData);
            }

            m_sortedInspectorRegions.Sort((InspectorRegionData a, InspectorRegionData b) =>
            {
                if (a.regionInfo.DrawOrder < b.regionInfo.DrawOrder)
                {
                    return -1;
                }
                if (a.regionInfo.DrawOrder > b.regionInfo.DrawOrder)
                {
                    return 1;
                }
                return 0;
            });

            // Default Inspector Regions that can be overriden by the overriding class. Add these to the Regions List if they have been overriden.
            List<System.Tuple<string, string>> defaultInspectorMethods = new List<System.Tuple<string, string>>()
            {
                new System.Tuple<string, string>("~Audio Options~ (Edit Audio Information)",    nameof(DrawAudioHandlerInfoOptions)),
                new System.Tuple<string, string>("~Debug Options~ (To Help With Testing)",      nameof(DrawDebugOptions)),
            };

            foreach (System.Tuple<string, string> defaultInspectorMethodInfo in defaultInspectorMethods)
            {
                string methodName = defaultInspectorMethodInfo.Item2;
                MethodInfo methodInfo = this.GetType().GetMethod(methodName, REFLECTION_SEARCH_FLAGS);
                if (methodInfo == null)
                {
                    continue;
                }
                if (methodInfo.DeclaringType == typeof(CustomEditorBase<K>))
                {
                    // Not Overriden. Therefore, an empty method/function
                    continue;
                }

                InspectorRegionData inspRegionData = new InspectorRegionData()
                {
                    methodInfo = methodInfo,
                    regionInfo = new InspectorRegion(defaultInspectorMethodInfo.Item1)
                };

                m_sortedInspectorRegions.Add(inspRegionData);
            }
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: On GUI Changed / Force Refresh
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected virtual void OnGUIChanged()
        {
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Set Dirty
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected new void SetDirty()
        {
            EditorUtility.SetDirty(Target);
        }

        protected void SetDirty(UnityEngine.Object markedObj)
        {
            EditorUtility.SetDirty(markedObj);
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Draw New Inspector Region
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        private void DrawNewInspectorRegion(string label, CallbackMethod InspectorDrawMethod)
        {
            EditorGUILayout.LabelField(label, EditorStyles.boldLabel);
            using (new EditorIndentation())
            {
                InspectorDrawMethod();
                AddSpaces(3);
            }
        }

        private void DrawNewInspectorRegion(InspectorRegionData newInspectorRegion)
        {
            if (string.IsNullOrEmpty(newInspectorRegion.regionInfo.DisplayLabel) == false)
            {
                EditorGUILayout.LabelField(newInspectorRegion.regionInfo.DisplayLabel, EditorStyles.boldLabel);
                using (new EditorIndentation())
                {
                    newInspectorRegion.methodInfo.Invoke(this, null);
                }
            }
            else
            {
                newInspectorRegion.methodInfo.Invoke(this, null);
            }

            AddSpaces(3);
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Script Description
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        private void DrawScriptDescription()
        {
            if (string.IsNullOrEmpty(ScriptDescription) == false)
            {
                DrawDescriptionBox(ScriptDescription);
            }
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Gameplay Fields
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        private void DrawGameplayFields()
        {
            serializedObject.Update();
            EditorGUI.BeginChangeCheck();
            DrawSerialisedFieldsObjects(m_serializableGameplayRelatedFields);
            if (EditorGUI.EndChangeCheck())
            {
                serializedObject.ApplyModifiedProperties();
            }

            OnDefaultInspectorElementsRendered();
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Editor Only Fields
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        private void DrawEditorOnlyFields()
        {
            serializedObject.Update();
            EditorGUI.BeginChangeCheck();
            DrawSerialisedFieldsObjects(m_serializableEditorOnlyFields);
            if (EditorGUI.EndChangeCheck())
            {
                serializedObject.ApplyModifiedProperties();
            }

            OnEditorInspectorElementsRendered();
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Serialised Fields Objects
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        private void DrawSerialisedFieldsObjects(List<SerialisableFieldData> serialisedFields)
        {
            int serialisedFieldsCount = serialisedFields.Count;
            if (serialisedFieldsCount == 0)
            {
                return;
            }

            for (int i = 0; i < serialisedFieldsCount; ++i)
            {
                if (serialisedFields[i].fieldInfo.FieldType == true.GetType()
                    || serialisedFields[i].fieldInfo.FieldType == false.GetType())
                {
                    // Instead of drawing a checkbox. Draw a dropdown box with "TRUE"/"FALSE" instead. I think it looks cleaner.
                    bool currentVal = (bool)serialisedFields[i].fieldInfo.GetValue(Target);
                    bool selectedResult = DrawToggleField(serialisedFields[i].label.text, currentVal, serialisedFields[i].label.tooltip, true);
                    if (selectedResult != currentVal)
                    {
                        serialisedFields[i].fieldInfo.SetValue(Target, selectedResult);
                    }
                    continue;
                }

                EditorGUILayout.PropertyField(serialisedFields[i].serialisedProperty, serialisedFields[i].label, true);
            }
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Audio Handler Info Options
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected virtual void DrawAudioHandlerInfoOptions()
        {
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Debug Options
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected virtual void DrawDebugOptions()
        {
        }



        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Add Spaces
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected void AddSpaces(int count = 3)
        {
            for (int i = 0; i < count; ++i)
            {
                EditorGUILayout.Space();
            }
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Is a Secondary Component?
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary>
        /// Determines whether or not this component is a secondary component.
        /// Basically if this component is listed with an ID of 2, 4, 6, 8, 10, etc. it 
        /// will be considered a secondary component. It doesn't mean anything; it's just
        /// a way of identifying whether or not the colours of the font/labels in this component
        /// should change colours
        /// </summary>
        /// <returns>True if secondary component</returns>
        protected bool IsSecondaryComponent()
        {
            if (Target is Component)
            {
                Component[] components = (Target as Component).gameObject.GetComponents<Component>();
                for (int i = 0; i < components.Length; ++i)
                {
                    if (components[i] == Target)
                        return (i % 2 != 0);
                }
            }
            return false;

        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Get Scaled Rect
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected static Rect GetScaledRect()
        {
            Rect previousElementRect = GUILayoutUtility.GetLastRect();
            float y = previousElementRect.y;
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
            Rect scale = GUILayoutUtility.GetLastRect();
            scale.y = y;
            scale.height = 15;
            return scale;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Get Tooltip Attribute String
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected string GetTooltipAttributeString(string variableName)
        {
            FieldInfo fieldInfo = Target.GetType().GetField(variableName, REFLECTION_SEARCH_FLAGS);
            if (fieldInfo != null)
            {
                TooltipAttribute[] tooltips = fieldInfo.GetCustomAttributes(typeof(TooltipAttribute), true) as TooltipAttribute[];
                if (tooltips.Length > 0)
                {
                    return tooltips[0].tooltip;
                }
            }
            return string.Empty;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Draw Foldout Option
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected bool DrawFoldoutOption(string label, ref bool showFoldout, string tooltip = null)
        {
            showFoldout = EditorGUILayout.Foldout(showFoldout, (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(label, tooltip) : new GUIContent(label)), EditorStyles.foldout);
            return showFoldout;
        }

        protected bool DrawFoldoutOption(string label, string tooltip = null)
        {
            bool showFoldout;
            if (m_foldoutBooleans.TryGetValue(label, out showFoldout) == false)
            {
                showFoldout = false;
            }

            bool result = EditorGUILayout.Foldout(showFoldout, (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(label, tooltip) : new GUIContent(label)), EditorStyles.foldout);
            if (result != showFoldout)
            {
                m_foldoutBooleans[label] = result;
            }
            return result;
        }

        protected bool DrawFoldoutOption(string label, object owner, string tooltip = null)
        {
            bool showFoldout;
            if (m_foldoutOwners.TryGetValue(owner, out showFoldout) == false)
            {
                showFoldout = false;
            }

            bool result = EditorGUILayout.Foldout(showFoldout, (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(label, tooltip) : new GUIContent(label)), EditorStyles.foldout);
            if (result != showFoldout)
            {
                m_foldoutOwners[owner] = result;
            }
            return result;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Splitter
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected void DrawSplitter()
        {
            DrawBox("", (int)EditorWindowContextWidth, 3, Color.white);
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Box
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected void DrawBox(string textToDisplayInBox, int width, int height, Color? backgroundColour)
        {
            EditorGUILayout.Space();
            Rect previousElementRect = GUILayoutUtility.GetLastRect();
            Rect drawRect = new Rect(previousElementRect.x, previousElementRect.y, width, height);

            if (backgroundColour.HasValue)
            {
                Color[] pix = new Color[width * height];
                for (int i = 0; i < pix.Length; ++i)
                {
                    pix[i] = backgroundColour.Value;
                }
                Texture2D backgroundTexture = new Texture2D(width, height);
                backgroundTexture.SetPixels(pix);
                backgroundTexture.Apply();

                GUIStyle style = new GUIStyle(GUI.skin.box);
                style.normal.background = backgroundTexture;

                GUI.Box(drawRect, "", style);
            }
            else
            {
                GUI.Box(drawRect, "");
            }
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Draw Progress Bar Window
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected void DrawProgressBarWindow(string windowTitle, string oneLineCurrentTask, float progress)
        {
            IsProgressBarWindowActive = true;
            EditorUtility.DisplayProgressBar(windowTitle, oneLineCurrentTask, progress);
        }

        protected void ClearProgressBarWindow()
        {
            IsProgressBarWindowActive = false;
            EditorUtility.ClearProgressBar();
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Draw Enum Field
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected E DrawVanillaEnumField<E>(string name, E currentValue, string tooltip = null) where E : System.Enum
        {
            GUIContent label = (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(name, tooltip) : new GUIContent(name));
            return (E)EditorGUILayout.EnumPopup(label, currentValue);
        }

        protected E DrawEnumField<E>(string name, E currentValue, bool drawInAlphabeticalOrder = true, string tooltip = null, int? firstVisibleTypeInAlphabeticalOrder = null, params E[] typesNotToShow) where E : System.Enum
        {
            GUIContent label = (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(name, tooltip) : new GUIContent(name));

            if (drawInAlphabeticalOrder)
            {
                // Caching Alphabetised Enum so we don't have to keep converting every frame.
                Dictionary<System.Type, string[]> alphabetisedEnumDict;
                if (m_alphabetisedEnumTypes.TryGetValue(name, out alphabetisedEnumDict) == false)
                {
                    alphabetisedEnumDict = new Dictionary<System.Type, string[]>();
                    m_alphabetisedEnumTypes.Add(name, alphabetisedEnumDict);
                }

                string[] sortedAlphabetically;
                if (alphabetisedEnumDict.TryGetValue(typeof(E), out sortedAlphabetically) == false)
                {
                    List<string> normallySortedValues = (System.Enum.GetValues(typeof(E)) as E[]).Select(e => e.ToString()).ToList();
                    List<string> alphabeticallySorted = new List<string>(normallySortedValues.OrderBy(o => o));
                    if (firstVisibleTypeInAlphabeticalOrder.HasValue)
                    {
                        foreach (string enumName in normallySortedValues)
                        {
                            E enumVal = (E)System.Enum.Parse(typeof(E), enumName);
                            int asInt = System.Convert.ToInt32(enumVal);
                            if (asInt == firstVisibleTypeInAlphabeticalOrder.Value)
                            {
                                alphabeticallySorted.Remove(enumVal.ToString());
                                alphabeticallySorted.Insert(0, enumVal.ToString());
                                break;
                            }
                        }
                    }
                    foreach (E type in typesNotToShow)
                    {
                        alphabeticallySorted.Remove(type.ToString());
                    }

                    sortedAlphabetically = alphabeticallySorted.ToArray();
                    alphabetisedEnumDict.Add(typeof(E), sortedAlphabetically);
                }

                int selectedIndex = GetIndexOf(sortedAlphabetically, currentValue.ToString());
                int result = DrawDropdownBox(name, sortedAlphabetically, selectedIndex, false, tooltip);
                if (result != selectedIndex)
                {
                    if (result == -1)
                    {
                        // Invalid Selection!
                        return currentValue;
                    }
                    return (E)System.Enum.Parse(typeof(E), sortedAlphabetically[result]);
                }
                return currentValue;
            }
            else
            {
                List<string> normallySortedValues = (System.Enum.GetValues(typeof(E)) as E[]).Select(e => e.ToString()).ToList();
                if (firstVisibleTypeInAlphabeticalOrder.HasValue && firstVisibleTypeInAlphabeticalOrder.Value >= 0 && firstVisibleTypeInAlphabeticalOrder.Value < normallySortedValues.Count)
                {
                    string swapTempVal = normallySortedValues[firstVisibleTypeInAlphabeticalOrder.Value];
                    normallySortedValues.RemoveAt(firstVisibleTypeInAlphabeticalOrder.Value);
                    normallySortedValues.Insert(0, swapTempVal);
                }

                foreach (E type in typesNotToShow)
                {
                    normallySortedValues.Remove(type.ToString());
                }

                int selectedIndex = normallySortedValues.IndexOf(currentValue.ToString());
                int result = DrawDropdownBox(name, normallySortedValues, selectedIndex, false, tooltip);
                if (result == -1)
                {
                    // Invalid Selection!
                    return currentValue;
                }
                return (E)System.Enum.Parse(typeof(E), normallySortedValues[result]);
            }
        }

        protected FieldModificationResult DrawEnumFieldWithUndoOption<E>(string name, ref E currentValue, bool drawInAlphabeticalOrder = true, string tooltip = null, int? firstVisibleTypeInAlphabeticalOrder = null, params E[] typesNotToShow) where E : System.Enum
        {
            E selectedVal = DrawEnumField(name, currentValue, drawInAlphabeticalOrder, tooltip, firstVisibleTypeInAlphabeticalOrder, typesNotToShow);
            if (selectedVal.Equals(currentValue) == false)
            {
                Undo.RecordObject(Target, $"Changed: {name}");
                currentValue = selectedVal;
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Description Box
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary> Renders text out in a box. Text will automatically be wrapped to fit the size of the Editor Window. </summary>
        /// <param name="description">Text that will be rendered.</param>
        /// <param name="drawColour">Colour of the text. Leave as null to let the system decide.</param>
        protected void DrawDescriptionBox(string description, Color? drawColour = null)
        {
#if UNITY_2019_1_OR_NEWER
            DrawSplitter();
#endif

            // Keep 15% of the Editor context window as padding.
            float acceptableDrawWidth = EditorWindowContextWidth * 0.85f;
            DrawWrappedText(description, acceptableDrawWidth, drawColour);

            AddSpaces(2);

#if UNITY_2019_1_OR_NEWER
            DrawSplitter();
            AddSpaces(1);
#endif
        }

        /// <summary> Draws a description area. Automatically inserts line breaks when text gets too long. </summary>
        /// <param name="text">Text we intend to render.</param>
        /// <param name="drawColour">Which colour will the text render as. Keep as null to let the default system decide.</param>
        protected void DrawFieldDescription(string description, Color? drawColour = null)
        {
            // Keep 15% of the Editor context window as padding.
            float acceptableDrawWidth = (EditorWindowContextWidth - XPositionOfUnityDrawObjectWithLabel) - TextLayoutPadding;
            DrawWrappedText(description, acceptableDrawWidth, drawColour, SimpleTextAlignment.Right);
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Draw Wrapped Text
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary> Finds the positions in the string to insert line breaks to keep the flow of the label nice and smooth. </summary>
        /// <param name="text">Text we intend to render.</param>
        /// <param name="maxDrawWidth">Max Draw Width allowed, leave null to draw 90% width of the editor window.</param>
        /// <param name="drawColour">Which colour will the text render as. Keep as null to let the default system decide.</param>
        /// <param name="textAlignment">The alignment of the displayed text.</param>
        protected void DrawWrappedText(string text, float? maxDrawWidth = null, Color? drawColour = null, SimpleTextAlignment textAlignment = SimpleTextAlignment.Left)
        {
            bool needsToBeCalculated = true;

            if (maxDrawWidth.HasValue == false)
            {
                // 85% Editor Window Width.
                maxDrawWidth = EditorWindowContextWidth * 0.85f;
            }

            WrappedStringInfo wrappedStringData;
            if (m_calculatedWrappedStrings.TryGetValue(text, out wrappedStringData))
            {
                // Already calculated Wrapped String. See if it needs to be recalculated.
                const float RecalculateAfterChangeAmount = 0.01f;
                float sizeDiff = Mathf.Abs(wrappedStringData.editorWidthAtCalcTime - maxDrawWidth.Value);
                if (sizeDiff < RecalculateAfterChangeAmount)
                {
                    // Window has not been resized, or not resized large enough that we need to recalculate.
                    needsToBeCalculated = false;
                }
            }

            // Render Description
            GUIStyle guiStyle = new GUIStyle();
            guiStyle.alignment = TextAnchor.UpperCenter;

            if (drawColour.HasValue == false)
            {
                if (IsUsingUnityProSkin)
                {
                    guiStyle.normal.textColor = new Color32(41, 192, 192, 255);
                }
                else
                {
                    guiStyle.normal.textColor = new Color32(36, 68, 196, 255);
                }
            }
            else
            {
                guiStyle.normal.textColor = drawColour.Value;
            }

            if (needsToBeCalculated)
            {
                List<string> lines = new List<string>();
                int startIndexOfCurrentLine = 0;
                float maxLineSize = 0.0f;

                while (true)
                {
                    int startIndexOfNextLine;
                    float lineWidth;
                    bool isFinalLine;
                    GetNextWrappedLine(text, maxDrawWidth.Value, startIndexOfCurrentLine, guiStyle, out startIndexOfNextLine, out lineWidth, out isFinalLine);

                    if (lineWidth > maxLineSize)
                    {
                        maxLineSize = lineWidth;
                    }

                    if (startIndexOfCurrentLine >= text.Length)
                    {
                        // Finished. We can get here like this if the text has multiple \n\n\n at the end.
                        break;
                    }

                    int charRange = (startIndexOfNextLine - startIndexOfCurrentLine);
                    string line = text.Substring(startIndexOfCurrentLine, charRange);

                    // Replace space at end of line, or newline character if it exists.
                    line = line.TrimEnd(new char[] { '\n', ' ' });

                    if (isFinalLine)
                    {
                        lines.Add(line);
                        break;
                    }
                    else
                    {
                        lines.Add(line + '\n');
                        startIndexOfCurrentLine = startIndexOfNextLine + 1;
                    }
                }

                // Add to dictionary so we don't need to calculate again next frame.
                wrappedStringData = new WrappedStringInfo()
                {
                    editorWidthAtCalcTime = maxDrawWidth.Value,
                    maxLineSize = maxLineSize,
                    wrappedTextLines = lines
                };

                m_calculatedWrappedStrings[text] = wrappedStringData;
            }

            if (wrappedStringData.wrappedTextLines.Count > 0)
            {
                foreach (string line in wrappedStringData.wrappedTextLines)
                {
                    AddSpaces(2);
                    Rect pos = GetScaledRect();

                    if (textAlignment == SimpleTextAlignment.Left)
                    {
                        pos.x = TextLayoutPadding;
                        pos.width = wrappedStringData.maxLineSize - pos.x;
                    }
                    else if (textAlignment == SimpleTextAlignment.Right)
                    {
                        pos.x = (EditorWindowContextWidth - wrappedStringData.maxLineSize) - TextLayoutPadding;
                        pos.width = (EditorWindowContextWidth - pos.x) - TextLayoutPadding;
                    }

                    EditorGUI.LabelField(pos, line, guiStyle);
                }

                AddSpaces(2);
            }
        }

        /// <summary> Returns the Char Index for the next point in the text where we should add a line break to create wrapped text. </summary>
        /// <param name="text">Text we are checking against.</param>
        /// <param name="acceptableDrawWidth">The maximum draw width we are allowing.</param>
        /// <param name="currentCharIndex">The start char index we are looking to find the break point from.</param>
        /// <param name="guiStyle">The GUI Style you intend to draw the label with</param>
        /// <param name="nextLineCharIndex">Returns the next index in the text where the line break should be added.</param>
        /// <param name="totalLineWidth">Returns the total width of this line</param>
        /// <param name="isFinalLine">Returns true if this is the final line</param>
        private void GetNextWrappedLine(string text, float acceptableDrawWidth, int currentCharIndex, GUIStyle guiStyle, out int nextLineCharIndex, out float totalLineWidth, out bool isFinalLine)
        {
            totalLineWidth = 0.0f;
            float lineWidthAtPreviousSpace = 0.0f;
            int lastSpaceIndex = -1;

            isFinalLine = false;

            for (; currentCharIndex < text.Length; ++currentCharIndex)
            {
                if (text[currentCharIndex] == '\n')
                {
                    break;
                }

                // Discovering the Char Size
                string unicodeChar = System.Char.ConvertFromUtf32((int)text[currentCharIndex]);
                Vector2 textDimensions = guiStyle.CalcSize(new GUIContent(unicodeChar));
                float charSize = textDimensions.x;

                totalLineWidth += charSize;
                if (totalLineWidth > acceptableDrawWidth)
                {
                    // Exceeded range. Go back to previous space if it exists. Otherwise just cut off the text here and go to the next line.
                    // This will drop us into the "End Of Line" section below.
                    if (lastSpaceIndex != -1)
                    {
                        currentCharIndex = lastSpaceIndex;
                        totalLineWidth = lineWidthAtPreviousSpace;
                    }
                    else
                    {
                        totalLineWidth -= charSize;
                    }
                    break;
                }
                else if (currentCharIndex == (text.Length - 1))
                {
                    // Final character.
                    isFinalLine = true;
                    ++currentCharIndex;
                    break;
                }
                else
                {
                    // Not yet exceeded range.
                    if (text[currentCharIndex] == ' ')
                    {
                        lastSpaceIndex = currentCharIndex;
                        lineWidthAtPreviousSpace = totalLineWidth;
                    }
                }
            }

            // Start of Next Line Index Found.
            nextLineCharIndex = currentCharIndex;
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Draw String/Text Field
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected string DrawStringField(string name, string currentValue, string tooltip = null)
        {
            GUIContent label = (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(name, tooltip) : new GUIContent(name));
            return EditorGUILayout.TextField(label, currentValue);
        }

        protected void DrawReadonlyStringField(string name, string currentValue, string tooltip = null)
        {
            using (new GUIDisable())
            {
                DrawStringField(name, currentValue, tooltip);
            }
        }

        protected string DrawTextField(string name, string currentValue, string tooltip = null)
        {
            return DrawStringField(name, currentValue, tooltip);
        }

        protected string DrawReadonlyTextField(string name, string currentValue, string tooltip = null)
        {
            using (new GUIDisable())
            {
                return DrawStringField(name, currentValue, tooltip);
            }
        }

        protected string DrawTextArea(string textToDisplay)
        {
            return EditorGUILayout.TextArea(textToDisplay);
        }

        protected FieldModificationResult DrawStringFieldWithUndoOption(string name, ref string currentValue, string tooltip = null)
        {
            string result = DrawStringField(name, currentValue, tooltip);
            if (string.IsNullOrEmpty(result))
            {
                if (string.IsNullOrEmpty(currentValue) == false)
                {
                    // String is no longer empty/null
                    Undo.RecordObject(Target, $"Changed {name}");
                    currentValue = result;
                    return FieldModificationResult.Modified;
                }
            }
            else if (result.Equals(currentValue) == false)
            {
                // Needed the 'if' above because we would get a null ref otherwise if we did an equals check here.
                Undo.RecordObject(Target, $"Changed {name}");
                currentValue = result;
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }

        protected FieldModificationResult DrawTextFieldWithUndoOption(string name, ref string currentValue, string tooltip = null)
        {
            return DrawStringFieldWithUndoOption(name, ref currentValue, tooltip);
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Draw Button
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected bool DrawButtonField(string name, string tooltip = null, ButtonAlignment alignment = ButtonAlignment.FullWidth)
        {
            GUIContent label = (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(name, tooltip) : new GUIContent(name));
            if (alignment == ButtonAlignment.FullWidth)
            {
                return GUILayout.Button(label);
            }

            const float padding = 30.0f;
            EditorGUILayout.Space();
            float contextWidth = EditorWindowContextWidth;
            Rect rect = GetScaledRect();
            rect.height = 18.0f;
            rect.width = contextWidth * 0.5f;
            if (alignment == ButtonAlignment.Left)
            {
                rect.x = padding;
            }
            else if (alignment == ButtonAlignment.Centered)
            {
                rect.x = contextWidth * 0.25f;
            }
            else
            {
                rect.x = (contextWidth * 0.5f) - padding;
            }

            AddSpaces(3);
            return GUI.Button(rect, label);
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Int Field
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected int DrawIntField(string name, int currentValue, string tooltip = null, int? minValue = null, int? maxValue = null)
        {
            GUIContent label = (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(name, tooltip) : new GUIContent(name));
            int result = EditorGUILayout.IntField(label, currentValue);
            if (minValue.HasValue && result < minValue.Value)
            {
                return minValue.Value;
            }
            if (maxValue.HasValue && result > maxValue.Value)
            {
                return maxValue.Value;
            }
            return result;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Float Field
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected float DrawFloatField(string name, float currentValue, string tooltip = null, float? minValue = null, float? maxValue = null)
        {
            GUIContent label = (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(name, tooltip) : new GUIContent(name));
            float result = EditorGUILayout.FloatField(label, currentValue);
            if (minValue.HasValue && result < minValue.Value)
            {
                return minValue.Value;
            }
            if (maxValue.HasValue && result > maxValue.Value)
            {
                return maxValue.Value;
            }
            return result;
        }

        protected FieldModificationResult DrawFloatFieldWithUndoOption(string name, ref float currentValue, string tooltip = null, float? minValue = null, float? maxValue = null)
        {
            float result = DrawFloatField(name, currentValue, tooltip, minValue, maxValue);
            if (result != currentValue)
            {
                Undo.RecordObject(Target, $"Changed {name}");
                currentValue = result;
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Toggle Field / Bool Field
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected bool DrawToggleField(string name, bool currentValue, string tooltip = null, bool useEnumPopup = true)
        {
            GUIContent label = (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(name, tooltip) : new GUIContent(name));
            if (useEnumPopup)
            {
                return ((BooleanState)EditorGUILayout.EnumPopup(label, (currentValue ? BooleanState.TRUE : BooleanState.FALSE)) == BooleanState.TRUE);
            }
            return EditorGUILayout.Toggle(label, currentValue);
        }

        protected FieldModificationResult DrawToggleFieldWithUndoOption(string name, ref bool currentValue, string tooltip = null, bool useEnumPopup = true)
        {
            bool result = DrawToggleField(name, currentValue, tooltip, useEnumPopup);
            if (result != currentValue)
            {
                Undo.RecordObject(Target, $"Changed {name}");
                currentValue = result;
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }

        protected bool DrawBoolField(string name, bool currentValue, string tooltip = null, bool useEnumPopup = true)
        {
            return DrawToggleField(name, currentValue, tooltip, useEnumPopup);
        }

        protected FieldModificationResult DrawBoolFieldWithUndoOption(string name, ref bool currentValue, string tooltip = null, bool useEnumPopup = true)
        {
            return DrawToggleFieldWithUndoOption(name, ref currentValue, tooltip, useEnumPopup);
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Dropdown Field
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected int DrawDropdownBox(string name, string[] options, int currentSelectedIndex, bool drawByAlpheticalOrder = true, string tooltip = null)
        {
            GUIContent label = (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(name, tooltip) : new GUIContent(name));
            if (options.Length == 0)
            {
                return -1;
            }

            if (drawByAlpheticalOrder)
            {
                List<string> sortedOptions = new List<string>();
                sortedOptions.AddRange(options.OrderBy(o => o));
                string[] asSortedArray = sortedOptions.ToArray();
                int currentSelectedSortedIndex = -1;
                if (currentSelectedIndex != -1)
                {
                    currentSelectedSortedIndex = sortedOptions.IndexOf(options[currentSelectedIndex]);
                }
                int result = EditorGUILayout.Popup(label, currentSelectedSortedIndex, asSortedArray);
                if (result == -1)
                {
                    return -1;
                }

                for (int i = 0; i < asSortedArray.Length; ++i)
                {
                    // Still need to return the correct index. All we did was change the layout visually.
                    if (asSortedArray[result].Equals(options[i]))
                    {
                        return i;
                    }
                }
            }

            return EditorGUILayout.Popup(label, currentSelectedIndex, options);
        }

        protected int DrawDropdownBox(string name, string[] options, string currentSelection, bool drawByAlpheticalOrder = true, string tooltip = null)
        {
            int selectedIndex = -1;
            if (string.IsNullOrEmpty(currentSelection) == false)
            {
                int selectionHash = currentSelection.GetHashCode();
                for (int i = 0; i < options.Length; ++i)
                {
                    if (selectionHash == options[i].GetHashCode())
                    {
                        selectedIndex = i;
                        break;
                    }
                }
            }
            return DrawDropdownBox(name, options, selectedIndex, drawByAlpheticalOrder, tooltip);
        }

        protected FieldModificationResult DrawDropdownBoxWithUndoOption(string name, string[] options, ref string currentSelection, bool drawByAlpheticalOrder = true, string tooltip = null)
        {
            int result = DrawDropdownBox(name, options, currentSelection, drawByAlpheticalOrder, tooltip);
            if (result != -1 && options[result].Equals(currentSelection) == false)
            {
                Undo.RecordObject(Target, $"Changed {name}");
                currentSelection = options[result];
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }

        protected int DrawDropdownBox(string name, List<string> options, int currentSelectedIndex, bool drawByAlpheticalOrder = true, string tooltip = null)
        {
            return DrawDropdownBox(name, options.ToArray(), currentSelectedIndex, drawByAlpheticalOrder, tooltip);
        }

        protected int DrawDropdownBox(string name, List<string> options, string currentSelection, bool drawByAlpheticalOrder = true, string tooltip = null)
        {
            int selectedIndex = -1;
            if (string.IsNullOrEmpty(currentSelection) == false)
            {
                int selectionHash = currentSelection.GetHashCode();
                for (int i = 0; i < options.Count; ++i)
                {
                    if (selectionHash == options[i].GetHashCode())
                    {
                        selectedIndex = i;
                        break;
                    }
                }
            }
            return DrawDropdownBox(name, options.ToArray(), selectedIndex, drawByAlpheticalOrder, tooltip);
        }

        protected FieldModificationResult DrawDropdownBoxViaDropdownChoicesObject(string name, out string selectionOutput, DropdownChoicesParameters parameters, string tooltip = null)
        {
            DropdownChoicesData dropdownChoiceData = GetDropdownChoicesWithUsedElementsRemoved(parameters);

            int result = DrawDropdownBox(name, dropdownChoiceData.remainingSelectableChoices, dropdownChoiceData.currentSelectionIndex, false, tooltip);
            if (result == -1)
            {
                selectionOutput = null;
            }
            else
            {
                selectionOutput = dropdownChoiceData.remainingSelectableChoices[result];
            }

            if (result != dropdownChoiceData.currentSelectionIndex)
            {
                // Dropdown Elements need to be recalculated. Remove from Dictionary so it get recalculated when necessary.
                if (m_cachedDropdownChoicesWithUsedElementsRemoved.ContainsKey(parameters.owningObject))
                {
                    m_cachedDropdownChoicesWithUsedElementsRemoved.Remove(parameters.owningObject);
                }
                return FieldModificationResult.Modified;
            }

            return FieldModificationResult.NoChange;
        }

        protected FieldModificationResult DrawDropdownBoxViaDropdownChoicesObject(string name, out int selectedIndex, DropdownChoicesParameters parameters, string tooltip = null)
        {
            DropdownChoicesData dropdownChoiceData = GetDropdownChoicesWithUsedElementsRemoved(parameters);

            int result = DrawDropdownBox(name, dropdownChoiceData.remainingSelectableChoices, dropdownChoiceData.currentSelectionIndex, false, tooltip);

            if (result == -1)
            {
                selectedIndex = -1;
            }
            else
            {
                // Converts from "Selectable Choices" Index to "All Choices" Index.
                selectedIndex = dropdownChoiceData.remainingChoicesIndexToAllChoicesIndex[result];
            }

            if (result != dropdownChoiceData.currentSelectionIndex)
            {
                // Dropdown Elements need to be recalculated. Remove from Dictionary so it get recalculated when necessary.
                if (m_cachedDropdownChoicesWithUsedElementsRemoved.ContainsKey(parameters.owningObject))
                {
                    m_cachedDropdownChoicesWithUsedElementsRemoved.Remove(parameters.owningObject);
                }

                return FieldModificationResult.Modified;
            }

            return FieldModificationResult.NoChange;
        }

        protected FieldModificationResult DrawDropdownBoxViaDropdownChoicesObject(string name, out string selectionOutput, out int selectedIndex, DropdownChoicesParameters parameters, string tooltip = null)
        {
            DropdownChoicesData dropdownChoiceData = GetDropdownChoicesWithUsedElementsRemoved(parameters);

            int result = DrawDropdownBox(name, dropdownChoiceData.remainingSelectableChoices, dropdownChoiceData.currentSelectionIndex, false, tooltip);
            if (result == -1)
            {
                selectionOutput = null;
                selectedIndex = -1;
            }
            else
            {
                // Converts from "Selectable Choices" Index to "All Choices" Index.
                selectionOutput = dropdownChoiceData.remainingSelectableChoices[result];
                selectedIndex = dropdownChoiceData.remainingChoicesIndexToAllChoicesIndex[result];
            }

            if (result != dropdownChoiceData.currentSelectionIndex)
            {
                // Dropdown Elements need to be recalculated. Remove from Dictionary so it get recalculated when necessary.
                if (m_cachedDropdownChoicesWithUsedElementsRemoved.ContainsKey(parameters.owningObject))
                {
                    m_cachedDropdownChoicesWithUsedElementsRemoved.Remove(parameters.owningObject);
                }
                return FieldModificationResult.Modified;
            }

            return FieldModificationResult.NoChange;
        }

        protected int DrawDropdownBoxWithElementsAlreadyUsedRemoved(string name, string[] otherInstancesReferencesOptions, string[] optionsToDisplay, int currentIndex, bool drawByAlphabeticalOrder = true, string tooltip = null)
        {
            int otherInstanceCount = otherInstancesReferencesOptions.Length;
            int optionsCount = optionsToDisplay.Length;

            List<string> remainingElements = new List<string>(optionsToDisplay);
            remainingElements.RemoveAll(optionDisplay =>
            {
                if (string.IsNullOrEmpty(optionDisplay))
                {
                    return true;
                }

                int optionHashCode = optionDisplay.GetHashCode();
                if (currentIndex != -1 && optionHashCode == optionsToDisplay[currentIndex].GetHashCode())
                {
                    return false;
                }

                for (int i = 0; i < otherInstanceCount; ++i)
                {
                    if (string.IsNullOrEmpty(otherInstancesReferencesOptions[i]) == false
                        && otherInstancesReferencesOptions[i].GetHashCode() == optionHashCode)
                    {
                        return true;
                    }
                }
                return false;
            });

            int selectedIndexOfRemainingSelection = 0;
            if (currentIndex != -1)
            {
                selectedIndexOfRemainingSelection = remainingElements.IndexOf(optionsToDisplay[currentIndex]);
            }
            selectedIndexOfRemainingSelection = DrawDropdownBox(name, remainingElements, selectedIndexOfRemainingSelection, drawByAlphabeticalOrder, tooltip);


            // We have selected an index relative to the "Remaining Elements" list. We need to convert the index BACK to the original List's format
            if (selectedIndexOfRemainingSelection != -1 && remainingElements.Count > 0)
            {
                int hashCode = remainingElements[selectedIndexOfRemainingSelection].GetHashCode();
                for (int i = 0; i < optionsCount; ++i)
                {
                    if (optionsToDisplay[i].GetHashCode() == hashCode)
                    {
                        return i;
                    }
                }
            }

            return -1;
        }

        protected int DrawDropdownBoxWithElementsAlreadyUsedRemoved(string name, int[] otherInstancesReferencesOptions, string[] optionsToDisplay, int currentIndex, bool drawByAlphabeticalOrder = true, string tooltip = null)
        {
            int otherInstanceCount = otherInstancesReferencesOptions.Length;
            int optionsCount = optionsToDisplay.Length;

            List<int> sortedIndicesToRemove = new List<int>(otherInstancesReferencesOptions);
            sortedIndicesToRemove.Sort((int a, int b) =>
            {
                if (a < b)
                    return -1;
                if (a == b)
                    return 0;
                return 1;
            });

            List<string> remainingElements = new List<string>(optionsToDisplay);
            for (int i = sortedIndicesToRemove.Count - 1; i > -1; --i)
            {
                if (remainingElements.Count > i)
                {
                    remainingElements.RemoveAt(i);
                }
            }

            int selectedIndexOfRemainingSelection = 0;
            if (currentIndex != -1)
            {
                selectedIndexOfRemainingSelection = remainingElements.IndexOf(optionsToDisplay[currentIndex]);
            }
            selectedIndexOfRemainingSelection = DrawDropdownBox(name, remainingElements, selectedIndexOfRemainingSelection, drawByAlphabeticalOrder, tooltip);


            // We have selected an index relative to the "Remaining Elements" list. We need to convert the index BACK to the original List's format
            if (selectedIndexOfRemainingSelection != -1 && remainingElements.Count > 0)
            {
                int hashCode = remainingElements[selectedIndexOfRemainingSelection].GetHashCode();
                for (int i = 0; i < optionsCount; ++i)
                {
                    if (optionsToDisplay[i].GetHashCode() == hashCode)
                    {
                        return i;
                    }
                }
            }

            return -1;
        }

        protected string DrawDropdownBoxWithElementsAlreadyUsedRemoved(string name, string[] otherInstancesReferencesOptions, string[] optionsToDisplay, string currentVal, bool drawByAlphabeticalOrder = true, string tooltip = null)
        {
            int currentIndex = -1;
            if (string.IsNullOrEmpty(currentVal) == false)
            {
                int count = optionsToDisplay.Length;
                for (int i = 0; i < count; ++i)
                {
                    if (optionsToDisplay[i].Equals(currentVal))
                    {
                        currentIndex = i;
                        break;
                    }
                }
            }

            int result = DrawDropdownBoxWithElementsAlreadyUsedRemoved(name, otherInstancesReferencesOptions, optionsToDisplay, currentIndex, drawByAlphabeticalOrder, tooltip);
            if (result != -1)
            {
                return optionsToDisplay[result];
            }
            return currentVal;
        }

        protected FieldModificationResult DrawDropdownBoxWithElementsAlreadyUsedRemovedWithUndoOption(string name, int[] otherInstancesReferencesOptions, string[] optionsToDisplay, ref int currentVal, bool drawByAlphabeticalOrder = true, string tooltip = null)
        {
            int result = DrawDropdownBoxWithElementsAlreadyUsedRemoved(name, otherInstancesReferencesOptions, optionsToDisplay, currentVal, drawByAlphabeticalOrder, tooltip);
            if (result != currentVal)
            {
                Undo.RecordObject(Target, $"Changed {name}");
                currentVal = result;
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }

        protected FieldModificationResult DrawDropdownBoxWithElementsAlreadyUsedRemovedWithUndoOption(string name, string[] otherInstancesReferencesOptions, string[] optionsToDisplay, ref string currentVal, bool drawByAlphabeticalOrder = true, string tooltip = null)
        {
            string result = DrawDropdownBoxWithElementsAlreadyUsedRemoved(name, otherInstancesReferencesOptions, optionsToDisplay, currentVal, drawByAlphabeticalOrder, tooltip);
            if (result.Equals(currentVal) == false)
            {
                Undo.RecordObject(Target, $"Changed {name}");
                currentVal = result;
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }

        protected List<int> DrawMultipleDropdownBoxesOneLine(string name, List<string[]> dropdownOptions, List<int> selectedIndices, List<string> tooltips = null, List<bool> drawInAlphabeticalOrders = null)
        {
            List<int> results = new List<int>();
            if (dropdownOptions == null)
            {
                // Nothing to draw.
                return results;
            }
            int dropdownsCount = dropdownOptions.Count;
            if (dropdownsCount == 0)
            {
                // Nothing to draw.
                return results;
            }
            if (dropdownsCount == 1)
            {
                // Just use the default draw method
                string tooltip = tooltips != null && tooltips.Count > 0 ? tooltips[0] : "";
                bool drawInAlphabeticalOrder = drawInAlphabeticalOrders != null && drawInAlphabeticalOrders.Count > 0 ? drawInAlphabeticalOrders[0] : true;
                int currentSelectedIndex = selectedIndices != null && selectedIndices.Count > 0 ? selectedIndices[0] : -1;
                int selectedIndex = DrawDropdownBox(name, dropdownOptions[0], currentSelectedIndex, drawInAlphabeticalOrder, tooltip);
                results.Add(selectedIndex);
                return results;
            }

            DrawLabel(name);
            const float pixelSeparationPerBox = 0.0f;
            float totalDrawWidth = DrawValueWidth;
            float drawWidthPerBox = (totalDrawWidth / dropdownsCount) - pixelSeparationPerBox;

            float startDrawXPos = StartDrawValueXPos; // From Center of Window, same value as totalDrawWidth
            Rect linePos = GUILayoutUtility.GetLastRect();

            for (int i = 0; i < dropdownsCount; ++i)
            {
                string tooltip = tooltips != null && tooltips.Count > i ? tooltips[i] : "";
                bool drawInAlphabeticalOrder = drawInAlphabeticalOrders != null && drawInAlphabeticalOrders.Count > i ? drawInAlphabeticalOrders[i] : true;
                int currentSelectedIndex = selectedIndices != null && selectedIndices.Count > i ? selectedIndices[i] : -1;
                string[] options = dropdownOptions[i];

                Rect drawRect = new Rect(x: startDrawXPos + ((drawWidthPerBox + pixelSeparationPerBox) * i),
                                         y: linePos.y,
                                         width: drawWidthPerBox,
                                         height: linePos.height);

                GUIContent label = (string.IsNullOrEmpty(tooltip) == false ? new GUIContent("", tooltip) : new GUIContent(""));
                if (options.Length > 0)
                {
                    if (drawInAlphabeticalOrder)
                    {
                        List<string> sortedOptions = new List<string>();
                        sortedOptions.AddRange(options.OrderBy(o => o));
                        GUIContent[] asSortedArray = sortedOptions.Select(o => new GUIContent(o)).ToArray();
                        int currentSelectedSortedIndex = -1;
                        if (currentSelectedIndex != -1)
                        {
                            currentSelectedSortedIndex = sortedOptions.IndexOf(options[currentSelectedIndex]);
                        }
                        int selectedResult = EditorGUI.Popup(drawRect, label, currentSelectedIndex, asSortedArray);
                        if (selectedResult != -1)
                        {
                            for (int j = 0; j < asSortedArray.Length; ++j)
                            {
                                // Still need to return the correct index. All we did was change the layout visually.
                                if (asSortedArray[selectedResult].text.Equals(options[j]))
                                {
                                    selectedResult = j;
                                    break;
                                }
                            }
                        }
                        results.Add(selectedResult);
                    }
                    else
                    {
                        GUIContent[] asArray = options.Select(o => new GUIContent(o)).ToArray();
                        int selectedResult = EditorGUI.Popup(drawRect, label, currentSelectedIndex, asArray);
                        results.Add(selectedResult);
                    }
                }
                else
                {
                    // No options available to show...
                    int selectedResult = EditorGUI.Popup(drawRect, label, currentSelectedIndex, new GUIContent[0]);
                    results.Add(selectedResult);
                }
            }

            return results;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Multiple Buttons One Line
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected void DrawMultipleButtonsOneLine(List<ButtonsInfo> buttonsList, float? startXPosition = null, float endXPadding = 15.0f, float spaceBetweenButtons = 5.0f)
        {
            if (buttonsList == null)
            {
                // Nothing to draw.
                return;
            }
            int buttonsCount = buttonsList.Count;
            if (buttonsCount == 0)
            {
                // Nothing to draw.
                return;
            }

            if (buttonsCount == 1)
            {
                // Just use the default draw method
                if (DrawButtonField(buttonsList[0].buttonName, buttonsList[0].tooltip))
                {
                    buttonsList[0].onClicked?.Invoke();
                }
                return;
            }

            float startDrawXPos = startXPosition ?? StartDrawValueXPos; // From Center of Window, same value as totalDrawWidth
            float totalDrawWidth = EditorWindowContextWidth - startDrawXPos - endXPadding;
            float drawWidthPerBox = (totalDrawWidth / buttonsCount) - spaceBetweenButtons;
            Rect linePos = GUILayoutUtility.GetLastRect();

            for (int i = 0; i < buttonsCount; ++i)
            {
                Rect drawRect = new Rect(x: startDrawXPos + ((drawWidthPerBox + spaceBetweenButtons) * i),
                                         y: linePos.y,
                                         width: drawWidthPerBox,
                                         height: 20);

                ButtonsInfo buttonInfo = buttonsList[i];
                GUIContent label = (string.IsNullOrEmpty(buttonInfo.tooltip) == false ? new GUIContent(buttonInfo.buttonName, buttonInfo.tooltip) : new GUIContent(buttonInfo.buttonName));
                if (GUI.Button(drawRect, label))
                {
                    buttonInfo.onClicked?.Invoke();
                }
            }

            AddSpaces(3);
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Date Selection
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected System.DateTime DrawDateSelection(string name, System.DateTime currentVal, int firstSelectableYear = 2019, int finalSelectableYear = 2050)
        {
            int selectedYear = Mathf.Clamp(currentVal.Year, firstSelectableYear, finalSelectableYear);
            int selectedMonth = currentVal.Month;

            int yearSelectionRange = finalSelectableYear - firstSelectableYear;

            string[] yearsOptions = new string[yearSelectionRange];
            for (int yearIndex = 0; yearIndex < yearSelectionRange; ++yearIndex)
            {
                yearsOptions[yearIndex] = (firstSelectableYear + yearIndex).ToString();
            }

            string[] monthOptions = new string[12] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

            int daysInMonth = System.DateTime.DaysInMonth(selectedYear, selectedMonth);
            string[] dayOptions = new string[daysInMonth];
            for (int dayIndex = 0; dayIndex < daysInMonth; ++dayIndex)
            {
                System.DateTime day = new System.DateTime(selectedYear, selectedMonth, dayIndex + 1);
                if (dayIndex < 9)
                {
                    dayOptions[dayIndex] = day.ToString("ddd") + "- 0" + (dayIndex + 1).ToString();
                }
                else
                {
                    dayOptions[dayIndex] = day.ToString("ddd") + "- " + (dayIndex + 1).ToString();
                }
            }

            List<string[]> dropdownChoices = new List<string[]>() { dayOptions, monthOptions, yearsOptions };
            List<int> selectedChoiceIndices = new List<int>() { currentVal.Day - 1, selectedMonth - 1, selectedYear - firstSelectableYear };
            List<bool> drawInAlphabeticalOrder = new List<bool>() { false, false, false };

            List<int> chosenOutput = DrawMultipleDropdownBoxesOneLine(name, dropdownChoices, selectedChoiceIndices, null, drawInAlphabeticalOrder);

            selectedYear = chosenOutput[2] + firstSelectableYear;
            selectedMonth = chosenOutput[1] + 1;
            daysInMonth = System.DateTime.DaysInMonth(selectedYear, selectedMonth);
            int selectedDay = Mathf.Clamp(chosenOutput[0] + 1, 1, daysInMonth);
            System.DateTime result = new System.DateTime(selectedYear, selectedMonth, selectedDay);
            return result;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Draw Vector 2 Field
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected Vector2 DrawVectorField(string name, Vector2 currentValue, string tooltip = null)
        {
            GUIContent label = (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(name, tooltip) : new GUIContent(name));
            return EditorGUILayout.Vector2Field(label, currentValue);
        }

        protected FieldModificationResult DrawVectorFieldWithUndoOption(string name, ref Vector2 currentValue, string tooltip = null)
        {
            Vector2 result = DrawVectorField(name, currentValue, tooltip);
            if (result != currentValue)
            {
                Undo.RecordObject(Target, $"Changed {name}");
                currentValue = result;
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }

        protected void DrawReadonlyVectorField(string name, Vector2 currentValue, string tooltip = null)
        {
            using (new GUIDisable())
            {
                DrawVectorField(name, currentValue, tooltip);
            }
        }

        protected Vector2Int DrawVectorField(string name, Vector2Int currentValue, string tooltip = null)
        {
            GUIContent label = (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(name, tooltip) : new GUIContent(name));
            return EditorGUILayout.Vector2IntField(label, currentValue);
        }

        protected FieldModificationResult DrawVectorFieldWithUndoOption(string name, ref Vector2Int currentValue, string tooltip = null)
        {
            Vector2Int result = DrawVectorField(name, currentValue, tooltip);
            if (result != currentValue)
            {
                Undo.RecordObject(Target, $"Changed {name}");
                currentValue = result;
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }

        protected void DrawReadonlyVectorField(string name, Vector2Int currentValue, string tooltip = null)
        {
            using (new GUIDisable())
            {
                DrawVectorField(name, currentValue, tooltip);
            }
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Draw Vector 3 Field
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected Vector3 DrawVectorField(string name, Vector3 currentValue, string tooltip = null)
        {
            GUIContent label = (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(name, tooltip) : new GUIContent(name));
            return EditorGUILayout.Vector3Field(label, currentValue);
        }

        protected FieldModificationResult DrawVectorFieldWithUndoOption(string name, ref Vector3 currentValue, string tooltip = null)
        {
            Vector3 result = DrawVectorField(name, currentValue, tooltip);
            if (result != currentValue)
            {
                Undo.RecordObject(Target, $"Changed {name}");
                currentValue = result;
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }

        protected void DrawReadonlyVectorField(string name, Vector3 currentValue, string tooltip = null)
        {
            using (new GUIDisable())
            {
                DrawVectorField(name, currentValue, tooltip);
            }
        }

        protected Vector3Int DrawVectorField(string name, Vector3Int currentValue, string tooltip = null)
        {
            GUIContent label = (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(name, tooltip) : new GUIContent(name));
            return EditorGUILayout.Vector3IntField(label, currentValue);
        }

        protected FieldModificationResult DrawVectorFieldWithUndoOption(string name, ref Vector3Int currentValue, string tooltip = null)
        {
            Vector3Int result = DrawVectorField(name, currentValue, tooltip);
            if (result != currentValue)
            {
                Undo.RecordObject(Target, $"Changed {name}");
                currentValue = result;
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }

        protected void DrawReadonlyVectorField(string name, Vector3Int currentValue, string tooltip = null)
        {
            using (new GUIDisable())
            {
                DrawVectorField(name, currentValue, tooltip);
            }
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Draw Vector 4 Field
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected Vector4 DrawVectorField(string name, Vector4 currentValue, string tooltip = null)
        {
            GUIContent label = (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(name, tooltip) : new GUIContent(name));
            return EditorGUILayout.Vector4Field(label, currentValue);
        }

        protected FieldModificationResult DrawVectorFieldWithUndoOption(string name, ref Vector4 currentValue, string tooltip = null)
        {
            Vector4 result = DrawVectorField(name, currentValue, tooltip);
            if (result != currentValue)
            {
                Undo.RecordObject(Target, $"Changed {name}");
                currentValue = result;
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }

        protected void DrawReadonlyVectorField(string name, Vector4 currentValue, string tooltip = null)
        {
            using (new GUIDisable())
            {
                DrawVectorField(name, currentValue, tooltip);
            }
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Draw LocId Options
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected LocIDs DrawLocIdOption(string headerName, LocIDs currentLocId, string tooltip = null)
        {
            LocManager.LanguageStrings[] languageStrings = LanguageStrings;

            DrawLabel(headerName);
            using (new EditorIndentation(2))
            {
                currentLocId = (LocIDs)DrawChangeableIntegerOption("CSV Line No:", (int)currentLocId, tooltip: tooltip, indentLabel: false, minValue: 0, maxValue: languageStrings.Length);
                try
                {
                    currentLocId = DrawEnumField("Loc ID:", currentLocId);
                }
                catch
                {
                    DrawTextField("Loc ID:", "No Loc ID available");
                }

                int locIndex = (int)currentLocId - 1;
                if (locIndex > 0)
                {
                    string locOutput = languageStrings[locIndex].Get(LocLanguages.English);
                    if (string.IsNullOrEmpty(locOutput))
                    {
                        locOutput = "MISSING!";
                    }
                    DrawTextField("English Output: ", locOutput);
                }
                else
                {
                    DrawTextField("English Output: ", "N/A");
                }
            }
            return currentLocId;
        }

        protected int DrawLocIdOption(string headerName, int currentCSVLine, string tooltip = null)
        {
            return (int)DrawLocIdOption(headerName, (LocIDs)currentCSVLine, tooltip);
        }

        protected FieldModificationResult DrawLocIdOptionWithUndoOption(string headerName, ref LocIDs currentValue, UnityEngine.Object targetObj = null, string tooltip = null)
        {
            LocIDs result = DrawLocIdOption(headerName, currentValue, tooltip);
            if (result != currentValue)
            {
                Undo.RecordObject(targetObj ?? Target, $"Changed {headerName}");
                currentValue = result;
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }

        protected FieldModificationResult DrawLocIdOptionWithUndoOption(string headerName, ref int currentValue, UnityEngine.Object targetObj = null, string tooltip = null)
        {
            int result = DrawLocIdOption(headerName, currentValue, tooltip);
            if (result != currentValue)
            {
                Undo.RecordObject(targetObj ?? Target, $"Changed {headerName}");
                currentValue = result;
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Draw Object Option
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected T DrawObjectOption<T>(string name, T obj, string tooltip = null) where T : UnityEngine.Object
        {
            GUIContent label;
            if (string.IsNullOrEmpty(tooltip) == false)
            {
                label = new GUIContent(name, tooltip);
            }
            else
            {
                label = new GUIContent(name);
            }

            return DrawObjectOption(label, obj);
        }

        protected FieldModificationResult DrawObjectOptionWithUndoOption<T>(string name, ref T obj, string tooltip = null) where T : UnityEngine.Object
        {
            T result = DrawObjectOption(name, obj, tooltip);
            if (result != obj)
            {
                Undo.RecordObject(Target, $"Changed {name}");
                obj = result;
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }

        protected void DrawReadonlyObjectOption<T>(string name, T obj, string tooltip = null) where T : UnityEngine.Object
        {
            using (new GUIDisable())
            {
                DrawObjectOption(name, obj, tooltip);
            }
        }

        protected T DrawObjectOption<T>(string name, T obj, string tooltipWhenAssignedAnObject, string tooltipWhenNoObjectIsAssigned) where T : UnityEngine.Object
        {
            GUIContent label = null;
            if (obj != null)
            {
                if (string.IsNullOrEmpty(tooltipWhenAssignedAnObject) == false)
                {
                    label = new GUIContent(name, tooltipWhenAssignedAnObject);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(tooltipWhenNoObjectIsAssigned) == false)
                {
                    label = new GUIContent(name, tooltipWhenNoObjectIsAssigned);
                }
                else if (string.IsNullOrEmpty(tooltipWhenAssignedAnObject) == false)
                {
                    label = new GUIContent(name, tooltipWhenAssignedAnObject + "\n\nThis object is required. You must assign something here!");
                }
            }

            if (label == null)
            {
                label = new GUIContent(name);
            }

            return DrawObjectOption(label, obj);
        }

        protected T DrawObjectOption<T>(GUIContent display, T obj) where T : UnityEngine.Object
        {
            // Can't do Template Specialization in C#. So have to do this hacky copy-paste job instead. If it's a Sprite show all text on the same line as Sprite
            //  Since Unity now shows the sprites in the inspector rather than the object box (like it used to).
            if (typeof(T) == typeof(Sprite))
            {
                T val = (T)EditorGUILayout.ObjectField(" ", obj, typeof(T), true);
                Rect spriteRect = GUILayoutUtility.GetLastRect();
                Rect drawPosition = new Rect(4.4f * (EditorGUI.indentLevel + 1), spriteRect.y, 300, spriteRect.height);
                EditorGUI.LabelField(drawPosition, display);
                return val;
            }
            else
            {
                return (T)EditorGUILayout.ObjectField(display, obj, typeof(T), true);
            }
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Audio Clip Option
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected AudioClip DrawAudioClipOption(string name, AudioClip audioClip, string tooltip = "")
        {
            return DrawAudioClipOption(name, audioClip, null, tooltip);
        }

        protected FieldModificationResult DrawAudioClipWithUndoOption(string name, ref AudioClip audioClip, string tooltip = "")
        {
            AudioClip result = DrawAudioClipOption(name, audioClip, tooltip);
            if (result != audioClip)
            {
                Undo.RecordObject(Target, $"Changed {name}");
                audioClip = result;
                return FieldModificationResult.Modified;
            }

            return FieldModificationResult.NoChange;
        }

        protected AudioClip DrawAudioClipOption(string name, AudioClip audioClip, AudioSource audioSource = null, string tooltip = "")
        {
            AudioClip returningValue = DrawObjectOption(name, audioClip, tooltip);
            if (returningValue != null)
            {
                if (audioSource == null)
                {
                    audioSource = ActiveAudioSource;
                    if (audioSource == null)
                    {
                        DrawWrappedText("No Audio Sources found in scene");
                        return returningValue;
                    }
                }

                if (audioSource.isPlaying)
                {
                    List<ButtonsInfo> buttons = new List<ButtonsInfo>()
                {
                    new ButtonsInfo()
                    {
                        buttonName = audioSource.clip == returningValue ? "Restart" : "Play",
                        onClicked = () =>
                        {
                            audioSource.Stop();
                            audioSource.clip = returningValue;
                            if (returningValue != null)
                                audioSource.Play();
                        },
                        tooltip = "Plays the associated sound",
                    },
                     new ButtonsInfo()
                    {
                        buttonName = "Stop",
                        onClicked = () => audioSource.Stop(),
                        tooltip = "Stops the associated sound",
                    },
                };
                    AddSpaces(1);
                    DrawMultipleButtonsOneLine(buttons, startXPosition: EditorWindowContextWidth * 0.5f);
                }
                else
                {
                    if (DrawButtonField("Play", "Plays the associated sound", ButtonAlignment.Right))
                    {
                        audioSource.clip = returningValue;
                        audioSource.Play();
                    }
                }

                AddSpaces(3);
            }
            return returningValue;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Label
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected void DrawLabel(string text, bool bUseCustomColour = false)
        {
            if (bUseCustomColour)
                DrawLabel(text, IsSecondaryComponent() ? SecondaryFontColour : MainFontColour);
            else
                EditorGUILayout.LabelField(text);
        }

        protected void DrawBoldLabel(string text)
        {
            EditorGUILayout.LabelField(text, EditorStyles.boldLabel);
        }

        protected void DrawLabel(string text, GUIStyle s)
        {
            EditorGUILayout.LabelField(text, s);
        }

        protected void DrawLabel(string text, Color colour)
        {
            GUIStyle s = new GUIStyle();
            s.normal.textColor = colour;
            DrawLabel(text, s);
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Scene Label
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected void DrawSceneLabel(string text, Vector3 worldPosition, int fontSize = 12)
        {
            GUIStyle style = new GUIStyle();
            style.alignment = TextAnchor.MiddleCenter;
            style.fontSize = fontSize;
            style.fontStyle = FontStyle.Bold;
            Handles.Label(worldPosition, text, style);
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Changeable Number Option (INT)
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected int DrawChangeableIntegerOption(string label, int currentNumber, int changingAmount = 1, string tooltip = null, bool indentLabel = false, int? minValue = null, int? maxValue = null)
        {
            if (string.IsNullOrEmpty(label) == false)
            {
                if (indentLabel)
                {
                    using (new EditorIndentation())
                    {
                        EditorGUILayout.LabelField(new GUIContent(label, tooltip));
                    }
                }
                else
                {
                    EditorGUILayout.LabelField(new GUIContent(label, tooltip));
                }
            }
            return DrawChangeableIntegerOption(GetScaledRect(), "", currentNumber, changingAmount, "", false, minValue, maxValue);
        }

        protected FieldModificationResult DrawChangeableIntegerWithUndoOption(string label, ref int currentNumber, int changingAmount = 1, string tooltip = null, bool indentLabel = false, int? minValue = null, int? maxValue = null)
        {
            int result = DrawChangeableIntegerOption(label, currentNumber, changingAmount, tooltip, indentLabel, minValue, maxValue);
            if (result != currentNumber)
            {
                Undo.RecordObject(Target, $"Changed {label}");
                currentNumber = result;
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }

        protected int DrawChangeableIntegerOption(Rect drawPos, string label, int curentNumber, int changingAmount = 1, string tooltip = null, bool indentLabel = false, int? minValue = null, int? maxValue = null)
        {
            if (string.IsNullOrEmpty(label) == false)
            {
                if (indentLabel)
                {
                    using (new EditorIndentation())
                    {
                        EditorGUILayout.LabelField(new GUIContent(label, tooltip));
                    }
                }
                else
                {
                    EditorGUILayout.LabelField(new GUIContent(label, tooltip));
                }
            }

            // Button Width
            float bw = 20;

            // Text Box width
            int currentNumOfDigits = (int)Mathf.Floor(Mathf.Log10(curentNumber) + 1);
            float sizePerDigit = 9.0f;

            float tw = Mathf.Max(30.0f, currentNumOfDigits * sizePerDigit);
            Rect pos = drawPos;
            pos.x = StartDrawValueXPos;
            pos.width = bw;
            if (GUI.Button(pos, "<"))
            {
                curentNumber -= changingAmount;
            }

            pos.x += pos.width + 5;
            pos.width = tw;
            using (new SetEditorIndentation(0))
            {
                curentNumber = int.Parse(EditorGUI.TextField(pos, curentNumber.ToString()));
            }

            pos.x += pos.width + 5;
            pos.width = bw;
            if (GUI.Button(pos, ">"))
            {
                curentNumber += changingAmount;
            }

            if (minValue.HasValue && curentNumber < minValue.Value)
            {
                curentNumber = minValue.Value;
            }

            if (maxValue.HasValue && curentNumber > maxValue.Value)
            {
                curentNumber = maxValue.Value;
            }

            return curentNumber;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Draw Changeable Number Option (FLOAT)
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected float DrawChangeableFloatOption(string label, float currentNumber, float changingAmount = 0.1f, string tooltip = null, bool indentLabel = false, float? minValue = null, float? maxValue = null)
        {
            if (string.IsNullOrEmpty(label) == false)
            {
                if (indentLabel)
                {
                    using (new EditorIndentation())
                    {
                        EditorGUILayout.LabelField(new GUIContent(label, tooltip));
                    }
                }
                else
                {
                    EditorGUILayout.LabelField(new GUIContent(label, tooltip));
                }
            }
            return DrawChangeableFloatOption(GetScaledRect(), "", currentNumber, changingAmount, "", false, minValue, maxValue);
        }

        protected FieldModificationResult DrawChangeableFloatOptionWithUndoOption(string label, ref float currentNumber, float changingAmount = 0.1f, string tooltip = null, bool indentLabel = false, float? minValue = null, float? maxValue = null)
        {
            float result = DrawChangeableFloatOption(label, currentNumber, changingAmount, tooltip, indentLabel, minValue, maxValue);
            if (result != currentNumber)
            {
                Undo.RecordObject(Target, $"Changed {name}");
                currentNumber = result;
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }

        protected float DrawChangeableFloatOption(Rect drawPos, string label, float curentNumber, float changingAmount = 0.1f, string tooltip = null, bool indentLabel = false, float? minValue = null, float? maxValue = null)
        {
            if (string.IsNullOrEmpty(label) == false)
            {
                if (indentLabel)
                {
                    using (new EditorIndentation())
                    {
                        EditorGUILayout.LabelField(new GUIContent(label, tooltip));
                    }
                }
                else
                {
                    EditorGUILayout.LabelField(new GUIContent(label, tooltip));
                }
            }
            float bw = 20; // Button Width
            float tw = 50; // Text Box width
            Rect pos = drawPos;
            pos.x = StartDrawValueXPos;
            pos.width = bw;
            if (GUI.Button(pos, "<"))
            {
                curentNumber -= changingAmount;
            }

            pos.x += pos.width + 5;
            pos.width = tw;

            using (new SetEditorIndentation(0))
            {
                curentNumber = float.Parse(EditorGUI.TextField(pos, curentNumber.ToString()));
            }

            pos.x += pos.width + 5;
            pos.width = bw;
            if (GUI.Button(pos, ">"))
            {
                curentNumber += changingAmount;
            }

            if (minValue.HasValue && curentNumber < minValue.Value)
            {
                curentNumber = minValue.Value;
            }
            if (maxValue.HasValue && curentNumber > maxValue.Value)
            {
                curentNumber = maxValue.Value;
            }

            return curentNumber;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Draw Range
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected float DrawRange(string name, float currentValue, float min = 0.0f, float max = 1.0f, string tooltip = null)
        {
            GUIContent label = (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(name, tooltip) : new GUIContent(name));
            return EditorGUILayout.Slider(label, currentValue, min, max);
        }

        protected int DrawRange(string name, int currentValue, int min = -1000, int max = 1000, string tooltip = null)
        {
            GUIContent label = (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(name, tooltip) : new GUIContent(name));
            return (int)EditorGUILayout.Slider(label, (float)currentValue, (float)min, (float)max);
        }

        protected FieldModificationResult DrawRangeWithUndoOption(string name, ref float currentValue, float min = 0.0f, float max = 1.0f, string tooltip = null)
        {
            float result = DrawRange(name, currentValue, min, max, tooltip);
            if (result != currentValue)
            {
                Undo.RecordObject(Target, $"Modified {name}");
                currentValue = result;
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }

        protected FieldModificationResult DrawRangeWithUndoOption(string name, ref int currentValue, int min = -1000, int max = 1000, string tooltip = null)
        {
            int result = DrawRange(name, currentValue, min, max, tooltip);
            if (result != currentValue)
            {
                Undo.RecordObject(Target, $"Modified {name}");
                currentValue = result;
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }

        protected float DrawFloatRange(string name, float currentValue, float min = 0.0f, float max = 1.0f, string tooltip = null)
        {
            return DrawRange(name, currentValue, min, max, tooltip);
        }

        protected FieldModificationResult DrawFloatRangeWithUndoOption(string name, ref float currentValue, float min = 0.0f, float max = 1.0f, string tooltip = null)
        {
            return DrawRangeWithUndoOption(name, ref currentValue, min, max, tooltip);
        }

        protected int DrawIntRange(string name, int currentValue, int min = -1000, int max = 1000, string tooltip = null)
        {
            return DrawRange(name, currentValue, min, max, tooltip);
        }

        protected FieldModificationResult DrawIntRangeWithUndoOption(string name, ref int currentValue, int min = -1000, int max = 1000, string tooltip = null)
        {
            return DrawRangeWithUndoOption(name, ref currentValue, min, max, tooltip);
        }

        protected float DrawSliderRange(string name, float currentValue, float min = 0.0f, float max = 1.0f, string tooltip = null)
        {
            return DrawRange(name, currentValue, min, max, tooltip);
        }

        protected float DrawSliderRange(string name, int currentValue, int min = -1000, int max = 1000, string tooltip = null)
        {
            return DrawRange(name, currentValue, min, max, tooltip);
        }

        protected FieldModificationResult DrawSliderRangeWithUndoOption(string name, ref float currentValue, float min = 0.0f, float max = 1.0f, string tooltip = null)
        {
            return DrawRangeWithUndoOption(name, ref currentValue, min, max, tooltip);
        }

        protected FieldModificationResult DrawSliderRangeWithUndoOption(string name, ref int currentValue, int min = -1000, int max = 1000, string tooltip = null)
        {
            return DrawRangeWithUndoOption(name, ref currentValue, min, max, tooltip);
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Draw Plus/Minus Buttons
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected PlusMinusButtons DrawPlusMinusButtons(float yOffset = 20.0f)
        {
            const float buttonWidth = 60;
            const float spacePixels = 5;
            const float offsetPosition = (buttonWidth * 2.0f) + spacePixels;

            Rect pos = GetScaledRect();
            pos.x = ((pos.x + pos.width) - offsetPosition);
            pos.y += yOffset;
            pos.width = buttonWidth;

            EditorGUILayout.Space();

            if (GUI.Button(pos, "+"))
            {
                return PlusMinusButtons.PlusPressed;
            }

            pos.x += pos.width + spacePixels;
            if (GUI.Button(pos, "-"))
            {
                return PlusMinusButtons.MinusPressed;
            }

            return PlusMinusButtons.None;
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Animation Curve
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected AnimationCurve DrawCurve(string name, ref AnimationCurve curve, string tooltip = null)
        {
            GUIContent label = (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(name, tooltip) : new GUIContent(name));
            return EditorGUILayout.CurveField(label, curve);
        }

        protected FieldModificationResult DrawCurveWithUndoOption(string name, ref AnimationCurve currentCurve, string tooltip = null)
        {
            AnimationCurve result = DrawCurve(name, ref currentCurve, tooltip);
            if (result != currentCurve)
            {
                Undo.RecordObject(Target, $"Modified {name}");
                currentCurve = result;
                return FieldModificationResult.Modified;
            }
            return FieldModificationResult.NoChange;
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Serialized Object Options
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected void DrawSerialisedObjectOptions(string name, SerializedProperty property, string tooltip = null)
        {
            serializedObject.Update();
            EditorGUI.BeginChangeCheck();
            GUIContent label = (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(name, tooltip) : new GUIContent(name));
            EditorGUILayout.PropertyField(property, label, true);
            if (EditorGUI.EndChangeCheck())
            {
                serializedObject.ApplyModifiedProperties();
            }
        }

        protected void DrawSerialisedObjectOptions(string name, string variableName, string tooltip = null)
        {
            SerializedProperty property = serializedObject.FindProperty(variableName);
            DrawSerialisedObjectOptions(name, property, tooltip);
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Array Options
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected void DrawSerialisedArrayOptions(string name, string arrayVariableName, string tooltip = null)
        {
            serializedObject.Update();
            SerializedProperty property = serializedObject.FindProperty(arrayVariableName);
            EditorGUI.BeginChangeCheck();
            GUIContent label = (string.IsNullOrEmpty(tooltip) == false ? new GUIContent(name, tooltip) : new GUIContent(name));
            EditorGUILayout.PropertyField(property, label, true);
            if (EditorGUI.EndChangeCheck())
            {
                serializedObject.ApplyModifiedProperties();
                OnArrayModification(arrayVariableName);
            }
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Array Header - Struct Type
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected bool DrawArrayHeader<T>(string arrayDisplayname, ref T[] array, bool drawAsFoldoutOption = true, int numberOfElementsExpected = -1) where T : struct
        {
            if (array == null)
            {
                array = new T[1];
            }

            bool retShowArrayContents;
            ArrayTypeDef<T> definition = array;
            definition = ResolveArrayHeader(arrayDisplayname, definition, numberOfElementsExpected, drawAsFoldoutOption, out retShowArrayContents);
            array = definition.storedArray;
            return retShowArrayContents;
        }

        protected bool DrawArrayHeader<T>(string arrayDisplayname, ref List<T> list, bool drawAsFoldoutOption = true, int numberOfElementsExpected = -1) where T : struct
        {
            if (list == null)
            {
                list = new List<T>();
            }

            bool retShowArrayContents;
            ArrayTypeDef<T> definition = list;
            definition = ResolveArrayHeader(arrayDisplayname, definition, numberOfElementsExpected, drawAsFoldoutOption, out retShowArrayContents);
            return retShowArrayContents;
        }

        private ArrayTypeDef<T> ResolveArrayHeader<T>(string arrayDisplayname, ArrayTypeDef<T> arrayDef, int numberOfElementsExpected,
                                                                                bool drawAsFoldoutOption, out bool retShowArrayContents) where T : struct
        {
            retShowArrayContents = true;
            if (drawAsFoldoutOption)
            {
                retShowArrayContents = DrawFoldoutOption(arrayDisplayname);
            }
            else
            {
                DrawLabel(arrayDisplayname);
            }

            if (arrayDef.Length < numberOfElementsExpected)
            {
                if (arrayDef.storedArray != null)
                {
                    ResizeArray(ref arrayDef.storedArray, numberOfElementsExpected);
                }
                else
                {
                    ResizeArray(ref arrayDef.storedList, numberOfElementsExpected);
                }
            }

            return arrayDef;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Array Header - Class Type
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected bool DrawReferenceArrayHeader<T>(string arrayDisplayname, ref T[] array, bool drawAsFoldoutOption = true, int numberOfElementsExpected = -1, bool forceAllElementsToReferenceAnObject = false) where T : class
        {
            if (array == null)
            {
                array = new T[1];
                if (CanAutomaticallyAssignNewClassValue(typeof(T)))
                {
                    array[0] = System.Activator.CreateInstance(typeof(T), true) as T;
                }
            }

            bool retShowArrayContents;
            ArrayTypeDef<T> definition = array;
            definition = ResolveReferenceArrayHeader(arrayDisplayname, definition, numberOfElementsExpected, drawAsFoldoutOption, forceAllElementsToReferenceAnObject, out retShowArrayContents);
            array = definition.storedArray;
            return retShowArrayContents;
        }

        protected bool DrawReferenceArrayHeader<T>(string arrayDisplayname, ref List<T> array, bool drawAsFoldoutOption = true, int numberOfElementsExpected = -1, bool forceAllElementsToReferenceAnObject = false) where T : class
        {
            if (array == null)
            {
                array = new List<T>();
                if (CanAutomaticallyAssignNewClassValue(typeof(T)))
                {
                    array.Add(System.Activator.CreateInstance(typeof(T), true) as T);
                }
            }

            bool retShowArrayContents;
            ArrayTypeDef<T> definition = array;
            definition = ResolveReferenceArrayHeader(arrayDisplayname, definition, numberOfElementsExpected, drawAsFoldoutOption, forceAllElementsToReferenceAnObject, out retShowArrayContents);
            return retShowArrayContents;
        }

        private ArrayTypeDef<T> ResolveReferenceArrayHeader<T>(string arrayDisplayname, ArrayTypeDef<T> arrayDef, int numberOfElementsExpected, bool drawAsFoldoutOption,
                                                                                                    bool forceAllElementsToReferenceAnObject, out bool retShowArrayContents) where T : class
        {
            retShowArrayContents = true;
            if (drawAsFoldoutOption)
            {
                retShowArrayContents = DrawFoldoutOption(arrayDisplayname);
            }
            else
            {
                DrawLabel(arrayDisplayname);
            }

            int count = arrayDef.Count;
            if (count < numberOfElementsExpected)
            {
                if (arrayDef.storedArray != null)
                {
                    ResizeReferenceArray(ref arrayDef.storedArray, numberOfElementsExpected);
                }
                else
                {
                    ResizeReferenceArray(ref arrayDef.storedList, numberOfElementsExpected);
                }
            }

            if (forceAllElementsToReferenceAnObject)
            {
                if (CanAutomaticallyAssignNewClassValue(typeof(T)))
                {
                    for (int i = 0; i < count; ++i)
                    {
                        if (arrayDef[i] == null)
                        {
                            arrayDef[i] = System.Activator.CreateInstance(typeof(T), true) as T;
                        }
                    }
                }
            }

            return arrayDef;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Array Elements - Struct Type
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected ArrayModificationResult DrawArrayElements<T>(ref T[] array, DrawArrayParameters<T> drawArrayParams) where T : struct
        {
            if (array == null)
            {
                array = new T[1];
            }

            ArrayTypeDef<T> definition = array;
            ArrayModificationResult arrayModificationResult = ResolveDrawArrayElements(ref definition, drawArrayParams);
            array = definition.storedArray;

            if (arrayModificationResult == ArrayModificationResult.AddedNewElement)
            {
                ResizeArray(ref array, array.Length + 1);
            }
            else if (arrayModificationResult == ArrayModificationResult.RemovedLastElement)
            {
                ResizeArray(ref array, array.Length - 1);
            }

            return arrayModificationResult;
        }

        protected ArrayModificationResult DrawArrayElements<T>(ref List<T> list, DrawArrayParameters<T> drawArrayParams) where T : struct
        {
            if (list == null)
            {
                list = new List<T>();
            }

            ArrayTypeDef<T> definition = list;
            ArrayModificationResult arrayModificationResult = ResolveDrawArrayElements(ref definition, drawArrayParams);

            if (arrayModificationResult == ArrayModificationResult.AddedNewElement)
            {
                ResizeArray(ref list, list.Count + 1);
            }
            else if (arrayModificationResult == ArrayModificationResult.RemovedLastElement)
            {
                ResizeArray(ref list, list.Count - 1);
            }

            return arrayModificationResult;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Array Elements - Class Type
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected ArrayModificationResult DrawReferenceArrayElements<T>(ref T[] array, DrawArrayParameters<T> drawArrayParams) where T : class
        {
            if (array == null)
            {
                array = new T[1];
            }

            ArrayTypeDef<T> definition = array;
            ArrayModificationResult arrayModificationResult = ResolveDrawArrayElements(ref definition, drawArrayParams);

            if (arrayModificationResult == ArrayModificationResult.AddedNewElement)
            {
                ResizeReferenceArray(ref array, array.Length + 1);
            }
            else if (arrayModificationResult == ArrayModificationResult.RemovedLastElement)
            {
                ResizeReferenceArray(ref array, array.Length - 1);
            }
            else if (arrayModificationResult != ArrayModificationResult.NoChange)
            {
                if (m_cachedDropdownChoicesWithUsedElementsRemoved.ContainsKey(array))
                {
                    m_cachedDropdownChoicesWithUsedElementsRemoved.Remove(array);
                }
            }

            return arrayModificationResult;
        }

        protected ArrayModificationResult DrawReferenceArrayElements<T>(ref List<T> list, DrawArrayParameters<T> drawArrayParams) where T : class
        {
            if (list == null)
            {
                list = new List<T>();
            }

            ArrayTypeDef<T> definition = list;
            ArrayModificationResult arrayModificationResult = ResolveDrawArrayElements(ref definition, drawArrayParams);

            if (arrayModificationResult == ArrayModificationResult.AddedNewElement)
            {
                ResizeReferenceArray(ref list, list.Count + 1);
            }
            else if (arrayModificationResult == ArrayModificationResult.RemovedLastElement)
            {
                ResizeReferenceArray(ref list, list.Count - 1);
            }
            else if (arrayModificationResult != ArrayModificationResult.NoChange)
            {
                if (m_cachedDropdownChoicesWithUsedElementsRemoved.ContainsKey(list))
                {
                    m_cachedDropdownChoicesWithUsedElementsRemoved.Remove(list);
                }
            }

            return arrayModificationResult;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Resolve Draw Array Elements
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        private ArrayModificationResult ResolveDrawArrayElements<T>(ref ArrayTypeDef<T> arrayDef, DrawArrayParameters<T> drawArrayParams)
        {
            ArrayModificationResult arrayModificationResult = ArrayModificationResult.NoChange;

            int length = arrayDef.Length;
            int finalIndex = length - 1;

            if (length > 1)
            {
                List<ButtonsInfo> sortButtons = GetDrawArrayParamsButtonList(arrayDef, drawArrayParams);
                DrawMultipleButtonsOneLine(sortButtons, 5.0f);
            }

            // Reorderable Array ~~~~~~~~~~~~~~~~~~~~~~~~~~~
            if (drawArrayParams.canReorderArray && length > 1)
            {
                using (new EditorIndentation(3))
                {
                    for (int i = 0; i < length; ++i)
                    {
                        if (drawArrayParams.addSpacesBetweenElements && i > 0)
                        {
                            AddSpaces(2);
                        }

                        // Must be done before drawing array elements. This gets us the mid point between all the drawn elements in the array to inser 
                        Rect previousElementPos = GetScaledRect();
                        bool hadGUIChangeEarlier = GUI.changed;

                        drawArrayParams.drawElementCallback(i);

                        if (GUI.changed && hadGUIChangeEarlier == false)
                        {
                            arrayModificationResult = ArrayModificationResult.ModifiedValue;
                        }

                        // Draw Reorderable Elements
                        const float bw = 20; // Button Width
                        Rect pos = GetScaledRect();
                        pos.width = bw;

                        if (i > 0)
                        {
                            // Centering Button (An array element can contain many displayed items. We want to center it between all elements).
                            pos.y = previousElementPos.y + ((pos.y - previousElementPos.y) * 0.5f);

                            // Cannot move element 0 upwards. Only element 1 onwards.
                            if (GUI.Button(pos, new GUIContent(" ^", "Move Element Up")))
                            {
                                T temp = arrayDef[i];
                                arrayDef[i] = arrayDef[i - 1];
                                arrayDef[i - 1] = temp;
                                drawArrayParams.onArraySorted?.Invoke();
                                arrayModificationResult = ArrayModificationResult.SwappedItems;
                            }
                        }

                        if (i < finalIndex)
                        {
                            pos.x += pos.width + 5;

                            // Same deal for downwards. Cannot move final element down.
                            if (GUI.Button(pos, new GUIContent(" v", "Move Element Down")))
                            {
                                T temp = arrayDef[i];
                                arrayDef[i] = arrayDef[i + 1];
                                arrayDef[i + 1] = temp;
                                drawArrayParams.onArraySorted?.Invoke();
                                arrayModificationResult = ArrayModificationResult.SwappedItems;
                            }
                        }
                    }
                }
            }

            // Non-Reorderable Array ~~~~~~~~~~~~~~~~~~~~~~~
            else
            {
                using (new EditorIndentation())
                {
                    for (int i = 0; i < length; ++i)
                    {
                        if (drawArrayParams.addSpacesBetweenElements && i > 0)
                        {
                            AddSpaces(2);
                        }

                        bool hadGUIChangeEarlier = GUI.changed;
                        drawArrayParams.drawElementCallback(i);

                        if (GUI.changed && hadGUIChangeEarlier == false)
                        {
                            arrayModificationResult = ArrayModificationResult.ModifiedValue;
                        }
                    }
                }
            }
            // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            if (drawArrayParams.isArrayResizeable)
            {
                EditorGUILayout.Space();
                PlusMinusButtons addOrRemoveElementResult = DrawPlusMinusButtons(0.0f);
                if (addOrRemoveElementResult == PlusMinusButtons.PlusPressed)
                {
                    arrayModificationResult = ArrayModificationResult.AddedNewElement;
                }
                else if (addOrRemoveElementResult == PlusMinusButtons.MinusPressed)
                {
                    arrayModificationResult = ArrayModificationResult.RemovedLastElement;
                }
                EditorGUILayout.Space();
            }

            return arrayModificationResult;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Resize Array - Struct Type
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected void ResizeArray<T>(ref T[] arrayField, int newSize, T? defaultValue = null) where T : struct
        {
            if (newSize < 0)
            {
                return;
            }

            T[] newArray = new T[newSize];
            for (int i = 0; i < newSize; ++i)
            {
                if (arrayField.Length > i)
                {
                    OnArrayVariableModification(ref newArray[i], ref arrayField[i]);
                }
                else if (defaultValue.HasValue)
                {
                    newArray[i] = defaultValue.Value;
                }
            }
            arrayField = newArray;
        }

        protected void ResizeArray<T>(ref List<T> listField, int newSize, T? defaultValue = null) where T : struct
        {
            if (newSize < 0)
            {
                return;
            }

            int count = listField.Count;
            for (; count < newSize; ++count)
            {
                if (defaultValue.HasValue)
                {
                    listField.Add(defaultValue.Value);
                }
                else
                {
                    listField.Add(default(T));
                }
            }

            int overLimit = count - newSize;
            if (overLimit > 0)
            {
                // Too many elements.
                int startRemovalIndex = newSize;
                listField.RemoveRange(newSize, overLimit);
            }
        }

        protected void ResizeArray<T>(ref ArrayTypeDef<T> arrayDef, int newSize, T? defaultValue = null) where T : struct
        {
            if (arrayDef.storedArray != null)
            {
                ResizeArray(ref arrayDef.storedArray, newSize, defaultValue);
            }
            else
            {
                ResizeArray(ref arrayDef.storedList, newSize, defaultValue);
            }
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Resize Array - Class Type
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected void ResizeReferenceArray<T>(ref T[] arrayField, int newSize) where T : class
        {
            if (newSize < 0)
            {
                return;
            }

            T[] newArray = new T[newSize];
            for (int i = 0; i < newSize; ++i)
            {
                if (arrayField.Length > i)
                {
                    OnArrayVariableModification(ref newArray[i], ref arrayField[i]);
                }
                else
                {
                    OnArrayVariableModification(ref newArray[i]);
                }
            }

            arrayField = newArray;
            OnArrayModification(ref arrayField);
        }

        protected void ResizeReferenceArray<T>(ref List<T> listField, int newSize) where T : class
        {
            if (newSize < 0)
            {
                return;
            }

            int count = listField.Count;
            if (CanAutomaticallyAssignNewClassValue(typeof(T)))
            {
                for (; count < newSize; ++count)
                {
                    listField.Add(System.Activator.CreateInstance(typeof(T), true) as T);
                }
            }
            else
            {
                for (; count < newSize; ++count)
                {
                    listField.Add(null);
                }
            }

            int overLimit = count - newSize;
            if (overLimit > 0)
            {
                // Too many elements.
                int startRemovalIndex = newSize;
                listField.RemoveRange(newSize, overLimit);
            }
        }

        private void ResizeReferenceArray<T>(ref ArrayTypeDef<T> arrayDef, int newSize) where T : class
        {
            if (arrayDef.storedArray != null)
            {
                ResizeReferenceArray(ref arrayDef.storedArray, newSize);
            }
            else
            {
                ResizeReferenceArray(ref arrayDef.storedList, newSize);
            }
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Remove Array Element At Index
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected void RemoveArrayElementAt<T>(ref T[] array, int index)
        {
            List<T> list = new List<T>(array);
            list.RemoveAt(index);
            array = list.ToArray();
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Clone Array - Struct Type
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected T[] CloneArray<T>(T[] originalArray) where T : struct
        {
            if (originalArray == null)
            {
                return null;
            }

            int length = originalArray.Length;
            T[] cloneArray = new T[length];
            for (int i = 0; i < length; ++i)
            {
                cloneArray[i] = originalArray[i];
            }
            return cloneArray;
        }

        protected List<T> CloneArray<T>(List<T> originalArray) where T : struct
        {
            if (originalArray == null)
            {
                return null;
            }

            T[] clone = CloneArray(originalArray.ToArray());
            return new List<T>(clone);
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Clone Array - Class Type
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected T[] CloneReferenceArray<T>(T[] originalArray) where T : class
        {
            if (originalArray == null)
            {
                return null;
            }

            int length = originalArray.Length;
            T[] cloneArray = new T[length];
            if (CanAutomaticallyAssignNewClassValue(typeof(T)))
            {
                PropertyInfo[] properties = typeof(T).GetType().GetProperties(REFLECTION_SEARCH_FLAGS);
                FieldInfo[] variables = typeof(T).GetType().GetFields(REFLECTION_SEARCH_FLAGS);

                for (int i = 0; i < length; ++i)
                {
                    cloneArray[i] = System.Activator.CreateInstance(typeof(T), true) as T;

                    foreach (PropertyInfo property in properties)
                    {
                        if (property.CanWrite == false)
                        {
                            continue;
                        }

                        object originalValue = property.GetValue(originalArray[i]);
                        property.SetValue(cloneArray[i], originalValue);
                    }


                    foreach (FieldInfo variable in variables)
                    {
                        object originalValue = variable.GetValue(originalArray[i]);
                        variable.SetValue(cloneArray[i], originalValue);
                    }
                }
            }
            else
            {
                for (int i = 0; i < length; ++i)
                {
                    cloneArray[i] = originalArray[i];
                }
            }

            return cloneArray;
        }

        protected List<T> CloneReferenceArray<T>(List<T> originalArray) where T : class
        {
            if (originalArray == null)
            {
                return null;
            }

            T[] clone = CloneReferenceArray(originalArray.ToArray());
            return new List<T>(clone);
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Insert Into Array
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected void InsertIntoArray<T>(ref T[] array, int indexToInsert, T newItem)
        {
            int arrayLengthBefore = array.Length;
            T[] newArray = new T[arrayLengthBefore + 1];
            for (int i = 0; i < indexToInsert; ++i)
            {
                newArray[i] = array[i];
            }
            for (int i = indexToInsert; i < arrayLengthBefore; ++i)
            {
                newArray[i + 1] = array[i];
            }

            newArray[indexToInsert] = newItem;
            array = newArray;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Prepend Array
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected void PrependArray<T>(ref T[] original, ref T[] prependingArray)
        {
            T[] newArray = prependingArray.Clone() as T[];
            AppendArray(ref newArray, ref original);
            original = newArray;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Append Array
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected void AppendArray<T>(ref T[] original, ref T[] appendingArray)
        {
            int originalLength = (original != null ? original.Length : 0);
            int newSize = originalLength + appendingArray.Length;
            T[] newArray = new T[newSize];

            int appendingIndex = 0;
            for (int i = 0; i < newSize; ++i)
            {
                if (originalLength > i)
                {
                    OnArrayVariableModification(ref newArray[i], ref original[i]);
                }
                else
                {
                    newArray[i] = appendingArray[appendingIndex++];
                }
            }

            original = newArray;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Callback Methods: On Array Modification
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected void OnArrayVariableModification<T>(ref T destination) where T : class
        {
            // MonoBehaviour is not allow to be instantiated in Code without using the Instantiate Method. This will cause a warning... Hence the check for 'MonoBehaviour'
            if (CanAutomaticallyAssignNewClassValue(typeof(T)))
                destination = System.Activator.CreateInstance(typeof(T), true) as T;
        }

        protected void OnArrayVariableModification<T>(ref T destination, ref T source)
        {
            destination = source;
        }

        protected void OnArrayModification(string whichArray)
        {
        }

        protected void OnArrayModification<T>(ref T[] arrayField)
        {
        }


        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Show "Open File Dialogue"
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary>
        /// Opens up the "Open File Dialog" Window on both Mac & PC. Will allow you to select what you want to have selected
        /// </summary>
        /// <param name="filters">Pass in the extensions you wish to allow the user (ie. you) to be able to select. e.g.: "txt". Pass in with a separating semicolon
        /// for multiple extension types (EG: "ogg;mp3;wav")</param>
        /// <returns>The absolute path to the desired file. Will return an empty string if the user chooses to exit the window without selecting a file</returns>
        protected string ShowOpenFileDialogueWindow(string filters)
        {
            string openFilename = EditorUtility.OpenFilePanel("Open File", sm_sOpenPathDirectory, filters);
            if (string.IsNullOrEmpty(openFilename) == false)
            {
                sm_sOpenPathDirectory = System.IO.Path.GetDirectoryName(openFilename) + '/';
            }
            return openFilename;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Show Dialogue Window
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary> Show Dialog Window (Message Box) </summary>
        protected void ShowDialogueWindow(string title, string message)
        {
            EditorUtility.DisplayDialog(title, message, "OK");
        }
        /// <summary> Show Dialog Window (Message Box) </summary>
        protected void ShowMessageBox(string title, string message)
        {
            ShowDialogueWindow(title, message);
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Show Confirmation Window
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary> Show Confirmation Window (Yes/No selectable) </summary>
        protected bool ShowConfirmationWindow(string title, string message, string confirm = "Yes", string deny = "No")
        {
            return EditorUtility.DisplayDialog(title, message, confirm, deny);
        }
        /// <summary> Show Confirmation Window (Yes/No selectable) </summary>
        protected bool ShowConfirmationMessageBox(string title, string message, string confirm = "Yes", string deny = "No")
        {
            return ShowConfirmationWindow(title, message, confirm, deny);
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Draw Option
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary>
        /// Determines the type and automatically draws the option for you. Recommended that you do not use this if you know the type already.
        /// </summary>
        protected object DrawOption<T>(string name, T value, string tooltip = null)
        {
            if (value is int)
                return DrawChangeableIntegerOption(name, (value as int?) ?? 0, tooltip: tooltip);
            if (value is float)
                return DrawChangeableFloatOption(name, (value as float?) ?? 0.0f, tooltip: tooltip);
            if (value is Vector2)
                return DrawVectorField(name, (value as Vector2?) ?? Vector2.zero, tooltip: tooltip);
            if (value is Vector2Int)
                return DrawVectorField(name, (value as Vector2Int?) ?? Vector2Int.zero, tooltip: tooltip);
            if (value is Vector3)
                return DrawVectorField(name, (value as Vector3?) ?? Vector3.zero, tooltip: tooltip);
            if (value is Vector3Int)
                return DrawVectorField(name, (value as Vector3Int?) ?? Vector3Int.zero, tooltip: tooltip);
            if (value is Vector4)
                return DrawVectorField(name, (value as Vector4?) ?? Vector4.zero, tooltip: tooltip);
            if (value is bool)
                return DrawToggleField(name, (value as bool?) ?? false, tooltip: tooltip);
            if (value is string)
                return DrawStringField(name, value as string ?? "", tooltip: tooltip);
            if (value is System.Enum)
                return DrawEnumField(name, value as System.Enum, tooltip: tooltip);

            return DrawObjectOption(name, value as UnityEngine.Object, tooltip: tooltip);
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Methods: Get Index Of (Array)
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected int GetIndexOf<T>(T[] array, T value)
        {
            int length = array.Length;
            for (int i = 0; i < length; ++i)
            {
                if (array[i].Equals(value))
                {
                    return i;
                }
            }
            return -1;
        }

        protected int GetIndexOf<T>(List<T> list, T value)
        {
            int length = list.Count;
            for (int i = 0; i < length; ++i)
            {
                if (list[i].Equals(value))
                    return i;
            }
            return -1;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Check if we can automatically assign a value on the editor's behalf
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected bool CanAutomaticallyAssignNewClassValue(System.Type T)
        {
            if (T == typeof(SceneAsset) || T.IsSubclassOf(typeof(SceneAsset)))
                return false;
            if (T == typeof(GameObject) || T.IsSubclassOf(typeof(GameObject)))
                return false;
            if (T == typeof(MonoBehaviour) || T.IsSubclassOf(typeof(MonoBehaviour)))
                return false;
            if (T == typeof(ScriptableObject) || T.IsSubclassOf(typeof(ScriptableObject)))
                return false;
            if (T == typeof(Sprite) || T.IsSubclassOf(typeof(Sprite)))
                return false;
            if (T == typeof(UnityEngine.UI.Image) || T.IsSubclassOf(typeof(UnityEngine.UI.Image)))
                return false;
            if (T == typeof(string) || T.IsSubclassOf(typeof(string)))
                return false;

            return true;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Get Zeroed Num Prefix String
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary>
        /// Returns a prefixed string base on the leading zeroes required for a string. 
        ///	   Example 1, (num = 5, max = 15, baseFormat = 10) => 5 is less than BaseFormat of 10, which is also less than max, so end result is "05". 
        ///	   Example 2, (num = 5, max = 115, baseFormat = 10) => 5 is less than 10, and 5 is less than 100 (max is 115), so end result is "005".
        /// </summary>
        /// <param name="num">Current Number to add prefixes to</param>
        /// <param name="max">Num to compare to</param>
        /// <param name="baseFormat">format to compare against, 10 by default because humans operate in Base 10. Base 2 is used for Binary, etc.</param>
        /// <param name="replaceChar">Char to insert. Zero by defualt.</param>
        /// <returns></returns>
        protected string GetZeroedNumPrefixString(int num, int max = 10, int baseFormat = 10, char replaceChar = '0')
        {
            string result = "";
            for (int currentBaseOfMax = baseFormat; currentBaseOfMax <= max; currentBaseOfMax *= baseFormat)
            {
                if (num < currentBaseOfMax)
                {
                    result += replaceChar;
                }
            }

            result += num.ToString();
            return result;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Get Dropdown Choices With Used Elements Removed
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected DropdownChoicesData GetDropdownChoicesWithUsedElementsRemoved(DropdownChoicesParameters parameters)
        {
            DropdownChoicesData[] calculatedChoicesData = null;
            if (parameters.forceRefesh == false)
            {
                if (m_cachedDropdownChoicesWithUsedElementsRemoved.TryGetValue(parameters.owningObject, out calculatedChoicesData))
                {
                    if (calculatedChoicesData != null && parameters.arrayIndex < calculatedChoicesData.Length)
                    {
                        return calculatedChoicesData[parameters.arrayIndex];
                    }

                    // Dirty Data. Needs to be calculated again.
                    calculatedChoicesData = null;
                }
            }

            string[] allDropdownOptions = parameters.allDropdownOptionsGetter.Invoke().ToArray();
            List<string> allUsedOptions = parameters.allUsedDropdownOptionsGetter.Invoke();

            int totalOptionsCount = allDropdownOptions.Length;
            int usedOptionsCount = allUsedOptions.Count;
            calculatedChoicesData = new DropdownChoicesData[usedOptionsCount];

            for (int currentIndex = 0; currentIndex < usedOptionsCount; ++currentIndex)
            {
                List<string> remainingElements = new List<string>(allDropdownOptions);
                remainingElements.RemoveAll(optionDisplay =>
                {
                    if (string.IsNullOrEmpty(optionDisplay))
                    {
                        // Don't Show Empty Elements Either
                        return true;
                    }
                    if (optionDisplay.Equals(allUsedOptions[currentIndex]))
                    {
                        // Don't Remove Currently Selected Option. Even if another element also has this option selected.
                        return false;
                    }

                    for (int otherInstanceId = 0; otherInstanceId < usedOptionsCount; ++otherInstanceId)
                    {
                        if (currentIndex == otherInstanceId)
                        {
                            // Don't Check Self
                            continue;
                        }

                        if (optionDisplay.Equals(allUsedOptions[otherInstanceId]))
                        {
                            // Other Element is using this. Remove ability to select.
                            return true;
                        }
                    }
                    return false;
                });

                List<string> elementsUsedByOthers = new List<string>(allDropdownOptions);
                elementsUsedByOthers.RemoveAll(option => remainingElements.Contains(option) == false);

                int selectedIndexOfRemainingSelection = remainingElements.IndexOf(allUsedOptions[currentIndex]);

                int[] remainingChoicesIndexToAllChoicesIndex;
                int selectableElementsCount = remainingElements.Count;
                if (selectableElementsCount == 0)
                {
                    // No Elements Selectable
                    remainingElements.Add("N\\A (No Selectable Data)");
                    remainingChoicesIndexToAllChoicesIndex = null;
                }
                else
                {
                    // Pairing up Remaining Elements List with their respective locations in the "All Options" List.
                    remainingChoicesIndexToAllChoicesIndex = new int[selectableElementsCount];
                    for (int selectableElementIndex = 0; selectableElementIndex < selectableElementsCount; ++selectableElementIndex)
                    {
                        remainingChoicesIndexToAllChoicesIndex[selectableElementIndex] = GetIndexOf(allDropdownOptions, remainingElements[selectableElementIndex]);
                    }
                }

                calculatedChoicesData[currentIndex] = new DropdownChoicesData()
                {
                    allChoices = allDropdownOptions,
                    choicesUsedByOtherElements = elementsUsedByOthers.ToArray(),
                    currentSelectionIndex = selectedIndexOfRemainingSelection,
                    remainingSelectableChoices = remainingElements.ToArray(),
                    remainingChoicesIndexToAllChoicesIndex = remainingChoicesIndexToAllChoicesIndex,
                };
            }

            m_cachedDropdownChoicesWithUsedElementsRemoved[parameters.owningObject] = calculatedChoicesData;
            return calculatedChoicesData[parameters.arrayIndex];
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Get Draw Array Params Button List
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        protected T CloneContentsOfItem<T>(T original) where T : new()
        {
            T clone = new T();
            List<FieldInfo> allFields = new List<FieldInfo>();
            List<PropertyInfo> allProperties = new List<PropertyInfo>();

            System.Type currentClassType = typeof(T);
            while (currentClassType != typeof(object))
            {
                FieldInfo[] fieldsOfClass = currentClassType.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                foreach (FieldInfo fieldInfo in fieldsOfClass)
                {
                    if (fieldInfo.IsLiteral || fieldInfo.IsInitOnly)
                    {
                        // Consts and Static fields not applicable.
                        continue;
                    }

                    bool canAddField = true;
                    foreach (FieldInfo f in allFields)
                    {
                        if (f.Name.Equals(fieldInfo.Name))
                        {
                            // Already has Field (inherited variable)
                            canAddField = false;
                            break;
                        }
                    }

                    if (canAddField)
                    {
                        allFields.Add(fieldInfo);
                    }
                }

                PropertyInfo[] propertiesOfClass = currentClassType.GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo propertyInfo in propertiesOfClass)
                {
                    if (propertyInfo.CanWrite == false)
                    {
                        continue;
                    }

                    bool canAddProperty = true;
                    foreach (PropertyInfo p in allProperties)
                    {
                        if (p.Name.Equals(propertyInfo.Name))
                        {
                            // Already has Property (inherited variable)
                            canAddProperty = false;
                            break;
                        }
                    }

                    if (canAddProperty)
                    {
                        allProperties.Add(propertyInfo);
                    }
                }

                currentClassType = currentClassType.BaseType;
            }

            // Clone Values
            foreach (FieldInfo fieldInfo in allFields)
            {
                object originalValue = fieldInfo.GetValue(original);
                fieldInfo.SetValue(clone, originalValue);
            }
            foreach (PropertyInfo propertyInfo in allProperties)
            {
                object originalValue = propertyInfo.GetValue(original);
                propertyInfo.SetValue(clone, originalValue);
            }

            return clone;
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //	* New Method: Get Draw Array Params Button List
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        private List<ButtonsInfo> GetDrawArrayParamsButtonList<T>(ArrayTypeDef<T> arrayTypeDef, DrawArrayParameters<T> drawArrayParams)
        {
            List<ButtonsInfo> buttonsInfo = new List<ButtonsInfo>();
            if (drawArrayParams.alphabeticalComparer != null)
            {
                buttonsInfo.Add(new ButtonsInfo()
                {
                    buttonName = "Sort Alphabetically",
                    tooltip = "Sort List Alphabeticlaly",
                    onClicked = () =>
                    {
                        arrayTypeDef.Sort(drawArrayParams.alphabeticalComparer);
                        drawArrayParams.onArraySorted?.Invoke();
                    },
                });
            }

            foreach (ArraySorterInfo<T> sorterInfo in new List<ArraySorterInfo<T>>() { drawArrayParams.otherSorter1, drawArrayParams.otherSorter2, drawArrayParams.otherSorter3 })
            {
                if (sorterInfo != null && sorterInfo.sorterFunc != null)
                {
                    buttonsInfo.Add(new ButtonsInfo()
                    {
                        buttonName = sorterInfo.buttonLabel,
                        tooltip = sorterInfo.tooltip,
                        onClicked = () =>
                        {
                            arrayTypeDef.Sort(sorterInfo.sorterFunc);
                            drawArrayParams.onArraySorted?.Invoke();
                        },
                    });
                }
            }

            return buttonsInfo;
        }
    }

} // Namespace Playside
#endif // #if UNITY_EDITOR