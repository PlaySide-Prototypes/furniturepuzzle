﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System;

public partial class WhipCracker : EditorWindow
{
	/// <summary>
	/// 
	/// </summary>
	/// <returns></returns>
	static int GetBuildIssues()
	{
		int buildIssueCount = 0;

		buildIssueCount += ICFinalProductionDefined();
		buildIssueCount += ICDevBuildEnabledForSubmission();
		buildIssueCount += ICBundleIdentifier();

		return buildIssueCount;
	}

	/// <summary>
	/// Issue check for undefined package name
	/// </summary>
	static int ICBundleIdentifier()
	{
		// Check if provisioning profile is defined
		string bundleId = PlayerSettings.applicationIdentifier;
		bool isApplicationIdDefined = string.IsNullOrEmpty(bundleId);
		bool isIdDefault = (bundleId == "com.Company.ProductName");

		if (isApplicationIdDefined || isIdDefault)
		{
			Issue newIssue = new Issue();

			newIssue.issueType = IssueTypes.Error;
			newIssue.issueTitle = "Provisioning profile must be defined";
			newIssue.issueComment = "Set provisioning profile in Player Settings, currently " + (isIdDefault? "'"+bundleId+"'" : "blank");

			//newIssue.contextObject = canvasScaler.gameObject;
			newIssue.subIssueCount = 1;
			newIssue.paths = new string[1] { "Player Settings/Other Settings/Provisioning Profile"};
			newIssue.issueFixActions = new Action[1] { (() => EditorApplication.ExecuteMenuItem("Edit/Project Settings/Player")) };
			newIssue.automatedFix = true;

			AddIssueToList(newIssue);
			return 1;
		}

		return 0;
	}

	/// <summary>
	/// Issue check if build is FINAL or PRODUCTION
	/// </summary>
	static int ICFinalProductionDefined()
	{
		string defineSymbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
		bool isFinal = defineSymbols.Contains("FINAL");
		bool isProduction = defineSymbols.Contains("PRODUCTION");
		bool isDevelopment = EditorUserBuildSettings.development;

		// Check if NOT Final or NOT Production
		if ((!isFinal || !isProduction ) && !isDevelopment)
		{
			Issue newIssue = new Issue();

			newIssue.issueType = IssueTypes.Warning;

			// Groovy title formatting
			newIssue.issueTitle = (!isFinal) ? "FINAL" : "";
			if (!isProduction && !isFinal)
				newIssue.issueTitle +=  " & ";
			newIssue.issueTitle += (!isProduction) ? "PRODUCTION" : "";
			newIssue.issueTitle += " not defined";

			newIssue.issueComment = "If you are performing a submission build, FINAL and PRODUCTION must be defined";

			newIssue.subIssueCount = 1;
			newIssue.paths = new string[1] { "Player Settings/Other Settings/Scripting Define Symbols" };

			if (!isProduction)
				defineSymbols += ";PRODUCTION";
			if (!isFinal)
				defineSymbols += ";FINAL";
			
			newIssue.issueFixActions = new Action[1] {
				(() => {
				PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, defineSymbols);
				EditorApplication.ExecuteMenuItem("Edit/Project Settings/Player");
				})
			};
			newIssue.automatedFix = true;

			AddIssueToList(newIssue);
			return 1;
		}

		return 0;
	}


	/// <summary>
	/// Issue check if build is FINAL
	/// </summary>
	static int ICDevBuildEnabledForSubmission()
	{
		string defineSymbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
		bool isFinal = defineSymbols.Contains("FINAL");
		bool isProduction = defineSymbols.Contains("PRODUCTION");
		bool isDevelopment = EditorUserBuildSettings.development;

		// Check if NOT Final or NOT Production
		if ((isFinal || isProduction) && isDevelopment)
		{
			Issue newIssue = new Issue();

			newIssue.issueType = IssueTypes.Warning;
			newIssue.issueTitle = "Development Build enabled";
			newIssue.issueComment = "If you are performing a submission build, disable Development Build";

			newIssue.subIssueCount = 1;
			newIssue.paths = new string[1] { "File/Build Settings..." };
			newIssue.issueFixActions = new Action[1] { (() => { EditorUserBuildSettings.development = false; EditorApplication.ExecuteMenuItem("File/Build Settings..."); }) };
			newIssue.automatedFix = true;

			AddIssueToList(newIssue);
			return 1;
		}

		return 0;
	}
}
