﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System;

public partial class WhipCracker : EditorWindow
{
	/// <summary>
	/// 
	/// </summary>
	/// <returns></returns>
	static int GetGameObjectIssues()
	{
		int gameObjectIssueCount = 0;

		gameObjectIssueCount += ICMissingComponents();

		return gameObjectIssueCount;
	}

	/// <summary>
	/// Issue check for use of deprecated PanelSwitchNotifier components
	/// </summary>
	static int ICMissingComponents()
	{
		GameObject[] gameObjects = Resources.FindObjectsOfTypeAll<GameObject>();
		List<GameObject> gameObjectsMissingComponents = new List<GameObject>(0);

		foreach (GameObject g in gameObjects)
		{
			Component[] components = g.GetComponents<Component>();
			for (int i = 0; i < components.Length; i++)
			{
				if (components[i] == null)
				{
					gameObjectsMissingComponents.Add(g);
				}
			}
		}

		// Create Issue if missing button events found
		if (gameObjectsMissingComponents.Count > 0)
		{
			Issue newIssue = new Issue();
			newIssue.subIssueCount = gameObjectsMissingComponents.Count;
			newIssue.contextObjects = new GameObject[newIssue.subIssueCount];
			newIssue.paths = new string[newIssue.subIssueCount];
			newIssue.issueFixActions = new Action[newIssue.subIssueCount];

			newIssue.issueType = IssueTypes.Warning;
			newIssue.issueTitle = "Found missing components (" + gameObjectsMissingComponents.Count + ")";
			newIssue.issueComment = "Remove or assign these missing components";

			for (int b = 0; b < newIssue.subIssueCount; b++)
			{
				GameObject contextObject = gameObjectsMissingComponents[b];
				newIssue.contextObjects[b] = contextObject;
				newIssue.paths[b] = contextObject.transform.root.name + "/" + AnimationUtility.CalculateTransformPath(contextObject.transform, contextObject.transform.root);

				// Select and Focus GameObject
				newIssue.issueFixActions[b] = () =>
				{
					Selection.objects = new GameObject[1] { contextObject };
					SceneView.FrameLastActiveSceneView();
				};
			}

			// Collapse if more issues than autoIssueCollapseThreshold
			newIssue.subIssuesVisible = (newIssue.subIssueCount < autoIssueCollapseThreshold);

			AddIssueToList(newIssue);
			return newIssue.subIssueCount;
		}

		return 0;
	}
}
