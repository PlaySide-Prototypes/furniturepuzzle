﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;

public class WhipCrackerBuildPreprocessor : IPreprocessBuildWithReport
{
	public int callbackOrder { get { return 0; } }

	public void OnPreprocessBuild(BuildReport report)
	{
		if (WhipCracker.openOnPreBuild)
			WhipCracker.OpenWhipCracker();
	}
}

public partial class WhipCracker : EditorWindow
{
	const string toolVersion = "1.0";

	public static bool openOnPreBuild = true;

	// Editor Window
	static EditorWindow thisWindow;
	static Vector2 minWindowSize = new Vector2(1205f, 320f);
	static Vector2 windowDimensions;
	static bool isPro = false;
	static int categoryCount;
	static int totalIssueCount;

	public enum Categories
	{
		Build,
		GameObject,
		UI,
		Art,
		Audio,
	}

	public enum IssueTypes
	{
		Summary,
		Warning,
		Error,
	}

	public class Issue
	{
		public IssueTypes issueType;			// Type of Issue eg. Summary, Warning, Error
		public int subIssueCount;				// Number of context objects for this issue
		public GameObject[] contextObjects;		// Object reference if issue is present on a gameobject
		public Action[] issueFixActions;		// Action used to jump to or fix the issue if applicable
		public bool automatedFix;				// Is the fix automated
		public string issueTitle;				// Title of issue
		public string issueComment;				// Description of issue
		public string[] paths;					// Paths to objects where subissues occur
		public bool subIssuesVisible = true;	// Default to On so we don't have to set it eery time we create an issue
	}

	// Issues
	static bool[] enabledCategories;
	static int[] categoryIssueCounts;
	static Issue categoryIssues;
	static int autoIssueCollapseThreshold = 5;

	static List<Issue> summaryIssues;       // Issues that aren't really issues but are nice to know
	static List<Issue> warningIssues;       // Issues that need fixing but won't break the game
	static List<Issue> errorIssues;         // Issues that are critical and may break the game
	static int currentIssueDrawIndex;       // Tracks which issue count is being drawn increments across all categories

	// General Texture
	static Texture2D bgTexture;

	// General Colors
	static Color whiteColor = Color.white;
	static Color freeLocHeaderColor = new Color(0.36f, 0.51f, 0.45f, 1f);
	static Color proLocHeaderColor = new Color(0.16f, 0.31f, 0.25f, 1f);

	//static Color freeTableBG2Color = new Color(.7f, .7f, .7f, 1f);
	//static Color proTableBG2Color = new Color(.2f, .2f, .2f, 1f);

	static Color proTableBG3Color = new Color(.15f, .15f, .15f, 1f);
	static Color freeTableBG3Color = new Color(.6f, .6f, .6f, 1f);

	// Issue Colors
	static Color issueBGColor = new Color(0.3f, 0.3f, 0.3f, 1f);
	static Color summaryBGColor = new Color(.6f, .6f, .6f, 1f);
	static Color warningBGColor = new Color(.9f, .74f, .1f, 1f);
	static Color errorBGColor = new Color(1.0f, .45f, .45f, 1f); // new Color(0.45f, 0.05f, 0.05f, 1f);

	static Color issueTitleTextColor = new Color(0f, 0f, 0f, 1f);
	static Color issueBodyTextColorPro = new Color(0.8f, 0.8f, 0.8f, 1f);
	static Color issueBodyTextColorFree = new Color(0.2f, 0.2f, 0.2f, 1f);
	
	static Color buttonFixAutomatedColor = new Color(1f, 1f, 0.6f, 1f);
	static Color buttonFixJumpColor = new Color(0.2f, 1f, 1f, 1f);


	// GUI Layout
	// ======================================
	// Layout Rects
	//static Rect leftColumnRect;
	static Rect leftColumnTopRect;
	//static Rect unlocalisedHeaderRect;
	static Rect issueTableHeaderRect;
	static Rect issueElementRect;
	static Rect issueElementTitleRect;
	static Rect issueSubIssueBackgroundRect;

	static float acculumatedIssueHeight;

	// Scroll
	static Vector2 issueListScrollPos;

	// Table
	static float leftColumnWidth = 160f;
	static float leftColumnRightPadding = 15f;
	static float leftColumnContentWidth;
	static float tableHeaderHeight = 22f;
	static float scrollBarPadding = 10f;

	// Issue
	static float issueTableWidth;
	static float issueItemWidth;
	static float issueItemHeight = 95f;
	static float issuePaddingWidth = 20f;
	//static float issueHeaderHeight = 15f;
	static float issueTitleHeight = 30f;

	static float issueDescriptionWidth;
	static float issueDescriptionHeight = 40;

	static float subIssueBGHorizontalPadding = 10f;
	static float subIssueBGVerticalPadding = 15f;

	// Issue Action
	static float issueActionItemHeightPadding = 10f;
	static float issueActionItemHeight = 30f;
	static float issueActionButtonHeight = 18f;

	// Styles
	static GUISkin guiSkin;
	static GUIStyle issueTitleStyle;
	static GUIStyle issueTitleContainerStyle;
	static GUIStyle issueElementBodyStyle;
	static GUIStyle issueElementPathStyle;
	static GUIStyle subIssueBackgroundStyle;
	static GUIStyle issueActionFixBtnStyle;
	static GUIStyle issueActionJumpBtnStyle;

	/// <summary>
	/// Open WhipCracker Tool
	/// </summary>
	[MenuItem("PlaySide/Whip Cracker (Validator)")]
	public static void OpenWhipCracker()
	{
		InitWhipCracker();
	}

	/// <summary>
	/// 
	/// </summary>
	void OnEnable()
	{
		//InitWhipCracker();
	}

	/// <summary>
	/// 
	/// </summary>
	void OnFocus()
	{
		InitWhipCracker();
		RefreshCategoryIssues();
	}

	/// <summary>
	/// 
	/// </summary>
	static void InitWhipCracker()
	{
		if (thisWindow == null)
			thisWindow = (EditorWindow) EditorWindow.GetWindow(typeof(WhipCracker), true, "Whip Cracker "+ toolVersion);

		windowDimensions = new Vector2(thisWindow.position.width, thisWindow.position.height);
		thisWindow.minSize = minWindowSize;

		// Initialize
		categoryCount = Enum.GetNames(typeof(Categories)).Length;
		enabledCategories = new bool[categoryCount];
		categoryIssueCounts = new int[categoryCount];

		LoadPrefs();

		// Create Issue lists
		summaryIssues = new List<Issue>();
		warningIssues = new List<Issue>();
		errorIssues = new List<Issue>();

		// Style
		isPro = EditorGUIUtility.isProSkin;//Application.HasProLicense();
		bgTexture = new Texture2D(1, 1);
		bgTexture.SetPixel(0, 0, whiteColor);
		bgTexture.Apply();

		RefreshCategoryIssues();
	}

	/// <summary>
	/// 
	/// </summary>
	static void InitStyles()
	{
		// Issue Title
		issueTitleStyle = new GUIStyle();
		issueTitleStyle.fontStyle = FontStyle.Bold;

		// Issue Header Container
		issueTitleContainerStyle = new GUIStyle();
		issueTitleContainerStyle.padding = new RectOffset(0, 0, 5, 0);

		// Issue Body
		issueElementBodyStyle = new GUIStyle();
		issueElementBodyStyle.wordWrap = true;
		issueElementBodyStyle.normal.textColor = (isPro) ? issueBodyTextColorPro : issueBodyTextColorFree; ;

		// Issue Path
		issueElementPathStyle = new GUIStyle();
		issueElementPathStyle.fontStyle = FontStyle.Italic;
		issueElementPathStyle.normal.textColor = (isPro) ? issueBodyTextColorPro : issueBodyTextColorFree; ;

		// SubIssue Background
		subIssueBackgroundStyle = new GUIStyle(((GUIStyle)"CurveEditorBackground"));

		// Issue Fix Button
		issueActionFixBtnStyle	= new GUIStyle(((GUIStyle)"flow node 3"));
		issueActionFixBtnStyle.padding = new RectOffset(0, 0, 25, 0);

		// Issue JumpTo Button
		issueActionJumpBtnStyle = new GUIStyle(((GUIStyle)"flow node 1"));
		issueActionJumpBtnStyle.padding = new RectOffset(0, 0, 25, 0);
	}

	/// <summary>
	/// Load in any saved tool preferences
	/// </summary>
	static void LoadPrefs()
	{
		for (int i = 0; i < categoryCount; i++)
		{
			string catPref = ((Categories)i).ToString() + "_catEnabled";

			if (EditorPrefs.HasKey(catPref))
			{
				// Load Category Enabled states
				enabledCategories[i] = EditorPrefs.GetBool(catPref);
			}
			else
			{
				// Create Category Enabled states if none exist
				enabledCategories[i] = true;
				EditorPrefs.SetBool(catPref, true);
			}

			// Reset Category Issue Count
			categoryIssueCounts[i] = 0;
		}
	}

	/// <summary>
	/// 
	/// </summary>
	void OnGUI()
	{
		windowDimensions = new Vector2(thisWindow.position.width, thisWindow.position.height);

		if (guiSkin == null)
		{
			guiSkin = GUI.skin;
			guiSkin.box.normal.background = bgTexture;
			InitStyles();
		}

		GUI.skin = guiSkin;

		// Issue Table Header
		issueTableHeaderRect = new Rect(leftColumnWidth - scrollBarPadding, 0, windowDimensions.x- leftColumnWidth - 5, tableHeaderHeight);
		GUI.color = isPro ? proLocHeaderColor : freeLocHeaderColor;
		GUI.Box(issueTableHeaderRect, GUIContent.none);

		// Left Column Background
		leftColumnTopRect = new Rect(0, 0, leftColumnWidth, windowDimensions.y);
		//GUI.color = Color.red;// isPro ? proTableBG2Color : freeTableBG2Color;
		//GUI.Box(leftColumnRect, GUIContent.none);

		// Categories Background
		//leftColumnRect = new Rect(0, 0, leftColumnWidth, windowDimensions.y);
		GUI.color = isPro ? proTableBG3Color : freeTableBG3Color;
		GUI.Box(leftColumnTopRect, GUIContent.none);

		
		// Reset GUI color
		GUI.color = whiteColor;

		EditorGUILayout.BeginHorizontal();
		{
			// Left Column
			DrawCategoryNav();

			EditorGUILayout.BeginVertical();
			{
				// Issue Table Header
				DrawIssueTableHeader();

				float listHeight = (windowDimensions.y - 40f);
				issueListScrollPos = EditorGUILayout.BeginScrollView(issueListScrollPos, false, false, GUILayout.Height(listHeight));
				{
					issueItemWidth = windowDimensions.x - leftColumnWidth;
					issueTableWidth = issueItemWidth - issuePaddingWidth - scrollBarPadding;
					acculumatedIssueHeight = 0f;

					// Right column
					EditorGUILayout.BeginVertical(GUILayout.Width(issueTableWidth));
					{
						// Issue Table Elements
						// Reset issue table index
						currentIssueDrawIndex = 0;

						if (errorIssues != null)
							DrawIssueList(errorIssues, IssueTypes.Error);
						if (warningIssues != null)
							DrawIssueList(warningIssues, IssueTypes.Warning);
						if (summaryIssues != null)
							DrawIssueList(summaryIssues, IssueTypes.Summary);
					}
					EditorGUILayout.EndVertical();
				}
				EditorGUILayout.EndScrollView();
			}
			EditorGUILayout.EndVertical();
		}
		EditorGUILayout.EndHorizontal();

		GUI.skin = null;
	}

	/// <summary>
	/// Draw Category Navigation Menu
	/// </summary>
	static void DrawCategoryNav()
	{
		// Pad in content so it doesn't push out the main issue table alignment
		leftColumnContentWidth = leftColumnWidth - leftColumnRightPadding;

		// Left column
		EditorGUILayout.BeginVertical(GUILayout.Width(leftColumnContentWidth));
		{
			EditorGUILayout.LabelField("", GUILayout.Height(10), GUILayout.Width(leftColumnContentWidth));
			EditorGUILayout.LabelField("Categories", EditorStyles.boldLabel, GUILayout.Width(leftColumnContentWidth));

			// Category Toggles
			for (int i = 0; i < categoryCount; i++)
			{
				string categoryText = ((Categories)i).ToString() + ((categoryIssueCounts[i] > 0) ? (" (" + categoryIssueCounts[i] + ")") : "");
				EditorGUI.BeginChangeCheck();
				{
					enabledCategories[i] = EditorGUILayout.ToggleLeft(categoryText, enabledCategories[i], GUILayout.Width(leftColumnContentWidth));

					// Save Category enabled state
					string catPref = ((Categories)i).ToString() + "_catEnabled";
					EditorPrefs.SetBool(catPref, enabledCategories[i]);
				}
				if (EditorGUI.EndChangeCheck())
					RefreshCategoryIssues();
			}

			EditorGUILayout.LabelField("", GUILayout.Width(leftColumnContentWidth));
		}
		EditorGUILayout.EndVertical();
		//EditorGUILayout.Space();
	}

	/// <summary>
	/// Draw Issue Table Header
	/// </summary>
	void DrawIssueTableHeader()
	{
		// Table heading
		EditorGUILayout.BeginHorizontal(GUILayout.Height(tableHeaderHeight));
		{
			EditorGUILayout.LabelField("Issues ("+ totalIssueCount.ToString() + ")", EditorStyles.boldLabel, GUILayout.Width(80));
			//EditorGUILayout.Space();
			//EditorGUILayout.LabelField("", EditorStyles.boldLabel, GUILayout.Width(8));
			//EditorGUILayout.LabelField("Object", EditorStyles.boldLabel, GUILayout.Width(175));
			//EditorGUILayout.LabelField("Text", EditorStyles.boldLabel, GUILayout.Width(300));

			//GUILayout.Label(csvIcon, GUILayout.Width(20), GUILayout.Height(20));
			//EditorGUILayout.LabelField("Line", EditorStyles.boldLabel, GUILayout.Width(35));
			//EditorGUILayout.LabelField("Translation", EditorStyles.boldLabel, GUILayout.Width(250));
			//GUILayout.FlexibleSpace();
		}
		EditorGUILayout.EndHorizontal();
	}

	/// <summary>
	/// Draw Issues Table
	/// </summary>
	/// <param name="_issueType"></param>
	void DrawIssueList(List<Issue> _issueList, IssueTypes _issueType)
	{
		bool isItemCulled = false;
		Color issueBGColor = whiteColor;
		int issueCount = _issueList.Count;
		GUIContent issueIconContent = null;

		// Tint background based on issue type
		switch (_issueType)
		{
			case IssueTypes.Summary:
				issueBGColor = summaryBGColor;
				issueIconContent = EditorGUIUtility.IconContent("console.infoicon.sml");
				break;
			case IssueTypes.Warning:
				issueBGColor = warningBGColor;
				issueIconContent = EditorGUIUtility.IconContent("console.warnicon.sml");
				break;
			case IssueTypes.Error:
				issueBGColor = errorBGColor;
				issueIconContent = EditorGUIUtility.IconContent("console.erroricon.sml");
				break;
		}

		for (int i = 0; i < issueCount; i++)
		{
			// Skip this item if offscreen
			if (isItemCulled)
			{
				currentIssueDrawIndex++;
				EditorGUILayout.BeginHorizontal(GUILayout.Height(issueItemHeight));
				EditorGUILayout.EndHorizontal();
				acculumatedIssueHeight += issueItemHeight;
			}
			else
			{
				DrawIssue(_issueList[i], _issueType, issueBGColor, issueIconContent);
				currentIssueDrawIndex++;
			}
		}
	}

	/// <summary>
	/// Draw an Issue table element
	/// </summary>
	/// <param name="_issue"></param>
	/// <param name="_issueType"></param>
	/// <param name="_issueTitleBGColor"></param>
	void DrawIssue(Issue _issue, IssueTypes _issueType, Color _issueTitleBGColor, GUIContent _issueIconContent)
	{
		float currentIssueHeight = issueItemHeight;

		// Expand height to fit additional issue context actions
		float subIssuesHeight = (!_issue.subIssuesVisible) ? (0) : ((Mathf.Max(0, _issue.subIssueCount - 1) * issueActionItemHeight));

		// Additional padding if issue context actions are present
		if (_issue.subIssueCount > autoIssueCollapseThreshold && _issue.subIssuesVisible)
			subIssuesHeight += issueActionItemHeight;
		else if (_issue.subIssueCount > 0)
			subIssuesHeight += issueActionItemHeightPadding;

		// Add SubIssue height to Issue height
		currentIssueHeight += subIssuesHeight;

		EditorGUILayout.BeginHorizontal(GUILayout.Height(currentIssueHeight));
		{
			// Issue BG, draw before title
			GUI.color = issueBGColor;
			issueElementRect = new Rect(0, acculumatedIssueHeight, issueItemWidth, currentIssueHeight - 2);
			GUI.Box(issueElementRect, GUIContent.none);

			// Issue Title BG
			GUI.color = _issueTitleBGColor;
			issueElementTitleRect = new Rect(0, acculumatedIssueHeight, issueItemWidth, issueTitleHeight);
			GUI.Box(issueElementTitleRect, GUIContent.none);

			// SubIssue BG Rect
			float subIssueYPos = acculumatedIssueHeight + issueDescriptionHeight + issueTitleHeight;
			issueSubIssueBackgroundRect = new Rect(subIssueBGVerticalPadding, subIssueYPos, (issueItemWidth - subIssueBGVerticalPadding * 2), (subIssuesHeight + issueTitleHeight - subIssueBGHorizontalPadding));

			GUI.color = whiteColor;
			GUI.Box(issueSubIssueBackgroundRect, GUIContent.none, subIssueBackgroundStyle);

			// Draw Issue Information
			EditorGUILayout.BeginVertical(GUILayout.Width(issueTableWidth));
			{
				// Issue Title
				EditorGUILayout.BeginHorizontal(issueTitleContainerStyle);
				{
					// Draw Issue Type Icon
					GUI.color = whiteColor;
					GUILayout.Label(_issueIconContent, GUILayout.Width(20), GUILayout.Height(20));

					// Issue Title
					GUI.color = issueTitleTextColor;
					EditorGUILayout.LabelField(_issue.issueTitle, issueTitleStyle);

					GUILayout.FlexibleSpace();

					// Copy issue to clipboard button
					GUI.color = whiteColor;
					if (GUILayout.Button("Copy Info"))
						CopyIssueToClipboard(_issue);
				}
				EditorGUILayout.EndHorizontal();

				EditorGUILayout.Space();

				// Issue Content
				EditorGUILayout.BeginHorizontal();
				{
					// Left padding
					GUILayout.Space(issuePaddingWidth);

					EditorGUILayout.BeginVertical(GUILayout.Width(issueTableWidth));
					{
						// Comment, calculate required height to fit whole comment
						GUI.color = whiteColor;
						//issueDescriptionHeight = issueElementBodyStyle.CalcHeight(new GUIContent(_issue.issueComment), issueDescriptionWidth);
						EditorGUILayout.LabelField(_issue.issueComment, issueElementBodyStyle, GUILayout.Height(issueDescriptionHeight));

						// Space between comment and location

						if(_issue.subIssueCount >= autoIssueCollapseThreshold)
						{
							_issue.subIssuesVisible = EditorGUILayout.Foldout(_issue.subIssuesVisible, "Issues ("+ _issue .subIssueCount+ ")");
						}


						// Context Objects
						if (_issue.subIssuesVisible)
						{
							for (int c = 0; c < _issue.subIssueCount; c++)
							{
								EditorGUILayout.BeginHorizontal(GUILayout.Height(issueActionItemHeight));
								{
									if (_issue.issueFixActions != null)
									{
										GUI.color = whiteColor;
										GUI.backgroundColor = (_issue.automatedFix ? buttonFixAutomatedColor : buttonFixJumpColor);

										if (GUILayout.Button((_issue.automatedFix ? "Fix" : "Goto"), (_issue.automatedFix ? issueActionFixBtnStyle : issueActionJumpBtnStyle), GUILayout.Width(70), GUILayout.Height(issueActionButtonHeight)))
											_issue.issueFixActions[c]();

										GUI.backgroundColor = whiteColor;
									}

									// Path
									//GUI.color = issueTitleTextColor;
									EditorGUILayout.LabelField(_issue.paths[c], issueElementPathStyle);

								}
								EditorGUILayout.EndHorizontal();
							}
						}
					}
					EditorGUILayout.EndHorizontal();
				}
				EditorGUILayout.EndVertical();
			}
			EditorGUILayout.EndVertical();
		}
		EditorGUILayout.EndHorizontal();

		// Incremement Issue element Y Pos offset
		acculumatedIssueHeight += currentIssueHeight;
	}

	/// <summary>
	/// Get current category issues
	/// </summary>
	static void RefreshCategoryIssues()
	{
		// Clear Issue Lists
		if (summaryIssues == null)
			summaryIssues = new List<Issue>();
		else
			summaryIssues.Clear();

		if (warningIssues == null)
			warningIssues = new List<Issue>();
		else
			warningIssues.Clear();

		if (errorIssues == null)
			errorIssues = new List<Issue>();
		else
			errorIssues.Clear();

		// Reset Issue Counts
		for (int i = 0; i < categoryCount; i++)
			categoryIssueCounts[i] = 0;

		// Get Issues
		if (enabledCategories[(int)Categories.Build])
			categoryIssueCounts[(int)Categories.Build]		= GetBuildIssues();

		if (enabledCategories[(int)Categories.GameObject])
			categoryIssueCounts[(int)Categories.GameObject] = GetGameObjectIssues();

		if (enabledCategories[(int)Categories.UI])
			categoryIssueCounts[(int)Categories.UI]			= GetUIIssues();

		if (enabledCategories[(int)Categories.Art])
			categoryIssueCounts[(int)Categories.Art]		= GetArtIssues();

		if (enabledCategories[(int)Categories.Audio])
			categoryIssueCounts[(int)Categories.Audio]		= GetAudioIssues();

		// Tally issues
		totalIssueCount = summaryIssues.Count + warningIssues.Count + errorIssues.Count;
	}

	/// <summary>
	/// Adds Issue to associated IssueType list
	/// </summary>
	/// <param name="_issueToAdd"></param>
	static void AddIssueToList(Issue _issueToAdd)
	{
		switch(_issueToAdd.issueType)
		{
			case IssueTypes.Summary:
				summaryIssues.Add(_issueToAdd);
				break;
			case IssueTypes.Warning:
				warningIssues.Add(_issueToAdd);
				break;
			case IssueTypes.Error:
				errorIssues.Add(_issueToAdd);
				break;
		}
	}

	/// <summary>
	/// Clipboard logging issues for task-management-readable format
	/// </summary>
	/// <param name="_issue"></param>
	static void CopyIssueToClipboard(Issue _issue)
	{
		string issueCopyBuffer = "";

		issueCopyBuffer += _issue.issueTitle + "\n_______________________________________\n";
		issueCopyBuffer += _issue.issueComment + "\n\n";

		for(int i=0; i<_issue.paths.Length; i++)
			issueCopyBuffer += _issue.paths[i] + "\n";

		EditorGUIUtility.systemCopyBuffer = issueCopyBuffer;
	}
}
