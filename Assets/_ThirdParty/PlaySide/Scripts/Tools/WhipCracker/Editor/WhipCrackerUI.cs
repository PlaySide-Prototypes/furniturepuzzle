﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System;

public partial class WhipCracker : EditorWindow
{
	/// <summary>
	/// 
	/// </summary>
	/// <returns></returns>
	static int GetUIIssues()
	{
		int uiIssueCount = 0;

		uiIssueCount += ICOrientationUIAndCamera();
		uiIssueCount += ICLocalisationUntranslatedText();
		uiIssueCount += ICEmptyButtonEvents();
		uiIssueCount += ICDeprecatedPanelSwitchNotifiers();

		return uiIssueCount;
	}

	/// <summary>
	/// Issue check for UI Orientation vs Camera Orientation
	/// </summary>
	static int ICOrientationUIAndCamera()
	{
		// TODO: Better method to determin the main canvas
		CanvasScaler[] canvasScalers = GameObject.FindObjectsOfType<CanvasScaler>();

		CanvasScaler canvasScaler = null;
		if (canvasScalers != null && canvasScalers.Length > 0)
			canvasScaler = GameObject.FindObjectsOfType<CanvasScaler>()[0];

		if (canvasScaler != null)
		{
			bool isUILandscape = canvasScaler.referenceResolution.x > canvasScaler.referenceResolution.y;
			bool isCameraLandscape = (PlayerSettings.defaultInterfaceOrientation == UIOrientation.LandscapeLeft) || (PlayerSettings.defaultInterfaceOrientation == UIOrientation.LandscapeRight);

			// Check if 
			if ((isUILandscape != isCameraLandscape) && !(PlayerSettings.defaultInterfaceOrientation == UIOrientation.AutoRotation))
			{
				Issue newIssue = new Issue();

				newIssue.issueType = IssueTypes.Error;
				newIssue.issueTitle = "Screen orientation mismatch";
				newIssue.issueComment = "UI and PlayerSettings Orientation Mismatch";

				newIssue.contextObjects = new GameObject[1] { canvasScaler.gameObject };
				newIssue.subIssueCount = 1;
				newIssue.paths = new string[1] { "' OR 'Player Settings/Resolution and Presentation/Default Orientation'" };

				newIssue.issueFixActions = new Action[1] { (() => Debug.Log("Fixing: " + newIssue.contextObjects[0].name)) };

				AddIssueToList(newIssue);
				return 1;
			}
		}

		return 0;
	}

	/// <summary>
	/// Issue check for UI Orientation vs Camera Orientation
	/// </summary>
	static int ICLocalisationUntranslatedText()
	{
		int untranslatedTextCount = Playside.LocalisationHub.GetUntranslatedTextCount();

		if (untranslatedTextCount > 0)
		{
			Issue newIssue = new Issue();

			newIssue.issueType = IssueTypes.Error;
			newIssue.issueTitle = "Untranslated Localised text found ("+ untranslatedTextCount+")";
			newIssue.issueComment = "Ensure localised strings are present in CSV and enable 'Is Translated'";

			newIssue.subIssueCount = 1;
			newIssue.paths = new string[1] { "Check Localisation Hub" };

			newIssue.issueFixActions = new Action[1] { (() => EditorApplication.ExecuteMenuItem("PlaySide/Localisation Hub")) };

			AddIssueToList(newIssue);
			return untranslatedTextCount;
		}

		return untranslatedTextCount;
	}

	

	/// <summary>
	/// Issue check for empty or missing button onclicks
	/// </summary>
	static int ICEmptyButtonEvents()
	{
		Button[] buttons = Resources.FindObjectsOfTypeAll<Button>();
		List<Button> buttonsMissingEvents = new List<Button>(0);

		for(int i=0; i< buttons.Length;i++)
		{
			int eventCount = buttons[i].onClick.GetPersistentEventCount();

			for(int k=0; k< eventCount; k++)
			{
				// Button has missing onclick
				if (buttons[i].onClick.GetPersistentTarget(k) == null || string.IsNullOrEmpty(buttons[i].onClick.GetPersistentMethodName(k)))
				{
					buttonsMissingEvents.Add(buttons[i]);
					break;
				}
			}
		}

		// Create Issue if missing button events found
		if (buttonsMissingEvents.Count > 0)
		{
			Issue newIssue = new Issue();
			newIssue.subIssueCount = buttonsMissingEvents.Count;
			newIssue.contextObjects = new GameObject[newIssue.subIssueCount];
			newIssue.paths = new string[newIssue.subIssueCount];
			newIssue.issueFixActions = new Action[newIssue.subIssueCount];

			newIssue.issueType = IssueTypes.Warning;
			newIssue.issueTitle = "Missing Button OnClick Events (" + buttonsMissingEvents.Count + ")";
			newIssue.issueComment = "Button OnClick has a missing or unlinked reference to an object or method";

			for (int b = 0; b < newIssue.subIssueCount; b++)
			{
				GameObject contextObject = buttonsMissingEvents[b].gameObject;
				newIssue.contextObjects[b] = contextObject;
				newIssue.paths[b] = contextObject.transform.root.name + "/" + AnimationUtility.CalculateTransformPath(contextObject.transform, contextObject.transform.root);

				// Select and Focus GameObject
				newIssue.issueFixActions[b] = () =>	{
					Selection.objects = new GameObject[1] { contextObject };
					SceneView.FrameLastActiveSceneView();
				};
			}

			// Collapse if more issues than autoIssueCollapseThreshold
			newIssue.subIssuesVisible = (newIssue.subIssueCount < autoIssueCollapseThreshold);

			AddIssueToList(newIssue);
			return newIssue.subIssueCount;
		}

		return 0;
	}

	/// <summary>
	/// Issue check for use of deprecated PanelSwitchNotifier components
	/// </summary>
	static int ICDeprecatedPanelSwitchNotifiers()
	{
		PanelSwitchNotifier[] panelSwitchNotifiers = Resources.FindObjectsOfTypeAll<PanelSwitchNotifier>();

		// Create Issue if missing button events found
		if (panelSwitchNotifiers.Length > 0)
		{
			Issue newIssue = new Issue();
			newIssue.subIssueCount = panelSwitchNotifiers.Length;
			newIssue.contextObjects = new GameObject[newIssue.subIssueCount];
			newIssue.paths = new string[newIssue.subIssueCount];
			newIssue.issueFixActions = new Action[newIssue.subIssueCount];

			newIssue.issueType = IssueTypes.Warning;
			newIssue.issueTitle = "Found PanelSwitchNotifier deprecated components (" + panelSwitchNotifiers.Length + ")";
			newIssue.issueComment = "Replace these with PanelNotifier components";

			for (int b = 0; b < newIssue.subIssueCount; b++)
			{
				GameObject contextObject = panelSwitchNotifiers[b].gameObject;
				newIssue.contextObjects[b] = contextObject;
				newIssue.paths[b] = contextObject.transform.root.name + "/" + AnimationUtility.CalculateTransformPath(contextObject.transform, contextObject.transform.root);

				// Select and Focus GameObject
				newIssue.issueFixActions[b] = () => {
					Selection.objects = new GameObject[1] { contextObject };
					SceneView.FrameLastActiveSceneView();
				};
			}

			// Collapse if more issues than autoIssueCollapseThreshold
			newIssue.subIssuesVisible = (newIssue.subIssueCount < autoIssueCollapseThreshold);

			AddIssueToList(newIssue);
			return newIssue.subIssueCount;
		}

		return 0;
	}
}
