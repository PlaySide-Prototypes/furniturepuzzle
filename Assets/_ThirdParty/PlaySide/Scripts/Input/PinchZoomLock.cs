﻿using UnityEngine;

public class PinchZoomLock : MonoBehaviour 
{
	/// <summary> Called when object/script is enabled in the hierarchy </summary>
	void OnEnable()
	{
		PinchZoom.instance.BlockInput();
	}

	/// <summary> Called when object/script is disabled in the hierarchy </summary>
	void OnDisable()
	{
		PinchZoom.instance.UnblockInput();
	}
}
