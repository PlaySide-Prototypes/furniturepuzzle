﻿using UnityEngine;

public class TouchGestures : MonoBehaviour
{
	public enum				GestureTypes { None, SwipeUp, SwipeLeft, SwipeRight, SwipeDown, Tap, SwipeLeftRight, SwipeUpDown };
	enum					States { None, RecentlyDown, SwipeRecognised };
	
	#region Inspector variables

	[SerializeField] float	swipeDistanceInches = 0.25f;		// Distance for a swipe - longer = slower but more accurate

	#endregion	// Inspector variables

	Vector3					touchDownPos;						// The initial position touched
	States					state;								// What the finger/stylus is currently doing
	float					swipeRecogniseDistanceSquared;		// Cached squared distance

	/// <summary> Called when the object/script activates </summary>
	void Awake()
	{
		ResetGestures();
		float swipeDistancePixels = Screen.dpi * swipeDistanceInches;

#if UNITY_ANDROID
//		swipeDistancePixels /= 2; //Gestures are odd since moving to half scale.
#endif

		swipeRecogniseDistanceSquared = swipeDistancePixels * swipeDistancePixels;
	}

	/// <summary> Resets touch input </summary>
	public void ResetGestures()
	{
		state = States.None;
		touchDownPos = Vector3.zero;
	}

	/// <summary> Call this once per frame to update gestures </summary>
	public GestureTypes UpdateGestures()
	{
#if UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.LeftArrow))
			return GestureTypes.SwipeLeft;
		else if (Input.GetKeyDown(KeyCode.RightArrow))
			return GestureTypes.SwipeRight;
		else if (Input.GetKeyDown(KeyCode.UpArrow))
			return GestureTypes.SwipeUp;
		else if (Input.GetKeyDown(KeyCode.DownArrow))
			return GestureTypes.SwipeDown;
		else if (Input.GetKeyDown(KeyCode.Space))
			return GestureTypes.Tap;
#endif
		switch (state)
		{
			case States.None:
				if (Input.GetMouseButtonDown(0))
				{
					touchDownPos = Input.mousePosition;
					state = States.RecentlyDown;
				}
				return GestureTypes.None;
				
			case States.RecentlyDown:
				// Two fingers?
				if (Input.touches.Length > 1)
				{
					state = States.None;
					return GestureTypes.None;
				}
				else
				{
					// Moved far enough yet?
					Vector3 posDiff = Input.mousePosition - touchDownPos;
					if (posDiff.sqrMagnitude > swipeRecogniseDistanceSquared)
					{
						state = States.SwipeRecognised;
	
						// Moved further vertically or horizontally?
						if (Mathf.Abs(posDiff.y) > Mathf.Abs(posDiff.x))
							return (posDiff.y > 0) ? GestureTypes.SwipeUp : GestureTypes.SwipeDown;
						else
							return (posDiff.x > 0) ? GestureTypes.SwipeRight : GestureTypes.SwipeLeft;
					}
					else
					{
						// If finger/stylus has released, end gesture
						if (Input.GetMouseButtonUp(0))
						{
							state = States.None;
							return GestureTypes.Tap;
						}
						else
							return GestureTypes.None;
					}
				}

			case States.SwipeRecognised:
				// If finger has released, reset state
				if (Input.GetMouseButtonUp(0))
					state = States.None;

				return GestureTypes.None;
				
			default:
				throw new UnityException("Unhandled finger/stylus state '" + state + "'");
		}
	}	
}
