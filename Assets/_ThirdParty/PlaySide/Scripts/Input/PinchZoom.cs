﻿using UnityEngine;

public class PinchZoom : MonoBehaviour
{
	enum PanPlanes { LocalXZRelative, LocalXYRelative };

	[System.Serializable]
	public class CameraLimits
	{
		public float left;
		public float right;
		public float top;
		public float bottom;
	}

	#region Inspector variables

	[Header("Hierarchy")]
	[SerializeField] Camera childCamera = null;

	[Header("Panning")]
	[SerializeField] PanPlanes panPlane = PanPlanes.LocalXZRelative;
	[SerializeField] Vector2 panSpeed = new Vector2(1f, 2f);
	[SerializeField] float panDecelRate = 0.85f;

	[Header("Zooming")]
	[SerializeField] float minZoom = 12f;
	[SerializeField] float maxZoom = 30f;
	[SerializeField] float zoomDecelRate = 0.85f;

	[Header("Snapping to Locations")]
	[SerializeField] float snapToPanSpeed = 10.0f;
	[SerializeField] float snapToZoomSpeed = 1.0f;
	[SerializeField] float snapToThreshold = 0.001f;

	#endregion  // Editor variables

	// Locally cached stuff
	Transform myTrans;

	// Panning boundaries
	internal float panLimitsLeft = float.MinValue;
	internal float panLimitsRight = float.MaxValue;
	internal float panLimitsTop = float.MinValue;
	internal float panLimitsBottom = float.MaxValue;

	// Touch specific variables
	enum TouchStates { None, Down, RecentlyUp }
	TouchStates touchStateOneFinger, touchStateTwoFingers;
	Vector2?[] oldTouchPositions = { null, null };
	Vector2 oldTouchVector;
	float oldTouchDistance;
	Vector2 oldTouchMidPos;

	// Deceleration - panning
	Vector3[] panVels = new Vector3[10];
	int panVelsSaved;
	Vector3 avePanVel;

	// Deceleration - zooming
	float[] zoomVels = new float[10];
	int zoomVelsSaved;
	float aveZoomVel;

	// States / modes
	public int inputBlockers { get; private set; }
	public void BlockInput() { ++inputBlockers; }
	public void UnblockInput() { if (inputBlockers > 0) --inputBlockers; }
	public bool IsSnapping() { return (snapToDestination != null) || (snapToZoom != 0.0f); }

	Vector3? snapToDestination = null;
	float snapToZoom;
	float zoomBeforeSnapping;
	bool orthographic;

	float CurrentZoom
	{
		get { return (orthographic ? childCamera.orthographicSize : childCamera.fieldOfView); }
		set
		{
			if (orthographic)
				childCamera.orthographicSize = value;
			else
				childCamera.fieldOfView = value;
		}
	}

	/// <summary> Singleton </summary>
	public static PinchZoom instance;

	/// <summary> Called when object/script activates </summary>
	void Awake()
	{
		if (instance != null)
			throw new UnityException("Singleton instance already exists");
		instance = this;

		myTrans = transform;
		orthographic = childCamera.orthographic;

		Reset();
	}

	/// <summary> Resets all touch control </summary>
	public void Reset()
	{
		ResetPanMomentum();
		ResetZoomMomentum();
	}

	/// <summary> Sets the PinchZoom enabled/disabled </summary>
	public void SetEnabled(bool _enable)
	{
		this.enabled = _enable;
	}

	/// <summary> Called once per frame </summary>
	void Update()
	{
		// Snapping to a target position takes preference over user input
		if (IsSnapping())
			UpdateSnapping();
		else
		{
			int touchCount = Input.touchCount;

			#if UNITY_EDITOR
			UpdateMouseZooming();
			#else
			UpdateTouchZooming(touchCount);
			#endif

			UpdateOneFingerPanning(touchCount);
		}
	}

	/// <summary> Called when object/Behaviour is destroyed </summary>
	void OnDestroy()
	{
		if (instance == this)
			instance = null;
	}

	/// <summary> Starts the camera moving towards a target position </summary>
	/// <param name="_pos"> Target position (null to not pan) </param>
	/// <param name="_zoom"> Target zoom (optional) </param>
	public void StartSnappingTo(Vector3? _pos, float _zoom = 0.0f)
	{
		snapToDestination = _pos;
		snapToZoom = _zoom;
		zoomBeforeSnapping = CurrentZoom;
	}

	/// <summary> Zooms back to the saved level before snapping </summary>
	public void RestorePreSnapZoom(Vector3? _snapToPos = null)
	{
		StartSnappingTo(_snapToPos, zoomBeforeSnapping);
		zoomBeforeSnapping = 0.0f;
	}

	/// <summary> Updates snapping towards a target position </summary>
	void UpdateSnapping()
	{
		// Update snapping
		if (snapToDestination != null)
		{
			Vector3 moveVel = childCamera.WorldToViewportPoint((Vector3)snapToDestination);

			// Convert (x,y) viewport position to (x,y,z) world position
			moveVel.x -= 0.5f;
			if (panPlane == PanPlanes.LocalXZRelative)
			{
				moveVel.z = moveVel.y - 0.5f;
				moveVel.y = 0.0f;
			}
			else
			{
				moveVel.y = moveVel.z - 0.5f;
				moveVel.z = 0.0f;
			}

			// Move towards it, finishing if reached
			if (moveVel.sqrMagnitude < snapToThreshold)
				snapToDestination = null;
			else
				myTrans.position += myTrans.TransformVector(moveVel) * snapToPanSpeed;
		}

		// Update zooming
		if (snapToZoom != 0.0f)
		{
			float zoom = CurrentZoom;
			if (zoom < snapToZoom)
			{
				zoom += snapToZoomSpeed;
				if (zoom > snapToZoom)
				{
					zoom = snapToZoom;
					snapToZoom = 0.0f;
				}
			}
			else
			{
				zoom -= snapToZoomSpeed;
				if (zoom < snapToZoom)
				{
					zoom = snapToZoom;
					snapToZoom = 0.0f;
				}
			}

			CurrentZoom = zoom;
		}
	}

	/// <summary> Sets all 4 camera panning limits </summary>
	/// <param name="_limits"> Camera limits </param>
	public void SetCameraLimits(CameraLimits _limits, bool overrideOnly = true)
	{
		panLimitsLeft = _limits.left;
		panLimitsRight = _limits.right;
		panLimitsTop = _limits.top;
		panLimitsBottom = _limits.bottom;
	}

	/// <summary> Clamps the position to within the current camera limits </summary>
	/// <param name="pos"> Position to limit </param>
	/// <returns> Clamped position </returns>
	Vector3 LimitPos(Vector3 pos)
	{
		if (pos.x < panLimitsLeft)
			pos.x = panLimitsLeft;
		if (pos.x > panLimitsRight)
			pos.x = panLimitsRight;
		if (pos.z < panLimitsTop)
			pos.z = panLimitsTop;
		if (pos.z > panLimitsBottom)
			pos.z = panLimitsBottom;

		return pos;
	}

	/// <summary> Updates the panning with one finger or left mouse button </summary>
	void UpdateOneFingerPanning(int _touchCount)
	{
		switch (touchStateOneFinger)
		{
			case TouchStates.None:
				if ((inputBlockers == 0) && Input.GetMouseButtonDown(0))
				{
					#if !UNITY_EDITOR
					if (_touchCount == 1)
					#endif
					{
						ResetPanMomentum();
						touchStateOneFinger = TouchStates.Down;
					}
				}
				break;

			case TouchStates.Down:
				if (!Input.GetMouseButton(0) || (inputBlockers != 0))
				{
					if (touchStateOneFinger == TouchStates.Down)
						StartPanMomentum();
				}
				else
				{
					#if !UNITY_EDITOR
					if (_touchCount == 1)
					#endif
					{
						Vector2 newTouchPosition = Input.mousePosition;
						if (oldTouchPositions[0] == null)
							oldTouchPositions[0] = newTouchPosition;

						Vector2 touchDelta = (Vector2)(oldTouchPositions[0] - newTouchPosition);
						Pan(touchDelta);

						oldTouchPositions[0] = newTouchPosition;
					}
				}
				break;

			case TouchStates.RecentlyUp:
				if (Input.GetMouseButtonDown(0))
				{
					ResetPanMomentum();
					touchStateOneFinger = TouchStates.Down;
				}
				else
					UpdatePanMomentum();
				break;

			default:
				throw new UnityException("Unhandled One Finger state " + touchStateOneFinger);
		}
	}

	/// <summary> Pans the camera around the map </summary>
	/// <param name="_inputDelta"> How much the input screen position has changed </param>
	void Pan(Vector2 _inputDelta)
	{
		_inputDelta *= (CurrentZoom / childCamera.pixelHeight * 2f);
		_inputDelta.x *= panSpeed.x; _inputDelta.y *= panSpeed.y;
		Vector3 inputDeltaVec3 = (panPlane == PanPlanes.LocalXZRelative) ? new Vector3(_inputDelta.x, 0.0f, _inputDelta.y) : new Vector3(_inputDelta.x, _inputDelta.y, 0.0f);

		Vector3 prevPos = myTrans.position;
		Vector3 newPos = prevPos + myTrans.TransformVector(inputDeltaVec3);
		myTrans.position = LimitPos(newPos);
		UpdatePanHistory(myTrans.position - prevPos);
	}

	#if UNITY_EDITOR
	/// <summary> Updates control with a mouse </summary>
	void UpdateMouseZooming()
	{
		if (inputBlockers != 0)
			return;

		const float scrollSpeed = 30f;

		if (Input.GetAxis("Mouse ScrollWheel") != 0)
			CurrentZoom = Mathf.Clamp((CurrentZoom - Input.GetAxis("Mouse ScrollWheel") * scrollSpeed), minZoom, maxZoom);
	}
	#endif

	/// <summary> Updates control on a device's touchscreen </summary>
	void UpdateTouchZooming(int _touchCount)
	{
		if ((_touchCount == 2) && (inputBlockers == 0))
		{
			Vector2[] newTouchPositions = { Input.GetTouch(0).position, Input.GetTouch(1).position };

			// Just changed to 2 fingers?
			if (touchStateTwoFingers != TouchStates.Down)
			{
				ResetPanHistory();
				ResetZoomMomentum();

				touchStateTwoFingers = TouchStates.Down;

				oldTouchPositions[0] = newTouchPositions[0];
				oldTouchPositions[1] = newTouchPositions[1];

				oldTouchVector = (Vector2)(oldTouchPositions[0] - oldTouchPositions[1]);
				oldTouchDistance = oldTouchVector.magnitude;

				oldTouchMidPos = (Vector2)(oldTouchPositions[0] + oldTouchPositions[1]) * 0.5f;
			}
			// Still down from before
			else
			{
				Vector2 newTouchVector = newTouchPositions[0] - newTouchPositions[1];
				float newTouchDistance = newTouchVector.magnitude;

				// Avoid null ref when lifting one finger
				if ((oldTouchPositions[0] == null) || (oldTouchPositions[1] == null))
					return;

				// Use mid position's delta for panning
				Vector2 newTouchMidPos = (Vector2)(oldTouchPositions[0] + oldTouchPositions[1]) * 0.5f;
				Vector2 moveDelta = oldTouchMidPos - newTouchMidPos;
				Pan(moveDelta);
				oldTouchMidPos = newTouchMidPos;

				// Use distance between positions for zooming
				float delta = oldTouchDistance / newTouchDistance;
				float prevZoom = CurrentZoom;
				CurrentZoom = Mathf.Clamp(prevZoom * delta, minZoom, maxZoom);
				UpdateZoomHistory(CurrentZoom - prevZoom);

				oldTouchPositions[0] = newTouchPositions[0];
				oldTouchPositions[1] = newTouchPositions[1];
				oldTouchVector = newTouchVector;
				oldTouchDistance = newTouchDistance;
			}
		}
		else
		{
			// Just released?
			if (touchStateTwoFingers == TouchStates.Down)
				StartZoomMomentum();
			// Slowing down?
			else if (touchStateTwoFingers == TouchStates.RecentlyUp)
				UpdateZoomMomentum();
		}
	}

	#region Pan momentum

	/// <summary> Saves this frame's velocity to the history for momentum </summary>
	/// <param name="_velocity"> Current velocity </param>
	void UpdatePanHistory(Vector3 _velocity)
	{
		panVels[panVelsSaved] = _velocity;

		panVelsSaved++;
		if (panVelsSaved == panVels.Length)
			panVelsSaved -= panVels.Length;
	}

	/// <summary> Starts the slowing-down momentum after releasing </summary>
	public void StartPanMomentum(Vector3? _velOverride = null)
	{
		if (_velOverride != null)
			avePanVel = myTrans.TransformVector((Vector3)_velOverride);
		else
		{
			avePanVel = Vector3.zero;
			int count = Mathf.Min(panVels.Length, panVelsSaved);
			for (int i = 0; i < count; ++i)
				avePanVel += panVels[i];

			if (count != 0)
				avePanVel /= count;
		}

		touchStateOneFinger = TouchStates.RecentlyUp;
	}

	/// <summary> Starts the slowing-down momentum after releasing </summary>
	public void UpdatePanMomentum()
	{
		avePanVel *= panDecelRate;
		myTrans.position = LimitPos(myTrans.position + avePanVel);

		if (avePanVel.sqrMagnitude < 0.01f)
		{
			ResetPanMomentum();
			touchStateOneFinger = TouchStates.None;
		}
	}

	/// <summary> Resets the slowing-down momentum + saved velocities </summary>
	public void ResetPanMomentum()
	{
		oldTouchPositions[0] = null;
		oldTouchPositions[1] = null;

		ResetPanHistory();

		touchStateOneFinger = TouchStates.None;
	}

	/// <summary> Resets the saved panning velocities </summary>
	void ResetPanHistory()
	{
		panVelsSaved = 0;
		avePanVel = Vector3.zero;
		for (int i = 0; i < panVels.Length; ++i)
			panVels[i] = Vector3.zero;
	}

	#endregion	// Pan momentum

	#region Zoom momentum

	/// <summary> Saves this frame's velocity to the history for momentum </summary>
	/// <param name="_velocity"> Current velocity </param>
	void UpdateZoomHistory(float _zoomVel)
	{
		zoomVels[zoomVelsSaved] = _zoomVel;

		zoomVelsSaved++;
		if (zoomVelsSaved == zoomVels.Length)
			zoomVelsSaved -= zoomVels.Length;
	}

	/// <summary> Starts the slowing-down momentum after releasing </summary>
	void StartZoomMomentum()
	{
		aveZoomVel = 0.0f;
		int count = Mathf.Min(zoomVels.Length, zoomVelsSaved);

		for (int i = 0; i < count; ++i)
			aveZoomVel += zoomVels[i];

		if (count != 0)
			aveZoomVel /= count;

		touchStateTwoFingers = TouchStates.RecentlyUp;
	}

	/// <summary> Starts the slowing-down momentum after releasing </summary>
	void UpdateZoomMomentum()
	{
		aveZoomVel *= zoomDecelRate;
		CurrentZoom = Mathf.Clamp(CurrentZoom + aveZoomVel, minZoom, maxZoom);

		if (Mathf.Abs(aveZoomVel) < 0.001f)
		{
			ResetZoomMomentum();
			touchStateTwoFingers = TouchStates.None;
		}
	}

	/// <summary> Resets the slowing-down momentum history </summary>
	void ResetZoomMomentum()
	{
		zoomVelsSaved = 0;
		aveZoomVel = 0.0f;
		oldTouchPositions[0] = null;
		oldTouchPositions[1] = null;
		for (int i = 0; i < zoomVels.Length; ++i)
			zoomVels[i] = 1.0f;

		touchStateTwoFingers = TouchStates.None;
	}

	#endregion	// Zoom momentum

	#region On-screen debugging
/*
	/// <summary> DEBUG </summary>
	Rect onGUIRect = new Rect(0.0f, 0.0f, 300.0f, 500.0f);
	GUIStyle guiStyle;
	void OnGUI()
	{
		if (guiStyle == null)
		{
			guiStyle = new GUIStyle();
			guiStyle.fontStyle = FontStyle.Bold;
			guiStyle.fontSize = 20;
			guiStyle.normal.textColor = Color.white;
		}

		GUI.Label(onGUIRect,	"Parent pos: " + transform.position +
			"\nChild camera pos: " + childCamera.transform.position +
			"\nChild camera local pos: " + childCamera.transform.localPosition +
			"\nSnapTo Dest pos = " + ((snapToDestination != null) ? (childCamera.ScreenToWorldPoint((Vector3)snapToDestination) - new Vector3(Screen.width * 0.5f, 0.0f, Screen.height * 0.5f)).ToString() : "none") +
			"\nOne finger state: " + touchStateOneFinger +
			"\nTwo finger state: " + touchStateTwoFingers +
			"\nzoomVels.Length = " + zoomVelsSaved +
			"\naveZoomVel = " + aveZoomVel + 
			"\nTouch Distance = " + oldTouchDistance +
			"\nTouch Vector = " + oldTouchVector,
			guiStyle);
	}
*/
	#endregion	// On-screen debugging
}

/*
 AsciiChog is watching you.
      _____
   <={ . o }=>
      \ x /
      -----
     |  _  |
     | -_- |
     |  -  |
      -----
      || V
       v .
*/
