﻿#if FALSE

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_StoreButton : MonoBehaviour
{
	public string productId;
	public Text priceTxt;
	public UnityEvent onPurchaseSuccess;

	[Header("Quantity")]
	public Text qtyText;
	public int defaultQty;

	int qty;

	private void Awake()
	{
		RemoteSettings.Updated += UpdateQty;
	}

	public void OnEnable()
	{
		if (priceTxt)
			priceTxt.text = Purchaser.Inst.GetPrice(productId);
		UpdateQty();
	}

	private void UpdateQty()
	{
		qty = Store.Inst.GetProductQty(productId);

		if (qtyText)
			qtyText.text = "x" + qty;

		if (Store.QtyLookup.ContainsKey(productId))
			Store.QtyLookup[productId] = qty;
		else
			Store.QtyLookup.Add(productId, qty);
	}

	public void BtnPurchase()
	{
		PanelManager.Instance.EnableScreen(PanelID.Darkenator);

#if UNITY_EDITOR
		Store.Inst.AwardProduct(productId);
		onPurchaseSuccess?.Invoke();
#else
		Purchaser.Inst.PurchaseProduct(productId, () => onPurchaseSuccess?.Invoke());
#endif
	}
}
#endif