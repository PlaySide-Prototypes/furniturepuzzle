﻿#if FALSE
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Purchasing;


// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
public class Purchaser : MonoBehaviour, IStoreListener { 

	private static IStoreController m_StoreController;          // The Unity Purchasing system.
	private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

	public static Purchaser Inst { get; private set; }

	// Product identifiers for all products capable of being purchased: 
	// "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
	// counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
	// also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

	// General product identifiers for the consumable, non-consumable, and subscription products.
	// Use these handles in the code to reference which product to purchase. Also use these values 
	// when defining the Product Identifiers on the store. Except, for illustration purposes, the 
	// kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
	// specific mapping to Unity Purchasing's AddProduct, below.
	//public static string kProductIDConsumable = "consumable";
	//public static string kProductIDNonConsumable = "nonconsumable";
	//public static string kProductIDSubscription = "subscription";

	// Apple App Store-specific product identifier for the subscription product.
	private static string kProductNameAppleSubscription = "com.unity3d.subscription.new";

	// Google Play Store-specific product identifier subscription product.
	private static string kProductNameGooglePlaySubscription = "com.unity3d.subscription.original";

	public static bool S_PurchasesRestored {
		get { return PlayerPrefs.GetInt("S_PurchasesRestored", 0) == 1; }
		private set {
			PlayerPrefs.SetInt("S_PurchasesRestored", value ? 1 : 0);
			PlayerPrefs.Save();
		}
	}

	private void Awake()
	{
		Inst = this;
	}

	void Start() {
		// If we haven't set up the Unity Purchasing reference
		if (m_StoreController == null) {
			// Begin to configure our connection to Purchasing
			InitializePurchasing();
		}
	}

	public void InitializePurchasing() {
		// If we have already connected to Purchasing ...
		if (IsInitialized()) {
			// ... we are done here.
			return;
		}

		// Create a builder, first passing in a suite of Unity provided stores.
		var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

		// Add a product to sell / restore by way of its identifier, associating the general identifier
		// with its store-specific identifiers.

		// Continue adding the non-consumable product.
		builder.AddProduct(Store.FppStarterPackCode, ProductType.NonConsumable);
		builder.AddProduct(Store.NoAdsCode, ProductType.NonConsumable);
		builder.AddProduct(Store.CoinMultiplierCode, ProductType.NonConsumable);
		builder.AddProduct(Store.SmallCoinPackCode, ProductType.Consumable);
		builder.AddProduct(Store.MediumCoinPackCode, ProductType.Consumable);
		builder.AddProduct(Store.LargeCoinPackCode, ProductType.Consumable);
		builder.AddProduct(Store.VeryLargeCoinPackCode, ProductType.Consumable);
		builder.AddProduct(Store.UltraLargeCoinPackCode, ProductType.Consumable);

		// Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
		// and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
		UnityPurchasing.Initialize(this, builder);
	}


	private bool IsInitialized() {
		// Only say we are initialized if both the Purchasing references are set.
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}

	static Action onSuccess;
	public void PurchaseProduct(string id, Action onSuccess = null) {
		if (onSuccess == null)
			onSuccess = () => AudioController.Play("UI_Purchase");
		else
			AudioController.Play("UI_Purchase");
		BuyProductID(id, onSuccess);
	}

	//public void BuyConsumable() {
	//	// Buy the consumable product using its general identifier. Expect a response either 
	//	// through ProcessPurchase or OnPurchaseFailed asynchronously.
	//	BuyProductID(kProductIDConsumable);
	//}


	//public void BuyNonConsumable() {
	//	// Buy the non-consumable product using its general identifier. Expect a response either 
	//	// through ProcessPurchase or OnPurchaseFailed asynchronously.
	//	BuyProductID(kProductIDNonConsumable);
	//}


	//public void BuySubscription() {
	//	// Buy the subscription product using its the general identifier. Expect a response either 
	//	// through ProcessPurchase or OnPurchaseFailed asynchronously.
	//	// Notice how we use the general product identifier in spite of this ID being mapped to
	//	// custom store-specific identifiers above.
	//	BuyProductID(kProductIDSubscription);
	//}


	void BuyProductID(string _productId, Action _onSuccess = null) {
		// If Purchasing has been initialized ...
		if (IsInitialized()) {
			// ... look up the Product reference with the general product identifier and the Purchasing 
			// system's products collection.
			Product product = m_StoreController.products.WithID(_productId);

			// If the look up found a product for this device's store and that product is ready to be sold ... 
			if (product != null && product.availableToPurchase) {
				Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
				// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
				// asynchronously.

				//PanelManager.Instance.EnableScreen(PanelID.Loading);
				//TextField.UpdateUiElement(TextField.DataField.LoadingText, LocIDs.Contacting_Store);

				onSuccess = _onSuccess;
				m_StoreController.InitiatePurchase(product);
			}
			// Otherwise ...
			else {
				// ... report the product look-up failure situation  
				Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
				PanelManager.Instance.popupController.PopupOrQueue(new PopupMessageBase.PopupInfo(
					LocIDs.Failed,
					null,
					LocIDs.Purchase_Failed,
					new LocIDs[] { LocIDs.Confirm_Ok }
				));
			}
		}
		// Otherwise ...
		else {
			// ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
			// retrying initiailization.
			Debug.Log("BuyProductID FAIL. Not initialized.");
			PopupMessageBase.PopupInfo info = new PopupMessageBase.PopupInfo()
			{
				popupId = PopupWindowContainer.PopupIDs.Default,
				titleLabelsID = LocIDs.Cant_Connect_Internet_Title,
				messageLabelsID = LocIDs.Cant_Connect_Internet_Desc,
				confirmLabelsID = new LocIDs[] { LocIDs.Confirm_Ok },
			};
			PanelManager.Instance.popupController.PopupOrQueue(info);
		}
	}


	// Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
	// Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
	public void RestorePurchases() {
		//#if UNITY_ANDROID
		//		return;
		//#endif

		Debug.Log("Purchases previously restored: " + S_PurchasesRestored);
		if (S_PurchasesRestored)
			return;

		// If Purchasing has not yet been set up ...
		if (!IsInitialized()) {
			// ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
			Debug.Log("RestorePurchases FAIL. Not initialized.");

			PopupMessageBase.PopupInfo info = new PopupMessageBase.PopupInfo()
			{
				popupId = PopupWindowContainer.PopupIDs.Default,
				titleLabelsID = LocIDs.Cant_Connect_Internet_Title,
				messageLabelsID = LocIDs.Cant_Connect_Internet_Desc,
				confirmLabelsID = new LocIDs[] { LocIDs.Confirm_Ok },
			};
			PanelManager.Instance.popupController.PopupOrQueue(info);

			return;
		}

		//PanelManager.Instance.EnableScreen(PanelID.Loading);
		//TextField.UpdateUiElement(TextField.DataField.LoadingText, LocIDs.Contacting_Store);

		// If we are running on an Apple device ... 
		if (Application.platform == RuntimePlatform.IPhonePlayer ||
			Application.platform == RuntimePlatform.OSXPlayer) {
			// ... begin restoring purchases
			Debug.Log("RestorePurchases started ...");

			// Fetch the Apple store-specific subsystem.
			var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
			// Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
			// the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
			apple.RestoreTransactions((result) => {
				// The first phase of restoration. If no more responses are received on ProcessPurchase then 
				// no purchases are available to be restored.
				if (result) {
					Debug.Log("Flagging purchases as restored");
					S_PurchasesRestored = true;
					RestorePurchasesButton.ToggleEnabled(false);
				}
				Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
				//PanelManager.Instance.DisableScreen(PanelID.Loading);
				PanelManager.Instance.popupController.PopupOrQueue(new PopupMessageBase.PopupInfo(
					result ? LocIDs.Success : LocIDs.Failed,
					null,
					result ? LocIDs.Purchase_Restore_Success : LocIDs.Purchase_Restore_Fail,
					new LocIDs[] { LocIDs.Confirm_Ok }
				));
			});
		}
		// Otherwise ...
		else {
			// We are not running on an Apple device. No work is necessary to restore purchases.
			Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
		}
	}


	//  
	// --- IStoreListener
	//

	public void OnInitialized(IStoreController controller, IExtensionProvider extensions) {
		// Purchasing has succeeded initializing. Collect our Purchasing references.
		Debug.Log("OnInitialized: PASS");

		// Overall Purchasing system, configured with products for this application.
		m_StoreController = controller;
		// Store specific subsystem, for accessing device-specific store features.
		m_StoreExtensionProvider = extensions;

		Store.Inst.UpdateButtons();
	}


	public void OnInitializeFailed(InitializationFailureReason error) {
		// Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
		Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
	}


	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) {
		Debug.Log("ProcessPurchase: " + args.purchasedProduct.definition.id);
		//PanelManager.Instance.DisableScreen(PanelID.Loading);
		Store.Inst.AwardProduct(args.purchasedProduct.definition.id);
		AudioController.Play("UI_Purchase_Generic");
		if (onSuccess != null) 
		{
			onSuccess();

			PopupMessageBase.PopupInfo info = new PopupMessageBase.PopupInfo()
			{
				popupId = PopupWindowContainer.PopupIDs.Default,
				titleLabelsID = LocIDs.Success,
				messageLabelsID = LocIDs.Success,
				confirmLabelsID = new LocIDs[] { LocIDs.Confirm_Ok },
			};
			PanelManager.Instance.popupController.PopupOrQueue(info);

			//AppsFlyerManager.instance.ProcessPurchase(args, m_StoreController, m_StoreExtensionProvider);

			AnalyticsManager.ValidateRecipt(args.purchasedProduct);

			Analytics.CustomEvent("purchase", new Dictionary<string, object>() {
				{ "product", args.purchasedProduct.definition.id },
				{ "purchases_made", ++AnalyticsManager.PurchasesMade },
				{ "play_time", PlayTimeTracker.GlobalPlayTime.TotalMinutes }
			});
		}
		else
			Debug.Log($"Not sending analytics for restored purchase {args.purchasedProduct.definition.id}");

		onSuccess = null;

		//AudioController.Play("UI_Purchase");

		// Return a flag indicating whether this product has completely been received, or if the application needs 
		// to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
		// saving purchased products to the cloud, and when that save is delayed. 
		return PurchaseProcessingResult.Complete;
	}


	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason) {
		// A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
		// this reason with the user to guide their troubleshooting actions.
		Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
		//PanelManager.Instance.DisableScreen(PanelID.Loading);

		PanelManager.Instance.popupController.PopupOrQueue(new PopupMessageBase.PopupInfo(
			LocIDs.Purchase_Failed,
			null, 
			LocIDs.Purchase_Failed,
			new LocIDs[] { LocIDs.Confirm_Ok }
		));

		//AudioController.Play("UI_Fail");
	}

	public string GetPrice(string productId) {
		if (m_StoreController == null)
			return LocManager.instance.Get(LocIDs.Unavailable);
		Product prod = m_StoreController.products.WithID(productId);
		return prod != null ? prod.metadata.localizedPriceString : LocManager.instance.Get(LocIDs.Unavailable);
	}

	internal void PurchaseProduct(object doubleCoinsCode) {
		throw new NotImplementedException();
	}
}
#endif