﻿#if FALSE

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;
using System.Globalization;

#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(Purchaser))]
public class Store : MonoBehaviour
{
	[System.Serializable]
	public struct FPPItem
	{
		public string itemId;
		public int shards;
	}

	public const string FppStarterPackCode = "dw5fpp";
	public const string NoAdsCode = "dw5noads";
	public const string CoinMultiplierCode = "dw5doublecoins";
	public const string SmallCoinPackCode = "dw5coins1";
	public const string MediumCoinPackCode = "dw5coins2";
	public const string LargeCoinPackCode = "dw5coins3";
	public const string VeryLargeCoinPackCode = "dw5coins4";
	public const string UltraLargeCoinPackCode = "dw5coins5";

	const float CoinMultiplier = 1.3f;

	[SerializeField] int fppCoins;
	[SerializeField] FPPItem[] fppItems;
	[SerializeField] int[] coinAmounts;

	public static Store Inst { get; private set; }

	public static Dictionary<string, int> QtyLookup { get; private set; } = new Dictionary<string, int>();

	Dictionary<string, bool> remoteBundlesEnabled = new Dictionary<string, bool>();

	public static bool NoAdsBought
	{
		get { return PlayerPrefs.GetInt("NoAdsBought", 0) == 1; }
		private set
		{
			PlayerPrefs.SetInt("NoAdsBought", value ? 1 : 0);
			if (value && NoAdsButton.instance)
			{
				NoAdsButton.instance.ToggleButton(false);
				AdManager.SetBannerShown(false);
			}
		}
	}
	public static bool FppBought
	{
		get { return PlayerPrefs.GetInt("FppBought", 0) == 1; }
		private set
		{
			PlayerPrefs.SetInt("FppBought", value ? 1 : 0);
			if (value)
			{
				if (NoAdsButton.instance)
					NoAdsButton.instance.ToggleButton(false);
				if (CoinMultiplierButton.instance)
					CoinMultiplierButton.instance.ToggleButton(false);
				if (FPPButton.instance)
					FPPButton.instance.ToggleButton(false);
			}
		}
	}
	public static bool CoinMultiplierBought
	{
		get { return PlayerPrefs.GetInt("CoinMultiplierBought", 0) == 1; }
		private set
		{
			PlayerPrefs.SetInt("CoinMultiplierBought", value ? 1 : 0);

			if (value && CoinMultiplierButton.instance)
				CoinMultiplierButton.instance.ToggleButton(false);
		}
	}

	public static float IAPCoinMultiplier
	{
		get { return CoinMultiplierBought ? CoinMultiplier : 1.0f; }
	}
	
	public static bool FppTimerStarted
	{
		get { return PlayerPrefs.GetInt("FppTimerStarted", 1) == 1; }
		set { PlayerPrefs.SetInt("FppTimerStarted", value ? 1 : 0); }
	}
	public DateTime FppStartTime
	{
		get
		{
			string t = PlayerPrefs.GetString("FppStartTime", null);
            if (string.IsNullOrEmpty(t))
            {
                return default;
            }
            DateTime timeInAUFormat;
            if (DateTime.TryParse(t, CultureInfo.GetCultureInfo("en-AU"), DateTimeStyles.None, out timeInAUFormat) == false)
            {
                return default;
            }
            DateTime timeInLocalFormat;
            if (DateTime.TryParse(timeInAUFormat.ToString(CultureInfo.InvariantCulture), out timeInLocalFormat) == false)
            {
                return default;
            }
            return timeInLocalFormat;
        }
        set
        {
            DateTime timeInAUFormat;
            if (DateTime.TryParse(value.ToString(CultureInfo.GetCultureInfo("en-AU")), out timeInAUFormat))
            {
                PlayerPrefs.SetString("FppStartTime", timeInAUFormat.ToString());
            }
            DateTime timeInLocalFormat;
            if (DateTime.TryParse(timeInAUFormat.ToString(CultureInfo.InvariantCulture), out timeInLocalFormat))
            {
                cachedFppStartTime = timeInLocalFormat;
            }
        }
	}

	DateTime cachedFppStartTime;

	private void Awake()
	{
		Inst = this;
		cachedFppStartTime = FppStartTime;
	}

	void Start()
	{
#if UNITY_EDITOR
		OnRemoteSettingsUpdated();
#else
			RemoteSettings.Updated += OnRemoteSettingsUpdated;
#endif
		if (!FppTimerStarted || ((cachedFppStartTime + TimeSpan.FromHours(48)) - DateTime.Now).TotalSeconds <= 0f)
		{
			FppStartTime = DateTime.Now;
			FppTimerStarted = true;
		}
	}

	private void OnDestroy()
	{
		RemoteSettings.Updated -= OnRemoteSettingsUpdated;
	}

	private void OnRemoteSettingsUpdated()
	{
		UpdateButtons();
	}

	private void Update()
	{
		if (!FppBought)
		{
			TimeSpan ts = (cachedFppStartTime + TimeSpan.FromHours(48)) - DateTime.Now;
			if (ts.TotalSeconds < 0)
				FppStartTime = DateTime.Now;
			//else
			//	TimeField.UpdateUiElement(TimeField.DataField.FppTimer, ts);
		}
	}

	private void UpdateFppTimer()
	{
		//for (int i = 0; i < itemBundles.Length; ++i)
		//{
		//	if (!itemBundles[i].remoteEnabled)
		//		continue;

		//	TimeSpan ts = (GetBundleTimerStart(itemBundles[i].bundleCode) + TimeSpan.FromHours(itemBundles[i].itemTimeLimitHours)) - DateTime.Now;
		//	if (ts.TotalSeconds <= 0)
		//		EnableSwitch.UpdateUiElement(itemBundles[i].buttonField, false);
		//}
	}

	public void CheckFppResurface()
	{
		//if (!S_FppResurfaced && !GetItemBundleBought(itemBundles[0].bundleCode))
		//{
		//	SetBundleTimerStart(itemBundles[0].bundleCode, DateTime.Now);
		//	S_FppResurfaced = true;
		//}
	}

#if UNITY_EDITOR
	//Debug function for giving a product after a delay.
	public IEnumerator DebugDelayedAwardProduct(string id)
	{
		yield return new WaitForSeconds(2.0f);

		AwardProduct(id);
	}
#endif

	public void AwardProduct(string id)
	{
        if (id == FppStarterPackCode)
        {
            FppBought = true;
            NoAdsBought = true;
            CoinMultiplierBought = true;

            InventoryManager.RewardResult result = new InventoryManager.RewardResult();
            result.coins = fppCoins;
            result.items = new List<InventoryManager.RewardItemEntry>();

            InventoryManager.AddCoins(fppCoins, InventoryManager.UpdateCurrencyMode.SaveDataOnly);

            for (int i = 0; i < fppItems.Length; ++i)
            {
                Cosmetic item = InventoryManager.AddItem(fppItems[i].itemId, fppItems[i].shards);
                if (item != null)
                {
                    result.items.Add(new InventoryManager.RewardItemEntry(fppItems[i].shards, fppItems[i].itemId));
                }
                else
                {
                    // This is either a non-existant item or a duplicate. Either way reward with Coins to compensate
                    item = InventoryManager.GetCoinCompensationForDupItem();
                    result.items.Add(new InventoryManager.RewardItemEntry(1, item.id));
                }
            }

            result.visual = BundleVisuals.ChestCommon;

            StartCoroutine(DelayedGrantFPP(result));
        }
        else
        {

            if (id == NoAdsCode)
                NoAdsBought = true;
            else if (id == CoinMultiplierCode)
                CoinMultiplierBought = true;
            else if (id.Contains("coins"))
            {
                int qty = GetProductQty(id);
                InventoryManager.AddCoins(qty);
            }

            StartCoroutine(DelayedRemovedDarkenator());
        }

		UpdateButtons();
	}

    private IEnumerator DelayedRemovedDarkenator()
    {
        const float DisplayForSeconds = 1.0f;
        yield return new WaitForSeconds(DisplayForSeconds);
        PanelManager.Instance.DisableScreen(PanelID.Darkenator);
    }

    // The problem with showing the rewards in the chest thing is that
    // we can get to this code from anwhere, because the player can have their purchases
    // restored outside of the store page. So handle coming to it from any screen.
    IEnumerator DelayedGrantFPP(InventoryManager.RewardResult _result)
	{
        const float DelayUntilRewardShown = 1.0f;
		float timeAvailableToShowReward = 0.0f;

        // Get rid of the Store Menu and Keep the Darkenator on until the Chest is shown.
        PanelManager.Instance.DisableScreen(PanelID.Store);

        while (timeAvailableToShowReward < DelayUntilRewardShown)
        {
            // We might not be in the Menu/Title screen when Android restores purchases... So hold off on showing that UNTIL the user returns
            while (GameManager.state != GameManager.State.Menu ||
                PanelManager.Instance.IsScreenEnabled(PanelID.Rewards) ||
                PanelManager.Instance.IsScreenEnabled(PanelID.EOR))
            {
                yield return null; // Wait until we can grant a reward.
            }

            timeAvailableToShowReward += Time.deltaTime;
            yield return null;
        }

        List<PanelID> panels = PanelManager.Instance.GetAllOpenPanels();

        for (int i = 0; i < panels.Count; ++i)
		{
			if (panels[i] != PanelID.BeanBG && panels[i] != PanelID.Rewards)
				PanelManager.Instance.DisableScreenInstant(panels[i]);
		}

		if (!PanelManager.Instance.IsScreenEnabled(PanelID.BeanBG))
			PanelManager.Instance.EnableScreen(PanelID.BeanBG);

		UI_Rewards.ShowReward(_result, () =>
		{
            for (int i = 0; i < panels.Count; ++i)
            {
                // Darkenator should remain hidden.
                if (panels[i] != PanelID.Darkenator)
                    PanelManager.Instance.EnableScreen(panels[i]);
            }

            PanelManager.Instance.EnableScreen(PanelID.Store);
        });
	}

	public void UpdateButtons()
	{
		//for (int i = 0; i < itemBundles.Length; ++i)
		//{
		//	EnableSwitch.UpdateUiElement(itemBundles[i].buttonField, !GetItemBundleBought(itemBundles[i].bundleCode) && itemBundles[i].remoteEnabled);
		//	TextFieldNoLoc.UpdateUiElement(itemBundles[i].costField, Purchaser.Inst.GetPrice(itemBundles[i].bundleCode));
		//}


		//EnableSwitch.UpdateUiElement(EnableSwitch.DataField.FppButton, !S_FppBought);
		//NoAdsButton.instance.ToggleButton(!S_NoAdsBought && RemoteNoAdsEnabled);
		//EnableSwitch.UpdateUiElement(EnableSwitch.DataField.CoinMultiplier, !S_CoinMultiplierBought && RemoteCoinMultiplierEnabled);

		//TextFieldNoLoc.UpdateUiElement(TextField.DataField.NoAdsPrice, Purchaser.Inst.GetPrice(noAdsCode));
		//TextFieldNoLoc.UpdateUiElement(TextField.DataField.CoinMultiplierPrice, Purchaser.Inst.GetPrice(doubleCoinsCode));
	}

	private void OnEnable() { UpdateButtons(); }

	public int GetProductQty(string _productId)
	{
		int qty = RemoteSettings.GetInt(_productId + "_qty", 1);

		if (_productId.Contains("coins") && _productId != CoinMultiplierCode)
		{
			qty = (int)(coinAmounts[int.Parse(_productId.Substring("dw5coins".Length)) - 1] * Helpers.GetTotalCoinMultiplier());
		}

		return qty;
	}
}
#if UNITY_EDITOR
	//[CustomPropertyDrawer(typeof(HintPackSpec))]
	//class HintPackSpecDrawer : PropertyDrawer
	//{

	//	// Draw the property inside the given rect
	//	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	//	{
	//		// Using BeginProperty / EndProperty on the parent property means that
	//		// prefab override logic works on the entire property.
	//		EditorGUI.BeginProperty(position, label, property);

	//		// Draw label
	//		position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

	//		// Don't make child fields be indented
	//		var indent = EditorGUI.indentLevel;
	//		EditorGUI.indentLevel = 1;

	//		// Calculate rects
	//		Rect productIdRect = new Rect(position.x, position.y, position.width / 2f, position.height);
	//		Rect numHintsRect = new Rect(position.x + (position.width / 2f), position.y, position.width / 2f, position.height);

	//		// Draw fields - passs GUIContent.none to each so they are drawn without labels
	//		EditorGUI.PropertyField(productIdRect, property.FindPropertyRelative("productId"), GUIContent.none);
	//		EditorGUI.PropertyField(numHintsRect, property.FindPropertyRelative("numHints"), GUIContent.none);

	//		// Set indent back to what it was
	//		EditorGUI.indentLevel = indent;

	//		EditorGUI.EndProperty();
	//	}
	//}
#endif

#endif