﻿#if FALSE

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestorePurchasesButton : MonoBehaviour
{
	public static RestorePurchasesButton instance;

	static bool shouldBeEnabled;

	private void Awake()
	{
		instance = this;

#if UNITY_IOS
		shouldBeEnabled = true;
#elif UNITY_EDITOR
		shouldBeEnabled = true;
#else
		shouldBeEnabled = false;
#endif
	}

	private void OnEnable()
	{
		if (!shouldBeEnabled)
			gameObject.SetActive(false);
	}

	public static void ToggleEnabled(bool _enabled)
	{
		shouldBeEnabled = _enabled;
		if (instance)
			instance.gameObject.SetActive(_enabled);
	}

	public void RestorePurchases()
	{
#if UNITY_EDITOR
		Store.Inst.StartCoroutine(Store.Inst.DebugDelayedAwardProduct(Store.FppStarterPackCode));
#else
		Purchaser.Inst.RestorePurchases();
#endif
	}
}
#endif