﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class PanelManager : MonoBehaviour
{
	#region Editor variables

	// If you receieve error: "The type or namespace name `PanelID' could not be found."
	// Then: Import DefaultAutogenScripts.package, located in External/PlaySide/ dir
	public PanelID[] initialPanels;
	public PanelID FallbackReturnToPanel;

	[Header("Popups")]
	public PopupMessageBase popupController;
	internal RateMe rateMe;
	

	#endregion	// Editor variables

	#region Non-editor variables

	public Panel[] panels { get; private set; }
	Stack<PanelID> navHistory = new Stack<PanelID>();
	public static PanelManager Instance;

	#endregion	// Non-editor variables

	#region Debug
	#if !FINAL
	public bool OnScreenNavigationDebugging;
	StringBuilder OnGUISB = new StringBuilder();
	private GUIStyle guiStyle;
	Rect OnGUIRect;
	void OnGUI()
	{
		if (!OnScreenNavigationDebugging)
			return;
		guiStyle = new GUIStyle("Label");
		guiStyle.fontSize = 20;

		PanelID[] history = navHistory.ToArray();
		OnGUISB.Length = 0;
		for (int i = history.Length - 1; i >= 0; --i)
		{
			if (i < history.Length - 1)
				OnGUISB.Append(" -> ");
			OnGUISB.Append(history[i].ToString());
			OnGUISB.Append("\n");
		}

		OnGUIRect = GUILayoutUtility.GetRect(new GUIContent(OnGUISB.ToString()), guiStyle);
		OnGUIRect.x = Screen.width * 0.01f;
		OnGUIRect.y = Screen.height * 0.5f;

		GUI.Label(OnGUIRect, OnGUISB.ToString(), guiStyle);
	}
	#endif
	#endregion	// Debug

    void Awake()
    {
		if (Instance != null)
			throw new UnityException("Singleton Instance trying to be set multiple times");
        Instance = this;

		Initialize();
    }

    // Index panels by enum int for later access
    public void Initialize()
    {
        Panel[] unsortedPanels = GetComponentsInChildren<Panel>(true);
        panels = new Panel[(int) Enum.GetValues( typeof( PanelID ) ).Length];

        for(int i=0; i<unsortedPanels.Length; i++)
        {
            int newIndex = (int)unsortedPanels[i].id;
            if(panels[newIndex] != null)
                Debug.LogError("Unable to initialize panels. Multiple panels share same ID: " + panels[newIndex].name + ", " + unsortedPanels[i].name);
            panels[(int)unsortedPanels[i].id] = unsortedPanels[i];


        }
		for (int i = 0; i < panels.Length; i++)
		{
			if(panels[i] != null)
				panels[i].Init();
		}

		rateMe = popupController.GetComponentInChildren<RateMe>(true);
		if (rateMe == null)
			Debug.LogWarning("RateMe component not found under the Popup hierarchy");
		else
			rateMe.InitRateMe();
    }

    void Start()
    {
        for (int i = 0; i < panels.Length; i++)
        {
			Panel panel = panels[i];
            if (panel != null)
            {
				// Check if initial panel to turn on
				for (int initialPanelNo = 0; initialPanelNo < initialPanels.Length; ++initialPanelNo)
                {
					if (panel.id == initialPanels[initialPanelNo])
					{
						panel.gameObject.SetActive(true);
						panel.Transition(Panel.onTransitionName);
						break;
					}
                }
            }
        }
    }

    // Play a Transition (In/Out/State) on a Panel
    public void Transition(PanelID panelId, string transitionName)
    {
        if(panels[(int)panelId]==null)
            Debug.LogWarning("Unable to transition Panel "+panelId);
        panels[(int)panelId].Transition(transitionName);
    }

    public void Transition(PanelID panelId)
    {
		panels[(int)panelId].Transition(Panel.onTransitionName);
    }

	public void SwitchToScreen(PanelID fromScreen, PanelID toScreen)
	{
		DisableScreen(fromScreen);
		EnableScreen(toScreen);
	}

	public void EnableScreen(int _p)
	{
		panels[_p].Transition(Panel.onTransitionName);
	}

	// Instantly Enable a Panel
    public void EnableScreen(PanelID panelId)
    {
		panels[(int)panelId].Transition(Panel.onTransitionName);
    }

	public void EnableScreenInstant(PanelID panelId)
	{
		panels[(int)panelId].Transition(Panel.onIdleTransitionName);
	}

	public void DisableScreenInstant(PanelID panelId)
	{
		panels[(int)panelId].Transition(Panel.offIdleTransitionName);
	}

    // Instantly Disable a Panel
    public void DisableScreen(PanelID panelId)
    {
		if (panels [(int)panelId] == null)
			throw new UnityException ("Panel '" + panelId + "' not found in the hierarchy");

		panels[(int)panelId].Transition(Panel.offTransitionName);
    }

	public void TurnOffScreenImmediately(PanelID panelId)
	{
		panels[(int)panelId].gameObject.SetActive(false);
	}

	// Checks if a panel is enabled
	public bool IsScreenEnabled(PanelID panelId)
	{
		return (panels[(int)panelId].gameObject.activeSelf);
	}

	// Instantly enable the specified panel, and disable all others
	public void EnableOnlyScreen(PanelID panelToLeaveOn)
	{
		List<PanelID> panelList = new List<PanelID>();
		panelList.Add(panelToLeaveOn);
		EnableOnlyScreens(panelList);
	}

	public void EnableOnlyScreens(List<PanelID> panelIDs)
	{
		for (int i = 0; i < panels.Length; ++i)
		{
			if(panels[i] != null)
				DisableScreen(panels[i].id);
		}

		for (int i = 0; i < panelIDs.Count; ++i)
			EnableScreen(panelIDs[i]);
	}

	public void DisableAllScreens(bool turnOffImmediately)
	{
		for (int i = 0; i < Enum.GetValues(typeof(PanelID)).Length; ++i)
		{
			Panel panel = panels[i];
			if ((panel != null) && (panel.gameObject.activeSelf))
			{
				if (turnOffImmediately)
					panel.gameObject.SetActive(false);
				else
					panel.Transition(Panel.offTransitionName);
			}
		}
	}

	public void SetPanelDisableCallback(PanelID panelId, Action callback)
	{
		panels[(int)panelId].disableCallback = callback;
	}

	public void CallOnNextFrame(Action callback)
	{
		StartCoroutine(Wait1FrameThenCall(callback));
	}
	IEnumerator Wait1FrameThenCall(Action callback)
	{
		yield return new WaitForEndOfFrame();

		callback();
	}

	#region Navigation history + back button functionality

	public void AddScreenToNavHistory(PanelID panelID)
	{
		if ((navHistory.Count == 0) || (navHistory.Peek() != panelID))
		{
			navHistory.Push(panelID);
		}
	}

	public void NavigateBack()
	{
		PanelID returningFrom = navHistory.Pop();
		PanelID returningTo = navHistory.Pop();

		SwitchToScreen(returningFrom, returningTo);
	}

	public PanelID PeekLastScreen()
	{
		return navHistory.Peek();
	}

	/// <summary> Special case functionality that goes back in history as necessary </summary>
	/// <param name="panelID"> Screen to return to </param>
	public void ReturnToScreen(PanelID panelID)
	{
		PanelID panelToShow = panelID;

		if (!navHistory.Contains (panelToShow))
			panelToShow = FallbackReturnToPanel;

		while (navHistory.Count > 0 && navHistory.Peek () != panelToShow) 
		{
			PanelID panel = navHistory.Pop ();
			if (IsScreenEnabled(panel))
				DisableScreen(panel);
		}

		EnableScreen(panelToShow);
	}

	#endregion	// Navigation history + back button functionality
}
