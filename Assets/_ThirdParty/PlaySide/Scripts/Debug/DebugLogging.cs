using UnityEngine;

public class DebugLogging
{
	public enum DebugTypes { None, Console, ConsoleWithOnScreenErrors, OnScreen };
	DebugTypes debugType;
	string prefix;

	/// <summary> Constructor </summary>
	public DebugLogging(DebugTypes _debugType, string _prefix = null)
	{
		debugType = _debugType;

#if FINAL || PRODUCTION
		// Ensure nothing gets logged onscreen in FINAL/PROD builds
		if (debugType != DebugTypes.None)
			debugType = DebugTypes.Console;
#endif

		prefix = _prefix;

		// Pad prefix with a space if necessary
		if (!string.IsNullOrEmpty(prefix) && (!prefix.EndsWith(" ", System.StringComparison.CurrentCulture)))
			prefix += " ";
	}

	/// <summary> Use instead of Debug.Log </summary>
	public void Log(string _message)
	{
#if !FINAL && !PRODUCTION
		if (debugType == DebugTypes.None)
			return;

		if (prefix != null)
			_message = prefix + _message;

		switch (debugType)
		{
			case DebugTypes.OnScreen:
				DebugConsole.Log(_message);
				break;

			case DebugTypes.Console:
			case DebugTypes.ConsoleWithOnScreenErrors:
				Debug.Log(_message);
				break;
		}
#endif
	}

	/// <summary> Use instead of Debug.LogWarning </summary>
	public void LogWarning(string _message)
	{
#if !FINAL && !PRODUCTION
		if (debugType == DebugTypes.None)
			return;

		if (prefix != null)
			_message = prefix + _message;

		switch (debugType)
		{
			case DebugTypes.OnScreen:
				DebugConsole.LogWarning(_message);
				break;

			case DebugTypes.Console:
			case DebugTypes.ConsoleWithOnScreenErrors:
				Debug.LogWarning(_message);
				break;
		}
#endif
	}

	/// <summary> Use instead of Debug.LogError </summary>
	public void LogError(string _message)
	{
#if !UNITY_EDITOR
		if (debugType == DebugTypes.None)
			return;

		if (prefix != null)
			_message = prefix + _message;

		switch (debugType)
		{
			case DebugTypes.OnScreen:
				DebugConsole.LogError(_message);
				break;

			case DebugTypes.ConsoleWithOnScreenErrors:
				DebugConsole.LogError(_message);
				break;

			case DebugTypes.Console:
				Debug.LogError(_message);
				break;
		}

#else // UNITY_EDITOR

		if (prefix != null)
			_message = prefix + _message;

		switch (debugType)
		{
			case DebugTypes.OnScreen:
				DebugConsole.LogError(_message);
				break;

			case DebugTypes.ConsoleWithOnScreenErrors:
				DebugConsole.LogError(_message);
				Debug.LogError(_message);
				break;

			case DebugTypes.None:
			case DebugTypes.Console:
				Debug.LogError(_message);
				break;
		}
#endif // UNITY_EDITOR
	}
}
