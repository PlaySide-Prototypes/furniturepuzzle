using UnityEngine;
using System.Collections.Generic;

public class DebugConsole : MonoBehaviour
{
	public enum LogTypes
	{
		Regular,
		Warning,
		Error
	}

	public class DebugLogEntry
	{
		public string message;
		public LogTypes logType;

		public DebugLogEntry(string _message, LogTypes _logType)
		{
			message = _message;
			logType = _logType;
		}
	}

	const float PaddingX = 10;
	const float PaddingY = 10;
	const float SpacingY = 0;

	List<DebugLogEntry> entries = new List<DebugLogEntry>();
	Rect rect = new Rect(PaddingX, PaddingY, Screen.width - PaddingX * 2, Screen.height - PaddingY * 2);
	int maxCount;
	float lineHeight;

#if !FINAL && !PRODUCTION
	static DebugConsole _instance;
	static DebugConsole instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = FindObjectOfType(typeof(DebugConsole)) as DebugConsole;
				if (_instance == null)
				{
					GameObject gameObj = new GameObject();
					gameObj.name = "DebugConsole";
					gameObj.AddComponent<DebugConsole>();
					DontDestroyOnLoad(gameObj);
				}
			}

			return _instance;
		}
	}
#endif

	void Prune()
	{
		if (entries.Count > maxCount)
			entries.RemoveRange(0, entries.Count - maxCount);
	}

	void ClearEntries()
	{
		entries.Clear();
	}

	void AddEntry(DebugLogEntry entry)
	{
		if (entry.message.Contains("\n"))
		{
			string[] splits = entry.message.Split('\n');
			for (int i = 0; i < splits.Length; ++i)
			{
				DebugLogEntry splitEntry = new DebugLogEntry(splits[i], entry.logType);
				entries.Add(splitEntry);
			}
		}
		else
			entries.Add(entry);
	}

	public static void Log(string message)
	{
#if !FINAL && !PRODUCTION
		instance.AddEntry(new DebugLogEntry(message, LogTypes.Regular));
#endif
	}

	public static void LogWarning(string message)
	{
#if !FINAL && !PRODUCTION
		instance.AddEntry(new DebugLogEntry(message, LogTypes.Warning));
#endif
	}

	public static void LogError(string message)
	{
#if !FINAL && !PRODUCTION
		instance.AddEntry(new DebugLogEntry(message, LogTypes.Error));
#endif
	}

	public static void Clear()
	{
#if !FINAL && !PRODUCTION
		instance.ClearEntries();
#endif
	}

	void Awake()
	{
#if !FINAL && !PRODUCTION
		if (_instance != null)
			Destroy(this);
		else
			_instance = this;
#else
		Destroy(this);
#endif
	}

	void OnGUI()
	{
		if (entries.Count == 0)
			return;

		if (lineHeight == 0)
			lineHeight = GUI.skin.label.CalcHeight(new GUIContent(entries[0].message), rect.width);

		if (maxCount == 0)
			maxCount = (int)((Screen.height - PaddingY * 2) / (lineHeight + SpacingY));

		Prune();

		rect.y = PaddingY;

		for (int i = 0; i < entries.Count; ++i)
		{
			DebugLogEntry entry = entries[i];
			switch (entry.logType)
			{
				case LogTypes.Regular:
					GUI.color = Color.white;
					break;
				case LogTypes.Warning:
					GUI.color = Color.yellow;
					break;
				case LogTypes.Error:
					GUI.color = Color.red;
					break;
			}
			GUI.Label(rect, entry.message);
			rect.y += lineHeight;
		}
	}
}
