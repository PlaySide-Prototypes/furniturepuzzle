﻿using UnityEngine;

public class DisableInFinal : MonoBehaviour
{
	void OnEnable()
	{
#if FINAL
		gameObject.SetActive(false);
#endif
	}
}
