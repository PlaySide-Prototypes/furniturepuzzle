﻿using UnityEngine;
using System.Collections;

public class DrawHelper : MonoBehaviour {

	#if UNITY_EDITOR
	public enum GizmoType {WireCube, WireSphere, WireMesh, Cube, Sphere}

	[SerializeField] bool		drawHelper = true; 
	[SerializeField] bool		drawDrawForward = false;
	[SerializeField] GizmoType 	gizmoType;
	[SerializeField] float		size = 0.5f;
	[SerializeField] Color		gizmoColor = Color.white;

	void OnDrawGizmos ()
	{
		if(drawDrawForward)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawLine(transform.position, transform.position + transform.forward*(size+0.5f) );
		}

		if(drawHelper)
		{	
			Gizmos.color = gizmoColor;
			switch(gizmoType)
			{
				case GizmoType.WireCube:
					Gizmos.DrawWireCube(transform.position, Vector3.one * size);
				break;

				case GizmoType.WireSphere:
					Gizmos.DrawWireSphere(transform.position, size);
				break;

				/*case GizmoType.WireMesh:
					Gizmos.DrawWireMesh(transform.position, size);
				break;*/

				case GizmoType.Cube:
					Gizmos.DrawCube(transform.position,  Vector3.one * size);
				break;

				case GizmoType.Sphere:
					Gizmos.DrawSphere(transform.position, size);
				break;
			}
		}
	}
	#endif
}
