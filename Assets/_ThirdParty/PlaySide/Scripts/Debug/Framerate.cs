using UnityEngine;
using System.Collections.Generic;

public class Framerate : MonoBehaviour
{
#if !FINAL
	[SerializeField] float fpsUpdateInterval = 0.5f;
	[SerializeField] bool startVisible = true;

	const string FORMAT = "{0:F1}\n{1:F1}";

	// recent fps
	float m_fpsRecent;
	float m_startTimeRecent;
	int m_startFrameCountRecent;

	// overall fps since level is started
	float m_fpsOverall;
	float m_startTimeOverall;
	float m_startFrameCountOverall;

	string m_labelStr;
	GUIStyle m_fpsLabelStyle;
	Rect m_rectMain;
	Rect m_rectShadow;

	public static Framerate Instance;

	void Awake()
	{
		if (Instance == null)
			Instance = this;
		else
			Destroy(gameObject);

		// DontDestroyOnLoad(gameObject);
	}

	public void Toggle()
	{
		enabled = !enabled;
	}

	void Start()
	{
		m_startTimeOverall = m_startTimeRecent = Time.realtimeSinceStartup;
		m_startFrameCountOverall = m_startFrameCountRecent = Time.frameCount;

		m_rectMain = new Rect(Screen.width * 0.08f, 0.0f, 64.0f, 64.0f);
		m_rectShadow = m_rectMain; m_rectShadow.x -= 2; m_rectShadow.y += 2;
		m_labelStr = string.Empty;

		if (startVisible != enabled)
			Toggle();
	}

	void Update()
	{
		float now = Time.realtimeSinceStartup;

		float diff = now - m_startTimeRecent;
		if (diff > fpsUpdateInterval)
		{
			int frameCount = Time.frameCount;
			m_fpsRecent = (frameCount - m_startFrameCountRecent) / diff;
			m_fpsOverall = (frameCount - m_startFrameCountOverall) / (now - m_startTimeOverall);
			m_startTimeRecent = now;
			m_startFrameCountRecent = frameCount;
			m_labelStr = string.Format(FORMAT, m_fpsRecent, m_fpsOverall);
		}
	}

	void OnGUI()
	{
		// Ensure GUIStyle is created (can't access GUI.skin outside OnGUI)
		if (m_fpsLabelStyle == null)
		{
			m_fpsLabelStyle = new GUIStyle(GUI.skin.label) { fontSize = 20 };
		}

		// Draw shadow
		GUI.color = Color.black;
		GUI.Label(m_rectShadow, m_labelStr, m_fpsLabelStyle);

		// Draw main label
		GUI.color = Color.yellow;
		GUI.Label(m_rectMain, m_labelStr, m_fpsLabelStyle);
	}
#endif
}