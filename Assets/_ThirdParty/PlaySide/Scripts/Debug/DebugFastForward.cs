﻿using UnityEngine;

public class DebugFastForward : MonoBehaviour
{
	#if UNITY_EDITOR
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.F))
		{
			if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
				Time.timeScale = 0.1f;
			else
				Time.timeScale = 6f;
		}
		else if (Input.GetKeyUp(KeyCode.F))
			Time.timeScale = 1f;
	}
#endif
}
