using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

public enum UIAudioTriggerTypes
{
	In,
	Out,
	Tick,
	Off,
	Purchase,
	Start,
	Custom,
	NotSure
}

public class PlaysideAudioUITrigger : MonoBehaviour
{
	public bool ignore = false;
	public UIAudioTriggerTypes type;
	public string customString;

	bool disabled;

#if UNITY_EDITOR
	/// <summary>
	/// Editor function for displaying a window to ensure that this script is given a query type.
	/// </summary>
	void Reset()
	{
		AudioUITriggerWindow.AddTriggerToQueryType(this);
	}
#endif

	/// <summary>
	/// Usually called from a button event- triggers the type of audio that this button is set to play to play.
	/// </summary>
	public void PlayAudio()
	{
		if (ignore)
			return;

		string audioToPlay;
		if (disabled)
			audioToPlay = "UI_Off";
		else
		{
			switch (type)
			{
				case UIAudioTriggerTypes.In: audioToPlay = "UI_In"; break;
				case UIAudioTriggerTypes.Out: audioToPlay = "UI_Out"; break;
				case UIAudioTriggerTypes.Tick: audioToPlay = "UI_Tick"; break;
				case UIAudioTriggerTypes.Off: audioToPlay = "UI_Off"; break;
				case UIAudioTriggerTypes.Purchase: audioToPlay = "UI_Purchase"; break;
				case UIAudioTriggerTypes.Start: audioToPlay = "UI_Start"; break;
				case UIAudioTriggerTypes.Custom: audioToPlay = "UI_Custom"; break;
				case UIAudioTriggerTypes.NotSure: return;
				default: throw new UnityException("Unhandled audio type!");
			}
		}

#if FALSE
		AudioController.Play(audioToPlay);
#endif
	}

	/// <summary>
	/// Stops the specified audio from playing.
	/// </summary>
	/// <param name="audioToStop"></param>
	public void StopAudio(string audioToStop)
	{
		if (ignore)
			return;
#if FALSE
		AudioController.Stop(audioToStop);
#endif
	}

	/// <summary>
	/// Should be called whenever the button that triggers this script 
	/// has it's enabled or interactable states changed. 
	/// Handles changing the played audio to a 'disabled' sound for buttons that can't be pushed,
	/// or back again.
	/// </summary>
	/// <param name="interactable">Whether the button can now be interacted with.</param>
	public void OnSetButtonInteractable(bool interactable)
	{
		disabled = !interactable;
	}
}

#if UNITY_EDITOR
[CustomEditor(typeof(PlaysideAudioUITrigger))]
public class PlaysideAudioUITriggerEditor : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		if (GUILayout.Button("Set trigger type"))
			AudioUITriggerWindow.AddTriggerToQueryType(target as PlaysideAudioUITrigger);
	}
}
#endif