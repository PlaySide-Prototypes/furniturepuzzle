﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AudioUITriggerWindow : EditorWindow
{
	public static Stack<PlaysideAudioUITrigger> triggersToQueryType = new Stack<PlaysideAudioUITrigger>();
	static AudioUITriggerWindow window;

	[MenuItem("PlaySide/Assign 'Not Sure' Audio Triggers in scene")]
	public static void AssignAllNotSureAudioTriggers()
	{
		triggersToQueryType.Clear();

		PlaysideAudioUITrigger[] triggers = FindObjectsOfType<PlaysideAudioUITrigger>();
		if (triggers.Length == 0)
			EditorUtility.DisplayDialog("No 'Not Sure' Audio Triggers in scene", "Congratulations! There aren't any 'not sure' audio triggers in the scene, it's all set up!", "Awesome!");
		else
		{
			for (int i = 0; i < triggers.Length; ++i)
			{
				if (triggers[i].type == UIAudioTriggerTypes.NotSure)
					AddTriggerToQueryType(triggers[i]);
			}
		}
	}

	public static void AddTriggerToQueryType(PlaysideAudioUITrigger trigger)
	{
		Selection.activeGameObject = trigger.gameObject;
		triggersToQueryType.Push(trigger);
		ShowWindow();
	}

	public static void ShowWindow()
	{
		if (window)
			window.Close();
		window = (AudioUITriggerWindow)GetWindow(typeof(AudioUITriggerWindow));
		window.Show();
	}

	string customString;
	Vector2 scrollPosition;

	// Seems like if you give buttons and text the same height, the buttons are slightly larger?
	// So I've given text a separate height that's slightly larger to account for this.
	readonly GUILayoutOption buttonHeight = GUILayout.Height(20);
	readonly GUILayoutOption textHeight = GUILayout.Height(22);

	void OnGUI()
	{
		if ((triggersToQueryType == null || triggersToQueryType.Count <= 0) && window)
		{
			window.Close();
			return;
		}

		// If we delete the component before selecting an option,
		// remove it from the stack.
		if (triggersToQueryType.Peek() == null)
		{
			triggersToQueryType.Pop();

			if (triggersToQueryType.Count > 0 && triggersToQueryType.Peek() != null)
				Selection.activeGameObject = triggersToQueryType.Peek().gameObject;
			else
				return;
		}


		GUILayout.Label("---- AUDIO TO PLAY ON NEW BUTTON ----");

		GUILayout.Space(20);

		if (GUILayout.Button("Select Button"))
		{
			Selection.activeGameObject = triggersToQueryType.Peek().gameObject;
		}

		GUILayout.Space(20);

		int audioTriggerTypes = System.Enum.GetValues(typeof(UIAudioTriggerTypes)).Length;

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.BeginVertical();
		for (int i = 0; i < audioTriggerTypes; ++i)
		{
			EditorGUILayout.BeginHorizontal();
			UIAudioTriggerTypes type = (UIAudioTriggerTypes)i;

			Color backgroundColour = GUI.backgroundColor;
			if (type == UIAudioTriggerTypes.NotSure)
				GUI.backgroundColor = Color.red;

			if (GUILayout.Button(type.ToString(), buttonHeight))
			{
				PlaysideAudioUITrigger trigger = triggersToQueryType.Pop();
				trigger.type = type;
				if (type == UIAudioTriggerTypes.Custom)
					trigger.customString = customString;

				customString = "";

				if (triggersToQueryType.Count > 0 && triggersToQueryType.Peek() != null)
					Selection.activeGameObject = triggersToQueryType.Peek().gameObject;
			}
			else if (type == UIAudioTriggerTypes.Custom)
			{
				customString = EditorGUILayout.TextField(customString, buttonHeight);
			}

			GUI.backgroundColor = backgroundColour;

			EditorGUILayout.EndHorizontal();
		}
		EditorGUILayout.EndVertical();

		EditorGUILayout.BeginVertical();
		scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
		for (int i = 0; i < audioTriggerTypes; ++i)
		{
			UIAudioTriggerTypes type = (UIAudioTriggerTypes)i;
			string description = "";
			switch (type)
			{
				case UIAudioTriggerTypes.In: description = "Used when a button tap changes the entire screen, either with a popup window (e.g. tapping on \"settings\" brings up that menu) or a totally different screen panel entirely (e.g. going from EOR back to main menu)"; break;
				case UIAudioTriggerTypes.Out: description = "Used when exiting a menu and returning to a previous screen (e.g. going from character select back to main menu). Any time a popup or new window is made, this is used when exiting."; break;
				case UIAudioTriggerTypes.Tick: description = "Used for tapping on UI elements that don't change the screen (e.g. tapping items in the settings menu, selecting equipment in a menu)."; break;
				case UIAudioTriggerTypes.Off: description = "Used when a button is inactive/unable to be pressed."; break;
				case UIAudioTriggerTypes.Purchase: description = "Used when buying anything in a store. The UI Purchase sound is only heard when the purchase is successfully completed, not just when tapping on the 'buy' button (for that, a UI_Tick would suffice)."; break;
				case UIAudioTriggerTypes.Start: description = "For whichever button causes gampelay to start."; break;
				case UIAudioTriggerTypes.Custom: description = "For very specific use-cases where a button is not part of the usual UI."; break;
			}
			GUILayout.Label(description, textHeight);
		}
		EditorGUILayout.EndScrollView();
		EditorGUILayout.EndVertical();
		EditorGUILayout.EndHorizontal();


	}

}
#endif