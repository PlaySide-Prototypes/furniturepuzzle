#if FALSE

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public sealed class LocalNotificationManager : MonoBehaviour
{
    enum NotificationTypes
    {
        Generic,
        CharacterRescue,
        EnvironmwntUnlocked
    }

    static int numberOfGenerics = 5;
    static int numberOfChests = 3;
	static float hoursForReminderNotification = 8;

	public static void RegisterForLocalNotifications()
	{
		Debug.Log("Registering for local notifications");

#if UNITY_IOS
		UnityEngine.iOS.NotificationServices.RegisterForNotifications(UnityEngine.iOS.NotificationType.Alert | UnityEngine.iOS.NotificationType.Badge | UnityEngine.iOS.NotificationType.Sound, true);
#endif

        PlayerPrefsManager.SetInt(PlayerPrefInts.PrefsAcceptedLocalNotifications, 1);
	}

	public static bool UserAcceptedLocalNotifications
	{
		get { return PlayerPrefsManager.GetInt(PlayerPrefInts.PrefsAcceptedLocalNotifications, 1) > 0; }
		set { PlayerPrefsManager.SetInt(PlayerPrefInts.PrefsAcceptedLocalNotifications, value ? 1 : 0); }
	}

	public static bool IsEligibleForLocalNotifications
	{
		get
		{
			Debug.Log("LocalNotificationManager >> IsEligibleForLocalNotifications > IsProbablyFirstTimeUser() == " + IsProbablyFirstTimeUser().ToString() + ", CheckLocalNotificationsPermitted() == " + CheckLocalNotificationsPermitted().ToString());
			return IsProbablyFirstTimeUser() || CheckLocalNotificationsPermitted();
		}
	}

	public static bool CheckLocalNotificationsPermitted()
	{
#if UNITY_IOS
		if (UnityEngine.iOS.NotificationServices.enabledNotificationTypes == UnityEngine.iOS.NotificationType.None)
		{
			return false;
		}
#endif

		return true;
	}

	public static void UserChoseToAcceptLocalNotifications()
	{
		UserAcceptedLocalNotifications = true;
		RegisterForLocalNotifications();
	}

	public static bool IsProbablyFirstTimeUser()
	{
		return (PlayerPrefsManager.GetInt(PlayerPrefInts.PrefsAcceptedLocalNotifications, 1) > 0);
	}

	private void Awake()
	{
		LogAllNotifications();
		ClearReceivedLocalNotifications();
		CancelScheduledLocalNotifications();

		DontDestroyOnLoad(gameObject);
	}

	private void OnApplicationPause(bool pause)
	{
        if (!GameDataManager.instance || !GameDataManager.instance.dataLoaded)
		{
			// We're not initialized yet! Don't try to access stuff, it won't work.
			// (eg: if application pause happens while Playfab is loading)
			return;
		}

        Debug.Log("Local notifications on application pause: " + pause);

        if (pause)
        {
            SetPushNotifications();
        }
        else
        {
            // Clear scheduled and received notifications
            ClearReceivedLocalNotifications();
            CancelScheduledLocalNotifications();
        }
    }
    
    public static void SetPushNotifications()
    {
        GameDataManager.instance.CheckIfLoaded();

        Debug.Log("Has user accepted local notifications? " + UserAcceptedLocalNotifications);

        // Schedule notifications
        if (UserAcceptedLocalNotifications && CheckLocalNotificationsPermitted())
        {
            LocIDs id;
            if (ChestManager.ChestBeingOpened())
            {
                DateTime chestFinishedTime = DateTime.Now.AddSeconds(ChestManager.ChestTimeRemaining());

                Enum.TryParse("Push_Notifications_Chest_Ready_" + UnityEngine.Random.Range(0, numberOfChests).ToString(), out id);

                ScheduleLocalNotification(chestFinishedTime.ToLocalTime(), LocManager.instance.Get(id), Assets.SimpleAndroidNotifications.NotificationIcon.Bell);
            }

            bool[] excludedNotifcation = new bool[Enum.GetValues(typeof(NotificationTypes)).Length];

            excludedNotifcation[(int)NotificationTypes.EnvironmwntUnlocked] =  EnvironmentManager.instance.environmentReached == (int)EnvironmentManager.instance.finalEnvironment;
            excludedNotifcation[(int)NotificationTypes.CharacterRescue] = PlayerPrefsManager.GetInt(PlayerPrefInts.CharactersUnlocked) == Enum.GetValues(typeof(CharacterType)).Length - 1;

            int typeOfNotification = UnityEngine.Random.Range(0, System.Enum.GetValues(typeof(NotificationTypes)).Length);
            int i = 0;

            Debug.Log("Starting While Loop");

            while(excludedNotifcation[typeOfNotification] && i < 100)
            {
                typeOfNotification = UnityEngine.Random.Range(0, System.Enum.GetValues(typeof(NotificationTypes)).Length);

                i++;
            }

            Debug.Log(i == 100 ? "Abandoned While Loop" : "Finished While Loop");

            switch ((NotificationTypes)typeOfNotification)
            {
                case NotificationTypes.Generic:
                    Enum.TryParse("Push_Notifications_Generic_" + UnityEngine.Random.Range(0, numberOfGenerics).ToString(), out id); 
                    ScheduleLocalNotification(DateTime.Now.AddHours(hoursForReminderNotification), LocManager.instance.Get(id), Assets.SimpleAndroidNotifications.NotificationIcon.Bell);
                    break;
                case NotificationTypes.CharacterRescue:
                    ScheduleLocalNotification(DateTime.Now.AddHours(hoursForReminderNotification),
                        LocManager.instance.Get(LocIDs.Push_Notifications_Character_Rescue).
                        Replace("[0]", LocManager.instance.Get(LocIDs.Smolder_Name + PlayerPrefsManager.GetInt(PlayerPrefInts.CharactersUnlocked))), 
                        Assets.SimpleAndroidNotifications.NotificationIcon.Bell);
                    break;
                case NotificationTypes.EnvironmwntUnlocked:
                    Enum.TryParse("Push_Notifications_Environment_Unlock_" + (KitArea)EnvironmentManager.instance.environmentReached, out id);
                    ScheduleLocalNotification(DateTime.Now.AddHours(hoursForReminderNotification), 
                        LocManager.instance.Get(id), 
                        Assets.SimpleAndroidNotifications.NotificationIcon.Bell);
                    break;
            }
        }
    }

	public static void ScheduleLocalNotification(DateTime _sendTime, string body, Assets.SimpleAndroidNotifications.NotificationIcon _icon)
	{
		string title = LocManager.instance.Get(LocIDs.Push_Notifications_Title);
		QueueLocalNotification(_sendTime, title, body, _icon);
	}

	public static DateTime GetNextDayOfType(DateTime _startDate, DayOfWeek _dayType)
	{
		int daysUntil = (((int)_dayType - (int)_startDate.DayOfWeek + 7) % 7);
		return _startDate.Date.AddDays(daysUntil);
	}

	public static void ClearReceivedLocalNotifications()
	{
		Debug.Log("LocalNotificationManager >> Clearing local notifications.");

#if UNITY_IOS
		UnityEngine.iOS.NotificationServices.ClearLocalNotifications();
#endif

#if UNITY_ANDROID
		Assets.SimpleAndroidNotifications.NotificationManager.CancelAll();
#endif
	}

	public static void CancelScheduledLocalNotifications()
	{
		Debug.Log("LocalNotificationManager >> Cancelling scheduled notifications.");
#if UNITY_IOS
		UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications();
#endif

#if UNITY_ANDROID
		Assets.SimpleAndroidNotifications.NotificationManager.CancelAll();
#endif
	}

	public static void LogAllNotifications()
	{
		#if UNITY_IOS
		int count = UnityEngine.iOS.NotificationServices.localNotifications != null ? UnityEngine.iOS.NotificationServices.localNotifications.Length : 0;
		int scheduledCount = UnityEngine.iOS.NotificationServices.scheduledLocalNotifications != null ? UnityEngine.iOS.NotificationServices.scheduledLocalNotifications.Length : 0;

		Debug.Log("LocalNotificationManager >> found " + scheduledCount + " scheduled local notifications and " + count + " currently received local notifications");
		for (int i = 0; i < count; i++)
		{
			UnityEngine.iOS.LocalNotification notif = UnityEngine.iOS.NotificationServices.GetLocalNotification(i);
			Debug.Log("LocalNotificationManager >> found received local notification for " + notif.fireDate.ToString() + " with content : " + notif.alertBody);
		}
		for (int i = 0; i < scheduledCount; i++)
		{
			UnityEngine.iOS.LocalNotification notif = UnityEngine.iOS.NotificationServices.scheduledLocalNotifications[i];
			Debug.Log("LocalNotificationManager >> found scheduled local notification for " + notif.fireDate.ToString() + " with content : " + notif.alertBody);
		}
		#endif
	}

	public static void QueueLocalNotification(DateTime _deliveryTime, string _title, string _msg, Assets.SimpleAndroidNotifications.NotificationIcon _icon)
	{
		RegisterForLocalNotifications();

		Debug.Log("LocalNotificationManager >> scheduling local notification for " + _deliveryTime.ToString() + " with content : " + _msg);

		#if UNITY_IOS
			UnityEngine.iOS.LocalNotification iosNotification = new UnityEngine.iOS.LocalNotification();
			iosNotification.fireDate = _deliveryTime;
			iosNotification.alertAction = _title;
			iosNotification.alertBody = _msg;
			iosNotification.soundName = UnityEngine.iOS.LocalNotification.defaultSoundName;
			iosNotification.alertAction = "Rise of the TMNT: Ninja Run";
			UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(iosNotification);
		#endif

		#if UNITY_ANDROID
		System.TimeSpan sendDelay = _deliveryTime.Subtract(System.DateTime.Now);

		Assets.SimpleAndroidNotifications.NotificationParams notificationParams = new Assets.SimpleAndroidNotifications.NotificationParams
		{
			Title = _title,
			Message = _msg,
			Delay = sendDelay,
			Sound = true,
			Vibrate = true,
			Light = true,
			SmallIcon = _icon,
			SmallIconColor = new Color(1f, 0.84f, 0f, 1f),
			LargeIcon = "app_icon",
			Ticker = "Ticker",
			Id = UnityEngine.Random.Range(0, int.MaxValue)
		};

		Assets.SimpleAndroidNotifications.NotificationManager.SendCustom(notificationParams);
		#endif
	}
}

#endif