using Malee;
using System.Collections.Generic;
using UnityEngine;
// Some Bentley shennanigans for ye all

#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// The FX Pool contains the spawn-related information we need to build a poolable system.
/// </summary>
[System.Serializable]
public class FXPool
{
	public FXTypes fXType;
	public GameObject prefab;
	public int poolSize = 10;

	internal int currentIndex = 0;
	internal FXPoolObject[] objects;

	public bool followsTarget;
}

/// <summary>
/// The FX Pool Object is simply a container to cache some basics to simplify caching of components during live play.
/// </summary>
public class FXPoolObject
{
	public GameObject gameObj;
	public Transform trans;
	public bool isPFX;
	public ParticleSystem pfx;

	public FxTransformFollow fxTransformFollow;

	public FXPoolObject()
	{
		gameObj = null;
		trans = null;
		isPFX = false;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="T:FXPoolObject"/> class, and will automatically cache anything else required.
	/// </summary>
	/// <param name="gameObject">Game object.</param>
	public FXPoolObject(GameObject gameObject)
	{
		gameObj = gameObject;
		trans = gameObject.transform;
		pfx = gameObj.GetComponent<ParticleSystem>();
		isPFX = (pfx != null);
		fxTransformFollow = gameObj.AddComponent<FxTransformFollow>();
	}
}

[System.Serializable]
public class FXPoolArray : ReorderableArray<FXPool> { }

#if UNITY_EDITOR
[CustomEditor(typeof(FXMaster), true)]
public class FXMasterEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		if (GUILayout.Button("Add New FX Type..."))
			FXTypeWindow.ShowWindow();
	}
}
#endif

public class FXMaster : MonoBehaviour
{
	private static FXMaster instance = null;

	[Reorderable] [SerializeField] private FXPoolArray pools;

	private static Dictionary<FXTypes, int> fxDict = new Dictionary<FXTypes, int>();

	/// <summary>
	/// Called before first Update()
	/// </summary>
	private void Start()
	{
		if (instance == null)
			instance = this;
		else
			Debug.LogError("Singleton instance already exists!");

		// Spawn all the things
		int count = pools.Length;
		fxDict.Clear();
		for (int i = 0; i < count; i++)
		{
			FXPool outPool = pools[i];

			MakePool(pools[i], out outPool);
			pools[i] = outPool;

			fxDict.Add(pools[i].fXType, i);
		}
	}

	/// <summary>
	/// Called when object/script is destroyed
	/// </summary>
	private void OnDestroy()
	{
		if (instance == this)
			instance = null;
	}

	/// <summary>
	/// Makes the pool of objects and stores it back in the container it referenced from to keep things centralised.
	/// </summary>
	/// <param name="poolIn">Pool in.</param>
	/// <param name="poolData">Pool data.</param>
	private void MakePool(FXPool poolIn, out FXPool poolData)
	{
		poolData = poolIn;
		poolData.objects = new FXPoolObject[poolData.poolSize];
		GameObject tmpObj;
		for (int i = 0; i < poolData.objects.Length; i++)
		{
			tmpObj = Instantiate(poolData.prefab);
			poolData.objects[i] = new FXPoolObject(tmpObj);
			poolData.objects[i].trans.parent = transform;	// By default, make all spawned items a child of this to keep the scene tidy
			poolData.objects[i].gameObj.SetActive(false);
		}
	}

	/// <summary>
	/// Emits the FX, which it does by enabling a gameobject that was previously set to disabled.
	/// </summary>
	/// <param name="fxType">Fx type.</param>
	/// <param name="pos">Position to emit the fx.</param>
	/// <param name="followTarget">Transform of the object to follow, null by default</param>
	public static GameObject EmitFX(FXTypes fxType, Vector3 pos, Transform followTarget = null, Quaternion? rot = null)
	{
		int poolID = -1;
		if (!fxDict.TryGetValue(fxType, out poolID))
		{
#if UNITY_EDITOR
			Debug.LogError("FX not found for type '" + fxType + "'");
#endif
			return null;
		}

		FXPool currentPool = instance.pools[poolID];

		currentPool.currentIndex++;	// Increment through the pool
 
		if (currentPool.currentIndex >= currentPool.objects.Length)  // Recycle when all objects exhausted
			currentPool.currentIndex = 0;

		int id = currentPool.currentIndex;

		FXPoolObject currentFXObject = currentPool.objects[id];
		currentFXObject.trans.position = pos;	// Position 
		if (rot != null)
			currentFXObject.trans.rotation = rot.Value;

		if (currentPool.followsTarget)
			currentFXObject.fxTransformFollow.SetTarget(followTarget);
		else
			currentFXObject.fxTransformFollow.SetTarget(null);

		currentFXObject.gameObj.SetActive(false);   // Disable / re-enable to retrigger
		currentFXObject.gameObj.SetActive(true);

		return currentFXObject.gameObj;
	}
}


/// <summary>
/// Class applied to PFX to make them follow a parent without being destroyed themselves
/// </summary>
public class FxTransformFollow : MonoBehaviour
{
	private Transform followTarget;

	private new Transform transform;
	private Vector3 position;
	private Quaternion rotation;

	private void Awake()
	{
		transform = GetComponent<Transform>();
	}

	/// <summary>
	/// Set the current follow target
	/// </summary>
	/// <param name="_target"></param>
	public void SetTarget(Transform _target)
	{
		followTarget = _target;
	}

	private void LateUpdate()
	{
		if (followTarget != null)
		{
			position = followTarget.position;
			rotation = followTarget.rotation;

			transform.SetPositionAndRotation(position, rotation);
		}
	}
}
