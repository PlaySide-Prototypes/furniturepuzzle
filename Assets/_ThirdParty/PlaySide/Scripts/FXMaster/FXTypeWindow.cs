#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class FXTypeWindow : EditorWindow
{
	const string filePath = "/External/PlaySide/Scripts/FXMaster/FXTypes.cs";

	Vector2 scrollPos;
	string newTypeName;

	//[MenuItem("PlaySide/FX Type Manager")]
	public static void ShowWindow()
	{
		FXTypeWindow window = (FXTypeWindow)GetWindow(typeof(FXTypeWindow));
		window.Show();
	}

	void OnGUI()
	{
		scrollPos = GUILayout.BeginScrollView(scrollPos);
		for (int i = 0; i < System.Enum.GetValues(typeof(FXTypes)).Length; ++i)
		{
			GUILayout.Label(((FXTypes)i).ToString());
		}
		newTypeName = GUILayout.TextField(newTypeName);
		if (GUILayout.Button("Add New"))
		{
			if (EditorUtility.DisplayDialog("Are you sure?", "Once created, fx types cannot be modified or deleted. Are you sure you want to add " + newTypeName + "?", "Sure!", "Actually, nah, want to change it."))
			{
				string fullPath = Application.dataPath + filePath;
				List<string> fxTypesFile = new List<string>(System.IO.File.ReadAllLines(fullPath));
				for (int i = fxTypesFile.Count - 1; i >= 0; --i)
				{
					if (fxTypesFile[i] == "}")
					{
						if (!fxTypesFile[i - 1].Contains(","))
							fxTypesFile[i - 1] += ",";

						fxTypesFile.Insert(i, '\t' + newTypeName);

						System.IO.File.WriteAllLines(fullPath, fxTypesFile.ToArray());

						AssetDatabase.Refresh();
						OnGUI();
						break;
					}
				}
			}
		}

		GUILayout.EndScrollView();
	}
}
#endif // UNITY_EDITOR