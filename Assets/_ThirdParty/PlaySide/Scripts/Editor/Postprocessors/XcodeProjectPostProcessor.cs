#if UNITY_IOS

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using System.IO;

public class XcodeProjectPostProcessor
{
	[PostProcessBuild]
	public static void OnPostprocessBuild(BuildTarget buildTarget, string path)
	{
		if (buildTarget == BuildTarget.iOS)
		{
			string debugStr = "XCodeProjectPostProcessor has run! (details below):";
			string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

			PBXProject proj = new PBXProject();
			proj.ReadFromString(File.ReadAllText(projPath));

			string target = proj.TargetGuidByName("Unity-iPhone");

			proj.SetBuildProperty(target, "ENABLE_BITCODE", "false");
			debugStr += "\n - BitCode disabled, thank Danny for your sanity.";
#if !FINAL && !PRODUCTION
			proj.SetBuildProperty(target, "DEBUG_INFORMATION_FORMAT", "dwarf");
			debugStr += "\n - dSYM disabled, thank Danny for your sanity.";
#endif
			proj.SetBuildProperty(target, "CLANG_ENABLE_MODULES", "YES");
			debugStr += "\n - Clang modules enabled.";
//			proj.AddCapability(target, PBXCapabilityType.GameCenter);
//			debugStr += "\nGameCenter enabled.";
			proj.AddCapability(target, PBXCapabilityType.InAppPurchase);
			debugStr += "\n - IAPs enabled.";

			File.WriteAllText(projPath, proj.WriteToString());

			// Get plist
			string plistPath = path + "/Info.plist";
			PlistDocument plist = new PlistDocument();
			plist.ReadFromString(File.ReadAllText(plistPath));

			// Get root
			PlistElementDict rootDict = plist.root;
			var buildKey = "ITSAppUsesNonExemptEncryption";
			rootDict.SetBoolean(buildKey, false);
			debugStr += "\n - AppStoreConnect warning for Encryption removed";
//			var buildKey2 = "NSPhotoLibraryAddUsageDescription";
//			rootDict.SetString(buildKey2, "To Save Screenshots");
//			var buildKey3 = "NSPhotoLibraryUsageDescription";
//			rootDict.SetString(buildKey3, "To Save Screenshots");
//			debugStr += "\n - Photo Library permissions added");
			// Note: NSCameraUsageDescription is done in the Build Settings
			File.WriteAllText(plistPath, plist.WriteToString());

			// Add frameworks for AppsFlyer
//			proj.AddFrameworkToProject(target, "iAd.framework", true);
//			proj.AddFrameworkToProject(target, "Security.framework", true);
//			proj.AddFrameworkToProject(target, "AdSupport.framework", true);
//			debugStr += "\n - AppsFlyer frameworks added";

			Debug.Log(debugStr);
		}
	}
}

#endif
