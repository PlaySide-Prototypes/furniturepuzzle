﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor; 
using System.IO;

public class SpriteProcessor : AssetPostprocessor
{
	bool firstImport = false;
	void OnPreprocessTexture()
	{
		// Only perform on first import
		if ( !AssetDatabase.LoadAssetAtPath(assetPath, typeof(object)) && assetPath.ToLower().Contains("/ui/"))
		{
			TextureImporter textureImporter = (TextureImporter)assetImporter;
			textureImporter.textureType = TextureImporterType.Sprite;
			firstImport = true;
		}
	}

	void OnPostprocessTexture(Texture2D tex)
	{
		// Only perform on first import
		if (!firstImport)
			return;
		
		TextureImporter textureImporter = (TextureImporter)assetImporter;
		if (tex.width <= 512 || tex.height <= 512)
			textureImporter.spritePackingTag = "UI";
	}
}
