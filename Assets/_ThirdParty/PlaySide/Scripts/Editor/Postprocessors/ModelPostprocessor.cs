using UnityEditor; 
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;


class ModelPostprocessor : AssetPostprocessor
{
	void OnPreprocessModel()
	{
		ModelImporter importer = assetImporter as ModelImporter;
		importer.materialName = ModelImporterMaterialName.BasedOnMaterialName;
		importer.materialSearch = ModelImporterMaterialSearch.Everywhere;

		// Check if meta file exists already to determin if this is the first import
		string projectPath = Application.dataPath.Substring(0, Application.dataPath.LastIndexOf("Assets"));
		bool firstImport = !System.IO.File.Exists(projectPath+assetPath + ".meta");

		// Only perform on first import
		if (firstImport)
		{
			bool isAnim = assetPath.ToLower().Contains("anm");
			bool isRig = assetPath.ToLower().Contains("rig");

			importer.materialLocation = ModelImporterMaterialLocation.External;

			// Treat al model files containing 'anm' or 'rig' symbol as Animations
			if (!isAnim)
			{
				importer.importAnimation = false;
				importer.importMaterials = true;

				// Non Rig/Anim FBX's have all anim related stuff disabled
				if(!isRig)
					importer.animationType = ModelImporterAnimationType.None;
			}
			else
				importer.importMaterials = false;

		}
	}	

	void OnPostprocessMaterial(Material material)
	{
		if (material.name.ToLower().Contains("lambert"))
		{
			string foundMaterials = "";
			foundMaterials += material.name+"\n";
			WhoopsLambert(material, foundMaterials);
		}
	}

	void WhoopsLambert(Material _material, string _foundMaterials)
	{
		EditorUtility.DisplayDialog("Looks like you've imported a LAMBERT MATERIAL!", "Better get rid of it before someone notices\n\n" + _foundMaterials, "Sure Thing!", "Sure Thing!");
		Debug.LogWarning("LAMBERT MATERIAL FOUND: Please remove it before pushing, select this to jump to it!\n"+_foundMaterials, _material); 
	}
}
