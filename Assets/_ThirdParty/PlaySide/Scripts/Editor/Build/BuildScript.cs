using UnityEditor;
using UnityEngine;
#if UNITY_2018_1_OR_NEWER
using UnityEditor.Build.Reporting;
#endif

public class BuildPlayerExample : MonoBehaviour
{
#if UNITY_ANDROID
	[MenuItem("PlaySide/Build/Build Android")]
	public static void BuildAndroid_MenuItem()
	{
		BuildNow(BuildTarget.Android, "Build_Android.apk", true);
	}
#endif

#if UNITY_IOS
	[MenuItem("PlaySide/Build/Build iOS")]
	public static void BuildIOS_MenuItem()
	{
		BuildNow(BuildTarget.iOS, "Build_iOS", true);
	}
#endif

	/// <summary> Starts an Android build </summary>
	public static void BuildAndroid()
	{
		BuildNow(BuildTarget.Android, "Build_Android.apk", false);
	}

	/// <summary> Starts an iOS build </summary>
	static void BuildIOS()
	{
		BuildNow(BuildTarget.iOS, "Build_iOS", false);
	}

	/// <summary> Builds for the specified plaform to the specified folder/file name </summary>
	/// <param name="_buildTarget"> Platform to build for </param>
	/// <param name="_buildName"> Folder/file name </param>
	/// <param name="_showWhipCracker"> True to allow WhipCracker to run </param>
	static void BuildNow(BuildTarget _buildTarget, string _buildName, bool _showWhipCracker = false)
	{
		WhipCracker.openOnPreBuild = _showWhipCracker;

		if (System.IO.Directory.Exists(Application.dataPath + "/../Builds/"))
			System.IO.Directory.Delete(Application.dataPath + "/../Builds/", true);
		
		BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
		{
			scenes = GetScenePaths(),
			locationPathName = Application.dataPath + "/../Builds/" + _buildName,
			target = _buildTarget,
			options = BuildOptions.None
		};

#if UNITY_2018_1_OR_NEWER
		BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);

		BuildSummary summary = report.summary;
		switch (summary.result)
		{
			case BuildResult.Succeeded:	Debug.Log("Build succeeded in '" + buildPlayerOptions.locationPathName + "': " + summary.totalSize + " bytes"); break;
			case BuildResult.Failed:	Debug.LogError("Build failed!"); break;
			case BuildResult.Cancelled:	Debug.Log("Build cancelled!"); break;

			default: Debug.LogError("Unhandled/invalid build result '" + summary.result + "'!"); break;
		}
#else
		// Unity 2017.x
		BuildPipeline.BuildPlayer(buildPlayerOptions);
#endif

		WhipCracker.openOnPreBuild = true;
	}

	/// <summary> Get all included Scenes added in the Build Settings </summary>
	/// <returns> Array of scene names </returns>
	static string[] GetScenePaths()
	{
		EditorBuildSettingsScene[] allScenes = EditorBuildSettings.scenes;
		int count = allScenes.Length;
		string[] sceneNames = new string[count];
		for (int i = 0; i < count; i++)
		{
			sceneNames[i] = allScenes[i].path;
		}

		return sceneNames;
	}
}
