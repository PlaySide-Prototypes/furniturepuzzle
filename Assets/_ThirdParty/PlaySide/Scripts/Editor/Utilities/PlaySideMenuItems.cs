﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

public class PlaySideMenuItems : MonoBehaviour
{
	[MenuItem("PlaySide/Clear PlayerPrefs.../CLEAR THEM!")]
	private static void ClearPlayerPrefs()
	{
		PlayerPrefs.DeleteAll();
	}

	[MenuItem("PlaySide/Reload Current Scene")]
	public static void ReloadCurrentScene()
	{
		if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
			EditorSceneManager.OpenScene(EditorSceneManager.GetActiveScene().path);
	}
}
