﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEditor;
using System;
using System.IO;

[InitializeOnLoad]
public class CheckForAudioToolkit
{
	static CheckForAudioToolkit()
	{
		var audioType = (from assembly in AppDomain.CurrentDomain.GetAssemblies()
					from type in assembly.GetTypes()
					where type.Name == "AudioController"
					select type);

		if (audioType.Count() > 0)
		{
			string[] triggerGUID = AssetDatabase.FindAssets("PlaysideAudioUITrigger");
			if (triggerGUID.Length == 0)
				return;

			for (int i = 0; i < triggerGUID.Length; ++i)
			{
				string triggerPath = AssetDatabase.GUIDToAssetPath(triggerGUID[0]);
				TextAsset triggerText = AssetDatabase.LoadAssetAtPath<TextAsset>(triggerPath);
				if (!triggerText)
					continue;

				string text = triggerText.text;
				while (text.Contains("#if FALSE"))
				{
					int falseIndex = text.IndexOf("#if FALSE");
					int falseNewLineIndex = text.IndexOf('\n', falseIndex);

					int endFalseIndex = text.IndexOf("#endif", falseNewLineIndex);
					int endFalseNewLineIndex = text.IndexOf('\n', endFalseIndex);

					text = text.Remove(endFalseIndex, endFalseNewLineIndex - endFalseIndex);
					text = text.Remove(falseIndex, falseNewLineIndex - falseIndex);
				}

				File.WriteAllText(triggerPath, text);
				EditorUtility.SetDirty(triggerText);

				string[] thisScriptGUIDs = AssetDatabase.FindAssets("CheckForAudioToolkit");
				if (thisScriptGUIDs.Length == 1)
					AssetDatabase.DeleteAsset(AssetDatabase.GUIDToAssetPath(thisScriptGUIDs[0]));

				break;
			}
		}
	}
}
