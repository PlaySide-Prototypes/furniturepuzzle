using UnityEditor;
using UnityEngine;

public class ApplySelectedPrefabs
{
	private const int requiredItemsForProgressBar = 5;
	private static bool showProgressBar;
	private static int changedObjectsCount;

	private System.Action<GameObject> prefabAction;

	/// <summary>
	/// Start Process of Applying Prefabs
	/// </summary>
	[MenuItem("PlaySide/Tools/Apply Selected Prefabs %&a", false, 100)]
	private static void ApplyPrefabs()
	{
		SearchPrefabConnections(ApplyToSelectedPrefabs);
	}

	/// <summary>
	/// Start Process of Reverting Prefabs
	/// </summary>
	[MenuItem("PlaySide/Tools/Revert Selected Prefabs %&r", false, 100)]
	private static void ResetPrefabs()
	{
		SearchPrefabConnections(RevertToSelectedPrefabs);
	}

	/// <summary>
	/// Validation for Buttons. Cannot Apply or Revert wihout having an object selected
	/// </summary>
	/// <returns></returns>
	[MenuItem("PlaySide/Tools/Apply Selected Prefabs %&a", true)]
	[MenuItem("PlaySide/Tools/Revert Selected Prefabs %&r", true)]
	private static bool IsSceneObjectSelected()
	{
		return Selection.activeTransform != null;
	}

	/// <summary>
	/// Search through the Selected Prefabs
	/// </summary>
	/// <param name="_prefabAction"></param>
	private static void SearchPrefabConnections(System.Action<GameObject> _prefabAction)
	{
		// Store all currently selected Gameobjects
		GameObject[] selectedObjects = Selection.gameObjects;

		// Check for the Progress bar
		int numberOfTransforms = selectedObjects.Length;
		showProgressBar = numberOfTransforms >= requiredItemsForProgressBar;
		changedObjectsCount = 0;

		//Iterate through all the selected gameobjects
		for(int i = 0; i < numberOfTransforms; i++)
		{
			GameObject currentSelected = selectedObjects[i];

			if (showProgressBar)
				UpdateProgressBar(currentSelected.name, (float)i / numberOfTransforms);

			IterateThroughObjectTree(_prefabAction, currentSelected);
		}

		if (showProgressBar)
			EditorUtility.ClearProgressBar();
	}

	/// <summary>
	/// Confirm the Object is a prefab before Applying or Reverting
	/// </summary>
	/// <param name="_prefabAction"></param>
	/// <param name="_object"></param>
	private static void IterateThroughObjectTree(System.Action<GameObject> _prefabAction, GameObject _object)
	{
		PrefabType prefabType = PrefabUtility.GetPrefabType(_object);

		// If the object being checked is a prefab execute the Apply Function and return
		if (prefabType == PrefabType.PrefabInstance || prefabType == PrefabType.DisconnectedPrefabInstance)
		{
			GameObject prefabRoot = PrefabUtility.FindRootGameObjectWithSameParentPrefab(_object);
			if (prefabRoot != null)
			{
				_prefabAction(prefabRoot);
				changedObjectsCount++;
				return;
			}
		}

		// If the object being checked isnt a prefab, search through its children for a nested prefab
		Transform objectTransform = _object.transform;
		int count = objectTransform.childCount;
		for (int i = 0; i < count; i++)
		{
			GameObject childGo = objectTransform.GetChild(i).gameObject;
			IterateThroughObjectTree(_prefabAction, childGo);
		}
	}

	/// <summary>
	/// Update the Progress Bar if its Required
	/// </summary>
	/// <param name="_prefabName"></param>
	/// <param name="_progress"></param>
	private static void UpdateProgressBar(string _prefabName, float _progress)
	{
		EditorUtility.DisplayProgressBar("Update prefabs", "Updating prefab " + _prefabName, _progress);
	}

	/// <summary>
	/// Apply the Prefabs
	/// </summary>
	/// <param name="_selectedObject"></param>
	private static void ApplyToSelectedPrefabs(GameObject _selectedObject)
	{
		Object prefabAsset = PrefabUtility.GetCorrespondingObjectFromSource(_selectedObject);
		if (prefabAsset == null)
			return;

		PrefabUtility.ReplacePrefab(_selectedObject, prefabAsset, ReplacePrefabOptions.ConnectToPrefab);
	}

	/// <summary>
	/// Revert the Prefabs
	/// </summary>
	/// <param name="_selectedObject"></param>
	private static void RevertToSelectedPrefabs(GameObject _selectedObject)
	{
		PrefabUtility.ReconnectToLastPrefab(_selectedObject);
		PrefabUtility.RevertPrefabInstance(_selectedObject);
	}
}
