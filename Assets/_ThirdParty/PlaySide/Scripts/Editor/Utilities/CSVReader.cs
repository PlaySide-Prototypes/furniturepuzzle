using UnityEngine;
using UnityEditor;

using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;
using UnityEditor.Callbacks;

public class CSVReader
{
	[MenuItem("PlaySide/CSV Editor/Set CSV Editor to use tabs")]
	static void ChangeToTabs()
	{
		EditorPrefs.SetBool("CSVEditorUseTabs", true);
	}
	[MenuItem("PlaySide/CSV Editor/Set CSV Editor to use tabs", true)]
	static bool ValidateChangeToTabs()
	{
		return !EditorPrefs.GetBool("CSVEditorUseTabs", true);
	}

	[MenuItem("PlaySide/CSV Editor/Set CSV Editor to use commas")]
	static void ChangeToSpaces()
	{
		EditorPrefs.SetBool("CSVEditorUseTabs", false);
	}
	[MenuItem("PlaySide/CSV Editor/Set CSV Editor to use commas", true)]
	static bool ValidateChangeToSpaces()
	{
		return EditorPrefs.GetBool("CSVEditorUseTabs", true);
	}

	[OnOpenAsset]
	public static bool CSVCheck(int instanceID, int line)
	{
		Object csvObject = EditorUtility.InstanceIDToObject(instanceID);
		string path = AssetDatabase.GetAssetPath(instanceID);
		if (csvObject && csvObject is TextAsset && path.EndsWith(".csv"))
		{
			CSVWindow.Init(csvObject as TextAsset);
			return true;
		}
		return false;
	}
}

struct FieldIndex
{
	public int x;
	public int y;

	public FieldIndex(int a_x, int a_y)
	{
		x = a_x; y = a_y;
	}
}

public class CSVWindow : EditorWindow
{
	static List<List<string>> csvFields;
	static List<List<int>> fieldControlIDs;
	static List<float> columnWidths;

	const float lineNoColWidth = 50f;
	const float colWidth = 100.0f;
	const float rowHeight = 30.0f;
	const float resizeSpeed = 30.0f;
	const float initialHeight = 20.0f;
	const float secondRowSpace = 40.0f;
	const float initialSliderPosition = 0.8f;
	const float minColumnSize = 10.0f;

	static TextAsset asset;

	static char fieldDelimeter { get { return EditorPrefs.GetBool("CSVEditorUseTabs", true) ? '\t' : ','; } }

	List<FieldIndex> selectedFields = new List<FieldIndex>();
	bool hasTypedWithCurrentSelection = false;

	Dictionary<FieldIndex, string> copyBuffer = new Dictionary<FieldIndex, string>();

	public static void Init(TextAsset a_assetToShow)
	{
		asset = a_assetToShow;

		CSVWindow window = GetWindow<CSVWindow>("CSV Reader", true);
		window.Show();

		string text = asset.text;
		text = text.Replace("\r\n", "\n");
		string[] lines = text.Split(new char[1] { '\n' }, System.StringSplitOptions.RemoveEmptyEntries);

		csvFields = new List<List<string>>();
		fieldControlIDs = new List<List<int>>();
		columnWidths = new List<float>();

		for (int i = 0; i < lines.Length; ++i)
		{
			List<string> line = new List<string>(lines[i].Split(fieldDelimeter));
			csvFields.Add(line);
			List<int> fieldControlIdEntry = new List<int>();
			for (int j = 0; j < line.Count; ++j)
			{
				fieldControlIdEntry.Add(0);

				if (i == 0)
					columnWidths.Add(colWidth);
			}
			fieldControlIDs.Add(fieldControlIdEntry);
		}

		LoadWidths();
	}

	Vector2 scrollPos;
	Rect rect = new Rect();
	GUIStyle lineNoStyle;
	bool splitFirstRow = true;



	void OnGUI()
	{
		if (csvFields == null)
			return;

		float scrollWidth = colWidth;
		for (int i = 0; i < columnWidths.Count; ++i)
			scrollWidth += columnWidths[i];

		float scrollHeight = (csvFields.Count + 2) * rowHeight;

		if (lineNoStyle == null)
		{
			lineNoStyle = new GUIStyle();
#if UNITY_PRO_LICENSE
			lineNoStyle.normal.textColor = new Color(0.75f, 0.75f, 0.75f);
#else
			lineNoStyle.normal.textColor = new Color(0.0f, 0.0f, 0.0f);
#endif
		}

		rect.height = rowHeight;
		rect.width = lineNoColWidth;

		// Add line numbers
		for (int i = 0; i < csvFields.Count; ++i)
		{
			rect.x = 0;
			if (i == 0)
				rect.y = initialHeight;
			else
			{
				if (scrollPos.y > i * rowHeight)
					continue;

				if (splitFirstRow)
					rect.y = initialHeight + secondRowSpace + i * rowHeight - scrollPos.y;
				else
					rect.y = (i + 1) * rowHeight - scrollPos.y;
			}

			EditorGUI.LabelField(rect, "  " + (i + 1), lineNoStyle);
		}

		rect.width = colWidth;

		rect.x = lineNoColWidth;
		rect.y = 0;


		if (csvFields.Count > 0)
		{
			rect.height = rowHeight * 0.5f;

			// Draw resizing sliders
			for (int j = 0; j < csvFields[0].Count; ++j)
			{
				rect.x -= scrollPos.x;
				rect.width = columnWidths[j] * 0.5f;


				if (GUI.Button(rect, "-"))
				{
					columnWidths[j] -= resizeSpeed;
					if (columnWidths[j] <= minColumnSize)
						columnWidths[j] = minColumnSize;
				}
				rect.x += columnWidths[j] * 0.5f;
				if (GUI.Button(rect, "+"))
					columnWidths[j] += resizeSpeed;

				rect.x += columnWidths[j] * 0.5f + scrollPos.x;
			}

			rect.y = initialHeight;
			rect.x = lineNoColWidth;

			rect.height = rowHeight;

			// Draw the first row. We do this separately here, so that it isn't part of the scroll view.
			if (splitFirstRow)
			{
				for (int j = 0; j < csvFields[0].Count; ++j)
				{
					Color bgColor = GUI.backgroundColor;
					if (selectedFields.Count > 1 && selectedFields.Contains(new FieldIndex(0, j)))
						GUI.backgroundColor = Color.yellow;

					rect.x -= scrollPos.x;
					rect.width = columnWidths[j];

					//rect.x = lineNoColWidth + (j * colWidth) - scrollPos.x;
					GUIContent content = new GUIContent("", "Field " + 0 + "," + j);
					fieldControlIDs[0][j] = EditorGUIUtility.GetControlID(content, FocusType.Passive);
					csvFields[0][j] = EditorGUI.TextField(rect, content, csvFields[0][j]);

					GUI.backgroundColor = bgColor;

					rect.x += columnWidths[j] + scrollPos.x;
				}
			}
		}

		GUILayoutUtility.GetRect(scrollWidth, rowHeight);

		scrollPos = EditorGUILayout.BeginScrollView(scrollPos, true, true);
		GUILayoutUtility.GetRect(scrollWidth, scrollHeight);

		if (splitFirstRow)
			rect.y += secondRowSpace;
		else
			rect.y = 0;

		// Draw the fields
		for (int i = (splitFirstRow?1:0); i < csvFields.Count; ++i)
		{
			rect.x = lineNoColWidth;

			for (int j = 0; j < csvFields[i].Count; ++j)
			{
				Color bgColor = GUI.backgroundColor;
				if (selectedFields.Count > 1 && selectedFields.Contains(new FieldIndex(i, j)))
					GUI.backgroundColor = Color.yellow;

				rect.width = columnWidths[j];

				GUIContent content = new GUIContent("", "Field " + i + "," + j);
				fieldControlIDs[i][j] = EditorGUIUtility.GetControlID(content, FocusType.Passive);
				csvFields[i][j] = EditorGUI.TextField(rect, content, csvFields[i][j]);

				GUI.backgroundColor = bgColor;

				rect.x += columnWidths[j];
			}

			rect.y += rowHeight;
		}
		EditorGUILayout.EndScrollView();

		EditorGUILayout.BeginHorizontal();
		splitFirstRow = GUILayout.Toggle(splitFirstRow, "Split First Row");
	
		// Add row button
		if (GUILayout.Button("+Row"))
			AddExtraRowToEnd();
		EditorGUILayout.LabelField("(Right-click inside cells to insert/remove columns + rows)");
		EditorGUILayout.EndHorizontal();

		// Save + Generate Script buttons
		EditorGUILayout.BeginHorizontal();
		if (GUILayout.Button("Save"))
			Save();
		if (GUILayout.Button("Generate Script"))
			GenerateScript();
		EditorGUILayout.EndHorizontal();

		if ((Event.current.button == 0 && Event.current.type == EventType.Repaint))
		{
			for (int i = 0; i < fieldControlIDs.Count; ++i)
			{
				for (int j = 0; j < fieldControlIDs[i].Count; ++j)
				{
					if (fieldControlIDs[i][j] == EditorGUIUtility.hotControl - 1)
					{
						if (Event.current.control || Event.current.command)
						{
							FieldIndex index = new FieldIndex(i, j);
							if (!selectedFields.Contains(index))
								selectedFields.Add(index);
						}
						else if (Event.current.shift)
						{
							FieldIndex start = selectedFields[selectedFields.Count - 1];
							FieldIndex end = new FieldIndex(i, j);
							FieldIndex current = start;
							while (current.x != end.x || current.y != end.y)
							{
								if (current.x != end.x)
								{
									current.x += (current.x < end.x ? 1 : -1);
									if (!selectedFields.Contains(current))
										selectedFields.Add(current);
								}
								if (current.x == end.x && current.y != end.y)
								{
									current.x = start.x;
									current.y += (current.y < end.y ? 1 : -1);
									if (!selectedFields.Contains(current))
										selectedFields.Add(current);
								}
							}
						}
						else
						{
							selectedFields.Clear();
							selectedFields.Add(new FieldIndex(i, j));
						}
						hasTypedWithCurrentSelection = false;
						break;
					}
				}
			}
		}

		if (Event.current.button == 1)
		{
			GenericMenu menu = new GenericMenu();
			menu.AddItem(new GUIContent("Add row above"), false, AddExtraRowAboveCurrent);
			menu.AddItem(new GUIContent("Add row below"), false, AddExtraRowBelowCurrent);
			menu.AddItem(new GUIContent("Duplicate row"), false, DuplicateRow);
			menu.AddItem(new GUIContent("Remove row"), false, RemoveCurrentRow);
			menu.AddItem(new GUIContent("Add column right"), false, AddExtraColumnRightOfCurrent);
			menu.AddItem(new GUIContent("Add column left"), false, AddExtraColumnLeftOfCurrent);
			menu.AddItem(new GUIContent("Duplicate column"), false, DuplicateColumn);
			menu.AddItem(new GUIContent("Remove column"), false, RemoveCurrentColumn);
			if (selectedFields.Count > 0)
			{
				menu.AddItem(new GUIContent("Copy"), false, Copy);
				if (copyBuffer.Count > 0)
					menu.AddItem(new GUIContent("Paste"), false, Paste);
			}
		
			menu.ShowAsContext();
		}

		MultiselectTyping();
	}

	enum BestTypeMatch
	{
		INT,
		FLOAT,
		STRING
	}

	void AddExtraColumnToEnd()
	{
		for (int i = 0; i < csvFields.Count; ++i)
		{
			csvFields[i].Add("");
			fieldControlIDs[i].Add(0);
			columnWidths.Add(colWidth);
		}
	}

	void AddExtraRow(int a_index, int a_indexToDuplicate = -1)
	{
		List<string> newRow = new List<string>();
		List<int> newFieldControlIds = new List<int>();
		if (a_indexToDuplicate >= 0 && csvFields.Count > 0)
		{
			for (int i = 0; i < csvFields[a_indexToDuplicate].Count; ++i)
			{
				newRow.Add(csvFields[a_indexToDuplicate][i]);
				newFieldControlIds.Add(0);
			}
		}
		else if (csvFields.Count > 0)
		{
			for (int i = 0; i < csvFields[(a_index > 0 ? a_index - 1 : 0)].Count; ++i)
			{
				newRow.Add("");
				newFieldControlIds.Add(0);
			}
		}
		else
		{
			newRow.Add("");
			newFieldControlIds.Add(0);
		}
		csvFields.Insert(a_index, newRow);
		fieldControlIDs.Insert(a_index, newFieldControlIds);
	}

	void AddExtraRowAboveCurrent()
	{
		AddExtraRow(selectedFields[selectedFields.Count - 1].x);
	}

	void AddExtraRowBelowCurrent()
	{
		AddExtraRow(selectedFields[selectedFields.Count - 1].x + 1);
	}

	void DuplicateRow()
	{
		int currentIndex = selectedFields[selectedFields.Count - 1].x;
		AddExtraRow(currentIndex, currentIndex);
	}

	void AddExtraRowToEnd()
	{
		AddExtraRow(csvFields.Count);
	}

	void RemoveCurrentRow()
	{
		csvFields.RemoveAt(selectedFields[selectedFields.Count - 1].x);
		fieldControlIDs.RemoveAt(selectedFields[selectedFields.Count - 1].x);
	}

	void AddExtraColumnRightOfCurrent()
	{
		int newIndex = selectedFields[selectedFields.Count - 1].y + 1;

		for (int i = 0; i < csvFields.Count; ++i)
		{
			csvFields[i].Insert(newIndex, "");
			fieldControlIDs[i].Insert(newIndex, 0);
		}

		columnWidths.Insert(newIndex, colWidth);
	}

	void AddExtraColumnLeftOfCurrent()
	{
		int newIndex = selectedFields[selectedFields.Count - 1].y;

		for (int i = 0; i < csvFields.Count; ++i)
		{
			csvFields[i].Insert(newIndex, "");
			fieldControlIDs[i].Insert(newIndex, 0);
		}

		columnWidths.Insert(newIndex, colWidth);
	}

	void DuplicateColumn()
	{
		int index = selectedFields[selectedFields.Count - 1].y;

		for (int i = 0; i < csvFields.Count; ++i)
		{
			csvFields[i].Insert(index + 1, csvFields[i][index]);
			fieldControlIDs[i].Insert(index + 1, 0);
		}

		columnWidths.Insert(index + 1, colWidth);
	}

	void RemoveCurrentColumn()
	{
		int index = selectedFields[selectedFields.Count - 1].y;
		for (int i = 0; i < csvFields.Count; ++i)
		{
			csvFields[i].RemoveAt(index);
			fieldControlIDs[i].RemoveAt(index);
		}

		columnWidths.RemoveAt(index);
	}

	bool keyHasBeenPressed = false;
	float lastBackSpaceTime = 0.0f; //Unity seems to send the backspace event multiple times? Check how long since we last pressed it, and ignore if we pressed it too quickly.
	const float TIME_BETWEEN_BACKSPACES = 0.1f;
	void MultiselectTyping()
	{
		if (selectedFields.Count <= 1)
		{
			keyHasBeenPressed = false;
			return;
		}

		if (keyHasBeenPressed)
		{
			if (Event.current.character == '\0' && Event.current.keyCode != KeyCode.Backspace)
				keyHasBeenPressed = false;
			return;
		}
		if (Event.current.character == '\0' && Event.current.keyCode != KeyCode.Backspace)
			return;

		if (Event.current.character == '\b' || Event.current.keyCode == KeyCode.Backspace)
		{
			if (Time.realtimeSinceStartup < lastBackSpaceTime + TIME_BETWEEN_BACKSPACES)
				return;
			else
				lastBackSpaceTime = Time.realtimeSinceStartup;
		}

		keyHasBeenPressed = true;
		for (int i = 0; i < selectedFields.Count; ++i)
		{
			int x = selectedFields[i].x;
			int y = selectedFields[i].y;

			if ((Event.current.character == '\b' || Event.current.keyCode == KeyCode.Backspace) && csvFields[x][y].Length > 0)
				csvFields[x][y] = csvFields[x][y].Substring(0, csvFields[x][y].Length - 1);
			else if (Event.current.character == '\n' || Event.current.character == '\r')
			{
				selectedFields.Clear();
				break;
			}
			else if (hasTypedWithCurrentSelection)
				csvFields[x][y] += Event.current.character;
			else
				csvFields[x][y] = Event.current.character.ToString();
		}
		hasTypedWithCurrentSelection = true;

		while (Event.GetEventCount() > 0)
		{
			Event outEvent = new Event();
			Event.PopEvent(outEvent);
		}
	}

	void GenerateScript()
	{
		string scriptName = Regex.Replace(asset.name, @"\s+", "") + "Stats";
		string scriptNameSingular = scriptName.Substring(0, scriptName.Length - 1);
		string scriptPath = Application.dataPath + "/Scripts/CSV/" + scriptName + ".cs";

		FileInfo file = new FileInfo(scriptPath);
		file.Directory.Create();

		if (File.Exists(scriptPath))
			File.Delete(scriptPath);

		string[] columnNames = new string[csvFields[0].Count];
		for (int i = 0; i < csvFields[0].Count; ++i)
		{
			columnNames[i] = Regex.Replace(csvFields[0][i], @"\s+", "");
		}

		string[] camelCaseColumnNames = new string[columnNames.Length];
		for (int i = 0; i < columnNames.Length; ++i)
		{
			camelCaseColumnNames[i] = char.ToLower(columnNames[i][0]) + columnNames[i].Remove(0, 1);
		}

		BestTypeMatch[] columnTypes = new BestTypeMatch[csvFields[0].Count];
		if (csvFields.Count < 2)
		{
			for (int i = 0; i < columnTypes.Length; ++i)
			{
				columnTypes[i] = BestTypeMatch.STRING;
			}
		}
		else
		{
			int outInt; float outFloat;
			for (int i = 0; i < csvFields[0].Count; ++i)
			{
				BestTypeMatch bestTypeMatch = BestTypeMatch.INT;
				for (int j = 1; j < csvFields.Count; ++j)
				{
					if (bestTypeMatch == BestTypeMatch.INT)
					{
						if (!int.TryParse(csvFields[j][i], out outInt))
							bestTypeMatch = BestTypeMatch.FLOAT;
					}
					if (bestTypeMatch == BestTypeMatch.FLOAT)
					{
						if (!float.TryParse(csvFields[j][i], out outFloat))
						{
							bestTypeMatch = BestTypeMatch.STRING;
							break;
						}
					}
				}
				columnTypes[i] = bestTypeMatch;
			}
		}

		string indexType;
		switch (columnTypes[0])
		{
			case BestTypeMatch.INT: indexType = "int"; break;
			case BestTypeMatch.FLOAT: indexType = "float"; break;
			default: indexType = "string"; break;
		}


		using (FileStream fs = File.Create(scriptPath))
		{
			string fileText =
@"using System.Collections.Generic;
using UnityEngine;

public class " + scriptName + @"
{
	public class " + scriptNameSingular + @"
	{
		enum CSVColumns
		{";
			for (int i = 0; i < csvFields[0].Count; ++i)
			{
				fileText += "\n\t\t\t" + columnNames[i] + ",";
			}
			fileText += @"
		}
";
			for (int i = 0; i < csvFields[0].Count; ++i)
			{
				string type;
				switch (columnTypes[i])
				{
					case BestTypeMatch.INT: type = "int "; break;
					case BestTypeMatch.FLOAT: type = "float "; break;
					default: type = "string "; break;
				}
				fileText += "\n\t\t public " + type + camelCaseColumnNames[i] + ";";
			}

			fileText += @"

		public " + scriptNameSingular + @"(string[] a_data)
		{";
			for (int i = 0; i < csvFields[0].Count; ++i)
			{
				switch (columnTypes[i])
				{
					case BestTypeMatch.STRING: fileText += "\n\t\t\t" + camelCaseColumnNames[i] + " = a_data[(int)CSVColumns." + columnNames[i] + "];"; break;
					case BestTypeMatch.FLOAT: fileText += "\n\t\t\t" + camelCaseColumnNames[i] + " = float.Parse(a_data[(int)CSVColumns." + columnNames[i] + "]);"; break;
					case BestTypeMatch.INT: fileText += "\n\t\t\t" + camelCaseColumnNames[i] + " = int.Parse(a_data[(int)CSVColumns." + columnNames[i] + "]);"; break;
				}
			}

			fileText += @"
		}
	}

	public static " + scriptNameSingular + @"[] stats;
	public static Dictionary<" + indexType + ", " + scriptNameSingular + @"> statsById;

	public static void ReadFromString(string a_string)
	{
		List<" + scriptNameSingular + "> statsList = new List<" + scriptNameSingular + @">();
		statsById = new Dictionary<" + indexType + ", " + scriptNameSingular + @">();

		string csv = a_string.Replace(" + "\"\\r\\n\", \"\\n\");" + @"
		string[] lines = csv.Split('\n');
		for (int rowNo = 1; rowNo < lines.Length; ++rowNo)
		{
			string line = lines[rowNo];
			if (!string.IsNullOrEmpty(line.Trim()))
			{
				string[] data = line.Split('" + fieldDelimeter + @"');
				" + scriptNameSingular + " stat = new " + scriptNameSingular + @"(data);
				statsList.Add(stat);
				statsById.Add(stat." + camelCaseColumnNames[0] + @", stat);
			}
		}

		stats = statsList.ToArray();
	}
}";

			byte[] info = new UTF8Encoding(true).GetBytes(fileText);
			fs.Write(info, 0, info.Length);
		}

		AssetDatabase.Refresh();
	}

	List<string> Save()
	{
		string localPath = AssetDatabase.GetAssetPath(asset);
		localPath = localPath.Remove(0, "Assets".Length);  //Remove assets because it's included in both paths.

		string fullPath = Application.dataPath + localPath;

		List<string> fullLines = new List<string>();
		for (int i = 0; i < csvFields.Count; ++i)
		{
			string fullLine = "";
			for (int j = 0; j < csvFields[i].Count; ++j)
			{
				fullLine += csvFields[i][j];
				if (j < csvFields[i].Count - 1)
					fullLine += fieldDelimeter;
			}
			fullLines.Add(fullLine);
		}

		for (int i = fullLines.Count - 1; i > 0; --i)
		{
			if (fullLines[i] == "")
				fullLines.RemoveAt(i);
		}

		File.WriteAllLines(fullPath, fullLines.ToArray());

		SaveWidths();

		AssetDatabase.Refresh();

		return fullLines;
	}

	static void SaveWidths()
	{
		string localPath = AssetDatabase.GetAssetPath(asset);
		AssetImporter importer = AssetImporter.GetAtPath(localPath);

		string[] stringWidths = new string[columnWidths.Count];
		for (int i = 0; i < stringWidths.Length; ++i)
			stringWidths[i] = columnWidths[i].ToString();

		importer.userData = string.Join(",", stringWidths);

		EditorUtility.SetDirty(asset);
		AssetDatabase.SaveAssets();
	}

	static void LoadWidths()
	{
		string localPath = AssetDatabase.GetAssetPath(asset);
		AssetImporter importer = AssetImporter.GetAtPath(localPath);

		string[] stringWidths = importer.userData.Split(new char[1] { ',' }, System.StringSplitOptions.RemoveEmptyEntries);
		for (int i = 0; i < stringWidths.Length; ++i)
		{
			if (i >= columnWidths.Count)
				break;

			columnWidths[i] = float.Parse(stringWidths[i]);
		}
	}

	void Copy()
	{
		if (selectedFields.Count <= 0)
			return;

		FieldIndex topLeftField = GetTopLeftSelectedField();

		// We've got the point that copy buffer is going to be relative to now, 
		// so populate the buffer.
		copyBuffer.Clear();

		for (int i = 0; i < selectedFields.Count; ++i)
		{
			copyBuffer.Add(new FieldIndex(selectedFields[i].x - topLeftField.x, selectedFields[i].y - topLeftField.y), 
				csvFields[selectedFields[i].x][selectedFields[i].y]);
		}
	}

	void Paste()
	{
		if (selectedFields.Count <= 0)
			return;

		FieldIndex topLeftField = GetTopLeftSelectedField();

		foreach (KeyValuePair<FieldIndex, string> copy in copyBuffer)
			csvFields[topLeftField.x + copy.Key.x][topLeftField.y + copy.Key.y] = copy.Value;
	}

	FieldIndex GetTopLeftSelectedField()
	{
		// First, find all of the left-most fields.
		List<FieldIndex> leftMostFields = new List<FieldIndex>();
		leftMostFields.Add(selectedFields[0]);
		for (int i = 1; i < selectedFields.Count; ++i)
		{
			if (selectedFields[i].y < leftMostFields[0].y)
			{
				leftMostFields.Clear();
				leftMostFields.Add(selectedFields[i]);
			}
			else if (selectedFields[i].y == leftMostFields[0].y)
				leftMostFields.Add(selectedFields[i]);
		}

		// Then, get the top-most field from the left fields.
		FieldIndex topLeftField = leftMostFields[0];
		for (int i = 1; i < leftMostFields.Count; ++i)
		{
			if (leftMostFields[i].x < topLeftField.x)
				topLeftField = leftMostFields[i];
		}

		return topLeftField;
	}
}