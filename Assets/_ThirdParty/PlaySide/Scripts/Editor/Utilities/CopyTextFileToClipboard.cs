using UnityEngine;
using UnityEditor;

public class CopyTextFileContentsToClipboard
{
	[MenuItem("Assets/Copy Text File Contents To Clipboard")]
	public static void CopyToClipboard()
	{
		EditorGUIUtility.systemCopyBuffer = (Selection.activeObject as TextAsset).text;
	}

	[MenuItem("Assets/Copy Text File Contents To Clipboard", true)]
	static bool CopyToClipboardValidation()
	{
		return Selection.activeObject is TextAsset;
	}
}
