﻿#if !FINAL && UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class FindPrefabWithOtherPrefab : EditorWindow
{
	[MenuItem("PlaySide/Find Prefabs With Other Prefab In Them")]
	public static void Init()
	{
		// Get existing open window or if none, make a new one:
		FindPrefabWithOtherPrefab window = (FindPrefabWithOtherPrefab)EditorWindow.GetWindow(typeof(FindPrefabWithOtherPrefab));
		window.Show();
		Clear();
	}

	static Object findPrefab;
	string folder = "Assets/Prefabs";

	static List<string> foundPaths = new List<string>();

	Vector2 scrollView;

	public static void Clear()
	{
		findPrefab = null;
		foundPaths.Clear();
	}

	private void OnGUI()
	{
		GUILayout.Label("Enter the prefab you would like to find: ");

		findPrefab = EditorGUILayout.ObjectField(findPrefab, typeof(GameObject), false);
		folder = EditorGUILayout.TextField("Folder", folder);

		string[] guids = AssetDatabase.FindAssets("t:Object", new string[1] { folder });

		if (GUILayout.Button("Search"))
		{
			
			for (int i = 0; i < guids.Length; ++i)
			{
				string path = AssetDatabase.GUIDToAssetPath(guids[i]);
				GameObject obj = AssetDatabase.LoadAssetAtPath<GameObject>(path);
				if (!obj)
					continue;

				if (PSUtils.Transforms.FindChildRecursive(obj.transform,
					(a) =>
					{
						if (!PrefabUtility.IsAnyPrefabInstanceRoot(a.gameObject))
							return false;

						// Comparing prefabs doesn't seem to work with the new nested prefab system.
						// So compare names and names of children to see if this is using the object.
						if (a.name != findPrefab.name)
							return false;

						GameObject findGO = (GameObject)findPrefab;
						if (a.transform.childCount != findGO.transform.childCount)
							return false;

						for (int j = 0; j < a.transform.childCount; ++j)
						{
							if (a.transform.GetChild(j).name != findGO.transform.GetChild(j).name)
								return false;
						}

						return true;
					}) != null)
					foundPaths.Add(path);
			}
		}

		scrollView = EditorGUILayout.BeginScrollView(scrollView);
		for (int i = 0; i < foundPaths.Count; ++i)
		{
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField(foundPaths[i]);
			if (GUILayout.Button("Select"))
				Selection.activeObject = AssetDatabase.LoadAssetAtPath<GameObject>(foundPaths[i]);
			EditorGUILayout.EndHorizontal();
		}
		EditorGUILayout.EndScrollView();
	}
}
#endif