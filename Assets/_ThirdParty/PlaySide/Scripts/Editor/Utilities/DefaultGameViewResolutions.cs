﻿// Pulled from https://answers.unity.com/questions/956123/add-and-select-game-view-resolution.html

using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class DefaultGameViewResolutions
{
	static object gameViewSizesInstance;
	static MethodInfo getGroup;

	static DefaultGameViewResolutions()
	{
		var sizesType = typeof(Editor).Assembly.GetType("UnityEditor.GameViewSizes");
		var singleType = typeof(ScriptableSingleton<>).MakeGenericType(sizesType);
		var instanceProp = singleType.GetProperty("instance");
		getGroup = sizesType.GetMethod("GetGroup");
		gameViewSizesInstance = instanceProp.GetValue(null, null);
	}

	struct ResolutionPreset
	{
		public string resName;
		public int sizeX;
		public int sizeY;
		public ResolutionPreset(string _resName, int _sizeX, int _sizeY)
		{
			resName = _resName;
			sizeX = _sizeX;
			sizeY = _sizeY;
		}
	}

	// Put all preset resoutions here
	static ResolutionPreset[] resolutionPresets = new ResolutionPreset[]
	{
		new ResolutionPreset("Landscape 1080p", 1920, 1080),
		new ResolutionPreset("Landscape iPad", 2048, 1536),
		new ResolutionPreset("Landscape iPad Pro", 2732, 2048),
		new ResolutionPreset("Landscape iPhone X", 2436, 1125),

		new ResolutionPreset("Portait 1080p", 1080, 1920),
		new ResolutionPreset("Portait iPad", 1536, 2048),
		new ResolutionPreset("Portait iPad Pro", 2048, 2732),
		new ResolutionPreset("Portait iPhone X", 1125, 2436),
	};


	static GameViewSizeGroupType[] sizeGroupTypes = new GameViewSizeGroupType[]
	{
		GameViewSizeGroupType.Standalone,
		GameViewSizeGroupType.iOS,
		GameViewSizeGroupType.Android
	};

	public enum GameViewSizeType
	{
		AspectRatio, FixedResolution
	}

	[MenuItem("PlaySide/Setup/Add Default Resolutions")]
	public static void AddDefaults()
	{
		// Add default resolutions to all standard platforms
		// TODO: Add separate set of presets for VR/console
		foreach (GameViewSizeGroupType grp in sizeGroupTypes)
		{
			foreach (ResolutionPreset rp in resolutionPresets)
			{
				if (FindSize(grp, rp.sizeX, rp.sizeY)<0)
					AddCustomSize(GameViewSizeType.FixedResolution, grp, rp.sizeX, rp.sizeY, rp.resName);
			}
		}
	}

	// This doesn't work
	/*[MenuItem("PlaySide/Setup/CLEAR All Resolutions")]
	public static void ClearAllResolutions()
	{
		// Add default resolutions to all standard platforms
		// TODO: Add separate set of presets for VR/console
		foreach (GameViewSizeGroupType grp in sizeGroupTypes)
		{
			foreach (ResolutionPreset rp in resolutionPresets)
			{
				//if (FindSize(grp, rp.sizeX, rp.sizeY) < 0)
				//{
					Debug.Log(grp.ToString() + " Deleting " + rp.resName);
					RemoveCustomSize(GameViewSizeType.FixedResolution, grp, rp.sizeX, rp.sizeY, rp.resName);
				//}
			}
		}
	}*/

	public static void SetSize(int index)
	{
		var gvWndType = typeof(Editor).Assembly.GetType("UnityEditor.GameView");
		var selectedSizeIndexProp = gvWndType.GetProperty("selectedSizeIndex",
			BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
		var gvWnd = EditorWindow.GetWindow(gvWndType);
		selectedSizeIndexProp.SetValue(gvWnd, index, null);
	}

	// Add Resoultion Preset
	public static void AddCustomSize(GameViewSizeType viewSizeType, GameViewSizeGroupType sizeGroupType, int width, int height, string text)
	{
		var group = GetGroup(sizeGroupType);
		var addCustomSize = getGroup.ReturnType.GetMethod("AddCustomSize"); // or group.GetType().
		var gvsType = typeof(Editor).Assembly.GetType("UnityEditor.GameViewSize");
		var ctor = gvsType.GetConstructor(new Type[] { typeof(int), typeof(int), typeof(int), typeof(string) });
		var newSize = ctor.Invoke(new object[] { (int)viewSizeType, width, height, text });
		addCustomSize.Invoke(group, new object[] { newSize });
	}

	// Remove Resoultion Preset
	public static void RemoveCustomSize(GameViewSizeType viewSizeType, GameViewSizeGroupType sizeGroupType, int width, int height, string text)
	{
		var group = GetGroup(sizeGroupType);
		var removeCustomSize = getGroup.ReturnType.GetMethod("RemoveCustomSize"); // or group.GetType().
		var gvsType = typeof(Editor).Assembly.GetType("UnityEditor.GameViewSize");
		var ctor = gvsType.GetConstructor(new Type[] { typeof(int), typeof(int), typeof(int), typeof(string) });
		var newSize = ctor.Invoke(new object[] { (int)viewSizeType, width, height, text });
		removeCustomSize.Invoke(group, new object[] { newSize });
	}

	// Check for existing resolution preset by dimensions
	public static bool SizeExists(GameViewSizeGroupType sizeGroupType, int width, int height)
	{
		return FindSize(sizeGroupType, width, height) != -1;
	}


	// Check for existing resolution preset by dimensions
	public static int FindSize(GameViewSizeGroupType sizeGroupType, int width, int height)
	{
		var group = GetGroup(sizeGroupType);
		var groupType = group.GetType();
		var getBuiltinCount = groupType.GetMethod("GetBuiltinCount");
		var getCustomCount = groupType.GetMethod("GetCustomCount");
		int sizesCount = (int)getBuiltinCount.Invoke(group, null) + (int)getCustomCount.Invoke(group, null);
		var getGameViewSize = groupType.GetMethod("GetGameViewSize");
		var gvsType = getGameViewSize.ReturnType;
		var widthProp = gvsType.GetProperty("width");
		var heightProp = gvsType.GetProperty("height");
		var indexValue = new object[1];
		for(int i = 0; i < sizesCount; i++)
		{
			indexValue[0] = i;
			var size = getGameViewSize.Invoke(group, indexValue);
			int sizeWidth = (int)widthProp.GetValue(size, null);
			int sizeHeight = (int)heightProp.GetValue(size, null);
			if (sizeWidth == width && sizeHeight == height)
				return i;
		}
		return -1;
	}

	static object GetGroup(GameViewSizeGroupType type)
	{
		return getGroup.Invoke(gameViewSizesInstance, new object[] { (int)type });
	}
}
