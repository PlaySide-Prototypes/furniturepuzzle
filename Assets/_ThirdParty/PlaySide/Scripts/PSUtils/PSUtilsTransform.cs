using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace PSUtils
{
	public class Transforms
	{
		/// <summary> Parents transform, setting position/rotation to 0 and scale to 1 </summary>
		public static void ParentAndResetTransform(Transform _child, Transform _parent)
		{
			_child.SetParent(_parent);
			_child.localPosition = _child.localEulerAngles = Vector3.zero;
			_child.localScale = Vector3.one;
		}

		/// <summary> Destroys all the children under a parent transform </summary>		
		public static void RemoveAllChildren(Transform a_parent)
		{
			for (int i = a_parent.childCount - 1; i >= 0; --i)
			{
				GameObject.Destroy(a_parent.GetChild(i).gameObject);
			}
		}

		/// <summary> Recursively gets the full name in the hierarchy </summary>
		public static string GetFullName(Transform _transform)
		{
			Stack<string> names = new Stack<string>();

			while (_transform != null)
			{
				names.Push(_transform.name);
				_transform = _transform.parent;
			}

			StringBuilder sb = new StringBuilder();
			if (names.Count > 0)
				sb.Append(names.Pop());
			while (names.Count > 0)
				sb.Append("/").Append(names.Pop());

			return sb.ToString();
		}
		public static string GetFullName(GameObject _gameObj) { return GetFullName(_gameObj.transform); }
		public static string GetFullName(MonoBehaviour _behaviour) { return GetFullName(_behaviour.transform); }

		/// <summary> Sets a GameObject's layer, and recursively all transforms under it too </summary>
		public static void SetLayerRecursively(Transform _parent, int _layer)
		{
			_parent.gameObject.layer = _layer;

			Transform[] children = _parent.GetComponentsInChildren<Transform>(true);
			for (int i = 0; i < children.Length; ++i)
				children[i].gameObject.layer = _layer;
		}
		
		public static Transform FindChildRecursive(Transform a_parent, string a_name)
		{
			Queue<Transform> queue = new Queue<Transform>();
			queue.Enqueue(a_parent);
			while (queue.Count > 0)
			{
				var c = queue.Dequeue();
				if (c.name == a_name)
					return c;
				foreach (Transform t in c)
					queue.Enqueue(t);
			}
			return null;
		}

		public delegate bool ChildSearch(Transform a_child);
		public static Transform FindChildRecursive(Transform a_parent, ChildSearch a_searchQuery)
		{
			Queue<Transform> queue = new Queue<Transform>();
			queue.Enqueue(a_parent);
			while (queue.Count > 0)
			{
				var c = queue.Dequeue();
				if (a_searchQuery(c))
					return c;
				foreach (Transform t in c)
					queue.Enqueue(t);
			}
			return null;
		}
	}
}
