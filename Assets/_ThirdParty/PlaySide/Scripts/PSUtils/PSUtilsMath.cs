using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Globalization;

#if UNITY_EDITOR
using System.Reflection;
using UnityEditor;
using System.Linq;
#endif

namespace PSUtils
{
	public class Math
	{
		// Higher smoothing value = arrives at b faster
		public static float ExponentialDecayLerp(float a, float b, float deltaTime, float smoothing)
		{
			return  Mathf.Lerp(a, b, 1.0f - Mathf.Pow(1.0f - smoothing, deltaTime));
		}

		// Higher smoothing value = arrives at b faster
		public static Vector2 ExponentialDecayLerp(Vector2 a, Vector2 b, float deltaTime, float smoothing)
		{
			return Vector2.Lerp(a, b, 1.0f - Mathf.Pow(1.0f - smoothing, deltaTime));
		}

		public static Vector2 ExponentialDecaySlerp(Vector2 a, Vector2 b, float deltaTime, float smoothing)
		{
			return Slerp(a, b, 1.0f - Mathf.Pow(1.0f - smoothing, deltaTime));
		}

		// Higher smoothing value = arrives at b faster
		public static Vector3 ExponentialDecayLerp(Vector3 a, Vector3 b, float deltaTime, float smoothing)
		{
			return Vector3.Lerp(a, b, 1.0f - Mathf.Pow(1.0f - smoothing, deltaTime));
		}

		// Higher smoothing value = arrives at b faster
		public static Quaternion ExponentialDecayLerp(Quaternion a, Quaternion b, float deltaTime, float smoothing)
		{
			return Quaternion.Lerp(a, b, 1.0f - Mathf.Pow(1.0f - smoothing, deltaTime));
		}

		// Higher smoothing value = arrives at b faster
		public static float ExponentialDecayLerpAngle(float a, float b, float deltaTime, float smoothing)
		{
			return Mathf.LerpAngle(a, b, 1.0f - Mathf.Pow(1.0f - smoothing, deltaTime));
		}

		public static Vector2 Slerp(Vector2 start, Vector2 end, float percent)
		{
			// Dot product - the cosine of the angle between 2 vectors.
			float dot = Vector2.Dot(start, end);
			// Clamp it to be in the range of Acos()
			// This may be unnecessary, but floating point
			// precision can be a fickle mistress.
			Mathf.Clamp(dot, -1.0f, 1.0f);
			// Acos(dot) returns the angle between start and end,
			// And multiplying that by percent returns the angle between
			// start and the final result.
			float theta = Mathf.Acos(dot) * percent;
			Vector2 RelativeVec = end - start * dot;
			RelativeVec.Normalize();
			// Orthonormal basis
			// The final result.
			return ((start * Mathf.Cos(theta)) + (RelativeVec * Mathf.Sin(theta)));
		}

		public static float ScalarProjection(float vecX, float vecY, float dirX, float dirY, bool isDirNormalised)
		{
			if (!isDirNormalised)
			{
				float length = Mathf.Sqrt(dirX * dirX + dirY * dirY);
				dirX /= length;
				dirY /= length;
			}
			return vecX * dirX + vecY * dirY;
		}

		/// <summary>
		/// Way of doing a Vector2 Dot without having to actually have Vector2 objects
		/// like Vector2.Dot()
		/// </summary>
		/// <param name="vec1X"></param>
		/// <param name="vec1Y"></param>
		/// <param name="vec2X"></param>
		/// <param name="vec2Y"></param>
		/// <returns></returns>
		public static float Vector2Dot(float vec1X, float vec1Y, float vec2X, float vec2Y)
		{
			return vec1X * vec2X + vec1Y * vec2Y;
		}
		
		
		// <summary> Attenuation factor for how much an influence affects something based on radius and falloff. (Commonly using in lighting calculations) </summary>
		public static float Attenuation_RadiusFalloff(float _radius, float _falloff, Vector3 _fromPos, Vector3 _toPos)
		{
			float dist = Vector3.Distance(_fromPos, _toPos);
			float denom = dist / _radius + 1.0f;
			float attenuation = 1.0f / (denom * denom);
			float t = (attenuation - _falloff) / (1.0f - _falloff);
			return Mathf.Max(t, 0.0f);
		}

		// <summary> Attenuation factor for how much an influence affects something based on radius and falloff. (Commonly using in lighting calculations) </summary>
		public static float Attenuation_InnerOuterRadiusFalloff(float _outerRadius, float _innerRadius, float _falloff, Vector3 _fromPos, Vector3 _toPos)
		{
			float dist = Vector3.Distance(_fromPos, _toPos) - _innerRadius;

			float radius = _outerRadius - _innerRadius;
			float denom = dist / radius + 1.0f;
			float attenuation = 1.0f / (denom * denom);
			float t = (attenuation - _falloff) / (1.0f - _falloff);
			return Mathf.Max(t, 0.0f);
		}
		
	}
}
