using UnityEngine;

namespace PSUtils
{
	public static class Input
	{
		private static Vector3 lastTouchPos = Vector3.zero;

		/// <summary> Checks if the left mouse button or a single finger just touched down this frame </summary>
		/// <returns> True if touched down, null if not </returns>
		public static bool JustTouchedDown()
		{
#if UNITY_EDITOR || UNITY_STANDALONE
			return UnityEngine.Input.GetMouseButtonDown(0);
#else
			return ((UnityEngine.Input.touchCount == 1) && (UnityEngine.Input.touches[0].phase == TouchPhase.Began));
#endif
		}

		/// <summary> Checks if the left mouse button or a single finger is being held down </summary>
		/// <returns> True if still holding down, null if not </returns>
		public static bool IsTouching()
		{
#if UNITY_EDITOR || UNITY_STANDALONE
			return UnityEngine.Input.GetMouseButton(0);
#else
			return (UnityEngine.Input.touchCount == 1);
#endif
		}

		/// <summary> Checks if the left mouse button or a single finger was just released this frame </summary>
		/// <returns> True if it just released, false if holding or not touching </returns>
		public static bool JustTouchedUp()
		{
#if UNITY_EDITOR || UNITY_STANDALONE
			return UnityEngine.Input.GetMouseButtonUp(0);
#else
			return (UnityEngine.Input.touchCount == 0);
#endif
		}

		/// <summary> Gets the current touch position </summary>
		/// <param name="_screenPos">Returns the screen touching position (Last touch position if not touching) </param>
		/// <returns> False if not touching </returns>
		public static bool GetTouchPosition(out Vector3 _outPosition)
		{
			if (IsTouching())
			{
#if UNITY_EDITOR || UNITY_STANDALONE
				_outPosition = UnityEngine.Input.mousePosition;
#else
				_outPosition = UnityEngine.Input.touches[0].position;
#endif
				lastTouchPos = _outPosition;
				return true;
			}
			else
			{
				_outPosition = lastTouchPos;
				return false;
			}
		}

		/// <summary> Gets the number of fingers currently down </summary>
		/// <returns> Touch count (0 or 1 for mouse input) </returns>
		public static int GetTouchCount()
		{
#if UNITY_EDITOR || UNITY_STANDALONE
			return UnityEngine.Input.GetMouseButton(0) ? 1 : 0;
#else
			return UnityEngine.Input.touchCount;
#endif
		}
	}
}
