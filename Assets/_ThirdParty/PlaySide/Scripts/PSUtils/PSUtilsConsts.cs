using UnityEngine;

namespace PSUtils
{
	public class Consts
	{
		// Because using things like Vector3.zero create a new one under-the-hood every time
		public static readonly Vector3 Vec3Zero = Vector3.zero;
		public static readonly Vector3 Vec3One = Vector3.one;
		public static readonly Vector3 Vec3Up = Vector3.up;
		public static readonly Vector3 Vec3Down = Vector3.down;
		public static readonly Vector3 Vec3Forwards = Vector3.forward;
		public static readonly Vector3 Vec3Backwards = Vector3.back;
		public static readonly Vector3 Vec3Right = Vector3.right;

		public static readonly Vector2 Vec2Zero = Vector2.zero;
		public static readonly Vector2 Vec2Right = Vector2.right;
		public static readonly Vector2 Vec2Left = Vector2.left;
		public static readonly Vector2 Vec2Up = Vector2.up;
		public static readonly Vector2 Vec2Down = Vector2.down;

		public static readonly Quaternion identity = Quaternion.identity;
		public static readonly Quaternion backwardsRot = Quaternion.LookRotation(Vec3Backwards);

		public static readonly WaitForSeconds oneSecond = new WaitForSeconds(1.0f);
		public static readonly WaitForSeconds fiveSeconds = new WaitForSeconds(5.0f);

		public const int magicGCCallsAmount = 8;
	}
}
