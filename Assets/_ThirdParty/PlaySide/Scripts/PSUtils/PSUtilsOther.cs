using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Globalization;

#if UNITY_EDITOR
using System.Reflection;
using UnityEditor;
using System.Linq;
#endif

namespace PSUtils
{
	public class Other
	{
		public static void ShuffleArray<T>(ref T[] a_array)
		{
			// Knuth shuffle algorithm :: courtesy of Wikipedia :)
			for (int t = 0; t < a_array.Length; t++)
			{
				T tmp = a_array[t];
				int r = UnityEngine.Random.Range(t, a_array.Length);
				a_array[t] = a_array[r];
				a_array[r] = tmp;
			}
		}


		public static bool ArrayContains<T>(T[] a_array, T a_element)
		{
			for (int i = 0; i < a_array.Length; ++i)
			{
				if (EqualityComparer<T>.Default.Equals(a_array[i], a_element))
					return true;
			}
			return false;
		}

		public static T[] ArrayCast<T, U>(U[] a_array) where U : IConvertible
		{
			T[] outArray = new T[a_array.Length];
			for (int i = 0; i < a_array.Length; ++i)
			{
				outArray[i] = (T)Convert.ChangeType(a_array[i], typeof(T));
			}
			return outArray;
		}

		public static bool ArrayContains<T>(T[] a_array, T a_element, out int a_index)
		{
			for (int i = 0; i < a_array.Length; ++i)
			{
				if (EqualityComparer<T>.Default.Equals(a_array[i], a_element))
				{
					a_index = i;
					return true;
				}
			}
			a_index = -1;
			return false;
		}

		public static bool ArrayValueComparison<T>(T[] a_array, T[] a_otherArray) where T : IComparable<T>
		{
			if (a_array.Length != a_otherArray.Length)
				return false;

			for (int i = 0; i < a_array.Length; ++i)
			{
				if (!EqualityComparer<T>.Default.Equals(a_array[i], a_otherArray[i]))
					return false;
			}

			return true;
		}

		public static void SetLayerRecursively(GameObject a_gameObj, int a_layer, bool a_includeSelf = true)
		{
			if (a_includeSelf)
				a_gameObj.layer = a_layer;

			Transform[] children = a_gameObj.GetComponentsInChildren<Transform>(true);

			for (int i = 0; i < children.Length; ++i)
				children[i].gameObject.layer = a_layer;
		}

		public static void SetLayerRecursively(GameObject a_gameObj, int a_layer, bool a_includeSelf = true, params GameObject[] a_ignoredItems)
		{
			if (a_includeSelf)
				a_gameObj.layer = a_layer;

			Transform[] children = a_gameObj.GetComponentsInChildren<Transform>(true);

			for (int i = 0; i < children.Length; ++i)
			{
				GameObject child = children[i].gameObject;
				if (ArrayContains(a_ignoredItems, child) == false)
				{
					child.layer = a_layer;
				}
			}
		}
		
		
	#if UNITY_EDITOR
		public static T GetActualObjectForSerializedProperty<T>(FieldInfo fieldInfo, SerializedProperty property) where T : class
		{
			var obj = fieldInfo.GetValue(property.serializedObject.targetObject);
			if (obj == null) { return null; }

			T actualObject = null;
			if (obj.GetType().IsArray)
			{
				var index = Convert.ToInt32(new string(property.propertyPath.Where(c => char.IsDigit(c)).ToArray()));
				actualObject = ((T[])obj)[index];
			}
			else
			{
				actualObject = obj as T;
			}
			return actualObject;
		}
	#endif

		public static Dictionary<T, U> MergeDictionaries<T, U>(Dictionary<T, U> a_older, Dictionary<T, U> a_newer) where T : System.IComparable<T>
		{
			foreach (KeyValuePair<T, U> pair in a_older)
			{
				bool newerHasKey = false;
				foreach (T key in a_newer.Keys)
				{
					if (EqualityComparer<T>.Default.Equals(key, pair.Key))
					{
						newerHasKey = true;
						break;
					}
				}
				if (!newerHasKey)
					a_newer.Add(pair.Key, pair.Value);
			}

			return a_newer;
		}

		/// <summary>
		/// Unloads unused assets, and GC collects enough times to
		/// *really* clean up garbage.
		/// </summary>
		public static void GCClear()
		{
			Resources.UnloadUnusedAssets();
			for (int i = 0; i < PSUtils.Consts.magicGCCallsAmount; ++i)
				GC.Collect();
		}

		public static string TimeToDynamicString(TimeSpan _timeSpan)
		{
			if(_timeSpan.Hours > 0)
			{
				return _timeSpan.ToString("hh\\h\\ mm\\m");
			}
			else if(_timeSpan.Minutes > 0)
			{
				return _timeSpan.ToString("mm\\m\\ ss\\s");
			}

			return _timeSpan.ToString("ss\\s");
		}

		public static string TimeToString(DateTime input, string _format = "yyyy-MM-dd-HH-mm-ss")
		{
			return input.ToString(_format);
		}

		public static DateTime StringToTime(string input)
		{
			if (input == "")
				return DateTime.Now;

			return DateTime.ParseExact(input, "yyyy-MM-dd-HH-mm-ss", CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// Returns a dictionary of how many of each array element are present.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="_array"></param>
		/// <param name="_outValues"></param>
		public static void CountArrayFrequencies<T>(T[] _array, ref Dictionary<T, int> _outValues)
		{
			T prev = default(T);

			System.Array.Sort(_array);
			for (var i = 0; i < _array.Length; i++)
			{
				if (!EqualityComparer<T>.Default.Equals(_array[i], prev))
				{
					_outValues.Add(_array[i], 1);
				}
				else
				{
					_outValues[_array[i]] += 1;
				}
				prev = _array[i];
			}
		}
	
	}
}
