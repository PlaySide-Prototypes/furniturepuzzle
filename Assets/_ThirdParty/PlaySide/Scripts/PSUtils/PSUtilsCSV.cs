using System.Collections.Generic;
using UnityEngine;

namespace PSUtils
{
	public class CSV
	{
		/// <summary> Returns a List of string arrays to be used when populating data </summary>
		/// <param name="_csv"> CSV file </param>
		public static List<string[]> PopulateFromCSV(TextAsset _csv, bool isTabs = true)
		{
			List<string[]> populatedList = new List<string[]>();

			string[] lines = _csv.text.Split('\n');

			for (int i = 1; i < lines.Length; i++)
			{
				if (string.IsNullOrEmpty(lines[i]))
					continue;

				string[] s_data = lines[i].TrimEnd('\r').Split(isTabs ? '\t' : ',');
				populatedList.Add(s_data);
			}

			if (populatedList.Count > 0)
				return populatedList;
			else
			{
				Debug.LogError("There was no data in this CSV: " + _csv.name);
				return null;
			}
		}
	}
}
