﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PS_ParentConstraint : MonoBehaviour {

	public bool isEnabled = true;
	[RangeAttribute(0, 1)]
	public float weight;
	public Transform targetParent;
	[HideInInspector] public Transform parent;
	[HideInInspector] private Transform t;
	private Vector3 targetPos;
	private Vector3 targetRot;


	void Update ()
	{
		UpdateConstraint();
	}

	public void UpdateConstraint()
	{
		if(!isEnabled)
			return;

		// Ensure transforms are cached
		if(!t)
			t = transform;
		if(!parent)
			parent = transform.parent;

		// Weight towards tagert parent
		if(targetParent && parent && t)
		{
			targetPos = Vector3.Lerp(parent.position, targetParent.position, weight);
			targetRot = Vector3.Lerp(parent.eulerAngles, targetParent.eulerAngles, weight);
			t.position = targetPos;
			t.eulerAngles = targetRot;
		}
	}
}
