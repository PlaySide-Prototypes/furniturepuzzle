using UnityEngine;
using System.Collections;

namespace Playside
{
    public abstract class UILocLabelBase : MonoBehaviour
    {
        [HideInInspector, SerializeField]
        public bool isTranslated = true;

        [HideInInspector, SerializeField]
        public int csvLineNo = 1;

        public string[] substituteStrings;

        public virtual Color TextColour { get; set; }

        protected abstract bool IsTextInitialised { get; }
        protected abstract void Init();
        protected abstract void SetTextContent(string _text, bool _isRightToLeftLanguage);
        protected virtual string DefaultText { get { return "MISSING!"; } }

        private string directText = "";
        private bool isSetDirectly = false;

        /// <summary> Called when object/script activates </summary>
        void Awake()
        {
            if (!IsTextInitialised)
                Init();
        }

        /// <summary> Called before first Update() </summary>
        void Start()
        {
            Refresh();
        }

        private void OnEnable()
        {
            Refresh();
        }

        /// <summary> Sets the localised text directly </summary>
        public void SetTextDirectly(string _text)
        {
            isSetDirectly = true;
            directText = _text;

            if (IsTextInitialised)
            {
                SetTextContent(_text, false);
            }
        }

        public virtual void Refresh()
        {
            if (isSetDirectly)
                return;

            if (!isTranslated)
                return;

            string text;

            if (csvLineNo == 0)
            {
                // LocID is None
                if (IsTextInitialised)
                    SetTextContent(string.Empty, false);

                return;
            }

            if (Playside.LocManager.Instance.IsValidCSVLineNo(csvLineNo) == false)
            {
                Playside.LocalisationOverrides.Debug.LogError(name + ": You are trying to use CSV Line No " + csvLineNo + ". However, this Line No does not exist", this);
            }
            else if (Playside.LocManager.Instance.Get(csvLineNo, out text))
            {
                if (string.IsNullOrEmpty(text))
                    text = DefaultText;

                if (!IsTextInitialised)
                    Init();

                // Extra support font?
                Playside.FontOverrideScriptable fontOverride = Playside.LocManager.GetOverrideFont();
                OverrideFont(fontOverride);

                // Apply substrings
                if (substituteStrings != null)
                {
                    for (int i = 0; i < substituteStrings.Length; i++)
                        text = text.Replace("[" + i + "]", substituteStrings[i]);
                }

                bool isRightToLeft = Playside.LocManager.Instance.IsLanguageRightToLeft();
                SetTextContent(text, isRightToLeft);
            }
            else
            {
                Playside.LocalisationOverrides.Debug.LogError("Invalid loc text on '" + gameObject.name + " CSV Line No '" + csvLineNo + "')", this);
            }
        }

        protected virtual void OverrideFont(Playside.FontOverrideScriptable _fontOverride)
        {
        }

        // Feed in a null value to disable subsitute strings
        public void SetSubstituteStrings(string[] _subsituteStrings)
        {
            substituteStrings = _subsituteStrings;
            Refresh();
        }

        public void SetSubstituteStrings(params object[] _substrings)
        {
            substituteStrings = ConvertParamsToSubstrings(_substrings);
            Refresh();
        }

        public void Set(int _csvLineNo)
        {
            isSetDirectly = false;
            csvLineNo = _csvLineNo;
            Refresh();
        }

        public void Set(int _csvLineNo, string[] _substrings)
        {
            isSetDirectly = false;
            csvLineNo = _csvLineNo;
            substituteStrings = _substrings;
            Refresh();
        }

        public void Set(int _csvLineNo, params object[] _substrings)
        {
            isSetDirectly = false;
            csvLineNo = _csvLineNo;
            substituteStrings = ConvertParamsToSubstrings(_substrings);
            Refresh();
        }

        public void Set(LocIDs _csvLineNo)
        {
            SetCSVLineNo((int)_csvLineNo);
        }

        public void Set(LocIDs _csvLineNo, string[] _substrings)
        {
            SetCSVLineNo((int)_csvLineNo, _substrings);
        }

        public void Set(LocIDs _csvLineNo, params object[] _substrings)
        {
            SetCSVLineNo((int)_csvLineNo, ConvertParamsToSubstrings(_substrings));
        }

        public void SetCSVLineNo(int _csvLineNo)
        {
            isSetDirectly = false;
            csvLineNo = _csvLineNo;
            Refresh();
        }

        public void SetCSVLineNo(int _csvLineNo, string[] _substrings)
        {
            isSetDirectly = false;
            csvLineNo = _csvLineNo;
            substituteStrings = _substrings;
            Refresh();
        }

        public void SetCSVLineNo(int _csvLineNo, params object[] _substrings)
        {
            isSetDirectly = false;
            csvLineNo = _csvLineNo;
            substituteStrings = ConvertParamsToSubstrings(_substrings);
            Refresh();
        }

        public void SetCSVLineNo(LocIDs _csvLineNo)
        {
            SetCSVLineNo((int)_csvLineNo);
        }

        public void SetCSVLineNo(LocIDs _csvLineNo, string[] _substrings)
        {
            SetCSVLineNo((int)_csvLineNo, _substrings);
        }

        public void SetCSVLineNo(LocIDs _csvLineNo, params object[] _substrings)
        {
            SetCSVLineNo((int)_csvLineNo, ConvertParamsToSubstrings(_substrings));
        }

        private string[] ConvertParamsToSubstrings(object[] _params)
        {
            if (_params == null || _params.Length == 0)
            {
                return null;
            }

            int substringsCount = _params.Length;
            string[] subStrings = new string[substringsCount];
            for (int i = 0; i < substringsCount; ++i)
            {
                if (_params[i] == null)
                    continue;

                try
                {
                    subStrings[i] = _params[i].ToString();
                }
                catch (System.Exception)
                {
                    subStrings[i] = "Error";
                }
            }

            return subStrings;
        }
    }
}
