﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Playside
{
    [CustomEditor(typeof(Playside.LocManager))]
    public class LocManagerEditor : Editor
    {
        int langaugeCount;

        bool[] foldouts;

        /// <summary>
        /// 
        /// </summary>
        void OnEnable()
        {
            langaugeCount = System.Enum.GetValues(typeof(LocLanguages)).Length;
            //LocManager locManager = (LocManager)target;
            //locManager.UpdateOrGenerateOverrides();

            foldouts = new bool[langaugeCount];
            for (int i = 0; i < foldouts.Length; i++)
                foldouts[i] = false;
        }

        /// <summary>
        /// 
        /// </summary>
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            /*
                    EditorGUILayout.LabelField("Font Overrides");
                    EditorGUI.indentLevel++;
                    LocManager locManager = (LocManager)target;
                    for (int i=0; i< locManager.fontOverrides.Length; i++)
                    {
                        foldouts[i] = EditorGUILayout.Foldout(foldouts[i], locManager.fontOverrides[i].language.ToString());

                        if (foldouts[i])
                        {
                            EditorGUI.indentLevel++;
                            locManager.fontOverrides[i].font = (Font)EditorGUILayout.ObjectField("Font", locManager.fontOverrides[i].font, typeof(Font), false);
                            locManager.fontOverrides[i].fontSizePercentage = EditorGUILayout.FloatField("Font Size Override %", locManager.fontOverrides[i].fontSizePercentage);
                            locManager.fontOverrides[i].tmpFont = (TMPro.TMP_FontAsset)EditorGUILayout.ObjectField("TMPFont", locManager.fontOverrides[i].tmpFont, typeof(TMPro.TMP_FontAsset), false);
                            locManager.fontOverrides[i].tmpFontSizePercentage = EditorGUILayout.FloatField("TMPFont Size Override %", locManager.fontOverrides[i].tmpFontSizePercentage);
                            EditorGUI.indentLevel--;
                        }
                    }
                    EditorGUI.indentLevel--;
            */
        }
    }
}