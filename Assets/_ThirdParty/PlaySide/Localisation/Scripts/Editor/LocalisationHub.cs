﻿//#if UNITY_EDITOR

using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using System.Text.RegularExpressions;

#pragma warning disable 0618

namespace Playside
{
    public class LocalisationHub : EditorWindow
    {
        const string toolVersion = "1.0";

        // Editor Window
        static EditorWindow thisWindow;
        static Vector2 minWindowSize = new Vector2(1205f, 320f);

        public struct LocalisableText
        {
            public int index;
            public Text label;
            public TextMeshProUGUI labelMesh;
            public string text;
            public Playside.UILocLabelText locTextLabel;
            public Playside.UILocLabelTextMesh locTextLabelMesh;
            public string relativePath;
            public bool storedOnDisk;
            public bool isPrefab;
            public bool isModified;
        }

        static bool isPro = false;

        // Localisation
        static Playside.LocManager.LanguageStrings[] allText = null;
        static LocLanguages previewLanguage;
        static int previewLanguageSliderValue;
        static int locLanguagesNum;

        // GUI
        static Vector2 windowDimensions;
        static Vector2 textListScrollPos;
        static Vector2 texUnlocedtListScrollPos;
        static Vector2 locIDstListScrollPos;
        static Vector2 LocCSVListScrollPos;
        static LocalisableText[] locableTexts;
        static List<LocalisableText> untranslatedLocableTexts;
        static string csvSearchText;

        // Layout
        static Rect leftColumnRect;
        static Rect leftColumnTopRect;
        static Rect localisedHeaderRect;
        static Rect unlocalisedHeaderRect;
        // Colors
        static Texture2D bgTexture;
        static Texture2D csvIcon;
        static Color whiteColor = Color.white;
        static Color redColor = new Color(1f, 0.7f, 0.7f, 1f);
        static Color lightBlueColor = new Color(0.85f, 0.85f, 1f, 1f);
        static Color greenColor = new Color(.5f, 1f, 0.8f, 1f);
        static Color yellowColor = new Color(1f, 0.85f, 0.3f, 1f);

        // Pro Colors
        //Color proBGColor = 				new Color(.22f, .22f, .22f, 1f);
        //Color proTableBGColor = 			new Color(.3f, .3f, .3f, 1f);
        static Color proTableBG2Color = new Color(.2f, .2f, .2f, 1f);
        static Color proTableBG3Color = new Color(.15f, .15f, .15f, 1f);
        static Color proLocHeaderColor = new Color(0.16f, 0.31f, 0.25f, 1f);
        //Color proRedColor = 				new Color(0.45f, 0.05f, 0.05f, 1f);

        // Personal Colors
        //Color freeBGColor = 				new Color(.76f, .76f, .76f, 1f);
        //Color freeTableBGColor = 			new Color(.6f, .6f, .6f, 1f);
        static Color freeTableBG2Color = new Color(.7f, .7f, .7f, 1f);
        static Color freeTableBG3Color = new Color(.6f, .6f, .6f, 1f);
        static Color freeLocHeaderColor = new Color(0.36f, 0.51f, 0.45f, 1f);
        //Color freeRedColor = 				new Color(0.5f, 0.33f, 0.33f, 1f);


        /// <summary> Compares the version in the class to the bundle identifier in the Project Settings, and regenerates the class if necessary </summary>
        [MenuItem("PlaySide/Localisation.../Open Localisation Hub")]
        public static void OpenLocalizationHub()
        {
            if (thisWindow == null)
                thisWindow = GetWindow(typeof(LocalisationHub), true, "Localisation Hub " + toolVersion);
            thisWindow.minSize = minWindowSize;
            windowDimensions = new Vector2(thisWindow.position.width, thisWindow.position.height);
            RefreshTextLabelArray(true);
        }
        
        public static void GenerateLocIDs()
        {
            if (EditorUtility.DisplayDialog("Generate Localisation ID file?",
                   "Are you sure you generate a new Localisation ID file, doing so will result in a script recompile", "Generate", "Cancel"))
            {
                Playside.GenerateLocalisationIDs.CreateLocalisationIDsClassFile();
                AssetDatabase.Refresh();
            }
        }

        void OnEnable()
        {
            locLanguagesNum = ((LocLanguages[])System.Enum.GetValues(typeof(LocLanguages))).Length;

            RefreshTextLabelArray(true);

            csvSearchText = "";

            isPro = EditorGUIUtility.isProSkin;//Application.HasProLicense();
            bgTexture = new Texture2D(1, 1);
            bgTexture.SetPixel(0, 0, whiteColor);
            bgTexture.Apply();
            csvIcon = (Texture2D)AssetDatabase.LoadAssetAtPath(Playside.LocalisationOverrides.Configuration.LocCSVIconTextureFilePath, typeof(Texture2D));
        }


        void OnFocus()
        {
            RefreshTextLabelArray(true);
        }


        void OnGUI()
        {
            if (thisWindow == null)
                thisWindow = GetWindow(typeof(LocalisationHub), true, "Localisation Hub " + toolVersion);

            windowDimensions = new Vector2(thisWindow.position.width, thisWindow.position.height);
            GUI.skin.box.normal.background = bgTexture;

            // Localised Table Header
            localisedHeaderRect = new Rect(0, 0, windowDimensions.x, 22);
            GUI.color = isPro ? proLocHeaderColor : freeLocHeaderColor;
            GUI.Box(localisedHeaderRect, GUIContent.none);

            // Unlocalised Table Header
            unlocalisedHeaderRect = new Rect(0, windowDimensions.y * 0.5f - 13f, windowDimensions.x, 22);
            GUI.color = isPro ? proTableBG3Color : freeTableBG3Color;
            GUI.Box(unlocalisedHeaderRect, GUIContent.none);

            // Left Column Background
            leftColumnTopRect = new Rect(0, 0, 265, 170);
            GUI.color = isPro ? proTableBG2Color : freeTableBG2Color;
            GUI.Box(leftColumnRect, GUIContent.none);

            // LocIDS Background
            leftColumnRect = new Rect(0, 0, 265, windowDimensions.y);
            GUI.color = isPro ? proTableBG3Color : freeTableBG3Color;
            GUI.Box(leftColumnTopRect, GUIContent.none);
            GUI.color = whiteColor;

            // Main left/right split
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.Space();

                // Left column
                EditorGUILayout.BeginVertical(GUILayout.Width(250));
                {
                    EditorGUILayout.LabelField("", GUILayout.Height(10));

                    DrawLocIDs();
                    EditorGUILayout.LabelField("");

                    GUI.color = yellowColor;
                    if (GUILayout.Button("Auto-Assign Localised Strings"))
                        AutoAssignLocalisedStrings();

                    GUI.color = greenColor;
                    if (GUILayout.Button("Export Unlocalised Text To CSV"))
                        ExportCSV();

                    GUI.color = whiteColor;
                    EditorGUILayout.LabelField("");

                    DrawLanguages();

                    if (GUILayout.Button("Clipboard Preview Language Characters"))
                        ClipboardUniqueCharacters();

                    EditorGUILayout.LabelField("");

                    GUI.color = whiteColor;

                    if (GUILayout.Button("Select Localisation CSV"))
                        SelectProjectObject(Playside.LocalisationOverrides.Configuration.LocCSVFilePath);

                    DrawLocalisedCSV();
                }
                EditorGUILayout.EndVertical();
                EditorGUILayout.Space();

                // Right column, localisation text list etc
                EditorGUILayout.BeginVertical(GUILayout.Width(windowDimensions.x - 275f));
                {
                    DrawTextObjectList(true);
                    DrawTextObjectList(false);
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.LabelField("");
        }


        void DrawLanguages()
        {
            previewLanguage = (LocLanguages)EditorGUILayout.EnumPopup("Preview Language:", previewLanguage);
            previewLanguageSliderValue = (int)previewLanguage;
            previewLanguageSliderValue = EditorGUILayout.IntSlider(previewLanguageSliderValue, 0, locLanguagesNum - 1);
            previewLanguage = (LocLanguages)previewLanguageSliderValue;
        }


        void DrawLocIDs()
        {
            LocIDs[] locIDValues = (LocIDs[])System.Enum.GetValues(typeof(LocIDs));
            int locIDsCount = locIDValues.Length;

            EditorGUILayout.BeginVertical();
            {
                // Generate Loc ID button
                GUI.color = lightBlueColor;
                if (GUILayout.Button("Generate Localisation IDs"))
                {
                    GenerateLocIDs();
                }
                GUI.color = whiteColor;
                EditorGUILayout.LabelField("", GUILayout.Height(5));

                // Localisation ID scroll list
                EditorGUILayout.LabelField("LocIDs (" + locIDsCount + ")", EditorStyles.boldLabel);

                locIDstListScrollPos = EditorGUILayout.BeginScrollView(locIDstListScrollPos, false, false, GUILayout.Height(100f));
                {
                    // List LocIDs, ignore default None value
                    for (int i = 0; i < locIDsCount; i++)
                    {
                        LocIDs locId = locIDValues[i];
                        if ((int)locId > 0)
                        {
                            int csvLine = (int)locId;

                            // Show English text as hover tooltip
                            GUIContent idContent = new GUIContent(csvLine.ToString() + "  " + locId, GetText(LocLanguages.English, csvLine));
                            EditorGUILayout.LabelField(idContent, GUILayout.Width(175));
                        }
                    }

                    EditorGUILayout.LabelField("", GUILayout.Height(5));
                }
                EditorGUILayout.EndScrollView();
            }
            EditorGUILayout.EndVertical();
        }


        // Draw english contents of localised CSV
        void DrawLocalisedCSV()
        {
            EditorGUILayout.LabelField("CSV Translations (" + (allText.Length - 1).ToString() + ")", EditorStyles.boldLabel);

            // Searchbar
            GUILayout.BeginHorizontal();
            {
                csvSearchText = GUILayout.TextField(csvSearchText, GUI.skin.FindStyle("ToolbarSeachTextField"));
                if (GUILayout.Button("", GUI.skin.FindStyle("ToolbarSeachCancelButton")))
                {
                    // Remove focus if cleared
                    csvSearchText = "";
                    GUI.FocusControl(null);
                }
            }
            GUILayout.EndHorizontal();
            EditorGUILayout.LabelField("", GUILayout.Height(5));

            // Localised String list
            LocCSVListScrollPos = EditorGUILayout.BeginScrollView(LocCSVListScrollPos, false, false);
            {
                string currentString = "";

                // Ignore language title row
                for (int i = 1; i < allText.Length + 1; i++)
                {
                    currentString = GetText(LocLanguages.English, i);

                    // Just draw strings normally if no search term
                    if (string.IsNullOrEmpty(csvSearchText))
                        EditorGUILayout.LabelField(i.ToString() + "  " + currentString, GUILayout.Width(175));
                    else
                    {
                        // Skip any that don't match search string
                        if (!currentString.ToLower().Contains(csvSearchText.ToLower()))
                            continue;
                        EditorGUILayout.LabelField(i.ToString() + "  " + currentString, GUILayout.Width(175));
                    }
                }
            }
            EditorGUILayout.EndScrollView();
        }


        // Draw Localised Text objects table
        void DrawTextObjectList(bool _showLocalised)
        {
            if (locableTexts == null || locableTexts.Length <= 0)
                return;

            // Table heading
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField((_showLocalised ? "Localised" : "Unlocalised"), EditorStyles.boldLabel, GUILayout.Width(80));
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("", EditorStyles.boldLabel, GUILayout.Width(8));
                EditorGUILayout.LabelField("Object", EditorStyles.boldLabel, GUILayout.Width(175));
                EditorGUILayout.LabelField("Text", EditorStyles.boldLabel, GUILayout.Width(300));

                if (_showLocalised)
                {
                    GUILayout.Label(csvIcon, GUILayout.Width(20), GUILayout.Height(20));
                    EditorGUILayout.LabelField("Line", EditorStyles.boldLabel, GUILayout.Width(35));
                    EditorGUILayout.LabelField("Translation", EditorStyles.boldLabel, GUILayout.Width(250));
                }
                GUILayout.FlexibleSpace();
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.LabelField("", GUILayout.Height(5));


            float listHeight = (windowDimensions.y * 0.5f - 40f);

            // Scroll View, weird extra braces because BeginHor/BeginVer/BeginScroll make things unreadable
            if (_showLocalised)
                textListScrollPos = EditorGUILayout.BeginScrollView(textListScrollPos, false, false, GUILayout.Height(listHeight), GUILayout.MinHeight(listHeight));
            else
                texUnlocedtListScrollPos = EditorGUILayout.BeginScrollView(texUnlocedtListScrollPos, false, false, GUILayout.Height(listHeight), GUILayout.MinHeight(listHeight));
            {
                EditorGUILayout.BeginVertical();
                {
                    // Table data
                    for (int i = 0; i < locableTexts.Length; i++)
                    {
                        bool isLocalized = locableTexts[i].locTextLabel != null || locableTexts[i].locTextLabelMesh != null;

                        // Filter to only show localised or unlocalised text
                        if (isLocalized != _showLocalised)
                            continue;


                        EditorGUILayout.BeginHorizontal();
                        {
                            if (GUILayout.Button((isLocalized ? "Unlocalise" : "Localise"), GUILayout.Width(70)))
                            {
                                SetLocComponent(i, !isLocalized);
                                break;
                            }
                            EditorGUILayout.Space();

                            // Prefab or GameObject icon, and button for applying prefab
                            if (locableTexts[i].isPrefab)
                            {
                                if (locableTexts[i].isModified)
                                {
                                    if (GUILayout.Button(EditorGUIUtility.FindTexture("PrefabNormal Icon"), GUILayout.Width(20f), GUILayout.Height(20f)))
                                    {
                                        ApplyPrefab(i);
                                    }
                                }
                                else
                                    GUILayout.Label(EditorGUIUtility.FindTexture("PrefabNormal Icon"), GUILayout.Width(20f), GUILayout.Height(20f));
                            }
                            else
                                GUILayout.Label(EditorGUIUtility.ObjectContent(null, typeof(GameObject)).image, GUILayout.Width(20f), GUILayout.Height(20f));

                            // UI Object Reference
                            if ((locableTexts[i].locTextLabel || locableTexts[i].locTextLabelMesh) && locableTexts[i].isPrefab && locableTexts[i].isModified)
                                GUI.color = yellowColor;

                            if (locableTexts[i].label)
                                EditorGUILayout.ObjectField(locableTexts[i].label, typeof(Text), true, GUILayout.Width(175));
                            else
                                EditorGUILayout.ObjectField(locableTexts[i].labelMesh, typeof(TextMeshProUGUI), true, GUILayout.Width(175));

                            EditorGUI.LabelField(GUILayoutUtility.GetLastRect(), new GUIContent("", locableTexts[i].relativePath));
                            GUI.color = whiteColor;

                            // UI Text string
                            EditorGUILayout.TextField(locableTexts[i].text, GUILayout.Width(300));

                            // CSV Data
                            if (isLocalized)
                            {
                                if (locableTexts[i].locTextLabel)
                                {
                                    PrefabUtility.RecordPrefabInstancePropertyModifications(locableTexts[i].locTextLabel);
                                    EditorGUI.BeginChangeCheck();
                                    {
                                        // Is Editor preview localisation enabled
                                        locableTexts[i].locTextLabel.isTranslated = GUILayout.Toggle(locableTexts[i].locTextLabel.isTranslated, "", GUILayout.Width(20));

                                        // Tint CSV Line and Translation red if text doesn't have localisation
                                        GUI.color = (locableTexts[i].locTextLabel.isTranslated) ? whiteColor : redColor;
                                        locableTexts[i].locTextLabel.csvLineNo = EditorGUILayout.IntField(locableTexts[i].locTextLabel.csvLineNo, GUILayout.Width(35));
                                        EditorGUILayout.TextField(GetText(previewLanguage, locableTexts[i].locTextLabel.csvLineNo), GUILayout.Width(250));
                                        GUI.color = whiteColor;

                                    }
                                    if (EditorGUI.EndChangeCheck())
                                    {
                                        RefreshTextLabelArray();
                                    }
                                }
                                if (locableTexts[i].locTextLabelMesh)
                                {
                                    PrefabUtility.RecordPrefabInstancePropertyModifications(locableTexts[i].locTextLabelMesh);
                                    EditorGUI.BeginChangeCheck();
                                    {
                                        // Is Editor preview localisation enabled
                                        locableTexts[i].locTextLabelMesh.isTranslated = GUILayout.Toggle(locableTexts[i].locTextLabelMesh.isTranslated, "", GUILayout.Width(20));

                                        // Tint CSV Line and Translation red if text doesn't have localisation
                                        GUI.color = (locableTexts[i].locTextLabelMesh.isTranslated) ? whiteColor : redColor;
                                        locableTexts[i].locTextLabelMesh.csvLineNo = EditorGUILayout.IntField(locableTexts[i].locTextLabelMesh.csvLineNo, GUILayout.Width(35));
                                        EditorGUILayout.TextField(GetText(previewLanguage, locableTexts[i].locTextLabelMesh.csvLineNo), GUILayout.Width(250));
                                        GUI.color = whiteColor;
                                    }
                                    if (EditorGUI.EndChangeCheck())
                                    {
                                        RefreshTextLabelArray();
                                    }
                                }
                            }

                            GUILayout.FlexibleSpace();
                        }
                        EditorGUILayout.EndHorizontal();
                        EditorGUILayout.LabelField("", GUILayout.Height(5));
                    }
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndScrollView();
        }

        /// <summary>
        /// Update array of localizable text objects, returns unlocalised text count
        /// </summary>
        /// <param name="_forceCSVRead"></param>
        /// <returns></returns>
        static int RefreshTextLabelArray(bool _forceCSVRead = false)
        {
            int untranslatedTextCount = 0;
            // Read category's CSV file if it's not already been read
            if (allText == null || _forceCSVRead)
                allText = Playside.LocManager.GetLanguageStrings();


            Text[] foundText = Resources.FindObjectsOfTypeAll(typeof(Text)) as Text[];
            TextMeshProUGUI[] foundTextMeshes = Resources.FindObjectsOfTypeAll(typeof(TextMeshProUGUI)) as TextMeshProUGUI[];
            List<Text> uniqueTexts = new List<Text>();
            List<TextMeshProUGUI> uniqueTextMeshes = new List<TextMeshProUGUI>();
            List<string> uniquePaths = new List<string>();

            foreach (Text t in foundText)
            {
                bool storedOnDisk = EditorUtility.IsPersistent(t.gameObject);
#if UNITY_2018_2_OR_NEWER
                bool isPrefab = (PrefabUtility.GetCorrespondingObjectFromSource(t.gameObject) != null);
#else
			bool isPrefab = (PrefabUtility.GetPrefabParent(t.gameObject) != null);
#endif
                string relativePath = "";

                if (storedOnDisk || isPrefab)
                {
                    relativePath = AnimationUtility.CalculateTransformPath(t.gameObject.transform, PrefabUtility.FindPrefabRoot(t.gameObject).transform);
                    if (!uniquePaths.Contains(relativePath))
                    {
                        uniqueTexts.Add(t);
                        uniquePaths.Add(relativePath);
                    }
                }
                else
                {
                    uniqueTexts.Add(t);
                }
            }

            foreach (TextMeshProUGUI t in foundTextMeshes)
            {
                bool storedOnDisk = EditorUtility.IsPersistent(t.gameObject);
#if UNITY_2018_2_OR_NEWER
                bool isPrefab = (PrefabUtility.GetCorrespondingObjectFromSource(t.gameObject) != null);
#else
			bool isPrefab = (PrefabUtility.GetPrefabParent(t.gameObject) != null);
#endif
                string relativePath = "";

                if (storedOnDisk || isPrefab)
                {
                    relativePath = AnimationUtility.CalculateTransformPath(t.gameObject.transform, PrefabUtility.FindPrefabRoot(t.gameObject).transform);
                    if (!uniquePaths.Contains(relativePath))
                    {
                        uniqueTextMeshes.Add(t);
                        uniquePaths.Add(relativePath);
                    }
                }
                else
                {
                    uniqueTextMeshes.Add(t);
                }
            }


            Text[] uniqueTextArray = uniqueTexts.ToArray();
            TextMeshProUGUI[] uniqueTextMeshesArray = uniqueTextMeshes.ToArray();

            // Cache list of unique localizable texts
            locableTexts = new LocalisableText[uniqueTextArray.Length + uniqueTextMeshesArray.Length];
            for (int i = 0; i < locableTexts.Length; i++)
            {
                locableTexts[i].index = i;

                if (i < uniqueTextArray.Length)
                {
                    locableTexts[i].label = uniqueTextArray[i];
                    locableTexts[i].text = uniqueTextArray[i].text;
                    Playside.UILocLabelText locTextLabel = uniqueTextArray[i].GetComponent<Playside.UILocLabelText>();
                    locableTexts[i].locTextLabel = locTextLabel;

                    // Check for unlocalised text
                    if (locTextLabel != null)
                    {
                        if (!locTextLabel.isTranslated)
                            untranslatedTextCount++;
                    }
                }
                else
                {
                    locableTexts[i].labelMesh = uniqueTextMeshesArray[i - uniqueTextArray.Length];
                    locableTexts[i].text = uniqueTextMeshesArray[i - uniqueTextArray.Length].text;
                    Playside.UILocLabelTextMesh locTextLabelMesh = uniqueTextMeshesArray[i - uniqueTextArray.Length].GetComponent<Playside.UILocLabelTextMesh>();
                    locableTexts[i].locTextLabelMesh = locTextLabelMesh;

                    // Check for unlocalised text
                    if (locTextLabelMesh != null)
                    {
                        if (!locTextLabelMesh.isTranslated)
                            untranslatedTextCount++;
                    }
                }

                GameObject labelGameObject;
                if (locableTexts[i].label)
                    labelGameObject = locableTexts[i].label.gameObject;
                else
                    labelGameObject = locableTexts[i].labelMesh.gameObject;

                locableTexts[i].storedOnDisk = EditorUtility.IsPersistent(labelGameObject);
#if UNITY_2018_2_OR_NEWER
                locableTexts[i].isPrefab = (PrefabUtility.GetCorrespondingObjectFromSource(labelGameObject) != null);
#else
			locableTexts[i].isPrefab = (PrefabUtility.GetPrefabParent(labelGameObject) != null);
#endif

                // Prefab relative path
                Transform prefabRootTrans = PrefabUtility.FindPrefabRoot(labelGameObject).transform;
                locableTexts[i].relativePath = AnimationUtility.CalculateTransformPath(labelGameObject.transform, prefabRootTrans);

                // Check if prefab is modified
                bool modified = false;
                if (locableTexts[i].isPrefab)
                {
                    // Check if the target text field is modified
                    PropertyModification[] props = PrefabUtility.GetPropertyModifications(labelGameObject);

                    if (props != null)
                    {
                        for (int p = 0; p < props.Length; p++)
                        {
                            if (props[p] == null || props[p].target == null)
                                continue;

                            modified = (props[p].target.GetType() == typeof(Playside.UILocLabelText) || props[p].target.GetType() == typeof(Playside.UILocLabelTextMesh));
                            if (modified)
                                break;
                        }
                    }
                }
                locableTexts[i].isModified = modified;
            }

            return untranslatedTextCount;
        }


        // Add or Remove UILocTextLabel components from target text component
        static void SetLocComponent(int _index, bool _localise)
        {
            if (locableTexts != null && (locableTexts[_index].label != null || locableTexts[_index].labelMesh != null))
            {
                if (_localise)
                {
                    if (locableTexts[_index].label)
                    {
                        if (locableTexts[_index].label.GetComponent<Playside.UILocLabelText>() == false)
                            Undo.AddComponent<Playside.UILocLabelText>(locableTexts[_index].label.gameObject);
                    }
                    else
                    {
                        if (locableTexts[_index].labelMesh.GetComponent<Playside.UILocLabelTextMesh>() == false)
                            Undo.AddComponent<Playside.UILocLabelTextMesh>(locableTexts[_index].labelMesh.gameObject);
                    }
                }
                else
                {
                    if (locableTexts[_index].label)
                    {
                        if (locableTexts[_index].label.GetComponent<Playside.UILocLabelText>() == true)
                            Undo.DestroyObjectImmediate(locableTexts[_index].label.gameObject.GetComponent<Playside.UILocLabelText>());
                    }
                    else
                    {
                        if (locableTexts[_index].labelMesh.GetComponent<Playside.UILocLabelTextMesh>() == true)
                            Undo.DestroyObjectImmediate(locableTexts[_index].labelMesh.gameObject.GetComponent<Playside.UILocLabelTextMesh>());
                    }
                }

                if (locableTexts[_index].isPrefab)
                    ApplyPrefab(_index);
            }

            // Update the localisable text table, assign loc string if it exists
            RefreshTextLabelArray();
            AutoAssignLocalisedStrings(true);
        }

        static void ApplyPrefab(int _index)
        {
            // Apply prefab and refresh text label array data
            GameObject prefabRoot;
            if (locableTexts[_index].label)
                prefabRoot = PrefabUtility.FindPrefabRoot(locableTexts[_index].label.gameObject);
            else
                prefabRoot = PrefabUtility.FindPrefabRoot(locableTexts[_index].labelMesh.gameObject);

#if UNITY_2018_2_OR_NEWER
            PrefabUtility.ReplacePrefab(prefabRoot, PrefabUtility.GetCorrespondingObjectFromSource(prefabRoot), ReplacePrefabOptions.ConnectToPrefab);
#else
		PrefabUtility.ReplacePrefab(prefabRoot, PrefabUtility.GetPrefabParent(prefabRoot), ReplacePrefabOptions.ConnectToPrefab);
#endif
            RefreshTextLabelArray();
        }



        /// <summary> Returns the English text from the currently selected CSV file + line no </summary>
        /// <returns> The english text string </returns>
        static string GetText(LocLanguages _language, int _csvLineNo)
        {
            // Clamp line no selection
            _csvLineNo = Mathf.Clamp(_csvLineNo, 1, allText.Length);

            // Return language from this line
            return allText[_csvLineNo - 1].Get(_language);
        }


        /// <summary> Returns CSV line number if a matching string can be found, or -1 if string is missing </summary>
        /// <returns> CSV Line Number </returns>
        static int GetCSVLineNumber(string _text)
        {
            for (int i = 0; i < allText.Length; i++)
            {
                if (_text.ToLower() == allText[i].Get(LocLanguages.English).ToLower())
                    return i + 1;
            }

            return -1;
        }


        /// <summary> Automatically asign CSV line numbers to matching english strings on Text that's not flagged as Translated</summary>
        static void AutoAssignLocalisedStrings(bool _suppressDialogue = false)
        {
            untranslatedLocableTexts = new List<LocalisableText>();
            List<int> foundLineNumbers = new List<int>();

            for (int i = 0; i < locableTexts.Length; i++)
            {
                if ((locableTexts[i].locTextLabel && !locableTexts[i].locTextLabel.isTranslated) ||
                    (locableTexts[i].locTextLabelMesh && !locableTexts[i].locTextLabelMesh.isTranslated))
                {
                    int lineNumber = GetCSVLineNumber(locableTexts[i].text);
                    if (lineNumber > -1)
                    {
                        untranslatedLocableTexts.Add(locableTexts[i]);
                        foundLineNumbers.Add(lineNumber);
                    }
                }
            }

            // Early return with dialog if no matching strings are found
            if (untranslatedLocableTexts.Count <= 0)
            {
                if (!_suppressDialogue)
                    EditorUtility.DisplayDialog("No matches found", "No unlocalised text matching CSV found in project!", "Ok", "");
                return;
            }

            // List all of the matches found
            string message = "";
            for (int i = 0; i < untranslatedLocableTexts.Count; i++)
                message += "'" + untranslatedLocableTexts[i].text + "'\n";

            if (_suppressDialogue || EditorUtility.DisplayDialog("Found " + untranslatedLocableTexts.Count + " unlocalised Texts", "Do you want to localise the following\n" + message, "Yes", "No"))
            {
                // Assign CSV line numbers and flag as translated
                for (int i = 0; i < untranslatedLocableTexts.Count; i++)
                {
                    if (untranslatedLocableTexts[i].locTextLabel)
                    {
                        untranslatedLocableTexts[i].locTextLabel.isTranslated = true;
                        untranslatedLocableTexts[i].locTextLabel.csvLineNo = foundLineNumbers[i];
                    }
                    if (untranslatedLocableTexts[i].locTextLabelMesh)
                    {
                        untranslatedLocableTexts[i].locTextLabelMesh.isTranslated = true;
                        untranslatedLocableTexts[i].locTextLabelMesh.csvLineNo = foundLineNumbers[i];
                    }
                    ApplyPrefab(untranslatedLocableTexts[i].index);
                }

                // Confirmation message in console
                message = "";
                for (int i = 0; i < untranslatedLocableTexts.Count; ++i)
                {
                    message += "'" + untranslatedLocableTexts[i].text + "' in " + untranslatedLocableTexts[i].relativePath + "\n";
                }
                Playside.LocalisationOverrides.Debug.LogInfo("Hooked up localisation to " + untranslatedLocableTexts.Count + " Text objects: \n" + message);
            }
        }


        /// <summary> Select Localisation CSV in project view</summary>
        static void SelectProjectObject(string _path)
        {
            UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath(_path, typeof(UnityEngine.Object));
            Selection.activeObject = obj;
            EditorGUIUtility.PingObject(obj);
        }


        /// <summary> Generates a new CSV file containing any unlocalised strings</summary>
        static void ExportCSV()
        {
            // Make sure any existing translations are applied so they don't get duped in
            AutoAssignLocalisedStrings(true);

            string defaultCSVPath = Playside.LocalisationOverrides.Configuration.LocCSVFilePath;
            string currentCSVText = System.IO.File.ReadAllText(defaultCSVPath);
            string csvText = currentCSVText;

            Dictionary<string, int> currentText = new Dictionary<string, int>();
            string[] lines = csvText.Split('\n');
            int totalStringCount = lines.Length;

            int count = lines.Length;
            for (int i = 0; i < count; i++)
            {
                string text = lines[i].Split('\t')[0];
                if (!currentText.ContainsKey(text))
                    currentText.Add(text, i);
            }

            // ID column, only add this if the CSV is empty
            if (string.IsNullOrEmpty(currentCSVText))
            {
                csvText += "ID\t";

                for (int i = 0; i < Enum.GetValues(typeof(LocLanguages)).Length; i++)
                    csvText += (LocLanguages)i + "\t";

                csvText = csvText.TrimEnd('\t');
                csvText += "\n";
            }

            if (locableTexts != null && locableTexts.Length > 0)
            {
                int newStringsCount = 0;

                //csvText += "\n";

                for (int x = 0; x < locableTexts.Length; x++)
                {
                    if ((locableTexts[x].locTextLabel && !locableTexts[x].locTextLabel.isTranslated) ||
                        (locableTexts[x].locTextLabelMesh && !locableTexts[x].locTextLabelMesh.isTranslated))
                    {

                        // Remove Tabs and New Lines as they break CSV formatting

                        string text = locableTexts[x].text.Replace("\n", "\\n").Replace("\t", "");
                        if (currentText.ContainsKey(text))
                        {
                            if (locableTexts[x].locTextLabel)
                                locableTexts[x].locTextLabel.csvLineNo = currentText[text];
                            else if (locableTexts[x].locTextLabelMesh)
                                locableTexts[x].locTextLabelMesh.csvLineNo = currentText[text];

                            continue;
                        }

                        csvText += "\t";

                        csvText += text;
                        for (int i = 0; i < Enum.GetValues(typeof(LocLanguages)).Length - 1; i++)
                        {
                            csvText += "\t";
                        }
                        csvText += "\n";
                        newStringsCount++;

                        totalStringCount++;
                        currentText.Add(text, totalStringCount);
                    }
                }

                if (newStringsCount != 0)
                {
                    System.IO.File.WriteAllText(defaultCSVPath, csvText);
                    AssetDatabase.Refresh();
                    RefreshTextLabelArray(true);
                    AutoAssignLocalisedStrings(true);

                    Playside.LocalisationOverrides.Debug.LogWarning("New Localisation CSV created in\n" + defaultCSVPath);
                    EditorUtility.DisplayDialog("Localisation CSV file Generated",
                        "New Localisation CSV created in\n" + defaultCSVPath, "Ok", "");

                    // Select Generated CSV
                    SelectProjectObject(Playside.LocalisationOverrides.Configuration.LocCSVFilePath);
                }
                else
                    EditorUtility.DisplayDialog("Localisation CSV Update Not Required", "No new Localisation strings were found", "Ok", "");
            }
        }

        void ClipboardUniqueCharacters()
        {
            string allTextFromLanguage = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz";

            // CSV starts at line 1, also ignore line 1 since it's just column titles
            int length = allText.Length;
            for (int i = 2; i < length + 1; ++i)
            {
                // Get as is, then upper and lower chars because TextMeshPro has the option to force Uppercase/Lowercase which we will need to account for with Glyphs.
                // Keeping it as is + Upper/Lower because we don't know how other languages behave when converting to/from Upper to Lower. So best to be safe. Duplicates will be filtered out.
                string langaugeCellString = GetText(previewLanguage, i);
                allTextFromLanguage += langaugeCellString;
                allTextFromLanguage += langaugeCellString.ToUpper();
                allTextFromLanguage += langaugeCellString.ToLower();
            }

            string uniqueCharacters = new string(allTextFromLanguage.Distinct().ToArray());
            EditorGUIUtility.systemCopyBuffer = uniqueCharacters;

            TextEditor te = new TextEditor();
            te.text = uniqueCharacters;
            te.SelectAll();
            te.Copy();
        }

        /// <summary>
        /// Get number of unlocalised or untranslated text components
        /// </summary>
        /// <returns>Number of Unlocalised or Untranslated texts</returns>
        public static int GetUntranslatedTextCount()
        {
            return RefreshTextLabelArray(true);
        }
    }
}
//#endif