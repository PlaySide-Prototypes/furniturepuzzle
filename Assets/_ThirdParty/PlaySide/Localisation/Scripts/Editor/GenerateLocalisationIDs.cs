﻿using UnityEngine;
using UnityEditor.Callbacks;
using UnityEditor;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace Playside
{
    public static class GenerateLocalisationIDs
    {
        /// <summary> Generates a new class file with the speficied version string </summary>
        /// <param name="bundleVersion"> Bundle version </param>
        /// <returns> The new generated class's filename </returns>
        public static bool CreateLocalisationIDsClassFile()
        {
            using (StreamWriter writer = new StreamWriter(Playside.LocalisationOverrides.Configuration.LocalisationIDsFilePath, false))
            {
                try
                {
                    string code = GenerateLocIDCode();
                    writer.WriteLine("{0}", code);
                }
                catch (System.Exception ex)
                {
                    string msg = " threw:\n" + ex.ToString();
                    Playside.LocalisationOverrides.Debug.LogError(msg);
                    EditorUtility.DisplayDialog("Error when trying to regenerate class", msg, "OK");
                    return false;
                }
            }
            return true;
        }


        /// <summary> Regenerates (and replaces) the code for ClassName with new bundle version id </summary>
        /// <param name='bundleVersion'> New bundle version </param>
        /// <returns> Code to write to file </returns>
        static string GenerateLocIDCode()
        {
            string[] localisationStrings;
            int[] localisationIndices;

            // Get any localisation IDs that exist
            GetLocalisationIDStrings(out localisationStrings, out localisationIndices);

            string code = "// This file is autogenerated in the Unity Editor, by LocalisationHub\r";
            code += "public enum LocIDs\r";
            code += "{\r";
            code += "\tNone = 0,\r";
            for (int i = 0; i < localisationStrings.Length; i++)
            {
                // CSV starts at line 1 so add 1 to index
                code += "\t" + localisationStrings[i] + " = " + (localisationIndices[i] + 1) + ",\r";
            }
            code += "}\r";
            return code;
        }

        public static void GetLocalisationIDStrings(out string[] _idStrings, out int[] _idIndices)
        {
            const string LocCSVFilePath = Playside.LocalisationOverrides.Configuration.LocCSVFileName;
            TextAsset textAsset = Resources.Load<TextAsset>(LocCSVFilePath);
            if (textAsset == null)
                throw new UnityException("Could not open CSV file Resources/" + LocCSVFilePath);
            string csv = textAsset.text.Replace("\r\n", "\n");

            string[] lines = csv.Split('\n');

            List<string> idStringsList = new List<string>();
            List<int> idIndicesList = new List<int>();

            int numLanguages = Enum.GetValues(typeof(LocLanguages)).Length;

            // Ignore first row
            for (int rowNo = 1; rowNo < lines.Length; ++rowNo)
            {
                string line = lines[rowNo];
                if (line == " " || !string.IsNullOrEmpty(line.Trim()))
                {
                    // Ignore first column of CSV, it contains ID and we don' tneed it in langStrings
                    string[] stringsRead = (line.Substring(line.IndexOf('\t') + 1)).Split('\t');
                    string id = line.Split('\t')[0];

                    // Store ID if there's an entry in the column
                    if (!string.IsNullOrEmpty(id) && stringsRead.Length >= numLanguages)
                    {
                        if (idStringsList.Contains(id))
                        {
                            EditorUtility.DisplayDialog("Duplicate Loc ID!", "Localisation ID '" + id + "' (Line: " + rowNo + ") " + " already exists at Line: " + idStringsList.IndexOf(id) + ".", "D'oh");
                        }
                        else
                        {
                            idStringsList.Add(id);
                            idIndicesList.Add(rowNo);
                        }
                    }
                }
            }

            _idStrings = idStringsList.ToArray();
            _idIndices = idIndicesList.ToArray();
        }
    }
}