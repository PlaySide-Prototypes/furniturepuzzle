﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Playside
{
    public partial class LocalisationImportHandler : ScriptableObject
    {
        [UnityEditor.CustomEditor(typeof(Playside.LocalisationImportHandler))]
        public class LocalisationImportHandlerEditor : Playside.CustomEditorBase<Playside.LocalisationImportHandler>
        {
            protected override void OnInitialised()
            {
                base.OnInitialised();

                // Clearing Context State. No need to show previous import status when reloading the Config File.
                Target.CurrentStateContext = "";

                // Ensure Languages array has the correct number of configurations accounted for.
                int totalLanguagesCount = System.Enum.GetValues(typeof(LocLanguages)).Length;
                if (Target.languageConfigurations == null)
                {
                    Target.languageConfigurations = new LanguageImportData[totalLanguagesCount];
                }
                else if (Target.languageConfigurations.Length != totalLanguagesCount)
                {
                    ResizeReferenceArray(ref Target.languageConfigurations, totalLanguagesCount);
                }

                for (int i = 0; i < totalLanguagesCount; ++i)
                {
                    // Ensuring the Language Identities match up with the current LocLanguages
                    LocLanguages languageVal;
                    if (System.Enum.TryParse(Target.languageConfigurations[i].languageId, out languageVal) == false)
                    {
                        Target.languageConfigurations[i].languageId = ((LocLanguages)i).ToString();
                    }
                }

                // Sorting by alphabetical order.
                System.Array.Sort(Target.languageConfigurations, (LanguageImportData a, LanguageImportData b) =>
                {
                    return string.Compare(a.languageId, b.languageId);
                });
            }

            protected void OnImportFinished()
            {
                if (IsProgressBarWindowActive)
                {
                    ClearProgressBarWindow();
                }
            }

            [InspectorRegion("~Guide~")]
            protected void DrawGuideDetails()
            {
                if (DrawButtonField("Take me to the confluence page"))
                {
                    Application.OpenURL("https://playsidestudios.atlassian.net/wiki/spaces/PS/pages/77103207/Localisation");
                }
            }

            [InspectorRegion("~Import Status~")]
            protected void DrawImportStatus()
            {
                using (new GUIDisable())
                {
                    if (string.IsNullOrEmpty(Target.CurrentStateContext))
                    {
                        DrawTextArea("Import Process not started\n\n\n\n\n");
                    }
                    else
                    {
                        DrawTextArea(Target.CurrentStateContext);
                    }
                }
            }

            [InspectorRegion("~Import Options~")]
            protected void DrawImportButtons()
            {
                DrawDescriptionBox("Downloads the spreadsheet from Google, regenerates the LocIDs, then creates the Glyph Atlases for the languages that are marked for Font Repackaging.");

                if (DrawButtonField("Begin Import Process"))
                {
                    Target.BeginLocalisationImportProcess(OnImportFinished);
                }
            }

            [InspectorRegion("~Google Sheet Details~")]
            protected void DrawGoogleSheetOptions()
            {
                DrawDescriptionBox("Google Doc Id:\n~~~~~~~~~~~~~~~~~~~~~~\nThe Id of the document. Can be found in the URL Link.\n\n"
                                 + "Sheet Id:\n~~~~~~~~~~~~~~~~~~~~~~\nThe gid code at the end of the URL Link.\n\n"
                                 + "~~~~~~~~~~~~~~~~~~~~~~\nThis is all explained on the confluence page with images. Please click the guide button above to see step by step instructions.");

                DrawStringFieldWithUndoOption("Google Doc Id:", ref Target.googleDocId, "The Id of the Google Doc we will be downloading/importing.");
                DrawStringFieldWithUndoOption("Sheet Id:", ref Target.sheetId, "The Sheet Id (a.k.a the gid) of that we will be downloading/importing. Leave this empty if the SheetId is 0");
            }

            [InspectorRegion("~Fallback Fonts~")]
            protected void DrawFallbackFontOptions()
            {
                DrawDescriptionBox("In case a character cannot be found in your assigned Font Sheet. Fallback to the following sheets and try again.");

                DrawArrayParameters<TMPro.TMP_FontAsset> drawParams = new DrawArrayParameters<TMPro.TMP_FontAsset>()
                {
                    canReorderArray = true,
                    isArrayResizeable = true,
                    drawElementCallback = DrawFallbackFontElement
                };

                DrawReferenceArrayElements(ref Target.fallbackFontSheets, drawParams);
            }

            private void DrawFallbackFontElement(int index)
            {
                DrawObjectOptionWithUndoOption($"Fallback Font {GetZeroedNumPrefixString(index + 1)}:", ref Target.fallbackFontSheets[index]);
            }

            [InspectorRegion("~Currencies~")]
            protected void DrawCurrencyOptions()
            {
                DrawDescriptionBox("List down the currencies of each region your app is targeting. These currency glyphs may be used in any language, so they all need to be included here.");
                DrawStringFieldWithUndoOption("Currencies:", ref Target.currenciesToInclude);
            }

            [InspectorRegion("~Language Configurations~")]
            protected void DrawLanguageConfigurationsOptions()
            {
                DrawDescriptionBox("Please hover over each option to see a description of the item you are editing.\nInside each language is an option to repackage font. Please turn this on for any language that is using any Non-ASCII characters.");

                DrawArrayParameters<LanguageImportData> drawParams = new DrawArrayParameters<LanguageImportData>()
                {
                    isArrayResizeable = false,
                    canReorderArray = false,
                    drawElementCallback = DrawLanguageConfigurationElement
                };

                using (new EditorIndentation(1))
                {
                    DrawReferenceArrayElements(ref Target.languageConfigurations, drawParams);
                }
            }

            protected void DrawLanguageConfigurationElement(int index)
            {
                string displayLabel = Target.languageConfigurations[index].languageId;
                if (DrawFoldoutOption(displayLabel, Target.languageConfigurations[index]) == false)
                {
                    return;
                }

                using (new EditorIndentation(1))
                {
                    LocLanguages locLanguage = (LocLanguages)System.Enum.Parse(typeof(LocLanguages), Target.languageConfigurations[index].languageId);
                    Target.languageConfigurations[index].languageId = DrawEnumField("Language:", locLanguage).ToString();
                    DrawStringFieldWithUndoOption("Culture Variant Id:", ref Target.languageConfigurations[index].cultureVariantId, "The Culture Variant Id for this Language. Required to convert to Upper/Lower case characters.");
                    DrawChangeableIntegerWithUndoOption("Min Font Size:", ref Target.languageConfigurations[index].minFontSize, minValue: 10, maxValue: 100, tooltip: "Minimum font size that can appear in the Glyph Atlas. The larger the range between this and the Max Font Size, the longer generation will take.");
                    DrawChangeableIntegerWithUndoOption("Max Font Size:", ref Target.languageConfigurations[index].maxFontSize, minValue: Target.languageConfigurations[index].minFontSize, maxValue: 200, tooltip: "Maximum font size that can appear in the Glyph Atlas. The larger the range between this and the Min Font Size, the longer generation will take.");
                    DrawEnumFieldWithUndoOption("Render Mode:", ref Target.languageConfigurations[index].renderMode, drawInAlphabeticalOrder: true, tooltip: "How will this Language Glyph Atlas be Generated/Rendered");
                    DrawChangeableIntegerWithUndoOption("Padding:", ref Target.languageConfigurations[index].padding, minValue: 0, maxValue: 30, tooltip: "Allowable pixels between the edges of the glyph atlas");

                    if (DrawFoldoutOption($"Unique Characters {locLanguage}"))
                    {
                        uint[] uniqueUnicodes = Target.FindAllUniqueCharsForLanguage(locLanguage, Target.languageConfigurations[index]);
                        System.Text.StringBuilder uniqueChars = new System.Text.StringBuilder(uniqueUnicodes.Length);
                        foreach (uint g in uniqueUnicodes)
                        {
                            uniqueChars.Append(char.ConvertFromUtf32((int)g));
                        }
                        uniqueChars.Append(Target.currenciesToInclude);
                        DrawWrappedText(uniqueChars.ToString());

                        if (DrawButtonField("Copy Unique Chars", alignment: ButtonAlignment.Right))
                        {
                            EditorGUIUtility.systemCopyBuffer = uniqueChars.ToString();

                            TextEditor te = new TextEditor();
                            te.text = uniqueChars.ToString();
                            te.SelectAll();
                            te.Copy();
                        }
                    }
                }

                // Adding spaces between the final drawn element of this array section and the first drawn item of the next section.
                AddSpaces(2);
            }
        }
    }
}
