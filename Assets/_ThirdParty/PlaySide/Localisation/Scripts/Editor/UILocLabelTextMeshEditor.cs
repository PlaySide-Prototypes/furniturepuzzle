using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using TMPro;

namespace Playside
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(Playside.UILocLabelTextMesh))]
    public class UILocLabelTextMeshEditor : Playside.CustomEditorBase<Playside.UILocLabelTextMesh>
    {
        #region Internal variables

        static Playside.LocManager.LanguageStrings[] allText = null;
        int viewLanguage = (int)LocLanguages.English;

        #endregion

        protected override void OnInitialised()
        {
            base.OnInitialised();
            allText = Playside.LocManager.GetLanguageStrings();
        }

        /// <summary> Called when the Inspector refreshes </summary>
        [InspectorRegion]
        public void DrawCSVLineOptions()
        {
            if (allText == null)
            {
                allText = Playside.LocManager.GetLanguageStrings();
            }

            DrawBoolFieldWithUndoOption("Is Translated", ref Target.isTranslated);
            DrawChangeableIntegerWithUndoOption("CSV Line No", ref Target.csvLineNo, 1, minValue: 1, maxValue: allText.Length);

            LocIDs currentLocId = LocIDs.None;
            {
                if (System.Enum.IsDefined(typeof(LocIDs), Target.csvLineNo))
                {
                    currentLocId = (LocIDs)Target.csvLineNo;
                }

                LocIDs newSelectedLocId = (LocIDs)EditorGUILayout.EnumPopup("Selected LOC ID:", currentLocId);
                if (currentLocId != newSelectedLocId)
                {
                    Target.csvLineNo = (int)newSelectedLocId;
                    SetDirty(Target);
                }
            }          
        }

        protected override void OnDefaultInspectorElementsRendered()
        {
            base.OnDefaultInspectorElementsRendered();

            viewLanguage = EditorGUILayout.IntSlider("View Language", viewLanguage, 0, (int)Enum.GetValues(typeof(LocLanguages)).Length - 1);
            EditorGUILayout.LabelField("Selected Language", ((LocLanguages)viewLanguage).ToString());
            EditorGUILayout.LabelField("Text Preview (EN)", GetText(LocLanguages.English));
            if (GUILayout.Button("Reload CSVs"))
            {
                allText = Playside.LocManager.GetLanguageStrings();
            }
        }

        protected override void OnGUIChanged()
        {
            base.OnGUIChanged();
            UpdateTextOnTarget();
        }

        /// <summary> Returns the English text from the currently selected CSV file + line no </summary>
        /// <returns> The english text string </returns>
        string GetText(LocLanguages language)
        {
            Playside.UILocLabelTextMesh locTextScript = (Playside.UILocLabelTextMesh)target;

            // Read category's CSV file if it's not already been read
            if (allText == null)
                allText = Playside.LocManager.GetLanguageStrings();

            // Clamp line no selection
            locTextScript.csvLineNo = Mathf.Clamp(locTextScript.csvLineNo, 1, allText.Length);

            // Return language from this line
            return allText[locTextScript.csvLineNo - 1].Get(language);
        }

        /// <summary> Finds the Text component and updates it to the current value </summary>
        void UpdateTextOnTarget()
        {
            if (Playside.LocManager.Instance == null)
            {
                return;
            }

            Playside.LocManager.Instance.SetLanguage((LocLanguages)viewLanguage);

            for (int i = 0; i < targets.Length; ++i)
            {
                // Only localise if isLocalised has been selected, this is to preserve in progress unlocalised strings in editor mode
                Playside.UILocLabelTextMesh uiLocLabel = (Playside.UILocLabelTextMesh)targets[i];
                if (uiLocLabel.isTranslated)
                {
                    uiLocLabel.SetCSVLineNo(uiLocLabel.csvLineNo);
                }
            }
        }

        void OnDestroy()
        {
            allText = null;
        }
    }
}