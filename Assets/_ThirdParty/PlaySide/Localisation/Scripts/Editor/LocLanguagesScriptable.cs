﻿//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//              Localisation Languages Scriptable
//              Author: Christopher Allport
//              Date Created: August 13, 2020
//              Last Updated: ---------------
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  Description:
//
//		  This Script acts as a middle man for designers/UI crew to edit the
//      Languages being used in the Localisation system. This script essentially
//      Takes input via the LocLanguageScriptable Config File (found in the project)
//      and then outputs those as const values in LocLanguages.cs
//
//        Essentially writing code to the script.
//
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Playside
{
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // *            Scriptable Object
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public partial class LocLanguagesScriptable : ScriptableObject
    {
        private class LocLanguageInfo
        {
            /// <summary> Language Name before it was overwritten. </summary>
            public string originalLanguageName;
            /// <summary> Language Name currently. </summary>
            public string newLanguageName;
            /// <summary> System Language this gets converted from. </summary>
            public SystemLanguage systemLanguage;
            /// <summary> Is Right-To-Left Language. </summary>
            public bool isRightToLeftLanguage;
            /// <summary> The Font Override associated with this language. </summary>
            public Playside.FontOverrideScriptable fontOverride;
        }

        private LocLanguageInfo[] locLanguagesData;

        protected void OnEnable()
        {
            RefreshData();
        }

        protected void RefreshData()
        {
            LocLanguages[] allLanguages = System.Enum.GetValues(typeof(LocLanguages)) as LocLanguages[];
            int languagesCount = allLanguages.Length;

            locLanguagesData = new LocLanguageInfo[languagesCount];
            for (int i = 0; i < languagesCount; ++i)
            {
                locLanguagesData[i] = new LocLanguageInfo();
                locLanguagesData[i].originalLanguageName = allLanguages[i].ToString();
                locLanguagesData[i].newLanguageName = locLanguagesData[i].originalLanguageName;
                locLanguagesData[i].fontOverride = Resources.Load<FontOverrideScriptable>(locLanguagesData[i].newLanguageName + "_FontOverride");

                locLanguagesData[i].isRightToLeftLanguage = Playside.LocLanguagesInfo.RightToLeftLanguages.Contains(allLanguages[i]);

                // Find out which system language this pairs with.
                locLanguagesData[i].systemLanguage = SystemLanguage.Unknown;
                foreach (KeyValuePair<UnityEngine.SystemLanguage, LocLanguages> langPair in Playside.LocLanguagesInfo.SystemLangToLocLang)
                {
                    if (langPair.Value == allLanguages[i])
                    {
                        locLanguagesData[i].systemLanguage = langPair.Key;
                        break;
                    }
                }
            }
        }


        [MenuItem("PlaySide/Localisation.../Open Languages Config")]
        public static void SelectLanguagesConfigFile()
        {
            UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath(Playside.LocalisationOverrides.Configuration.LocalisationLanguagesConfigFilePath, typeof(UnityEngine.Object));
            Selection.activeObject = obj;
            EditorGUIUtility.PingObject(obj);
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // *            Custom Editor
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#if UNITY_EDITOR
    public partial class LocLanguagesScriptable : ScriptableObject
    {
        [UnityEditor.CustomEditor(typeof(Playside.LocLanguagesScriptable))]
        protected class LocLanguagesScriptableEditor : Playside.CustomEditorBase<LocLanguagesScriptable>
        {
            private bool hasMadeLocLanguageChanges = false;

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            // *            Languages Edit
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            [InspectorRegion]
            private void DrawLocLanguageGenerationOptions()
            {
                if (hasMadeLocLanguageChanges)
                {
                    DrawDescriptionBox("Changes have been detected. You must click on \"Generate Localisation Languages\" below for your changes to take effect.");
                }

                if (DrawButtonField("Generate Localisation Languages"))
                {
                    GenerateLocLanguages();
                    hasMadeLocLanguageChanges = false;
                }
            }

            [InspectorRegion("~Languages~")]
            private void DrawLanguagesConfigOptions()
            {
                DrawArrayParameters<LocLanguageInfo> drawParams = new DrawArrayParameters<LocLanguageInfo>()
                {
                    canReorderArray = true,
                    isArrayResizeable = true,
                    addSpacesBetweenElements = true,
                    alphabeticalComparer = AlphabeticalComparer,
                    drawElementCallback = DrawLanguageElement,
                };

                ArrayModificationResult arrayModResult = DrawReferenceArrayElements(ref Target.locLanguagesData, drawParams);
                if (arrayModResult != ArrayModificationResult.NoChange)
                {
                    hasMadeLocLanguageChanges = true;
                }
            }

            private void DrawLanguageElement(int index)
            {
                DrawStringFieldWithUndoOption($"  Language {GetZeroedNumPrefixString(index + 1)}:", ref Target.locLanguagesData[index].newLanguageName);
                DrawEnumFieldWithUndoOption("  System Language:", ref Target.locLanguagesData[index].systemLanguage, true, "Which System language is this associated with");
                DrawBoolFieldWithUndoOption("  Is Right To Left:", ref Target.locLanguagesData[index].isRightToLeftLanguage, "Is this language supposed to be rendered from Right To Left?");

                if (Target.locLanguagesData[index].systemLanguage != SystemLanguage.English)
                {
                    DrawReadonlyObjectOption("  Font Override:", Target.locLanguagesData[index].fontOverride, "It's okay if this is null. You'll just need to click on Generate Localisation Languages.");
                }
            }

            private int AlphabeticalComparer(LocLanguageInfo val1, LocLanguageInfo val2)
            {
                return string.Compare(val1.newLanguageName, val2.newLanguageName);
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            // *            Languages Generation
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            /// <summary> Generates the LocLanguages.cs script file. </summary>
            private void GenerateLocLanguages()
            {
                using (System.IO.StreamWriter writer = new System.IO.StreamWriter(Playside.LocalisationOverrides.Configuration.LocalisationLanguagesFilePath, false))
                {
                    try
                    {
                        List<string> rightToLeftLanguages = new List<string>();
                        Dictionary<string, string> systemLanguageToLocLanguage = new Dictionary<string, string>();

                        System.Text.StringBuilder scriptCode = new System.Text.StringBuilder();
                        scriptCode.AppendLine("// Game Specific language list. Auto-Generated by LocLanguagesScriptable.cs");
                        scriptCode.AppendLine("using System.Collections.Generic;");
                        scriptCode.AppendLine();
                        scriptCode.AppendLine("public enum LocLanguages");
                        scriptCode.AppendLine("{");

                        foreach (LocLanguageInfo languageInfo in Target.locLanguagesData)
                        {
                            if (string.IsNullOrEmpty(languageInfo.newLanguageName))
                            {
                                continue;
                            }

                            // Setup for next phase
                            if (languageInfo.isRightToLeftLanguage)
                            {
                                rightToLeftLanguages.Add(languageInfo.newLanguageName);
                            }

                            systemLanguageToLocLanguage[languageInfo.systemLanguage.ToString()] = languageInfo.newLanguageName;

                            // Writing out language to file.
                            scriptCode.AppendLine("    " + languageInfo.newLanguageName + ',');

                            // Only need to worry about FontOverride files for languages besides English.
                            if (languageInfo.systemLanguage == SystemLanguage.English)
                            {
                                continue;
                            }

                            // Either renaming FontOverride to match new language identity, or creating one if non-existant.
                            string newFilePath = Playside.LocalisationOverrides.Configuration.LocalisationResourcesFolder + languageInfo.newLanguageName + "_FontOverride.asset";
                            if (languageInfo.fontOverride != null)
                            {
                                string originalFilePath = Playside.LocalisationOverrides.Configuration.LocalisationResourcesFolder + languageInfo.originalLanguageName + "_FontOverride.asset";
                                if (originalFilePath.Equals(newFilePath) == false)
                                {
                                    // only need to rename FontOverride file if language name has changed.
                                    try
                                    {
                                        System.IO.File.Move(originalFilePath, newFilePath);
                                    }
                                    catch (System.Exception e)
                                    {
                                        string msg = $"Failed to rename [{originalFilePath}] with [{newFilePath}] because system threw exception: {e.Message}";
                                        Playside.LocalisationOverrides.Debug.LogError(msg);
                                        ShowDialogueWindow("Error", msg);
                                    }
                                }
                            }
                            else // Create new font override for newly defined language
                            {
                                string fileToCopyPath = Playside.LocalisationOverrides.Configuration.PathToLocalisationRepo + "Editor/Default_FontOverride_Template.asset";
                                try
                                {
                                    System.IO.File.Copy(fileToCopyPath, newFilePath);
                                }
                                catch (System.Exception e)
                                {
                                    string msg = $"Failed to create new FontOverride at [{newFilePath}] because system threw exception: {e.Message}";
                                    Playside.LocalisationOverrides.Debug.LogError(msg);
                                    ShowDialogueWindow("Error", msg);
                                }
                            }
                        }
                        scriptCode.AppendLine("}");
                        scriptCode.AppendLine();

                        scriptCode.AppendLine("namespace Playside");
                        scriptCode.AppendLine("{");
                        {
                            scriptCode.AppendLine("    public static class LocLanguagesInfo");
                            scriptCode.AppendLine("    {");
                            {
                                // Write-in Right to Left Languages
                                {
                                    scriptCode.AppendLine("        /// <summary> Which languages must be drawn from Right To Left. </summary>");
                                    scriptCode.AppendLine("        public static readonly List<LocLanguages> RightToLeftLanguages = new List<LocLanguages>()");
                                    scriptCode.AppendLine("        {");
                                    foreach (string rtlLanguage in rightToLeftLanguages)
                                    {
                                        scriptCode.AppendLine("            LocLanguages." + rtlLanguage + ',');
                                    }
                                    scriptCode.AppendLine("        };");
                                }
                                scriptCode.AppendLine();

                                // Write-in SystemLanguages To Localisation Languages Conversion
                                {
                                    scriptCode.AppendLine("        /// <summary> Which Language should the default System Language of the device convert into. Defaults to English if System Language is not specified here. </summary>");
                                    scriptCode.AppendLine("        public static readonly Dictionary<UnityEngine.SystemLanguage, LocLanguages> SystemLangToLocLang = new Dictionary<UnityEngine.SystemLanguage, LocLanguages>()");
                                    scriptCode.AppendLine("        {");
                                    foreach (KeyValuePair<string, string> sysLangLocLangPair in systemLanguageToLocLanguage)
                                    {
                                        scriptCode.AppendLine("            { UnityEngine.SystemLanguage." + sysLangLocLangPair.Key + ",     LocLanguages." + sysLangLocLangPair.Value + " },");
                                    }
                                    scriptCode.AppendLine("        };");
                                }

                            } // End of public static class LocLanguagesInfo
                            scriptCode.AppendLine("    }");

                        } // End of Namespace Playside
                        scriptCode.AppendLine("}");

                        // Write the newly created script code to file.
                        writer.Write(scriptCode.ToString());
                    }
                    catch (System.Exception ex)
                    {
                        string msg = " threw:\n" + ex.ToString();
                        Playside.LocalisationOverrides.Debug.LogError(msg);
                        ShowDialogueWindow("Error when trying to regenerate class", msg);
                    }
                }
                UnityEditor.AssetDatabase.Refresh();
                Target.RefreshData();
            }
        }
    }
#endif
}
