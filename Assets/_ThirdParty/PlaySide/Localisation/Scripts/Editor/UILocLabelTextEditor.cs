using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

namespace Playside
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(Playside.UILocLabelText))]
    public class UILocLabelTextEditor : Playside.CustomEditorBase<Playside.UILocLabelText>
    {
        #region Internal variables

        static Playside.LocManager.LanguageStrings[] allText = null;
        int viewLanguage = (int)LocLanguages.English;

        #endregion

        protected override void OnInitialised()
        {
            base.OnInitialised();
            allText = Playside.LocManager.GetLanguageStrings();
        }

        /// <summary> Called when the Inspector refreshes </summary>
        [InspectorRegion]
        public void DrawCSVLineOptions()
        {
            DrawScriptConversionRecommendation();

            DrawBoolFieldWithUndoOption("Is Translated", ref Target.isTranslated);
            DrawChangeableIntegerWithUndoOption("CSV Line No", ref Target.csvLineNo, 1, minValue: 1, maxValue: allText.Length);

            LocIDs currentLocId = LocIDs.None;
            {
                if (System.Enum.IsDefined(typeof(LocIDs), Target.csvLineNo))
                {
                    currentLocId = (LocIDs)Target.csvLineNo;
                }

                LocIDs newSelectedLocId = (LocIDs)EditorGUILayout.EnumPopup("Selected LOC ID:", currentLocId);
                if (currentLocId != newSelectedLocId)
                {
                    Target.csvLineNo = (int)newSelectedLocId;
                    SetDirty(Target);
                }
            }
        }

        protected override void OnDefaultInspectorElementsRendered()
        {
            base.OnDefaultInspectorElementsRendered();

            viewLanguage = EditorGUILayout.IntSlider("View Language", viewLanguage, 0, (int)Enum.GetValues(typeof(LocLanguages)).Length - 1);
            EditorGUILayout.LabelField("Selected Language", ((LocLanguages)viewLanguage).ToString());
            EditorGUILayout.LabelField("Text Preview (EN)", GetText(LocLanguages.English));
            if (GUILayout.Button("Reload CSVs"))
            {
                allText = Playside.LocManager.GetLanguageStrings();
            }

            if (GUILayout.Button($"Convert to {nameof(Playside.UILocLabelTextMesh)}")
                && EditorUtility.DisplayDialog("Are you sure?", $"This will replace your {nameof(Playside.UILocLabelText)} component with a {nameof(Playside.UILocLabelTextMesh)} component", "Go Ahead", "Abort"))
            {
                ConvertTextComponentToTextMeshPro(out Playside.UILocLabelTextMesh textMeshProLoc, out TMPro.TextMeshProUGUI textMeshPro);
            }
        }

        protected override void OnGUIChanged()
        {
            base.OnGUIChanged();
            UpdateTextOnTarget();
        }

        void DrawScriptConversionRecommendation()
        {
            GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(1));
            string[] textlines = (($"It is recommended that you switch over to using")
                               + ($"\n{nameof(Playside.UILocLabelTextMesh)} instead of using {nameof(Playside.UILocLabelText)}.")
                               + ($"\n\n{nameof(Playside.UILocLabelTextMesh)} supports writing RightToLeft")
                               + ($"\ntext for languages such as Arabic.")
                               + ($"\n\nYou can do so with the debug button below."))
                               .Split(new char[] { '\n', });

            GUIStyle s = new GUIStyle();
            s.alignment = TextAnchor.UpperCenter;
            if (EditorGUIUtility.isProSkin)
            {
                s.normal.textColor = new Color32(255, 255, 255, 255);
            }
            else
            {
                s.normal.textColor = new Color32(36, 68, 196, 255);
            }

            foreach (string line in textlines)
            {
                EditorGUILayout.Space();
                EditorGUILayout.Space();
                Rect previousElementRect = GUILayoutUtility.GetLastRect();
                float y = previousElementRect.y;
                EditorGUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                EditorGUILayout.EndHorizontal();
                Rect pos = GUILayoutUtility.GetLastRect();
                pos.y = y;
                pos.height = 15;
                EditorGUI.LabelField(pos, line, s);
            }
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(1));
            EditorGUILayout.Space();
        }

        /// <summary> Returns the English text from the currently selected CSV file + line no </summary>
        /// <returns> The english text string </returns>
        string GetText(LocLanguages language)
        {
            Playside.UILocLabelText locTextScript = (Playside.UILocLabelText)target;

            // Read category's CSV file if it's not already been read
            if (allText == null)
                allText = Playside.LocManager.GetLanguageStrings();

            // Clamp line no selection
            locTextScript.csvLineNo = Mathf.Clamp(locTextScript.csvLineNo, 1, allText.Length);

            // Return language from this line
            return allText[locTextScript.csvLineNo - 1].Get(language);
        }

        /// <summary> Finds the Text component and updates it to the current value </summary>
        void UpdateTextOnTarget()
        {
            if (Playside.LocManager.Instance == null)
            {
                return;
            }

            Playside.LocManager.Instance.SetLanguage((LocLanguages)viewLanguage);

            for (int i = 0; i < targets.Length; ++i)
            {
                // Only localise if isLocalised has been selected, this is to preserve in progress unlocalised strings in editor mode
                Playside.UILocLabelText uiLocLabel = (Playside.UILocLabelText)targets[i];
                if (uiLocLabel.isTranslated)
                {
                    uiLocLabel.SetCSVLineNo(uiLocLabel.csvLineNo);
                }
            }
        }

        void OnDestroy()
        {
            allText = null;
        }

        bool ConvertTextComponentToTextMeshPro(out Playside.UILocLabelTextMesh textMeshProLoc, out TMPro.TextMeshProUGUI textMeshPro)
        {
            textMeshProLoc = null;
            textMeshPro = null;

            Playside.UILocLabelText labelTarget = target as Playside.UILocLabelText;
            GameObject targetObject = labelTarget.gameObject;

            // Finding any script that is referencing this UILocLabelText so we can replace it with the Newly created UILocLabelTextMesh
            List<Playside.ReferenceFinder.FieldReferencesData> referencesToScript = Playside.ReferenceFinder.GetFieldsReferencingTarget(labelTarget);
            foreach (Playside.ReferenceFinder.FieldReferencesData refData in referencesToScript)
            {
                if ((refData.isACollectionVariableType && refData.fieldInfo.FieldType == typeof(Playside.UILocLabelText[]))
                    || refData.fieldInfo.FieldType == typeof(Playside.UILocLabelText))
                {
                    // Abort! Abort!
                    if (EditorUtility.DisplayDialog("Issue discovered", $"{refData.target.name} has a reference to {nameof(Playside.UILocLabelText)} with name {refData.fieldInfo.Name}.\nThis cannot be converted to {nameof(Playside.UILocLabelTextMesh)}. Would you like to abort conversion?", "Abort", "Keep Going"))
                    {
                        return false;
                    }
                }
            }

            // Storing Text Component Data /////////////////////////
            Text textCom = targetObject.GetComponent<Text>();
            string text = textCom.text;
            FontStyle fontStyle = textCom.fontStyle;
            float lineSpacing = textCom.lineSpacing;
            int fontSize = textCom.fontSize;
            bool richText = textCom.supportRichText;
            HorizontalWrapMode horzOverflow = textCom.horizontalOverflow;
            TextAnchor alignment = textCom.alignment;
            Color colour = textCom.color;
            Material material = textCom.material;
            bool rayCastTarget = textCom.raycastTarget;

            // Storing UILocLabelText Data /////////////////////////
            bool isTranslated = labelTarget.isTranslated;
            int csvLineNo = labelTarget.csvLineNo;

            string[] substrings = null;
            if (labelTarget.substituteStrings != null && labelTarget.substituteStrings.Length > 0)
            {
                substrings = new string[labelTarget.substituteStrings.Length];
                for (int i = 0; i < labelTarget.substituteStrings.Length; ++i)
                {
                    substrings[i] = labelTarget.substituteStrings[i];
                }
            }
            bool forceUpperCase = labelTarget.forceUpperCase;

            // Remove old Components so we can replace with new /////////////////////////
            DestroyImmediate(labelTarget);
            DestroyImmediate(textCom);

            // Convert Text Component To TextMeshPro /////////////////////////
            textMeshPro = targetObject.AddComponent<TMPro.TextMeshProUGUI>();
            textMeshPro.text = text;
            textMeshPro.fontSize = (float)fontSize;
            textMeshPro.lineSpacing = lineSpacing;
            textMeshPro.richText = richText;
            textMeshPro.overflowMode = TMPro.TextOverflowModes.Overflow;
            textMeshPro.enableWordWrapping = horzOverflow == HorizontalWrapMode.Wrap;
            textMeshPro.color = colour;
            textMeshPro.material = material;
            textMeshPro.raycastTarget = rayCastTarget;

            string fontOverideFileName = LocLanguages.English.ToString() + "_FontOverride";
            Playside.FontOverrideScriptable fontOverride = Resources.Load<Playside.FontOverrideScriptable>(fontOverideFileName);
            if (fontOverride != null)
            {
                textMeshPro.font = fontOverride.tmpFont;
            }

            switch (fontStyle)
            {
                case FontStyle.Bold: textMeshPro.fontStyle = TMPro.FontStyles.Bold; break;
                case FontStyle.BoldAndItalic: textMeshPro.fontStyle = TMPro.FontStyles.Bold | TMPro.FontStyles.Italic; break;
                case FontStyle.Italic: textMeshPro.fontStyle = TMPro.FontStyles.Italic; break;
                case FontStyle.Normal: textMeshPro.fontStyle = TMPro.FontStyles.Normal; break;
            }

            switch (alignment)
            {
                case TextAnchor.LowerLeft: textMeshPro.alignment = TMPro.TextAlignmentOptions.BottomLeft; break;
                case TextAnchor.LowerCenter: textMeshPro.alignment = TMPro.TextAlignmentOptions.Bottom; break;
                case TextAnchor.LowerRight: textMeshPro.alignment = TMPro.TextAlignmentOptions.BottomRight; break;
                case TextAnchor.MiddleLeft: textMeshPro.alignment = TMPro.TextAlignmentOptions.MidlineLeft; break;
                case TextAnchor.MiddleCenter: textMeshPro.alignment = TMPro.TextAlignmentOptions.Midline; break;
                case TextAnchor.MiddleRight: textMeshPro.alignment = TMPro.TextAlignmentOptions.MidlineRight; break;
                case TextAnchor.UpperLeft: textMeshPro.alignment = TMPro.TextAlignmentOptions.TopLeft; break;
                case TextAnchor.UpperCenter: textMeshPro.alignment = TMPro.TextAlignmentOptions.Top; break;
                case TextAnchor.UpperRight: textMeshPro.alignment = TMPro.TextAlignmentOptions.TopRight; break;
            }

            // Convert UILocLabelText to UILocLabelTextMesh /////////////////////////
            textMeshProLoc = targetObject.AddComponent<Playside.UILocLabelTextMesh>();
            textMeshProLoc.isTranslated = isTranslated;
            textMeshProLoc.csvLineNo = csvLineNo;
            textMeshProLoc.substituteStrings = substrings;
            textMeshProLoc.forceUpperCase = forceUpperCase;


            // Reassigning References from Old LocLabel to new one /////////////////////////
            foreach (Playside.ReferenceFinder.FieldReferencesData refData in referencesToScript)
            {
                if (refData.isACollectionVariableType)
                {
                    List<Playside.UILocLabelBase> list = refData.fieldInfo.GetValue(refData.target) as List<Playside.UILocLabelBase>;
                    if (list != null)
                    {
                        foreach (int index in refData.indexIdsReferencingTargetScript)
                        {
                            list[index] = textMeshProLoc;
                        }
                        refData.fieldInfo.SetValue(refData.target, list);
                    }
                    else
                    {
                        Playside.UILocLabelBase[] array = refData.fieldInfo.GetValue(refData.target) as Playside.UILocLabelBase[];
                        if (array != null)
                        {
                            foreach (int index in refData.indexIdsReferencingTargetScript)
                            {
                                array[index] = textMeshProLoc;
                            }
                            refData.fieldInfo.SetValue(refData.target, array);
                        }
                    }
                }
                else
                {
                    refData.fieldInfo.SetValue(refData.target, textMeshProLoc);
                }

                EditorUtility.SetDirty(refData.target);
                EditorUtility.SetDirty(refData.target.gameObject);
            }


            EditorUtility.SetDirty(textMeshPro);
            EditorUtility.SetDirty(textMeshProLoc);
            EditorUtility.SetDirty(targetObject);

            return true;
        }
    }
}
