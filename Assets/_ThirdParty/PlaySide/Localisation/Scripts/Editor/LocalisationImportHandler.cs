﻿//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//              Localisation Import Handler
//              Author: Christopher Allport
//              Date Created: February 21, 2020
//              Last Updated: August 7, 2020
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  Description:
//
//		  This Script handles the automatic generation of localisation data by
//      fetching and converting data from a Google Sheet document into a tab 
//      separated spreadsheet, then saving it into the project directory.
//
//        Once completed, this script also then parses the data in the
//      spreadsheet, compiles a new LocIDs enum structure based off that data,
//      and uses the unique characters imported in each language to generate 
//      the glyph atlases that will be used.
//
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine.TextCore.LowLevel;
using TMPro;
using System;

namespace Playside
{
    public partial class LocalisationImportHandler : ScriptableObject
    {
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // *            Declarations
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        private const char ColumnSeparator = '\t';
        private const char RowSeparator = '\n';

        private const string ProgressBarWindowTitle = "Localisation Import Status";
        private const string ProgressBarOneLiner = "Importing Localisation";

        private const string SuccessPopupMessage = "      Fetch from Google's Servers successful.\n\n"
                                                 + "                            \\\\ (^_^) //\n"
                                                 + "                        Have a nice day!";

        private static readonly int[] AllowableAtlasSizes = new int[] { 256, 512, 1024, 2048, 4096 };

        private delegate StepResult AutoGenerationStepHandlerDelegate();

        private enum StepResult
        {
            Running,
            Success,
            Failed
        }

        private class AutoGenerationStepHandler
        {
            public StepResult status;
            public AutoGenerationStepHandlerDelegate methodToRun;
        }

        [System.Serializable]
        private class LanguageImportData
        {
            public string languageId;
            public string cultureVariantId;
            public int minFontSize = 30;
            public int maxFontSize = 44;
            public GlyphRenderMode renderMode = GlyphRenderMode.SDFAA;
            public int padding = 4;
        }

        private class LanguageFontData
        {
            public Playside.FontOverrideScriptable langFontOverride;
            public string fontPath;
            public Font sourceLanguageFont;
        }

        private class LanguageAtlasData
        {
            public TMP_FontAsset fontAsset;
            public int fontSize;
            public int atlasWidth;
            public int atlasHeight;
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // *            Inspector Fields
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        [SerializeField, HandledInCustomInspector] private string googleDocId = "1dZbS8zq5thOvA7jRfNMOmd0esnz-vdJ0F_bD007zzgQ";
        [SerializeField, HandledInCustomInspector] private string sheetId /* gid */ = "1512473444"; // Leave this as null if the SheetId is 0

        [SerializeField, HandledInCustomInspector] private TMP_FontAsset[] fallbackFontSheets = new TMP_FontAsset[0];
        [SerializeField, HandledInCustomInspector] private string currenciesToInclude = "$€¥£元₩₹₽₺฿₪₱د.إ﷼";
        [SerializeField, HandledInCustomInspector] private LanguageImportData[] languageConfigurations = new LanguageImportData[0];

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // *            Non-Inspector Fields
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        private Playside.GoogleSpreadsheetStream.FetchStatus googleSheetFetchResult;
        private string googleDocContents;
        private List<LocLanguages> languagesImported = new List<LocLanguages>();
        private string sanitisedInputFromDoc;

        private System.Action onFinished;
        private List<AutoGenerationStepHandler> autoGenerationSteps;
        private string[] allTMPFontAssets;
        private string charactersMissingDuringGeneration = string.Empty;

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // *            Attr Accessors
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        public bool HasInitialisedFontEngine { get; private set; } = false;

        public int CurrentStepIndex { get; private set; } = 0;
        public int TotalStepsCount { get { return autoGenerationSteps.Count; } }
        public float Progress { get { return (float)CurrentStepIndex / TotalStepsCount; } }

        public string CurrentStateContext { get; private set; } = "";


        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // *            Unity Methods
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        [MenuItem("PlaySide/Localisation.../Open Localisation Import Settings")]
        public static void SelectLanguageImportConfigFile()
        {
            UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath(Playside.LocalisationOverrides.Configuration.LocalisationImportConfigFilePath, typeof(UnityEngine.Object));
            Selection.activeObject = obj;
            EditorGUIUtility.PingObject(obj);
        }

        [MenuItem("PlaySide/Localisation.../Import CSV Data from Google", priority = 0)]
        public static void BeginImportProcessMenuOption()
        {
            LocalisationImportHandler importHandler = AssetDatabase.LoadAssetAtPath(Playside.LocalisationOverrides.Configuration.LocalisationImportConfigFilePath, typeof(LocalisationImportHandler)) as LocalisationImportHandler;
            if (importHandler == null)
            {
                EditorUtility.DisplayDialog("Failed", $"Cannot find {nameof(LocalisationImportHandler)} at {Playside.LocalisationOverrides.Configuration.LocalisationImportConfigFilePath}", "Ok");
            }
            else
            {
                importHandler.BeginLocalisationImportProcess(null);
            }
        }

        protected void EditorUpdate()
        {
            // This updates the Fetch Request and Automation Process over multiple frames. Each Part of the Automation process 
            //  has it's own methods assigned to it and updated here per frame.
            AutoGenerationStepHandlerDelegate updateMethod = autoGenerationSteps[CurrentStepIndex].methodToRun;
            autoGenerationSteps[CurrentStepIndex].status = updateMethod();

            EditorUtility.DisplayProgressBar(ProgressBarWindowTitle, ProgressBarOneLiner, Progress);

            if (autoGenerationSteps[CurrentStepIndex].status == StepResult.Running)
            {
                return;
            }

            if (autoGenerationSteps[CurrentStepIndex].status == StepResult.Failed)
            {
                // Quit Processing.
                UnityEditor.EditorApplication.update -= EditorUpdate;
                OnProcessingFinished();
                return;
            }

            ++CurrentStepIndex;
            if (CurrentStepIndex >= TotalStepsCount)
            {
                // Finished Processing \(^_^)/
                UnityEditor.EditorApplication.update -= EditorUpdate;
                EditorUtility.DisplayDialog($"Success", SuccessPopupMessage, "Ok");
                OnProcessingFinished();
                return;
            }

            // Begin next step
            autoGenerationSteps[CurrentStepIndex].status = StepResult.Running;
        }

        /// <summary> Invoked when processing finished with either a Success or Failed status. </summary>
        private void OnProcessingFinished()
        {
            if (HasInitialisedFontEngine)
            {
                FontEngine.DestroyFontEngine();
                HasInitialisedFontEngine = false;
            }

            EditorUtility.ClearProgressBar();

            if (onFinished != null)
            {
                onFinished.Invoke();
                onFinished = null;
            }
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // *            Methods
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary> Log the status of the Import process. </summary>
        private void SetCurrentStateContext(string _stateContext, LogType _logType = LogType.Log)
        {
            CurrentStateContext += _stateContext + "\n";

            if (_logType == LogType.Assert || _logType == LogType.Exception)
            {
                Playside.LocalisationOverrides.Debug.Assert(false, _stateContext);
            }
            else if (_logType == LogType.Error)
            {
                Playside.LocalisationOverrides.Debug.LogError(_stateContext);
            }
            else if (_logType == LogType.Warning)
            {
                Playside.LocalisationOverrides.Debug.LogWarning(_stateContext);
            }
            else
            {
                Playside.LocalisationOverrides.Debug.LogInfo(_stateContext);
            }
        }

        /// <summary> Gets the Language Import Configuration Data for the Selected Language </summary>
        /// <returns> A class containing the configuration data. </returns>
        private LanguageImportData GetLanguageImportData(LocLanguages _language)
        {
            int length = languageConfigurations.Length;
            for (int i = 0; i < length; ++i)
            {
                if (languageConfigurations[i].languageId.Equals(_language.ToString()))
                {
                    return languageConfigurations[i];
                }
            }

            return null;
        }

        /// <summary> Gets all the unique characters for a language that could be found in the Localisation Strings file. </summary>
        /// <returns>Returns a list of uint values denoting the Unicode values for each of the unique characters found for the language.</returns>
        private uint[] FindAllUniqueCharsForLanguage(LocLanguages _language, LanguageImportData _langImportData)
        {
            CultureInfo languageCulture;
            try
            {
                languageCulture = CultureInfo.GetCultureInfo(_langImportData.cultureVariantId);
                if (languageCulture == null)
                {
                    return new uint[0];
                }
            }
            catch
            {
                Playside.LocalisationOverrides.Debug.LogError($"{_language} has assigned [ {_langImportData.cultureVariantId} ] as its {nameof(_langImportData.cultureVariantId)}, however this is invalid. Please enter the correct Id.");
                return new uint[0];
            }

            LocManager.LanguageStrings[] languageStrings = LocManager.GetLanguageStrings();

            // Include default English and Punctuation as well... Commonly found in our Localisation when translating through google translate. So good to have so we can identify.
            //  Also the currencies for common markets, as these can show up in any language.
            string allTextFromLanguage = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz";

            int length = languageStrings.Length;
            for (int i = 0; i < length; ++i)
            {
                // Get as is, then upper and lower chars because TextMeshPro has the option to force Uppercase/Lowercase which we will need to account for with Glyphs.
                // Keeping it as is + Upper/Lower because we don't know how other languages behave when converting to/from Upper to Lower. So best to be safe. Duplicates will be filtered out.
                string langaugeCellString = languageStrings[i].Get(_language);
                allTextFromLanguage += langaugeCellString;
                allTextFromLanguage += langaugeCellString.ToUpper(languageCulture);
                allTextFromLanguage += langaugeCellString.ToLower(languageCulture);
            }

            int finalIndexOfLanguageString = allTextFromLanguage.Length - 1;
            List<uint> charList = new List<uint>();
            for (int i = 0; i < allTextFromLanguage.Length; ++i)
            {
                uint unicode = allTextFromLanguage[i];

                // Handle surrogate pairs
                if (i < finalIndexOfLanguageString && char.IsHighSurrogate((char)unicode) && char.IsLowSurrogate(allTextFromLanguage[i + 1]))
                {
                    unicode = (uint)char.ConvertToUtf32(allTextFromLanguage[i], allTextFromLanguage[i + 1]);
                    ++i;
                }

                // Check to make sure we don't include duplicates
                if (charList.Contains(unicode) == false)
                {
                    charList.Add(unicode);
                }
            }

            uint[] uniqueCharacters = charList.ToArray();
            return uniqueCharacters;
        }

        /// <summary> Returns as a List of all languages using the Same Glyph Atlas. Unity will auto-unload the Glyph Sheet when function exits. So can only return a List of languages that reuse same glyph. </summary>
        private List<List<LocLanguages>> FindWhichLanguagesAreUsingTheSameGlyphAtlas()
        {
            Dictionary<TMP_FontAsset, List<LocLanguages>> glyphToLocLanguauges = new Dictionary<TMP_FontAsset, List<LocLanguages>>();

            LocLanguages[] allLanguages = System.Enum.GetValues(typeof(LocLanguages)) as LocLanguages[];
            foreach (LocLanguages language in allLanguages)
            {
                LanguageFontData languageFontData = TryLoadLanguageFontDetails(language, allTMPFontAssets);
                if (languageFontData == null)
                {
                    // Font issues found. Errors will be printed from within the above function.
                    continue;
                }

                List<LocLanguages> languagesUsingThisGlyph;
                if (glyphToLocLanguauges.TryGetValue(languageFontData.langFontOverride.tmpFont, out languagesUsingThisGlyph) == false)
                {
                    languagesUsingThisGlyph = new List<LocLanguages>();
                    glyphToLocLanguauges.Add(languageFontData.langFontOverride.tmpFont, languagesUsingThisGlyph);
                }

                languagesUsingThisGlyph.Add(language);
            }

            return glyphToLocLanguauges.Values.ToList();
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // *          Step 00: Begin Import Process
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary> Called from the Unity Custom Inspector. Begins the Import Process. </summary>
        /// <param name="_isTestOnly">
        /// If set to true, data will be imported but the glyph atlases will not replace existing ones. 
        /// Instead they will be added to the project with the "_Debug" suffix.
        /// </param>
        public void BeginLocalisationImportProcess(System.Action _onFinished)
        {
            onFinished = _onFinished;

            CurrentStateContext = string.Empty;
            allTMPFontAssets = null;
            charactersMissingDuringGeneration = string.Empty;

            SetCurrentStateContext($"Started Import Process at: {System.DateTime.Now}");
            SetCurrentStateContext($"Fetching from Google Server [Doc ID: {googleDocId}] [Sheet Id: {sheetId}]");

            // Setting up the auto generation steps. The System will execute each of the following methods until either all are complete, or one returns a "failed" status.
            autoGenerationSteps = new List<AutoGenerationStepHandler>()
            {
                new AutoGenerationStepHandler() { methodToRun = WaitForGoogleFetchResult },
                new AutoGenerationStepHandler() { methodToRun = ParseGoogleDocument },
                new AutoGenerationStepHandler() { methodToRun = ExportLocStringsCSV },
                new AutoGenerationStepHandler() { methodToRun = RegenerateLocIDs },
                new AutoGenerationStepHandler() { methodToRun = InitialiseFontEngine },
            };

            // Setup "Generate Glyphs for each language" steps.
            List<List<LocLanguages>> languagesUsingSameGlyphs = FindWhichLanguagesAreUsingTheSameGlyphAtlas();
            foreach (List<LocLanguages> languagesForGlyph in languagesUsingSameGlyphs)
            {
                autoGenerationSteps.Add(new AutoGenerationStepHandler()
                {
                    methodToRun = () => GenerateFontAssets(languagesForGlyph)
                });
            }

            autoGenerationSteps.Add(new AutoGenerationStepHandler()
            {
                methodToRun = GenerateFallbackGlyph
            });

            // Request Document from Google.
            CurrentStepIndex = 0;
            autoGenerationSteps[0].status = StepResult.Running;

            EditorUtility.DisplayProgressBar(ProgressBarWindowTitle, ProgressBarOneLiner, 0.0f);

            googleSheetFetchResult = Playside.GoogleSpreadsheetStream.FetchStatus.Running;
            UnityEditor.EditorApplication.update += EditorUpdate;

            Playside.GoogleSpreadsheetStream.FetchSpreadsheetEditorMode(googleDocId, sheetId, OnGoogleFetchRequestFinished);
        }

        /// <summary> Callback when GoogleSpreadsheetStream request is finished. </summary>
        /// <param name="_gss">The Google Spreadsheet Output</param>
        /// <param name="_fetchResult">Success/Failure Status</param>
        private void OnGoogleFetchRequestFinished(GoogleSpreadsheetStream _gss, GoogleSpreadsheetStream.FetchStatus _fetchResult)
        {
            googleSheetFetchResult = _fetchResult;

            SetCurrentStateContext($"Fetch Result came back as {googleSheetFetchResult}");

            if (googleSheetFetchResult == Playside.GoogleSpreadsheetStream.FetchStatus.Failed)
            {
                EditorUtility.DisplayDialog($"Failure", $"Failed to Fetch Sheet. ", "Ok");
                return;
            }

            googleDocContents = _gss.DocumentContents;
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // *          Step 01: Wait for Response from Google
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary> Step 01, waiting for spreadsheet to be received from Google. </summary>
        private StepResult WaitForGoogleFetchResult()
        {
            if (googleSheetFetchResult == Playside.GoogleSpreadsheetStream.FetchStatus.Running)
            {
                return StepResult.Running;
            }
            if (googleSheetFetchResult == Playside.GoogleSpreadsheetStream.FetchStatus.Failed)
            {
                return StepResult.Failed;
            }
            return StepResult.Success;
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // *          Step 02: Parse/Sanitise Google Spreadsheet
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary> 
        /// Step 02, scans through the contents of the downloaded spreadsheet, removes the first cell (context) because we don't need it, 
        /// then ensures the LocIDs are not in an illegal format. 
        /// </summary>
        private StepResult ParseGoogleDocument()
        {
            // Converting downloaded CSV contents from google into Unicode format (We are dealing with non-ASCII languages, so it's essential).
            byte[] bytes = System.Text.Encoding.Default.GetBytes(googleDocContents);
            string asUTF8 = System.Text.Encoding.UTF8.GetString(bytes);
            string[] sheetRows = asUTF8.Split(RowSeparator);
            int lineCount = sheetRows.Length;
            if (lineCount == 0)
            {
                EditorUtility.DisplayDialog($"Failure", $"Failed to Fetch Sheet. Result ended with a sheet containing no data!", "Ok");
                return StepResult.Failed;
            }

            SetCurrentStateContext($"Scanning Data - Languages Used");
            System.Text.StringBuilder csvStringBuilder;
            if (TryBuildCSVHeaderCells(sheetRows, out csvStringBuilder) == false)
            {
                // Error discovering languages. Errors will be printed via the above function
                return StepResult.Failed;
            }

            SetCurrentStateContext($"Scanning Data - CSV Contents");
            if (TryBuildCSVContentsCells(sheetRows, ref csvStringBuilder) == false)
            {
                // Error parsing CSV contents. Errors will be print via the above function
                return StepResult.Failed;
            }

            sanitisedInputFromDoc = csvStringBuilder.ToString();
            return StepResult.Success;
        }

        /// <summary>Parses through the header contents of the spreadsheet to determine which languages are defined.</summary>
        /// <param name="_sheetRows">Contents of the spreadsheet.</param>
        /// <param name="_csvStringBuilder">Returns a StringBuilder with the Header contents of the spreadsheet. Continue to use this strinbuilder for the remainder of the file.</param>
        /// <returns>True if successful, False if an issue was discovered.</returns>
        private bool TryBuildCSVHeaderCells(string[] _sheetRows, out System.Text.StringBuilder _csvStringBuilder)
        {
            languagesImported.Clear();
            LocLanguages[] applicableLanguages = System.Enum.GetValues(typeof(LocLanguages)) as LocLanguages[];
            string[] languageIdentificationCells = _sheetRows[0].Split(ColumnSeparator);

            // Can Skip the First two Cells because they are 'Context' and ID~!
            const int LanugageCellStartIdOffset = 2;
            for (int i = LanugageCellStartIdOffset; i < languageIdentificationCells.Length; ++i)
            {
                string langaugeInUse = languageIdentificationCells[i].Replace("\r", "");
                LocLanguages? language = null;
                foreach (LocLanguages availableLanguage in applicableLanguages)
                {
                    // Try to determine which language this is.
                    if (Regex.Match(langaugeInUse, availableLanguage.ToString(), RegexOptions.IgnoreCase).Success)
                    {
                        language = availableLanguage;
                        break;
                    }
                }

                if (language.HasValue == false)
                {
                    EditorUtility.DisplayDialog($"Failure", $"Could not determine which language [{langaugeInUse}] refers to. Please either add this language to the LocLanguages"
                                                           + "Enum, or modify the Language title in the Google Doc so that it matches the language Id in the enum.", "Ok");

                    _csvStringBuilder = null;
                    return false;
                }

                languagesImported.Add(language.Value);
            }

            // Build first row (Title Cells / Header Cells) of CSV
            string locIdCellTitle = languageIdentificationCells[1];
            string languagesInUse = string.Join($"{ColumnSeparator}", languagesImported.Select(l => l.ToString()));

            _csvStringBuilder = new System.Text.StringBuilder();
            _csvStringBuilder.Append($"{locIdCellTitle}{ColumnSeparator}{languagesInUse}");
            return true;
        }

        /// <summary>
        /// Parses through the remaining contents (non-header) of the spreadsheet; 
        /// removes illegal characters from the LocIDs enum column and replaces newline characters with readable ascii equivalents.
        /// </summary>
        /// <param name="_sheetRows">Contents of the spreadsheet.</param>
        /// <param name="_csvStringBuilder">The StringBuilder with the Header contents of the spreadsheet. This function continues to append the spreadsheet contents to the string builder.</param>
        /// <returns>True if successful, False if an issue was discovered.</returns>
        private bool TryBuildCSVContentsCells(string[] _sheetRows, ref System.Text.StringBuilder _csvStringBuilder)
        {
            // Skipping past the first row since we already dealt with it (Language Identifiers).
            int lineCount = _sheetRows.Length;
            for (int rowId = 1; rowId < lineCount; ++rowId)
            {
                _csvStringBuilder.Append(RowSeparator);

                // Ignoring the first column because we don't need to store context!
                string rowData = _sheetRows[rowId].Replace("\r", "");
                string[] cellsInRow = rowData.Split('\t');
                int colsCount = cellsInRow.Length;

                // Skip 'Context' Tab (Cell Column 0). We don't need to store it. Replace any Illegal Enum Characters as this is used as Enum IDs
                const int LocEnumCellId = 1;
                const string IllegalCharactersRegex = "\\s|`|~|!|@|#|\\$|%|\\^|&|\\*|\\(|\\)|\\[|\\]|\\||\\|\\||:|;|\\'|\"|<|,|\\.|>|\\?|\\/|\\+|\\-";
                string locId = Regex.Replace(cellsInRow[LocEnumCellId], IllegalCharactersRegex, "_");
                while (locId.Length > 0 && Regex.IsMatch($"{locId[0]}", "\\d"))
                {
                    // Enums also cannot start with numbers. So keep removing number from first character until no numbers are the first character.
                    locId = locId.Substring(1);
                }

                _csvStringBuilder.Append(locId);

                // Translations start from  2 columns across, onwards.
                const int LanguageCellStartID = 2;
                for (int j = LanguageCellStartID; j < colsCount; ++j)
                {
                    string cellString = $"{ColumnSeparator}{cellsInRow[j].Replace("\r", "")}";

                    // Google sheets adds two spaces to represent a newline
                    cellString = cellString.Replace("  ", "\\n");

                    // Unicode character for newline that can someimes be used (usually comes from Google Translate).
                    cellString = cellString.Replace($"{((char)10)}", "\\n");

                    _csvStringBuilder.Append(cellString);
                }
            }

            return true;
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // *        Step 03: Export Sanitised Spreadsheet to Assets Folder
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary> Step 03: Export sanitised spreadsheet data to Assets folder. </summary>
        private StepResult ExportLocStringsCSV()
        {
            if (System.IO.File.Exists(Playside.LocalisationOverrides.Configuration.LocCSVFilePath) == false)
            {
                EditorUtility.DisplayDialog($"Failure", $"{Playside.LocalisationOverrides.Configuration.LocCSVFilePath} does not exist! Cannot overwrite it!", "Ok");
                return StepResult.Failed;
            }

            SetCurrentStateContext($"Exporting Data to CSV [{Playside.LocalisationOverrides.Configuration.LocCSVFilePath}]");

            try
            {
                // File may be locked, readonly, etc. So Try/Catch and output failure reason if failed.
                System.IO.File.WriteAllText(Playside.LocalisationOverrides.Configuration.LocCSVFilePath, sanitisedInputFromDoc);
                AssetDatabase.Refresh();
            }
            catch (System.Exception e)
            {
                EditorUtility.DisplayDialog($"Failure", $"Error Occurred\n{e.Message}", "Ok");
                return StepResult.Failed;
            }

            return StepResult.Success;
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // *        Step 04: Regenerate LocIDs Enum Values
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary> Step 04: Regenerate LocIDs Enum Values from the LocIDs column in the spreadsheet. </summary>
        private StepResult RegenerateLocIDs()
        {
            // Clearing progress bar here just in case the LocIds import cause compilation issues.
            EditorUtility.ClearProgressBar();

            SetCurrentStateContext($"Regenerating Loc Ids");
            Playside.GenerateLocalisationIDs.CreateLocalisationIDsClassFile();
            AssetDatabase.Refresh();
            return StepResult.Success;
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // *        Step 05: Initialise Font Engine
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary> Step 05: Initiate Font Engine. This will be used to help generate the glyph atlases in the following steps. </summary>
        private StepResult InitialiseFontEngine()
        {
            SetCurrentStateContext($"Initialising Font Engine");
            FontEngineError error = FontEngine.InitializeFontEngine();
            if (error != FontEngineError.Success)
            {
                return StepResult.Failed;
            }

            // Collect all TMP_FontAssets also, just before the next step.
            HasInitialisedFontEngine = true;
            allTMPFontAssets = AssetDatabase.FindAssets("t:TMP_FontAsset");
            return StepResult.Success;
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // *        Step 06 + : Generate Glyph Atlases
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary> Step 06 + : Generates the Font/Glyph Assets for the chosen Languages. </summary>
        private StepResult GenerateFontAssets(List<LocLanguages> _selectedLanguages)
        {
#if !UNITY_2019_1_OR_NEWER
            SetCurrentStateContext($"Skipping atlas generation for {_selectedLanguage} because this Unity Version is not supported.", LogType.Warning);
            return StepResult.Success;
#else
            List<(LocLanguages language, LanguageImportData configuration)> usableLanguages = new List<(LocLanguages language, LanguageImportData configuration)>();
            foreach (LocLanguages language in _selectedLanguages)
            {
                LanguageImportData langImportData = GetLanguageImportData(language);
                if (langImportData == null)
                {
                    SetCurrentStateContext($"Could not find {nameof(LanguageImportData)} for {language}. Please make sure it's defined in the {nameof(LocalisationImportHandler)} Scriptable (it'll get automatically generated for you if you just select the scriptable in the Unity Editor).", LogType.Error);
                    continue;
                }

                usableLanguages.Add((language, langImportData));
            }

            if (usableLanguages.Count == 0)
            {
                return StepResult.Failed;
            }

            List<uint> uniqueChars = new List<uint>();
            foreach ((LocLanguages language, LanguageImportData configuration) in usableLanguages)
            {
                SetCurrentStateContext($"Fetching all unique characters for {language}");
                uint[] uniqueCharsForLang = FindAllUniqueCharsForLanguage(language, configuration);
                uniqueChars.AddRange(uniqueCharsForLang);
            }

            // Removing same Unicode characters found in the additional languages. Multiple languages may reuse the same characters.
            uniqueChars = uniqueChars.Distinct().ToList();

            // Just need to get the TMP Font data for the first language since all of these languages are using the same settings.
            LanguageFontData[] languageFontDatas = new LanguageFontData[usableLanguages.Count];
            for (int i = 0; i < languageFontDatas.Length; ++i)
            {
                languageFontDatas[i] = TryLoadLanguageFontDetails(usableLanguages[i].language, allTMPFontAssets);
                if (languageFontDatas[i] == null)
                {
                    // Font issues found. Errors will be printed from within the above function.
                    return StepResult.Failed;
                }
            }

            // Create a new font asset
            string selectedLanguagesAsString = string.Join(", ", usableLanguages.Select(x => x.language.ToString()));
            SetCurrentStateContext($"Generating New Atlas for languages [{selectedLanguagesAsString}]");
            LanguageImportData languageImportData = usableLanguages[0].configuration;
            LanguageAtlasData languageAtlasData = TryGenerateGlyphAtlas(usableLanguages, languageFontDatas[0], uniqueChars.ToArray());
            if (languageAtlasData == null)
            {
                // Couldn't generate atlas. Errors will be printed from within the above function.
                return StepResult.Failed;
            }

            // Make it a persistent asset so it will be saved
            string fontFileDirectory = System.IO.Path.GetDirectoryName(languageFontDatas[0].fontPath);
            string fontFileName = System.IO.Path.Combine(fontFileDirectory, System.IO.Path.GetFileNameWithoutExtension(languageFontDatas[0].fontPath));
            string fontAssetPath = fontFileName;

            AssetDatabase.CreateAsset(languageAtlasData.fontAsset, fontAssetPath + ".asset");

            languageAtlasData.fontAsset.atlasPopulationMode = AtlasPopulationMode.Static;

            // Add font material to asset so it will be saved
            languageAtlasData.fontAsset.material.name = fontAssetPath + " Material";
            AssetDatabase.AddObjectToAsset(languageAtlasData.fontAsset.material, languageAtlasData.fontAsset);

            // Add font atlas to asset so it will be saved
            languageAtlasData.fontAsset.atlasTexture.name = fontAssetPath + " Atlas";
            AssetDatabase.AddObjectToAsset(languageAtlasData.fontAsset.atlasTexture, languageAtlasData.fontAsset);

            // Reassigning new font asset back to font overrride scriptables (must be applied to all)
            for (int i = 0; i < languageFontDatas.Length; ++i)
            {
                languageFontDatas[i].langFontOverride.tmpFont = languageAtlasData.fontAsset;
                languageFontDatas[i].langFontOverride.normalMaterial = languageAtlasData.fontAsset.material;
                languageFontDatas[i].langFontOverride.strokedMaterial = languageAtlasData.fontAsset.material;
                languageFontDatas[i].langFontOverride.underlinedMaterial = languageAtlasData.fontAsset.material;
                EditorUtility.SetDirty(languageFontDatas[i].langFontOverride);
            }

            // Save all modified assets
            EditorUtility.SetDirty(languageAtlasData.fontAsset);

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            SetCurrentStateContext($"Successfully Generated [{selectedLanguagesAsString}] Font Atlas at path [{fontAssetPath}] with FontSize={languageAtlasData.fontSize}, AtlasWidth={languageAtlasData.atlasWidth}, AtlasHeight={languageAtlasData.atlasHeight}, RenderMode={usableLanguages[0].configuration.renderMode}");
            return StepResult.Success;

#endif // !UNITY_2019_1_OR_NEWER
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // *        Step 07 : Generate Fallback Glyph Atlases
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary> Step 07 : Generates the Font/Glyph Assets for the fallback. </summary>
        private StepResult GenerateFallbackGlyph()
        {
#if !UNITY_2019_1_OR_NEWER
            SetCurrentStateContext($"Skipping atlas generation for Fallback Glyphs because this Unity Version is not supported.", LogType.Warning);
            return StepResult.Success;
#else
            string allMissingCharacters = charactersMissingDuringGeneration + currenciesToInclude;
            if (string.IsNullOrEmpty(allMissingCharacters))
            {
                SetCurrentStateContext("No missing characters found in existing Glyph Atlases");
                return StepResult.Success;
            }

            if (fallbackFontSheets == null || fallbackFontSheets.Any(f => f != null) == false)
            {
                SetCurrentStateContext("Characters are missing, but no fallback font sheets were found.", LogType.Error);
                return StepResult.Failed;
            }

            // Getting Unique Missing Chars
            int finalIndexOfLanguageString = allMissingCharacters.Length - 1;
            List<uint> charList = new List<uint>();
            for (int i = 0; i < allMissingCharacters.Length; ++i)
            {
                uint unicode = allMissingCharacters[i];

                // Handle surrogate pairs
                if (i < finalIndexOfLanguageString && char.IsHighSurrogate((char)unicode) && char.IsLowSurrogate(allMissingCharacters[i + 1]))
                {
                    unicode = (uint)char.ConvertToUtf32(allMissingCharacters[i], allMissingCharacters[i + 1]);
                    ++i;
                }

                // Check to make sure we don't include duplicates
                if (charList.Contains(unicode) == false)
                {
                    charList.Add(unicode);
                }
            }

            uint[] uniqueCharacters = charList.ToArray();

            for (int fallbackFontIndex = 0; fallbackFontIndex < fallbackFontSheets.Length; ++fallbackFontIndex)
            {
                TMP_FontAsset fallbackFont = fallbackFontSheets[fallbackFontIndex];
                if (fallbackFont == null)
                {
                    continue;
                }

                if (fallbackFont.sourceFontFile.name.Equals("Arial"))
                {
                    // Using Unity's Default Arial Font Sheet. This has every character. No need to regenerate.
                    return StepResult.Success;
                }

                // Removing Old Glyph without deleting the TMP_FontAsset reference itself.
                string assetPath = AssetDatabase.GetAssetPath(fallbackFont);
                fallbackFont = AssetDatabase.LoadAssetAtPath(assetPath, typeof(TMP_FontAsset)) as TMP_FontAsset; //TMP_FontAsset.CreateFontAsset(fallbackFont.sourceFontFile, 32, 4, GlyphRenderMode.SDFAA, 512, 512, AtlasPopulationMode.Dynamic, false)


                uint[] stillMissingCharactersFromThisFont;
                if (fallbackFont.TryAddCharacters(uniqueCharacters, out stillMissingCharactersFromThisFont) == false)
                {
                    string missingGeneratedChars = string.Join(", ", stillMissingCharactersFromThisFont.Select(u => $"{char.ConvertFromUtf32((int)u)}"));
                    SetCurrentStateContext($"FallbackFont [{fallbackFontIndex}] cannot generate the following characters either [{missingGeneratedChars}]...", LogType.Warning);
                }

                assetPath = assetPath.Substring(0, assetPath.Length - ".asset".Length); // Removing .asset at the end.

                // Add font material to asset so it will be saved
                fallbackFont.material.name = assetPath + " Material";
                //AssetDatabase.AddObjectToAsset(fallbackFont.material, fallbackFont);

                // Add font atlas to asset so it will be saved
                fallbackFont.atlasTexture.name = assetPath + " Atlas";
                //AssetDatabase.AddObjectToAsset(fallbackFont.atlasTexture, fallbackFont);

                //AssetDatabase.CreateAsset(fallbackFont, assetPath + ".asset");
                EditorUtility.SetDirty(fallbackFont);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();

                uniqueCharacters = stillMissingCharactersFromThisFont;
                if (stillMissingCharactersFromThisFont == null || stillMissingCharactersFromThisFont.Length == 0)
                {
                    SetCurrentStateContext($"Missing characters successfully integrated into Fallback Glyphs");
                    return StepResult.Success;
                }
            }

            string remainingMissingCharacters = string.Join(", ", uniqueCharacters.Select(u => $"{char.ConvertFromUtf32((int)u)}"));
            SetCurrentStateContext($"Fallback Glyphs could not generate the following missing characters either [{remainingMissingCharacters}]. This is a problem as you will have missing characters in your game. "
                                + "Please add a new fallback atlas that can contain these characters", LogType.Error);
            return StepResult.Failed;
#endif
        }

        /// <summary> Retrieves the Existing Font details for the selected language. </summary>
        /// <param name="_selectedLanguage">Which language are we retrieving existing ont details for.</param>
        /// <param name="_allTMPFontAssets">All TMP Font Assets in the Resources folder. Loading resources takes some time, so if you already have the data, pass it in. Otherwise it will be loaded for you.</param>
        /// <returns>Returns null if Existing Font Details for selected language could not be loaded. Otherwise returns all data via the LanguageFontData class.</returns>
        private LanguageFontData TryLoadLanguageFontDetails(LocLanguages _selectedLanguage, string[] _allTMPFontAssets = null)
        {
            LanguageFontData languageFontData = new LanguageFontData();

            // NOTE: We HAVE TO reload all fonts everytime we run through a language. Whilst creating the Font Atlas, Unity cleans up resources, which sets our FontOverrides back to null. So this needs to be done here.
            //  * Resource load in FontOverride SciptableObjects
            //  * Assets stored in '/Resources/Localisation/' folder
            //  * Asset name format '[languagename]_FontOverride'
            string fontOverideFileName;
            if (_selectedLanguage == LocLanguages.English)
            {
                fontOverideFileName = "Default_FontOverride";
            }
            else
            {
                fontOverideFileName = _selectedLanguage + "_FontOverride";
            }
            languageFontData.langFontOverride = Resources.Load<Playside.FontOverrideScriptable>(fontOverideFileName);

            if (languageFontData.langFontOverride == null)
            {
                if (_selectedLanguage != LocLanguages.English)
                {
                    const string LocalisationResourcesFolder = Playside.LocalisationOverrides.Configuration.LocalisationResourcesFolder;
                    Playside.LocalisationOverrides.Debug.LogWarning($"No FontOverride found for {_selectedLanguage}. Please ensure that there is a {nameof(Playside.FontOverrideScriptable)} located in \"{LocalisationResourcesFolder}\" called \"{_selectedLanguage}_FontOverride\"");
                    SetCurrentStateContext($"Could not find Language Font Override for {_selectedLanguage}", LogType.Error);
                }
                return null;
            }

            if (languageFontData.langFontOverride.tmpFont == null)
            {
                SetCurrentStateContext($"{_selectedLanguage}_FontOverride does not have {nameof(languageFontData.langFontOverride.tmpFont)} assigned. Please assign it.", LogType.Error);
                return null;
            }

            if (_allTMPFontAssets == null)
            {
                _allTMPFontAssets = AssetDatabase.FindAssets("t:TMP_FontAsset");
            }

            foreach (string assetGuid in _allTMPFontAssets)
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(assetGuid);
                TMP_FontAsset tmpfont = AssetDatabase.LoadAssetAtPath(assetPath, typeof(TMP_FontAsset)) as TMP_FontAsset;
                if (tmpfont == languageFontData.langFontOverride.tmpFont)
                {
                    languageFontData.fontPath = assetPath;
                    break;
                }
            }

            if (string.IsNullOrEmpty(languageFontData.fontPath))
            {
                EditorUtility.DisplayDialog($"{_selectedLanguage} Failure", $"Could not find any {nameof(TMP_FontAsset)} relating to {_selectedLanguage}. No changes to atlas made.", "Ok");
                return null;
            }

            string sourceFontPath = AssetDatabase.GUIDToAssetPath(languageFontData.langFontOverride.tmpFont.creationSettings.sourceFontFileGUID);
            languageFontData.sourceLanguageFont = AssetDatabase.LoadAssetAtPath<Font>(sourceFontPath);
            if (languageFontData.sourceLanguageFont == null)
            {
                EditorUtility.DisplayDialog($"{_selectedLanguage} Failure", $"{_selectedLanguage} does not have any SourceFileFont associated with its creationSettings.", "Ok");
                return null;
            }

            return languageFontData;
        }

        /// <summary> True to generate a Glyph Atlas for the desired language </summary>
        /// <param name="_selectedLanguagse">Languages you wish to generate an atlas for.</param>
        /// <param name="_languageFontData">The Font Data already associated with this Language</param>
        /// <param name="uniqueCharsToInsert">The Unique Characters to add to this Glyph Atlas.</param>
        /// <returns>Returns null if the Atlas could not be generated. Otherwise returns data via the LanguageAtlasData class.</returns>
        private LanguageAtlasData TryGenerateGlyphAtlas(List<(LocLanguages language, LanguageImportData configuration)> _selectedLanguages, LanguageFontData _languageFontData, uint[] uniqueCharsToInsert)
        {
            // Only need to grab first Language Import Data since each language will reuse the same configuration for this Glyph
            string selectedLanguages = string.Join(", ", _selectedLanguages.Select(x => x.language.ToString()));
            LanguageImportData langImportConfigurationData = _selectedLanguages[0].configuration;
            LanguageAtlasData bestGeneratedLanguageAtlas = null;

            // Make sure we only pass in Unique characters. No need to generate glyphs for characters that already exist.
            int allowableAssetSizesCount = AllowableAtlasSizes.Length;
            uint[] missingCharacters = null;
            for (int allowedWidthIndex = 0; allowedWidthIndex < allowableAssetSizesCount; ++allowedWidthIndex)
            {
                int atlasWidth = AllowableAtlasSizes[allowedWidthIndex];

                // Start from Max Font Size and move down until you reach the smooth spot. If no font at any of the applicable font 
                //   sizes can fit in the atlas at the current atlasWidth/height. Pass though again with a bigger atlas and try again.
                for (int fontSize = langImportConfigurationData.maxFontSize; fontSize >= langImportConfigurationData.minFontSize; --fontSize)
                {
                    // # Condition : Less than or Equal to allowedWidthIndex, this will attempt to keep assets as close to the same size as possible.
                    for (int allowedHeightIndex = 0; allowedHeightIndex <= allowedWidthIndex; ++allowedHeightIndex)
                    {
                        int atlasHeight = AllowableAtlasSizes[allowedHeightIndex];

                        uint[] newMissingCharacters;
                        LanguageAtlasData generatedAtlasData = TryGenerateGlyphAtlasAtSize(selectedLanguages, langImportConfigurationData, _languageFontData, uniqueCharsToInsert, fontSize, atlasWidth, atlasHeight, out newMissingCharacters);

                        if (generatedAtlasData == null)
                        {
                            continue;
                        }

                        Texture2D[] atlases = generatedAtlasData.fontAsset.atlasTextures;
                        if (atlases.Length > 1)
                        {
                            // Atlas size is not large enough to fit all glyphs on a single texture. Go for next size up.
                            continue;
                        }

                        if (newMissingCharacters == null)
                        {
                            // Atlas Generation Success. All characters fit on a single texture at the current size and there are no missing characters.
                            return generatedAtlasData;
                        }

                        if (missingCharacters == null || missingCharacters.Length > newMissingCharacters.Length)
                        {
                            // This atlas is not perfect, but it's better than the last one we generated. Store it for now. It might remain our best one.
                            bestGeneratedLanguageAtlas = generatedAtlasData;
                            missingCharacters = newMissingCharacters;
                        }
                    }
                }
            }

            // Atlas Generation Failure
            if (missingCharacters != null && missingCharacters.Length > 0)
            {
                foreach (uint g in missingCharacters)
                {
                    charactersMissingDuringGeneration += $"{char.ConvertFromUtf32((int)g)}";
                    Playside.LocalisationOverrides.Debug.LogWarning($"{selectedLanguages} Atlas Generation Error: Missing glyph {g:X4} '{char.ConvertFromUtf32((int)g)}' into font {_languageFontData.langFontOverride.tmpFont.name}");
                }
            }

            return bestGeneratedLanguageAtlas;
        }

        /// <summary> Tries to generate a Glyph Atlas at the Specified Size </summary>
        /// <param name="selectedLanguages"> Which languages are we trying to geneerate for (this is used for error messaging). </param>
        /// <param name="langImportConfigurationData"> Configuration Data </param>
        /// <param name="_languageFontData"> Font setup for these specific languages </param>
        /// <param name="uniqueCharsToInsert"> Which characters are to be inserted </param>
        /// <param name="fontSize"> Sampling Point Size (Font Size) </param>
        /// <param name="atlasWidth"> Width of Atlas to generate </param>
        /// <param name="atlasHeight"> Height of Atlas to generate </param>
        /// <param name="missingCharacters"> Return an array of missing characters if atlas failed to generate. </param>
        private LanguageAtlasData TryGenerateGlyphAtlasAtSize(string selectedLanguages, LanguageImportData langImportConfigurationData, LanguageFontData _languageFontData, uint[] uniqueCharsToInsert, int fontSize, int atlasWidth, int atlasHeight, out uint[] missingCharacters)
        {
            TMP_FontAsset fontAsset = null;
            try
            {
                // Can error when trying to create a Font Asset if the Font Sheet isn't set up correctly.
                fontAsset = TMP_FontAsset.CreateFontAsset(_languageFontData.sourceLanguageFont, fontSize, langImportConfigurationData.padding, langImportConfigurationData.renderMode, atlasWidth, atlasHeight, AtlasPopulationMode.Dynamic);
            }
            catch (System.Exception e)
            {
                SetCurrentStateContext($"Font asset could not be created for Language [{selectedLanguages}]: Error = {e.Message}", LogType.Error);
            }

            if (fontAsset == null)
            {
                SetCurrentStateContext($"Font asset could not be created for Language [{selectedLanguages}]", LogType.Error);
                missingCharacters = null;
                return null;
            }

            // ~ Including Fallback Fonts for these glyphs atlases ~
            if (_languageFontData.langFontOverride.tmpFont.fallbackFontAssetTable != null)
            {
                fontAsset.fallbackFontAssetTable = new List<TMP_FontAsset>(_languageFontData.langFontOverride.tmpFont.fallbackFontAssetTable);
            }
            else
            {
                fontAsset.fallbackFontAssetTable = new List<TMP_FontAsset>();
            }

            foreach (TMPro.TMP_FontAsset fallbackFont in fallbackFontSheets)
            {
                if (fallbackFont != null && fontAsset.fallbackFontAssetTable.Contains(fallbackFont) == false)
                {
                    fontAsset.fallbackFontAssetTable.Add(fallbackFont);
                }
            }

            // Ensure the settings of the new Font Asset are the same as our current one.
            fontAsset.tabSize = _languageFontData.langFontOverride.tmpFont.tabSize;
            fontAsset.creationSettings = new FontAssetCreationSettings()
            {
                atlasWidth = atlasWidth,
                atlasHeight = atlasHeight,
                renderMode = (int)langImportConfigurationData.renderMode,
                characterSetSelectionMode = 7, // 7 => Custom Characters
                fontStyle = _languageFontData.langFontOverride.tmpFont.creationSettings.fontStyle,
                fontStyleModifier = _languageFontData.langFontOverride.tmpFont.creationSettings.fontStyleModifier,
                includeFontFeatures = _languageFontData.langFontOverride.tmpFont.creationSettings.includeFontFeatures,
                packingMode = 4, // 4 => Optimum
                padding = langImportConfigurationData.padding,
                pointSize = fontSize,
                pointSizeSamplingMode = 1, // 1 => Custom Size (use our defined Font Size above)
                sourceFontFileGUID = _languageFontData.langFontOverride.tmpFont.creationSettings.sourceFontFileGUID,
                sourceFontFileName = _languageFontData.langFontOverride.tmpFont.creationSettings.sourceFontFileName
            };

            LanguageAtlasData languageAtlasData = new LanguageAtlasData()
            {
                fontAsset = fontAsset,
                fontSize = fontSize,
                atlasWidth = atlasWidth,
                atlasHeight = atlasHeight
            };

            try
            {
                // Try / Catch because the TMP_FontAsset can have Null Glyphs Exception if exceptionally bad text is inserted or atlas size too small. 
                //  Don't need to error check the currencies. If this font sheet can't include them (Character does not exist in this language), we will use a fallback font sheet (Arial, etc.) to handle it.
                fontAsset.TryAddCharacters(currenciesToInclude);

                if (fontAsset.TryAddCharacters(uniqueCharsToInsert, out missingCharacters) == false)
                {
                    Playside.LocalisationOverrides.Debug.LogInfo($"Failed to generate {selectedLanguages} atlas at size {atlasWidth} x {atlasHeight} at font size {fontSize}");
                }
            }
            catch (System.Exception e)
            {
                Playside.LocalisationOverrides.Debug.LogError($"Error while generating atlas for {selectedLanguages}: {e.Message}");
                missingCharacters = null;
                return null;
            }

            return languageAtlasData;
        }
    }
}
