﻿using UnityEngine;
using System.Collections;

namespace Playside
{
    public class LocalisedURLOpener : Playside.UILocLabelBase
    {
        #region Inspector variables

        public string fallbackUrlToOpen;

        #endregion    // Inspector variables

        string urlToOpen;

        protected override string DefaultText { get { return fallbackUrlToOpen; } }

        /// <summary> Opens the URL </summary>
        public void OpenURL()
        {
            Application.OpenURL(urlToOpen);
        }

        protected override bool IsTextInitialised { get { return true; } }

        /// <summary> Called from Awake() </summary>
        protected override void Init()
        {
        }

        protected override void SetTextContent(string _text, bool _isRightToLeftLanguage)
        {
            urlToOpen = _text;
        }
    }
}
