﻿//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//              Localisation Game Specific Overrides
//              Author: Christopher Allport
//              Date Created: August 7, 2020
//              Last Updated: --------------
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  Description:
//
//		  This Script defines the const configurations for the Localisation
//      system. This includes defining paths, which keyboard keys activate
//      debug commands, and which languages are Right-to-Left, etc.
//
//        Additionally, there are two additional static classes besides the
//      configuration class that defines how the Localisation system should
//      output debug logs and how to save. These are using Unity's default
//      logging system and PlayerPrefs system. If your game uses different
//      systems compared to these, please overwrite the functions to use your
//      versions.
//
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

using System.Collections.Generic;

namespace Playside.LocalisationOverrides
{
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //	* Localisation System Configuration
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public static class Configuration
    {        
        /// <summary> Saving currently selected language with the following Save Key </summary>
        public const string SelectedLanguagePlayerPrefsKey = "CurrentLanguage";

        /// <summary> Default value: Press '`' (or '~') key to switch the visible language at any time. </summary>
        public const UnityEngine.KeyCode SwitchLanguageDebugKeyCode = UnityEngine.KeyCode.BackQuote;

        /// <summary> Path to Local Localisation Repo in Project. </summary>
        public const string PathToLocalisationRepo = "Assets/External/Playside/Localisation/";

        /// <summary> Path to Localisation Resources Folder. </summary>
        public const string LocalisationResourcesFolder = PathToLocalisationRepo + "Resources/";

        /// <summary> Path to Localisation Scripts Folder. </summary>
        public const string PathToLocalisationScriptsFolder = PathToLocalisationRepo + "Scripts/";

        /// <summary> Path to Localisation Textures Folder. </summary>
        public const string PathToLocalisationTexturesFolder = PathToLocalisationRepo + "Textures/";

        /// <summary> Loaded from the resources folder, so filename must not have extension. Also needs to include the folder hierarchy in the filename if it exists. </summary>
        public const string LocCSVFileName = "Loc_Strings";
        public const string LocCSVFilePath = LocalisationResourcesFolder + LocCSVFileName + ".csv";

        /// <summary> Auto-generated LocIDs enum csharp file name & path. </summary>
        public const string LocalisationIDsFileName = "LocIDs.cs";
        public const string LocalisationIDsFilePath = PathToLocalisationScriptsFolder + LocalisationIDsFileName;

        /// <summary> Auto-generated LocLanguages enum csharp file name & path. </summary>
        public const string LocalisationLanguagesFileName = "LocLanguages.cs";
        public const string LocalisationLanguagesFilePath = PathToLocalisationScriptsFolder + LocalisationLanguagesFileName;

        /// <summary> CSV Icon Path. </summary>
        public const string LocCSVIconTextureFileName = "CSV_Icon.tga";
        public const string LocCSVIconTextureFilePath = PathToLocalisationTexturesFolder + LocCSVIconTextureFileName;

        /// <summary> Localisation Import Configuration File Path. </summary>
        public const string LocalisationImportConfigFileName = "LocalisationImportConfig";
        public const string LocalisationImportConfigFilePath = PathToLocalisationRepo + "Editor/" + LocalisationImportConfigFileName + ".asset";

        /// <summary> Localisation Languages Configuration File Path. </summary>
        public const string LocalisationLanguagesConfigFileName = "LocLanguagesConfig";
        public const string LocalisationLanguagesConfigFilePath = PathToLocalisationRepo + "Editor/" + LocalisationLanguagesConfigFileName + ".asset";
    }


    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //	* Save System Override
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public static class SaveSystem
    {
        public static void Save()
        {
            UnityEngine.PlayerPrefs.Save();
        }

        public static int GetInt(string _key, int _defaultValue = 0)
        {
            return UnityEngine.PlayerPrefs.GetInt(_key, _defaultValue);
        }

        public static void SetInt(string _key, int _value)
        {
            UnityEngine.PlayerPrefs.SetInt(_key, _value);
        }

        public static string GetString(string _key, string _defaultValue = "")
        {
            return UnityEngine.PlayerPrefs.GetString(_key, _defaultValue);
        }

        public static void SetString(string _key, string _value)
        {
            UnityEngine.PlayerPrefs.SetString(_key, _value);
        }
    }


    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //	* Debug Logging Override
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public static class Debug
    {
        public static void LogInfo(string _msg, UnityEngine.Object _context = null)
        {
            UnityEngine.Debug.Log(_msg, _context);
        }

        public static void LogWarning(string _msg, UnityEngine.Object _context = null)
        {
            UnityEngine.Debug.LogWarning(_msg, _context);
        }

        public static void LogError(string _msg, UnityEngine.Object _context = null)
        {
            UnityEngine.Debug.LogError(_msg, _context);
        }

        public static void Assert(bool _condition, string _msg, UnityEngine.Object _context = null)
        {
            UnityEngine.Debug.Assert(_condition, _msg, _context);
        }
    }
}