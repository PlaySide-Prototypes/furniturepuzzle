using UnityEngine;
using System.Collections;
using UnityEngine.UI;


namespace Playside
{
    [RequireComponent(typeof(Text))]
    public class UILocLabelText : Playside.UILocLabelBase
    {
        public Text textLabel { get; private set; }
        public bool forceUpperCase = false;

        public override Color TextColour
        {
            get { return textLabel.color; }
            set { textLabel.color = value; }
        }

        protected override bool IsTextInitialised { get { return (textLabel != null); } }

        // Font Overriding
        Font originalFont;
        int originalTextSize;

        /// <summary> Called from Awake() </summary>
        protected override void Init()
        {
            textLabel = GetComponent<Text>();
            if (textLabel == null)
                throw new UnityException("Could not find Text component attached to '" + gameObject.name + "'");

            originalFont = textLabel.font;
            originalTextSize = textLabel.fontSize;
        }

        protected override void SetTextContent(string _text, bool _isRightToLeftLanguage)
        {
            if (forceUpperCase)
                _text = _text.ToUpper();

            textLabel.text = _text;

#if UNITY_EDITOR
            if (_isRightToLeftLanguage)
            {
                Playside.LocalisationOverrides.Debug.LogError($"{name}: {nameof(UILocLabelText)} could not display the text as Right To Left because {nameof(Text)} does not support it. Suggest you change from {nameof(Text)} to {nameof(TMPro.TextMeshPro)}");
            }
#endif
        }

        protected override void OverrideFont(Playside.FontOverrideScriptable _fontOverride)
        {
            if (_fontOverride != null && _fontOverride.font != null)
            {
                textLabel.font = _fontOverride.font;
                textLabel.fontSize = (int)(originalTextSize * _fontOverride.fontSizePercentage);
            }
            else
            {
                textLabel.font = originalFont;
                textLabel.fontSize = originalTextSize;
            }
        }
    }
}