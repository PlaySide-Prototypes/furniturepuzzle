﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Playside
{
    [CreateAssetMenu(fileName = "New Font Override", menuName = "Localisation/FontOverride", order = 150)]
    public class FontOverrideScriptable : ScriptableObject
    {
        public Font font;
        public TMPro.TMP_FontAsset tmpFont;
        public Material normalMaterial;
        public Material strokedMaterial;
        public Material underlinedMaterial;
        public float fontSizePercentage = 1f;
        public float tmpFontSizePercentage = 1f;
    }
}