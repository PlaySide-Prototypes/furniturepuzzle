using UnityEngine;
using System.Collections;
using System;

namespace Playside
{
    public class LocNotifier : MonoBehaviour
    {
        public void ChangeToNextLanguage() { ChangeLanguage(true); }
        public void ChangeToPreviousLanguage() { ChangeLanguage(false); }

        /// <summary> Changes to the next/previous language </summary>
        /// <param name="increase"> True changes to the next language, false changes to the previous language </param>
        public void ChangeLanguage(bool increase)
        {
            // Move to next/previous
            int language = (int)(Playside.LocManager.Instance.currentLanguage) + (increase ? 1 : -1);
            ChangeLanguage(language);
        }

        /// <summary> Changes to the specified language </summary>
        /// <param name="a_language"> New language to change to </param>
        public void ChangeLanguage(int a_language)
        {
            // Wrap around if necessary
            int numLanguages = Enum.GetValues(typeof(LocLanguages)).Length;
            if (a_language >= numLanguages)
                a_language = 0;
            else if (a_language < 0)
                a_language = numLanguages - 1;

            Playside.LocManager.Instance.SetLanguage((LocLanguages)a_language);
            Playside.LocManager.Instance.SaveLanguageSelection();
        }
    }
}
