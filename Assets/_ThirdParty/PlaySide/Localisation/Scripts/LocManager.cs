using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Playside
{
    public partial class LocManager : Playside.Manager.ManagerBase<LocManager>
    {
        /// <summary> Text string for each language </summary>
        public class LanguageStrings
        {
            public string[] strings;

            public LanguageStrings(string[] _strings)
            {
                strings = _strings;
            }

            public string Get(LocLanguages _language)
            {
                int langCellColId = LocManager.locLanguageCellIndexIds[(int)_language];
                return strings[langCellColId];
            }
        }

        public enum FontStyles
        {
            Normal,
            Stroked,
            Underlined,
        }

        /// <summary> Localisation CSV could be in a different order than our LocLanguages Enum. This converts LocLanguage to the correct CSV Column. </summary>
        private static int[] locLanguageCellIndexIds = new int[0];

        /// <summary> Languages that contain characters not supported in most fonts, and need to be switched back to a default font. </summary>
        private Playside.FontOverrideScriptable[] fontOverrides;

        /// <summary> All text strings in all languages </summary>
        LanguageStrings[] allText;

        /// <summary> Currently selected language </summary>
        public LocLanguages currentLanguage { get; private set; }

        /// <summary> Called when object/script activates </summary>
        protected override void Initialise()
        {
            LoadFontOverrides();
            allText = GetLanguageStrings();
        }

        public override void Save()
        {
        }

        /// <summary> Called before the first Update() </summary>
        protected void Start()
        {
            const string SelectedLanguagePlayerPrefKey = Playside.LocalisationOverrides.Configuration.SelectedLanguagePlayerPrefsKey;

            LocLanguages systemLanguage = GetInstalledLanguage();
            LocLanguages currentLanguage = (LocLanguages)Playside.LocalisationOverrides.SaveSystem.GetInt(SelectedLanguagePlayerPrefKey, (int)systemLanguage);
            SetLanguage(currentLanguage);
        }

#if UNITY_EDITOR
        protected void Update()
        {
            if (Input.GetKeyDown(Playside.LocalisationOverrides.Configuration.SwitchLanguageDebugKeyCode))
            {
                int numLanguages = Enum.GetValues(typeof(LocLanguages)).Length;
                LocLanguages nextLanguage = (LocLanguages)((int)(currentLanguage + 1) % numLanguages);
                SetLanguage(nextLanguage);
                SaveLanguageSelection();
                Playside.LocalisationOverrides.Debug.LogInfo($"Changed language to: {nextLanguage}");
            }
        }
#endif


        /// <summary>
        /// Resource Load in FontOverrides
        /// </summary>
        void LoadFontOverrides()
        {
            int numLanguages = Enum.GetValues(typeof(LocLanguages)).Length;
            fontOverrides = new Playside.FontOverrideScriptable[numLanguages];

            List<string> missingOverrideFonts = new List<string>();
            Playside.FontOverrideScriptable fallbackFontOverride = Resources.Load<Playside.FontOverrideScriptable>("Default_FontOverride");

            // Resource load in FontOverride SciptableObjects
            // Assets stored in '/Resources/Localisation/' folder
            // Asset name format '[language]_FontOverride'
            for (int i = 0; i < numLanguages; ++i)
            {
                LocLanguages overrideLanguage = (LocLanguages)i;
                string fontOverrideFileName = overrideLanguage + "_FontOverride";
                fontOverrides[i] = Resources.Load<Playside.FontOverrideScriptable>(fontOverrideFileName);

                if (fontOverrides[i] == null)
                {
                    fontOverrides[i] = fallbackFontOverride;

                    // English is expected to use the fallback font override by default.
                    if (overrideLanguage != LocLanguages.English)
                    {
                        missingOverrideFonts.Add(overrideLanguage.ToString());
                    }
                    else if (fontOverrides[i] == null)
                    {
                        missingOverrideFonts.Add(LocLanguages.English.ToString());
                    }
                }
            }

            if (missingOverrideFonts.Count > 0)
            {
                Playside.LocalisationOverrides.Debug.LogWarning("No FontOverride found for " + string.Join(", ", missingOverrideFonts));
            }
        }

        /// <summary> Imports the Loc Strings CSV file and reads through the contents, filling in strings for each language. </summary>
        public static LanguageStrings[] GetLanguageStrings()
        {
            const string LocCSVFilePath = Playside.LocalisationOverrides.Configuration.LocCSVFileName;
            TextAsset textAsset = Resources.Load<TextAsset>(LocCSVFilePath);
            if (textAsset == null)
                throw new UnityException("Could not open CSV file Resources/" + LocCSVFilePath + ". If it is present, check its icon in Unity- if it doesn't show as a text doc, then try right click->reimport.");

            string csv = textAsset.text.Replace("\r\n", "\n");

            string[] lines = csv.Split('\n');
            List<LanguageStrings> langStrings = new List<LanguageStrings>();
            int numLanguages = Enum.GetValues(typeof(LocLanguages)).Length;

            for (int rowNo = 0; rowNo < lines.Length; ++rowNo)
            {
                string line = lines[rowNo];
                if (line == " " || !string.IsNullOrEmpty(line.Trim()))
                {
                    // Ignore first column of CSV, it contains ID and we don't need it in langStrings
                    string[] stringsRead = (line.Substring(line.IndexOf('\t') + 1)).Split('\t');

                    if (stringsRead.Length < numLanguages)
                        throw new UnityException("Error parsing 'Loc_Strings.csv' line " + rowNo + ": Incorrect number of columns (has " + stringsRead.Length + ", expected " + numLanguages + "\n" + line);

                    // Replace all literal \n's with actual newlines
                    for (int strNo = 0; strNo < stringsRead.Length; ++strNo)
                        stringsRead[strNo] = stringsRead[strNo].Replace("\\n", "\n");

                    langStrings.Add(new LanguageStrings(stringsRead));
                }
            }

            if (langStrings.Count > 0)
            {
                // Find out which Cell Column this language belongs to.
                string[] languagesHeader = langStrings[0].strings;
                int numColumns = languagesHeader.Length;
                locLanguageCellIndexIds = new int[numLanguages];
                for (int langId = 0; langId < numLanguages; ++langId)
                {
                    LocLanguages lookingForLanguage = (LocLanguages)langId;
                    if (lookingForLanguage == LocLanguages.English)
                    {
                        // English is always first column
                        locLanguageCellIndexIds[langId] = 0;
                        continue;
                    }

                    // Assigned to English Cell Column by default in case we can't find the language.
                    locLanguageCellIndexIds[langId] = -1;

                    for (int cellColId = 0; cellColId < numColumns; ++cellColId)
                    {
                        if (string.Compare(languagesHeader[cellColId], lookingForLanguage.ToString(), ignoreCase: true) == 0)
                        {
                            // Found our language.
                            locLanguageCellIndexIds[langId] = cellColId;
                            break;
                        }
                    }

                    if (locLanguageCellIndexIds[langId] == -1)
                    {
                        Debug.LogError("Could not find \"" + lookingForLanguage + "\" in the Localisation Strings file. Did you rename this language? Please ensure both match up. Defaulting to English text for this language.");
                        locLanguageCellIndexIds[langId] = 0;
                    }
                }
            }

            return langStrings.ToArray();
        }

        /// <summary> Sets/changes the current language </summary>
        /// <param name="_language"> Language to show </param>
        public void SetLanguage(LocLanguages _language)
        {
            currentLanguage = _language;
            RefreshAllLocalizedText();
        }


        /// <summary> Finds & refreshes all localised text labels in the UI hierarchy </summary>
        /// Please note that this function should be adjusted based on the location of your locmanager- 
        /// this won't work if all localised text elements aren't children of the loc manager's gameobject.
        public void RefreshAllLocalizedText()
        {
            Playside.UILocLabelBase[] allLocScripts = FindObjectsOfType<Playside.UILocLabelBase>();
            for (int i = 0; i < allLocScripts.Length; ++i)
                allLocScripts[i].Refresh();
        }


        /// <summary> Saves the current language selection </summary>
        public void SaveLanguageSelection()
        {
            const string SelectedLanguagePlayerPrefKey = Playside.LocalisationOverrides.Configuration.SelectedLanguagePlayerPrefsKey;
            Playside.LocalisationOverrides.SaveSystem.SetInt(SelectedLanguagePlayerPrefKey, (int)currentLanguage);
            Playside.LocalisationOverrides.SaveSystem.Save();
        }

        public bool IsLanguageRightToLeft()
        {
            return Playside.LocLanguagesInfo.RightToLeftLanguages.Contains(currentLanguage);
        }

        public static bool IsLanguageRightToLeft(LocLanguages _language)
        {
            return Playside.LocLanguagesInfo.RightToLeftLanguages.Contains(_language);
        }

        public bool IsValidCSVLineNo(int _csvLineNo)
        {
            if ((_csvLineNo < 1) || (_csvLineNo > allText.Length))
            {
                return false;
            }
            return true;
        }
        /// <summary> Gets the specified text string </summary>
        /// <param name="_csvLineNo"> Text line number in the CSV file </param>
        /// <param name="_output"> Output string </param>
        public bool Get(int _csvLineNo, out string _output)
        {
            if (IsValidCSVLineNo(_csvLineNo) == false)
            {
                _output = string.Empty;
                return false;
            }
            else
            {
                LanguageStrings langString = allText[_csvLineNo - 1];
                _output = langString.Get(currentLanguage);
                return true;
            }
        }


        /// <summary> Gets the specified text string </summary>
        /// <param name="_locId"> Loc ID of the String </param>
        /// <param name="_output"> Output string </param>
        public bool Get(LocIDs _locId, out string _output)
        {
            return Get((int)_locId, out _output);
        }

        /// <summary> Gets the specified text string </summary>
        /// <param name="_csvLineNo"> Text line number in the CSV file </param>
        public string Get(int _csvLineNo)
        {
            if ((_csvLineNo < 1) || (_csvLineNo > allText.Length))
                return string.Empty;
            else
                return allText[_csvLineNo - 1].Get(currentLanguage);
        }

        /// <summary> Gets the specified text string </summary>
        /// <param name="_locID"> String's ID </param>
        public string Get(LocIDs _locID)
        {
            return Get((int)_locID);
        }

        /// <summary> Gets the specified text string </summary>
        /// <param name="_locID"> String's ID </param>
        public string GetWithSubstrings(LocIDs _locID, string[] substrings)
        {
            string result = Get((int)_locID);
            if (substrings != null)
            {
                for (int i = 0; i < substrings.Length; ++i)
                {
                    result = result.Replace("[" + i + "]", substrings[i]);
                }
            }
            return result;
        }

        public string GetWithSubstrings(LocIDs _locID, LocIDs[] substrings)
        {
            string result = Get((int)_locID);
            if (substrings != null)
            {
                for (int i = 0; i < substrings.Length; ++i)
                {
                    result = result.Replace("[" + i + "]", Get(substrings[i]));
                }
            }
            return result;
        }

        /// <summary> Returns the specified string in the specified language. </summary>
        /// <param name="_locID"> String's ID</param>
        /// <param name="_language"> Language to use </param>
        /// <returns></returns>
        public string Get(LocIDs _locID, LocLanguages _language)
        {
            return allText[(int)_locID - 1].Get(_language);
        }

        /// <summary> Parses the input string as a loc id, then returns the string with that id in the current language. </summary>
        /// <param name="_locID"> ID to use </param>
        /// <returns></returns>
        public string Get(string _locID)
        {
            return Get((LocIDs)Enum.Parse(typeof(LocIDs), _locID));
        }

        /// <summary> Parses the input string as a loc id, then returns the string in the specified language. </summary>
        /// <param name="_locID"> ID to use </param>
        /// <param name="_language"></param>
        /// <returns></returns>
        public string Get(string _locID, LocLanguages _language)
        {
            return Get((LocIDs)Enum.Parse(typeof(LocIDs), _locID), _language);
        }

        /// <summary> Returns the physical string for the given loc ID with the substrings applied </summary>
        /// <param name="_locID"> ID to use </param>
        /// <param name="_subStrings"> Substrings to apply </param>
        /// <returns></returns>
        public string Get(LocIDs _locID, string[] _subStrings)
        {
            string value = Get(_locID);

            if (_subStrings == null)
            {
                return value;
            }
            else
            {
                for (int i = 0; i < _subStrings.Length; ++i)
                {
                    value = value.Replace("[" + i + "]", _subStrings[i]);
                }

                return value;
            }
        }

        /// <summary> Temporary use only, returns the input text string </summary>
        /// <param name="_input"> Text to be localised later </param>
        /// <returns> Output string </returns>
        public string GetLater(string _input)
        {
            return _input;
        }

        LocLanguages GetInstalledLanguage()
        {
            LocLanguages installedLanguage;
            if (Playside.LocLanguagesInfo.SystemLangToLocLang.TryGetValue(Application.systemLanguage, out installedLanguage))
            {
                return installedLanguage;
            }

            // System Language Conversion is not defined. Default to English.
            return LocLanguages.English;
        }

        /// <summary>
        /// Get current language Override Font
        /// </summary>
        /// <returns></returns>
        public static Font GetLanguageOverrideFont()
        {
            return instance.fontOverrides[(int)instance.currentLanguage].font;
        }

        /// <summary>
        /// Get current language Override TextMesh Font
        /// </summary>
        /// <returns></returns>
        public static TMPro.TMP_FontAsset GetLanguageOverrideTMPFont()
        {
            return instance.fontOverrides[(int)instance.currentLanguage].tmpFont;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Playside.FontOverrideScriptable GetOverrideFont()
        {
            return instance.fontOverrides[(int)instance.currentLanguage];
        }
    }
}
