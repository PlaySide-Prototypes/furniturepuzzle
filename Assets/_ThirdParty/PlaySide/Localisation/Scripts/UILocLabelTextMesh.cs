using UnityEngine;
using System.Collections;
using TMPro;
using System;

namespace Playside
{
    [RequireComponent(typeof(TMP_Text))]
    public class UILocLabelTextMesh : Playside.UILocLabelBase
    {
        public TMP_Text textMesh { get; private set; }
        public bool forceUpperCase = false;
        public Playside.LocManager.FontStyles translatedFontStyle = Playside.LocManager.FontStyles.Stroked;

        public override Color TextColour
        {
            get { return textMesh.color; }
            set { textMesh.color = value; }
        }

        protected override bool IsTextInitialised { get { return (textMesh != null); } }

        // Font Overriding
        TMPro.TMP_FontAsset originalFont;
        float originalTextSize;
        Material originalmaterial;

        /// <summary> Called from Awake() </summary>
        protected override void Init()
        {
            textMesh = GetComponent<TMP_Text>();
            if (textMesh == null)
                throw new UnityException("Could not find Text component attached to '" + gameObject.name + "'");

            originalFont = textMesh.font;
            originalTextSize = textMesh.fontSize;
            originalmaterial = textMesh.fontSharedMaterial;
        }

        protected override void SetTextContent(string _text, bool _isRightToLeftLanguage)
        {
            if (forceUpperCase)
                _text = _text.ToUpper();

            textMesh.text = _text;
            textMesh.isRightToLeftText = _isRightToLeftLanguage;
        }

        protected override void OverrideFont(Playside.FontOverrideScriptable _fontOverride)
        {
            if (_fontOverride != null && _fontOverride.tmpFont != null)
            {
                textMesh.fontSize = (int)(originalTextSize * _fontOverride.tmpFontSizePercentage);
                textMesh.font = _fontOverride.tmpFont;

                switch (translatedFontStyle)
                {
                    case Playside.LocManager.FontStyles.Normal:
                        if (_fontOverride.normalMaterial != null)
                            textMesh.fontSharedMaterial = _fontOverride.normalMaterial;
                        break;
                    case Playside.LocManager.FontStyles.Stroked:
                        if (_fontOverride.strokedMaterial != null)
                            textMesh.fontSharedMaterial = _fontOverride.strokedMaterial;
                        break;
                    case Playside.LocManager.FontStyles.Underlined:
                        if (_fontOverride.underlinedMaterial != null)
                            textMesh.fontSharedMaterial = _fontOverride.underlinedMaterial;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                textMesh.font = originalFont;
                textMesh.fontSize = originalTextSize;
                textMesh.fontSharedMaterial = originalmaterial;
            }
        }
    }
}
