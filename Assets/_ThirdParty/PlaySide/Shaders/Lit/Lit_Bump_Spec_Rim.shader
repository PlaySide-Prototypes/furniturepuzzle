﻿Shader "PlaySide/Lit/Bump_Rim"
{
	Properties
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
		_Shininess ("Shininess", Range (0.03, 2)) = 0.078125
		_Gloss ("Glossiness", Range (0, 1)) = 0.25
		_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
		_BumpMap ("Normalmap", 2D) = "bump" {}
	    _RimPower ("RimPower", Float ) = 0.5
	    _RimColor ("RimColor", Color) = (0.5,0.5,0.5,1)
	}

	SubShader 
	{ 
		Tags { "RenderType"="Opaque" }
		LOD 400
		
		CGPROGRAM
		#pragma surface surf BlinnPhong
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _BumpMap;
		sampler2D _Overlay;
		fixed4 _Color;
		half _Shininess;
		half _Gloss;
		uniform half _RimPower;
		uniform half4 _RimColor;

		struct Input
		{
			float2 uv_MainTex;
			float2 uv_Overlay;
			float3 viewDir;
			float4 color : COLOR;
		};

		void surf (Input IN, inout SurfaceOutput o)
		{
			fixed4 tex = tex2D(_MainTex, IN.uv_MainTex)*IN.color*_Color;
			o.Albedo = tex.rgb;
			o.Gloss = _Shininess;
			o.Alpha = tex.a * _Color.a;
			o.Specular = _Gloss;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));

			half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
		    o.Emission = (_RimColor.rgb * pow (rim, _RimPower))*_RimColor.a;
		}
		ENDCG
	}
	FallBack "Legacy Shaders/Specular"
}
