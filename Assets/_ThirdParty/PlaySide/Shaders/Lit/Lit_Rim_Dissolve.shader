﻿Shader "PlaySide/Lit/Rim_Dissolve" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_RimPower ("RimPower", Float ) = 0.5
   		_RimColor ("RimColor", Color) = (0.5,0.5,0.5,1)
   		_DissolveColor ("Dissolve Color", Color) = (1, 1, 1,1)
		_DissolveMask ("Dissolve Mask", 2D) = "white" {}
		_DissolveWidth ("Dissolve Width", Float) = 0.1
		_Dissolve ("Dissolve Amount",  Range(0.0, 1.0)) = 0.1
      	_Cutoff ("Cutoff", Range(0.0, 1.0)) = 0.5
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue" = "Transparent" "IgnoreProjector" = "True" }
		LOD 200

		ZWrite On

		CGPROGRAM
		#pragma surface surf Lambert alphatest:_Cutoff
		#pragma target 3.0
	
		sampler2D _MainTex;
		sampler2D _DissolveMask;

		struct Input {
			float2 uv_MainTex;
			float3 viewDir;
			float4 color : COLOR;
		};

		fixed4 _Color;
		fixed4 _RimColor;
		half _RimPower;
		float4 _DissolveColor;
		float _DissolveWidth;
		float _Dissolve;
		//float _Cutoff;

		void surf (Input IN, inout SurfaceOutput  o) {
			fixed4 col = tex2D(_MainTex, IN.uv_MainTex) * IN.color * _Color;
			//half4 tex = ;
			fixed mask = tex2D(_DissolveMask, IN.uv_MainTex);
			float fade = 1 - ((_Dissolve * 1.2) - 0.1);
			float3 edgeglow = col.rgb;

			// Brighten dissolve border
			if(mask < fade + _DissolveWidth * 0.5)
				edgeglow += _DissolveColor/(1-fade);
			else if(mask < fade + _DissolveWidth)
				edgeglow += _DissolveColor * mask;

			//clip(mask - fade);
			if(mask - fade < 0.01)
				col.a = 0;

			o.Albedo = col.rgb;
			o.Alpha = col.a;

			half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
    		o.Emission = edgeglow + (col.rgb+_RimColor.rgb * pow (rim, _RimPower))*_RimColor.a;

		}
		ENDCG
	}
	FallBack "Diffuse"
}
