﻿Shader "PlaySide/Lit/Rim"
{
	Properties
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_RimColor ("Rim Color", Color) = (0.5,0.5,0.5,1)
		_RimPower ("Rim Power", Float ) = 0.5
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			float3 viewDir;
			float4 color : COLOR;
		};

		fixed4 _Color;
		fixed4 _RimColor;
		half _RimPower;

		void surf (Input IN, inout SurfaceOutput  o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * IN.color * _Color;
			o.Albedo = c.rgb;

			half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
    		o.Emission = (_RimColor.rgb * pow (rim, _RimPower))*_RimColor.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
