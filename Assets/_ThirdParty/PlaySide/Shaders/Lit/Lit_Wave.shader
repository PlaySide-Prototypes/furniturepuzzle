// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "PlaySide/Effect/Lit_Wave" {
   Properties {
   	  _Color ("Main Color", Color) = (1,1,1,1)
	  _MainTex ("Texture", 2D) = "white" {}
	  _Disp ("Displacement Map", 2D) = "white" {}
	  _Height ("Height", Float) = 1.0
	}
	
	SubShader {
	  Tags { "RenderType"="Opaque" "Queue" = "Geometry" }
	 Pass
		{
			Tags { "LightMode" = "ForwardBase" }
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"

			uniform float4 _TimeEditor;
            uniform float4 _LightColor0; 
			sampler2D _MainTex; 
			sampler2D _Disp; 
			float4 _MainTex_ST;
			fixed3 _Color;
			float _Height;

			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
				float3 color : COLOR;
				
				LIGHTING_COORDS(1,2)
			};

			v2f vert (appdata_full v) 
			{				
				v2f o;
				o.uv = v.texcoord;
				o.color = v.color.rgb;

				// Displacement
				float2 dispUV  = TRANSFORM_TEX (v.texcoord, _MainTex);
                float dispMap = tex2Dlod(_Disp, float4(dispUV,0,0)).r;
                v.vertex.xyz += v.normal * dispMap * _Height;

                // Convert to View, include shadows
				o.pos = UnityObjectToClipPos (v.vertex);
				TRANSFER_VERTEX_TO_FRAGMENT(o);
				return o;
			}
			
			half4 frag (v2f i) : COLOR
			{
				half3 shadow = 1+(SHADOW_ATTENUATION(i)-1) * (SHADOW_ATTENUATION(i)-1) * (UNITY_LIGHTMODEL_AMBIENT.rgb-1);
				half3 col = tex2D(_MainTex, i.uv.xy).rgb* i.color * shadow;

				return half4(col, 1);
			}
			ENDCG
		}
	}
	FallBack "VertexLit"
}


