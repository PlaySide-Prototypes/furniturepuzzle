﻿Shader "PlaySide/Lit/Bump_OverlayWrap"
{
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
	_Shininess ("Shininess", Range (0.03, 1)) = 0.078125
	_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
	_BumpMap ("Normalmap", 2D) = "bump" {}
	_Overlay ("Overlay Map", 2D) = "black" {}
	_OverlayAmount ("Overlay Amount", Range (0, 1)) = 1.0
}

CGINCLUDE
sampler2D _MainTex;
sampler2D _BumpMap;
sampler2D _Overlay;
fixed4 _Color;
fixed _OverlayAmount;
half _Shininess;
float2 OverlayOffset;

struct Input {
	float2 uv_MainTex;
	float2 uv2_Overlay;
	float4 screenPos;
	float3 viewDir;
};

void surf (Input IN, inout SurfaceOutput o) {
	fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
	tex.rgb *= _Color.rgb; 

	// Screen space UV with edge
	o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));	
	float2 screenUV = IN.screenPos.xy / IN.screenPos.w;

	float uvOffset = dot (normalize(IN.viewDir), o.Normal);
	//screenUV.x *= IN.uv2_Overlay.x; 
	//screenUV.y *= IN.uv2_Overlay.y;
    screenUV.x += OverlayOffset.x + IN.uv2_Overlay.x;// + uvOffset;
    screenUV.y += OverlayOffset.y + IN.uv2_Overlay.y;// + uvOffset;


    // Add Overlay
	fixed4 overlay = tex2D(_Overlay, screenUV);
	overlay = overlay*overlay.a * _OverlayAmount * uvOffset;
	//tex.rgb += overlay;

	o.Albedo = tex.rgb;
	o.Emission = overlay;
	o.Gloss = tex.a;
	o.Specular = _Shininess;
}
ENDCG

SubShader { 
	Tags { "RenderType"="Opaque" }
	LOD 400
	
	CGPROGRAM
	#pragma surface surf BlinnPhong
	#pragma target 3.0
	ENDCG
}

SubShader { 
	Tags { "RenderType"="Opaque" }
	LOD 400
	
	CGPROGRAM
	#pragma surface surf BlinnPhong nodynlightmap
	ENDCG
}

FallBack "Legacy Shaders/Specular"
}
