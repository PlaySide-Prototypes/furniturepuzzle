// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.29 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.29;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:False,bkdf:False,hqlp:False,rprd:False,enco:True,rmgx:False,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:False,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.9490197,fgcg:0.9215687,fgcb:0.8509805,fgca:1,fgde:0.011,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4013,x:32719,y:32712,varname:node_4013,prsc:2|diff-5051-OUT,spec-4879-RGB,gloss-1973-OUT;n:type:ShaderForge.SFN_Tex2d,id:1836,x:31858,y:32514,ptovrint:False,ptlb:Diffuse,ptin:_Diffuse,varname:node_1836,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:4879,x:31994,y:32704,ptovrint:False,ptlb:Specular,ptin:_Specular,varname:node_4879,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_ValueProperty,id:1973,x:32377,y:32934,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:node_1973,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Cubemap,id:887,x:31983,y:32356,ptovrint:False,ptlb:Cubemap,ptin:_Cubemap,varname:node_887,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,pvfc:0;n:type:ShaderForge.SFN_Multiply,id:5918,x:32291,y:32339,varname:node_5918,prsc:2|A-887-RGB,B-4879-RGB;n:type:ShaderForge.SFN_Add,id:1998,x:32373,y:32546,varname:node_1998,prsc:2|A-1836-RGB,B-5918-OUT;n:type:ShaderForge.SFN_ValueProperty,id:996,x:32015,y:32953,ptovrint:False,ptlb:Vert,ptin:_Vert,varname:node_996,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Add,id:6020,x:32185,y:32836,varname:node_6020,prsc:2|A-181-RGB,B-996-OUT;n:type:ShaderForge.SFN_VertexColor,id:181,x:31833,y:32858,varname:node_181,prsc:2;n:type:ShaderForge.SFN_Multiply,id:3897,x:32346,y:32755,varname:node_3897,prsc:2|A-1998-OUT,B-6020-OUT;n:type:ShaderForge.SFN_ViewVector,id:8953,x:31425,y:32928,varname:node_8953,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:3235,x:31425,y:33061,prsc:2,pt:False;n:type:ShaderForge.SFN_Dot,id:7285,x:31635,y:32986,varname:node_7285,prsc:2,dt:0|A-8953-OUT,B-3235-OUT;n:type:ShaderForge.SFN_OneMinus,id:5945,x:31823,y:33049,varname:node_5945,prsc:2|IN-7285-OUT;n:type:ShaderForge.SFN_Power,id:4738,x:31989,y:33146,varname:node_4738,prsc:2|VAL-5945-OUT,EXP-196-OUT;n:type:ShaderForge.SFN_Slider,id:196,x:31556,y:33231,ptovrint:False,ptlb:Fresnel,ptin:_Fresnel,varname:node_196,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:5;n:type:ShaderForge.SFN_Multiply,id:8077,x:32144,y:33243,varname:node_8077,prsc:2|A-4738-OUT,B-8551-OUT;n:type:ShaderForge.SFN_Slider,id:8551,x:31762,y:33415,ptovrint:False,ptlb:ReflectonStrength,ptin:_ReflectonStrength,varname:node_8551,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:5;n:type:ShaderForge.SFN_Multiply,id:3725,x:32329,y:33264,varname:node_3725,prsc:2|A-887-RGB,B-8077-OUT;n:type:ShaderForge.SFN_Multiply,id:1614,x:32414,y:33129,varname:node_1614,prsc:2|A-1836-RGB,B-3725-OUT;n:type:ShaderForge.SFN_Add,id:5051,x:32555,y:33001,varname:node_5051,prsc:2|A-3897-OUT,B-1614-OUT;proporder:1836-4879-1973-887-996-196-8551;pass:END;sub:END;*/

Shader "Shader Forge/Env_Road_Wet" {
    Properties {
        _Diffuse ("Diffuse", 2D) = "white" {}
        _Specular ("Specular", 2D) = "white" {}
        _Gloss ("Gloss", Float ) = 1
        _Cubemap ("Cubemap", Cube) = "_Skybox" {}
        _Vert ("Vert", Float ) = 0
        _Fresnel ("Fresnel", Range(0, 5)) = 0
        _ReflectonStrength ("ReflectonStrength", Range(0, 5)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform sampler2D _Specular; uniform float4 _Specular_ST;
            uniform float _Gloss;
            uniform samplerCUBE _Cubemap;
            uniform float _Vert;
            uniform float _Fresnel;
            uniform float _ReflectonStrength;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = gloss;
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float4 _Specular_var = tex2D(_Specular,TRANSFORM_TEX(i.uv0, _Specular));
                float3 specularColor = _Specular_var.rgb;
                float specularMonochrome = max( max(specularColor.r, specularColor.g), specularColor.b);
                float normTerm = (specPow + 8.0 ) / (8.0 * Pi);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*normTerm*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float4 _Diffuse_var = tex2D(_Diffuse,TRANSFORM_TEX(i.uv0, _Diffuse));
                float4 _Cubemap_var = texCUBE(_Cubemap,viewReflectDirection);
                float3 diffuseColor = (((_Diffuse_var.rgb+(_Cubemap_var.rgb*_Specular_var.rgb))*(i.vertexColor.rgb+_Vert))+(_Diffuse_var.rgb*(_Cubemap_var.rgb*(pow((1.0 - dot(viewDirection,i.normalDir)),_Fresnel)*_ReflectonStrength))));
                diffuseColor *= 1-specularMonochrome;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
