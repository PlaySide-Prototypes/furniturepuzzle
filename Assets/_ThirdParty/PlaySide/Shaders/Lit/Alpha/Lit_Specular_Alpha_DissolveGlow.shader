// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.27 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.27;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:False,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:False,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:True,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4013,x:32598,y:32782,varname:node_4013,prsc:2|diff-1444-OUT,spec-5830-A,gloss-4870-OUT,emission-7310-OUT,clip-5412-OUT;n:type:ShaderForge.SFN_Color,id:1304,x:31571,y:32623,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_1304,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2d,id:5830,x:31358,y:32432,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_5830,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Multiply,id:1596,x:31571,y:32432,varname:node_1596,prsc:2|A-5830-RGB,B-9360-RGB;n:type:ShaderForge.SFN_VertexColor,id:9360,x:31358,y:32611,varname:node_9360,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1722,x:31762,y:32601,varname:node_1722,prsc:2|A-1596-OUT,B-1304-RGB;n:type:ShaderForge.SFN_Tex2d,id:189,x:31174,y:32955,ptovrint:False,ptlb:DissolveMask,ptin:_DissolveMask,varname:_MainTex_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_ValueProperty,id:4870,x:32282,y:32783,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:node_4870,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.25;n:type:ShaderForge.SFN_Add,id:3002,x:31933,y:32709,varname:node_3002,prsc:2|A-1722-OUT,B-275-RGB;n:type:ShaderForge.SFN_Color,id:275,x:31571,y:32795,ptovrint:False,ptlb:DissolveColor,ptin:_DissolveColor,varname:_Color_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.5586207,c3:0,c4:1;n:type:ShaderForge.SFN_Slider,id:492,x:31005,y:33196,ptovrint:False,ptlb:Dissolve,ptin:_Dissolve,varname:node_492,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Lerp,id:1444,x:32172,y:32586,varname:node_1444,prsc:2|A-3002-OUT,B-1722-OUT,T-9497-OUT;n:type:ShaderForge.SFN_Lerp,id:2312,x:31509,y:32972,varname:node_2312,prsc:2|A-7271-OUT,B-1733-OUT,T-492-OUT;n:type:ShaderForge.SFN_Vector1,id:7271,x:31174,y:33117,varname:node_7271,prsc:2,v1:1;n:type:ShaderForge.SFN_Subtract,id:1733,x:31340,y:32972,varname:node_1733,prsc:2|A-189-R,B-492-OUT;n:type:ShaderForge.SFN_Vector1,id:965,x:31492,y:33190,varname:node_965,prsc:2,v1:0.025;n:type:ShaderForge.SFN_Add,id:8453,x:31665,y:33115,varname:node_8453,prsc:2|A-492-OUT,B-965-OUT;n:type:ShaderForge.SFN_Step,id:5412,x:31878,y:33125,varname:node_5412,prsc:2|A-9099-OUT,B-8453-OUT;n:type:ShaderForge.SFN_Step,id:9497,x:31878,y:32957,varname:node_9497,prsc:2|A-9099-OUT,B-492-OUT;n:type:ShaderForge.SFN_OneMinus,id:9099,x:31665,y:32957,varname:node_9099,prsc:2|IN-2312-OUT;n:type:ShaderForge.SFN_Multiply,id:7310,x:32371,y:32877,varname:node_7310,prsc:2|A-3002-OUT,B-4461-OUT;n:type:ShaderForge.SFN_OneMinus,id:4461,x:32166,y:32906,varname:node_4461,prsc:2|IN-9497-OUT;proporder:1304-5830-189-4870-275-492;pass:END;sub:END;*/

Shader "PlaySide/Lit/Alpha/Specular_DissolveGlow" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("MainTex", 2D) = "black" {}
        _DissolveMask ("DissolveMask", 2D) = "white" {}
        _Gloss ("Gloss", Float ) = 0.25
        _DissolveColor ("DissolveColor", Color) = (1,0.5586207,0,1)
        _Dissolve ("Dissolve", Range(0, 1)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 2.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _DissolveMask; uniform float4 _DissolveMask_ST;
            uniform float _Gloss;
            uniform float4 _DissolveColor;
            uniform float _Dissolve;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float4 _DissolveMask_var = tex2D(_DissolveMask,TRANSFORM_TEX(i.uv0, _DissolveMask));
                float node_9099 = (1.0 - lerp(1.0,(_DissolveMask_var.r-_Dissolve),_Dissolve));
                clip(step(node_9099,(_Dissolve+0.025)) - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 specularColor = float3(_MainTex_var.a,_MainTex_var.a,_MainTex_var.a);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 node_1722 = ((_MainTex_var.rgb*i.vertexColor.rgb)*_Color.rgb);
                float3 node_3002 = (node_1722+_DissolveColor.rgb);
                float node_9497 = step(node_9099,_Dissolve);
                float3 diffuseColor = lerp(node_3002,node_1722,node_9497);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float3 emissive = (node_3002*(1.0 - node_9497));
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 2.0
            uniform sampler2D _DissolveMask; uniform float4 _DissolveMask_ST;
            uniform float _Dissolve;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 _DissolveMask_var = tex2D(_DissolveMask,TRANSFORM_TEX(i.uv0, _DissolveMask));
                float node_9099 = (1.0 - lerp(1.0,(_DissolveMask_var.r-_Dissolve),_Dissolve));
                clip(step(node_9099,(_Dissolve+0.025)) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
