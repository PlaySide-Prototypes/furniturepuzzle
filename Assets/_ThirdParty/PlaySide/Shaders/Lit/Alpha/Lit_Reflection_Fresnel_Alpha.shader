// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.29 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.29;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:False,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.9490197,fgcg:0.9215687,fgcb:0.8509805,fgca:1,fgde:0.011,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4013,x:33193,y:32719,varname:node_4013,prsc:2|diff-9796-OUT,spec-5174-OUT,alpha-8984-OUT;n:type:ShaderForge.SFN_Color,id:1304,x:31904,y:32369,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_1304,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_VertexColor,id:4227,x:31656,y:32748,varname:node_4227,prsc:2;n:type:ShaderForge.SFN_Multiply,id:4178,x:32242,y:32544,varname:node_4178,prsc:2|A-1304-RGB,B-6308-OUT;n:type:ShaderForge.SFN_Cubemap,id:6176,x:31671,y:32906,ptovrint:False,ptlb:Cubemap,ptin:_Cubemap,varname:node_6176,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,pvfc:0;n:type:ShaderForge.SFN_Multiply,id:3298,x:32424,y:32906,varname:node_3298,prsc:2|A-6176-RGB,B-5581-OUT;n:type:ShaderForge.SFN_Dot,id:8359,x:31573,y:33176,varname:node_8359,prsc:2,dt:0|A-3019-OUT,B-377-OUT;n:type:ShaderForge.SFN_ViewVector,id:3019,x:31386,y:33176,varname:node_3019,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:377,x:31386,y:33314,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:5581,x:32231,y:33176,varname:node_5581,prsc:2|A-1292-OUT,B-4110-OUT;n:type:ShaderForge.SFN_Power,id:1292,x:31985,y:33176,varname:node_1292,prsc:2|VAL-2176-OUT,EXP-7514-OUT;n:type:ShaderForge.SFN_Tex2d,id:9643,x:31656,y:32536,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_9643,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:6308,x:31917,y:32533,varname:node_6308,prsc:2|A-9643-RGB,B-4227-RGB;n:type:ShaderForge.SFN_Add,id:9796,x:32779,y:32658,varname:node_9796,prsc:2|A-4178-OUT,B-387-OUT;n:type:ShaderForge.SFN_OneMinus,id:2176,x:31749,y:33176,varname:node_2176,prsc:2|IN-8359-OUT;n:type:ShaderForge.SFN_Multiply,id:387,x:32602,y:32818,varname:node_387,prsc:2|A-5174-OUT,B-3298-OUT;n:type:ShaderForge.SFN_Multiply,id:5174,x:31938,y:32777,varname:node_5174,prsc:2|A-9643-A,B-4227-A;n:type:ShaderForge.SFN_ValueProperty,id:7514,x:31664,y:33409,ptovrint:False,ptlb:Fresnel,ptin:_Fresnel,varname:node_7514,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:4110,x:31974,y:33388,ptovrint:False,ptlb:ReflectionStrength,ptin:_ReflectionStrength,varname:_Fresnel_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Multiply,id:8984,x:32242,y:32687,varname:node_8984,prsc:2|A-1304-A,B-5174-OUT;proporder:1304-9643-6176-7514-4110;pass:END;sub:END;*/

Shader "PlaySide/Lit/Alpha/Reflection_Fresnel_Mask" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _Cubemap ("Cubemap", Cube) = "_Skybox" {}
        _Fresnel ("Fresnel", Float ) = 0
        _ReflectionStrength ("ReflectionStrength", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform samplerCUBE _Cubemap;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Fresnel;
            uniform float _ReflectionStrength;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = 0.5;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_5174 = (_MainTex_var.a*i.vertexColor.a);
                float3 specularColor = float3(node_5174,node_5174,node_5174);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 diffuseColor = ((_Color.rgb*(_MainTex_var.rgb*i.vertexColor.rgb))+(node_5174*(texCUBE(_Cubemap,viewReflectDirection).rgb*(pow((1.0 - dot(viewDirection,i.normalDir)),_Fresnel)*_ReflectionStrength))));
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor,(_Color.a*node_5174));
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
