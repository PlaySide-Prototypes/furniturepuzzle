// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "PlaySide/Lit/Alpha/Rim" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _NormalMap ("Normal", 2D) = "gray"
        _Spec ("Spec", 2D) = "white" {}
        _RimPower ("RimPower", Float ) = 0.5
        _RimColor ("RimColor", Color) = (0.5,0.5,0.5,1)
        [Enum(UnityEngine.Rendering.BlendMode)] _SrcBlend ("Source Blend Mode", Float) = 5.0 // SrcAlpha
        [Enum(UnityEngine.Rendering.BlendMode)] _DstBlend ("Dest Blend Mode", Float) = 10.0 // OneMinusSrcAlpha
        [Enum(UnityEngine.Rendering.CullMode)] _CullMode ("Cull Mode", Float) = 2.0 // Back
        [Enum(UnityEngine.Rendering.CompareFunction)] _ZTest ("Z Test", Float) = 4.0 // LessEqual
    }
    SubShader {
        
        Tags {
          "RenderType"="Transparent" "Queue" = "Transparent" "IgnoreProjector" = "True"
        }

        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }

            ZTest [_ZTest]
            ZWrite Off
            AlphaTest Off
            Blend [_SrcBlend] [_DstBlend]
            Cull [_CullMod]
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0

            // Lighting
            uniform half4 _LightColor0;

            // Rim
            uniform half _Gloss;
            uniform half _RimPower;
            uniform half4 _RimColor;

            // Textures
            uniform half4 _MainTex_ST;
            uniform sampler2D _MainTex;
            uniform sampler2D _Spec;
            uniform sampler2D _NormalMap;


            struct VertexInput {
                float4 vertex : POSITION;
				fixed4 color : COLOR;
				fixed3 normal : NORMAL;
				fixed4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };

            struct VertexOutput {
                float4 pos : SV_POSITION;
                fixed4 color : COLOR;
                float2 uv0 : TEXCOORD0;
				half3 posWorld : TEXCOORD1;
				half3 tangentWorld : TEXCOORD2;
				half3 normalWorld : TEXCOORD3;
				half3 binormalWorld : TEXCOORD4;
                LIGHTING_COORDS(5,6) // Required for Light and Shadow
            };

            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;

                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv0 = v.texcoord0;
                o.color = v.color;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);

                o.tangentWorld = normalize(mul(unity_ObjectToWorld, half4(v.tangent.xyz, 0.0)).xyz);
                o.normalWorld = normalize( mul(half4(v.normal, 0.0), unity_WorldToObject).xyz);
                o.binormalWorld = normalize( cross(o.normalWorld, o.tangentWorld) * v.tangent.w);

                TRANSFER_VERTEX_TO_FRAGMENT(o) // Required for Shadow
                return o;
            }

			half4 frag(VertexOutput i) : COLOR
            {
                // Textures
                half4 diffuse = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
				half4 specular = tex2D(_Spec,TRANSFORM_TEX(i.uv0, _MainTex));
				half3 normalmap = UnpackNormal(tex2D(_NormalMap, TRANSFORM_TEX(i.uv0, _MainTex))).rgb;

                // Vectors
				half3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
				half3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				half3 halfDirection = normalize(viewDirection+lightDirection);

                // World space tangent 
				half3x3 local2WorldTranspose = half3x3(i.tangentWorld, i.binormalWorld, i.normalWorld);
				half3 normalDir = normalize(mul(normalmap, local2WorldTranspose));

                // Lighting:
				half attenuation = LIGHT_ATTENUATION(i);
				half3 attenColor = attenuation * _LightColor0.xyz;
				half NdotL = max(0, dot( normalDir, lightDirection ));
				half3 indirectDiffuse = UNITY_LIGHTMODEL_AMBIENT.rgb;
				half3 directDiffuse = max( 0.0, NdotL) * attenColor;

                // Specular
				half specPow = exp2(specular.a * 10.0 + 1.0);
                specular.rgb *= attenColor * pow(max(0,dot(halfDirection, normalDir)),specPow);

                // Rim
				half3 rimBase = saturate(1.0 - dot(normalDir, reflect(-viewDirection, normalDir))) * _RimColor.rgb;
				half3 rim = pow(rimBase, _RimPower);

                // Final color
                diffuse.rgb = diffuse.rgb*i.color.rgb*(directDiffuse + indirectDiffuse) + (specular + rim) * diffuse.a;

                return diffuse;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
}