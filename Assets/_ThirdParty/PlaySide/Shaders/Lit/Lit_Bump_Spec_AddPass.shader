﻿Shader "PlaySide/Lit/Bump_Emissive"
{
	Properties
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
		_Shininess ("Shininess", Range (0.03, 1)) = 0.078125
		_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
		_BumpMap ("Normalmap", 2D) = "bump" {}
		_Overlay ("Overlay Map", 2D) = "black" {}
		_OverlayAmount ("Overlay Amount", Range (0, 1)) = 1.0
	}

	SubShader
	{ 
		Tags { "RenderType"="Opaque" }
		LOD 400
		
		CGPROGRAM
		#pragma surface surf BlinnPhong
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _BumpMap;
		sampler2D _Overlay;
		fixed4 _Color;
		fixed _OverlayAmount;
		half _Shininess;

		struct Input {
			float2 uv_MainTex;
			float2 uv_Overlay;
			float4 color : COLOR;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
			fixed4 overlay = tex2D(_Overlay, IN.uv_Overlay);
			tex.rgb *= _Color.rgb * IN.color.rgb;
			tex.rgb += overlay*overlay.a*_OverlayAmount;
			o.Albedo = tex.rgb;
			o.Emission = overlay*overlay.a*_OverlayAmount;
			o.Gloss = tex.a;
			o.Alpha = tex.a * _Color.a * IN.color.a;
			o.Specular = _Shininess;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
		}
		ENDCG
	}
FallBack "Legacy Shaders/Specular"
}
