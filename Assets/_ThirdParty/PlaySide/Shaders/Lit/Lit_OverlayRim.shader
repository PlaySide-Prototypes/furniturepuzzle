﻿Shader "PlaySide/Lit/OverlayWrap_Rim" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Overlay ("Overlay Map", 2D) = "black" {}
		_OverlayAmount ("Overlay Amount", Range (0, 1)) = 1.0
		_RimPower ("RimPower", Float ) = 0.5
   		_RimColor ("RimColor", Color) = (0.5,0.5,0.5,1)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Overlay;

		struct Input {
			float2 uv_MainTex;
			float2 uv2_Overlay;
			float4 screenPos;
			float3 worldNormal;
			float3 viewDir;
			INTERNAL_DATA
		};

		fixed4 _Color;
		fixed4 _RimColor;
		half _RimPower;
		fixed _OverlayAmount;
		float2 OverlayOffset;

		void surf (Input IN, inout SurfaceOutput  o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;

			// Screen space UV with edge
			//o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));	
			float2 screenUV = IN.screenPos.xy;// / IN.screenPos.w;
			float edges = pow(dot (normalize(IN.viewDir), o.Normal ),1/_RimPower);

		    // Add Overlay
		    screenUV += abs(o.Normal).xy*0.0005;
//		    screenUV.xy = IN.uv2_Overlay.xy;
			fixed4 overlay = tex2D(_Overlay, screenUV.xy + IN.uv2_Overlay);
			overlay = overlay*overlay.a * _OverlayAmount * (1-edges);

			half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
    		o.Emission = _RimColor.rgb * pow (rim, _RimPower)-overlay.r;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
