// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "PlaySide/Effect/ScreenBlood"
{
	Properties
	{
		_MainTex ("Base", 2D) = "" {}
		_Blood ("Blood Mask", 2D) = "" {}
		//_BumpMap ("Normalmap", 2D) = "bump" {}
		//_BlendAmount("Blend Amount", Float) = 1
		//_EdgeSharpness("Sharpness", Float) = 1
		//_Distortion("Distortion", Float) = 1
	}
	
		Subshader{
			Tags { "RenderType" = "Transparent" "Queue" = "Transparent" "IgnoreProjector" = "True" }
			Pass {

			ZTest Always Cull Off ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha
			Fog	{ Mode off }

			CGPROGRAM
			#pragma fragmentoption ARB_precision_hint_fastest 
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct v2f
			{
				float4 pos : POSITION;
				float4 color : COLOR;
				float2 uv : TEXCOORD0;
			};

			sampler2D _MainTex;
			sampler2D _Blood;
			//sampler2D _BumpMap; 


			// Use input color to be driven by Raw Image (UI)
			// _BlendAmount = R, _EdgeSharpness = G, Distortion = B
			//float _BlendAmount;
			//float _EdgeSharpness;
			//float _Distortion;

			v2f vert(appdata_full v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.color = v.color;
				o.uv = v.texcoord.xy;
				return o;
			}
			 
			half4 frag(v2f i) : COLOR
			{
				float4 blood = tex2D(_Blood, i.uv);
				float4 mainColor = tex2D(_MainTex, i.uv);
				blood.a = blood.a + (i.color.r * 2 - 1);
				blood.a = saturate(blood.a * i.color.g*2.0 - (i.color.g - 1) * 0.5);

				//Distortion: Expensive, commented out
				//half2 bump = UnpackNormal(tex2D(_BumpMap, i.uv)).rg;
				//float4 mainColor = tex2D(_MainTex, i.uv+bump.rg*blood.a*i.color.b);
				//float4 overlayColor = blood;
				//overlayColor.rgb = mainColor.rgb*(blood.rgb+0.5)*1.2; //overlay
				//blood = lerp(blood,overlayColor,0.3);
				//mainColor.rgb *= 1- blood.a*0.5; //inner-shadow border

				mainColor = lerp(mainColor, blood, blood.a);
				return half4(mainColor.rgb, blood.a);
			}
			ENDCG
		}
	}
	Fallback off	
} 