Shader "PlaySide/UI/Sheen"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		[PerRendererData] _Color ("Color", Color) = (1,1,1,1)	
		_Sheen ("Sheen Texture", 2D) = "black" {}
		_SheenColor ("Sheen Color", Color) = (1,1,1,1)	
		_SheenSpeed ("Sheen Speed", vector) = (1.0, 1.0, 1.0, 1.0)
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0

		[Enum(UnityEngine.Rendering.BlendMode)] _SrcBlend ("Source Blend Mode", Float) = 5.0 // SrcAlpha
		[Enum(UnityEngine.Rendering.BlendMode)] _DstBlend ("Dest Blend Mode", Float) = 10.0 // OneMinusSrcAlphaal
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}
		
		Stencil
		{
			Ref [_Stencil]
			Comp [_StencilComp]
			Pass [_StencilOp] 
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
		}

		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend [_SrcBlend] [_DstBlend]
		ColorMask [_ColorMask]

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

			#pragma multi_compile __ UNITY_UI_ALPHACLIP
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float4 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				fixed4 texcoord  : TEXCOORD0;
				float4 worldPosition : TEXCOORD1;
			};
			
			fixed4 _Color;
			fixed4 _TextureSampleAdd;
			float4 _ClipRect;
			fixed4 _SheenColor;
			float4 _SheenSpeed;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.worldPosition = IN.vertex;
				OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

				OUT.texcoord = IN.texcoord;

				OUT.color = IN.color * _Color;

				// Scroll Sheen
				float xSheen = frac((_Time.y+ IN.color.r) * _SheenSpeed.x) * (1+_SheenSpeed.w )-.5;
				float ySheen = frac((_Time.y+ IN.color.r) * _SheenSpeed.y) * (1+_SheenSpeed.w )-.5;
				OUT.texcoord.zw = OUT.texcoord.xy + float2(xSheen, ySheen);

				// Rotate sheen texture by _SheenSpeed.z radians(?)
				float s = sin (radians(_SheenSpeed.z));
				float c = cos (radians(_SheenSpeed.z));
				float2x2 rotationMatrix = float2x2( c, -s, s, c);
				OUT.texcoord.zw = mul ( OUT.texcoord.zw - 0.5, rotationMatrix );

				#ifdef UNITY_HALF_TEXEL_OFFSET
				OUT.vertex.xy += (_ScreenParams.zw-1.0)*float2(-1,1);
				#endif
				

				return OUT;
			}

			sampler2D _MainTex;
			sampler2D _Sheen;

			fixed4 frag(v2f IN) : SV_Target
			{
				// IN.color.rgb is used to offset scroll so dunn worry abaut it
				// ------------------------------------------

				half4 color = (tex2D(_MainTex, IN.texcoord.xy) + _TextureSampleAdd);
				half4 sheen = tex2D(_Sheen, IN.texcoord.zw) * _SheenColor;

				// Add Sheen
				color.rgb += sheen.rgb * sheen.a;

				// Clip alpha to sheen and diffuse alpha intersection
				color.a *= IN.color.a * sheen.a;

				color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
				
				#ifdef UNITY_UI_ALPHACLIP
				clip (color.a - 0.001);
				#endif

				return color;
			}
		ENDCG
		}
	}
}
