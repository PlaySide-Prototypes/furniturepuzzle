﻿// Used with textures/raw images/traditional material
Shader "PlaySide/UI/Vignette_Texture"
{
	Properties
	{
		[PerRendererData] _MainTex ("Texture", 2D) = "white" {}
		[PerRendererData] _Color ("Color", Color) = (1,1,1,1)
		_Intensity ("Intensity", Float) = 1
		_Pinch ("Pinch", Float) = 1

		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
		[Enum(UnityEngine.Rendering.BlendMode)] _SrcBlend ("Source Blend Mode", Float) = 5 // SrcAlpha
		[Enum(UnityEngine.Rendering.BlendMode)] _DstBlend ("Dest Blend Mode", Float) = 10 // OneMinusSrcAlpha
	}

	Subshader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}
		
		Stencil
		{
			Ref [_Stencil]
			Comp [_StencilComp]
			Pass [_StencilOp] 
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
		}

		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend [_SrcBlend] [_DstBlend] //Blend Zero SrcColor
		ColorMask [_ColorMask]
	 	Pass
	 	{
			ZTest Always Cull Back ZWrite Off
			Fog { Mode off }      

			CGPROGRAM
			#pragma fragmentoption ARB_precision_hint_fastest 
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct v2f {
				float4 pos : POSITION;
				float2 texcoord : TEXCOORD0;
				float4 color : COLOR;
			};
			
			sampler2D _MainTex;
			fixed _Intensity;
			fixed _Pinch;
			float4 _Color;

			v2f vert( appdata_full v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.texcoord = v.texcoord.xy;
				o.color = v.color;

				return o;
			} 
				
			half4 frag(v2f IN) : COLOR
			{
				fixed4 color = tex2D(_MainTex, IN.texcoord)*IN.color;
				half2 coords = IN.texcoord;

				coords = (coords - 0.5) * 2.0;		
				half coordDot = dot (coords,coords);

				float mask = 1-pow(1.0 - coordDot * _Pinch, (_Intensity*coordDot) ); 
				color.a *= mask;

				return color;
			}
			ENDCG 
	  	}
	}
} 