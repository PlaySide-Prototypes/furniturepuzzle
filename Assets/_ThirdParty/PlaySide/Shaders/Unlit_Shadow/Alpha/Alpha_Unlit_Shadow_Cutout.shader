Shader "PlaySide/Unlit_Shadow/Alpha/Cutout"
{
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
	}

	SubShader
	{
		Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="TransparentCutout"}
		LOD 100
	
		// Diffuse Pass
		Pass
		{
			Name "Diffuse"
			Tags {  "LightMode" = "ForwardBase" }

			ZWrite On
			Alphatest Greater [_Cutoff]
			AlphaToMask True
			Cull Back
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
			#pragma multi_compile_fog
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"

			sampler2D _MainTex;
			float4 _MainTex_ST;
			uniform float4 _LightColor0; 
			uniform float _Cutoff;

			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR; 
				LIGHTING_COORDS(1,2)
				UNITY_FOG_COORDS(3)
			};

			v2f vert (appdata_full v)
			{				
				v2f o;
				o.color = v.color;
				o.pos = UnityObjectToClipPos(v.vertex );
				o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
				TRANSFER_VERTEX_TO_FRAGMENT(o);
				UNITY_TRANSFER_FOG(o,o.pos);
				return o;
			}
			
			half4 frag (v2f i) : COLOR
			{
				half3 shadow = 1+(SHADOW_ATTENUATION(i)-1) * (SHADOW_ATTENUATION(i)-1) * (UNITY_LIGHTMODEL_AMBIENT.rgb-1);
				half4 col = tex2D(_MainTex, i.uv.xy)* i.color;
				col.rgb *= shadow;
				UNITY_APPLY_FOG(i.fogCoord, col);
				clip( col.a * i.color.a - _Cutoff );

				return col;
			}
			ENDCG
		}

		// Shadow Caster Pass
		Pass
		{
			Name "Caster"
			Tags { "LightMode" = "ShadowCaster" }
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_shadowcaster
			#include "UnityCG.cginc"
 
			struct v2f { 
				float4 color : COLOR; 
				V2F_SHADOW_CASTER;
				float2  uv : TEXCOORD1;
			};

			uniform float4 _MainTex_ST;

			v2f vert( appdata_full v )
			{
				v2f o;
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.color = v.color;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET(o)
				
				return o;
			}

			uniform sampler2D _MainTex;
			uniform fixed _Cutoff;
			uniform fixed4 _Color;

			float4 frag( v2f i ) : SV_Target
			{
				fixed4 col = tex2D( _MainTex, i.uv );
				clip( col.a * i.color.a - _Cutoff );
				
				SHADOW_CASTER_FRAGMENT(i)
			}
			ENDCG
		}
	}
}