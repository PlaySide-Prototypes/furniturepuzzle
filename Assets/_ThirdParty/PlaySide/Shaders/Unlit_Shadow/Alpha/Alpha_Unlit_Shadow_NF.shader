Shader "PlaySide/Unlit_Shadow/Alpha/Diffuse_NF"
{
   Properties {
	  _MainTex ("Texture", 2D) = "white" {}
      [Enum(UnityEngine.Rendering.BlendMode)] _SrcBlend ("Source Blend Mode", Float) = 5.0 // SrcAlpha
      [Enum(UnityEngine.Rendering.BlendMode)] _DstBlend ("Dest Blend Mode", Float) = 10.0 // OneMinusSrcAlpha
      [Enum(UnityEngine.Rendering.CullMode)] _CullMode ("Cull Mode", Float) = 2.0 // Back
      [Enum(UnityEngine.Rendering.CompareFunction)] _ZTest ("Z Test", Float) = 4.0 // LessEqual
	}
	
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue" = "Transparent" "IgnoreProjector" = "True" }
		Pass
		{
			Name "Diffuse"
			Tags {  "LightMode" = "ForwardBase" }
			ZTest [_ZTest]
			ZWrite Off
			AlphaTest Off
			Blend [_SrcBlend] [_DstBlend]
			Cull [_CullMode]
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"

			uniform float4 _LightColor0; 
			sampler2D _MainTex;
			float4 _MainTex_ST;
			 
			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR; 
				LIGHTING_COORDS(1,2)
			};

			v2f vert (appdata_full v)
			{				
				v2f o;
				o.pos = UnityObjectToClipPos (v.vertex); 
				o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
				o.color = v.color;
				TRANSFER_VERTEX_TO_FRAGMENT(o);
				return o;
			}
			
			half4 frag (v2f i) : COLOR
			{
				half3 shadow = 1+(SHADOW_ATTENUATION(i)-1) * (SHADOW_ATTENUATION(i)-1) * (UNITY_LIGHTMODEL_AMBIENT.rgb-1);
				half4 col = tex2D(_MainTex, i.uv.xy) * i.color ;
				col.rgb*= shadow;

				return col;
			}
			ENDCG
		}

		// Shadow Caster Pass
		Pass
		{
			Name "Caster"
			Tags { "LightMode" = "ShadowCaster" }
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_shadowcaster
			#include "UnityCG.cginc"
 
			struct v2f { 
				float4 color : COLOR; 
				V2F_SHADOW_CASTER;
				float2  uv : TEXCOORD1;
			};

			uniform float4 _MainTex_ST;

			v2f vert( appdata_full v )
			{
				v2f o;
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.color = v.color;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET(o)
				
				return o;
			}

			uniform sampler2D _MainTex;
			uniform fixed _Cutoff;
			uniform fixed4 _Color;

			float4 frag( v2f i ) : SV_Target
			{
				fixed4 col = tex2D( _MainTex, i.uv );
				clip( col.a * i.color.a - _Cutoff );
				
				SHADOW_CASTER_FRAGMENT(i)
			}
			ENDCG
		}
	}
}
