// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "PlaySide/Unlit_Shadow/Rim"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Color", Color) = (1,1,1,1)
		_RimColor ("RimColor", Color) = (0.5,0.5,0.5,1)
		_RimPower ("RimPower", Float ) = 0.5
		[Enum(UnityEngine.Rendering.CullMode)] _CullMode ("Cull Mode", Float) = 2.0 // Back
		[Enum(UnityEngine.Rendering.CompareFunction)] _ZTest ("Z Test", Float) = 4.0 // LessEqual
		[MaterialToggle] _ZWrite ("Z Write", Float) = 1
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "Queue" = "Geometry" }
		Pass
		{
			Tags { "LightMode" = "ForwardBase" }
			ZTest [_ZTest]
			ZWrite [_ZWrite]
			Cull [_CullMode]
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
			#pragma multi_compile_fog
			#include "UnityCG.cginc"
            #include "AutoLight.cginc"

			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
				LIGHTING_COORDS(1,2)
				UNITY_FOG_COORDS(n)
			};

			uniform float4 _LightColor0;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _Color;
			float4 _RimColor;
			float _RimPower;

			v2f vert (appdata_full v)
			{				
				v2f o;
				o.pos = UnityObjectToClipPos (v.vertex);
				o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
				o.color = v.color;

				// Store Rim in vertex color alpha
				o.color.a = dot(UnityObjectToWorldNormal(v.normal),  normalize(_WorldSpaceCameraPos.xyz - mul(unity_ObjectToWorld, v.vertex).xyz));

				TRANSFER_VERTEX_TO_FRAGMENT(o);
				UNITY_TRANSFER_FOG(o,o.pos);
				return o;
			}
			
			half4 frag (v2f i) : COLOR
			{
				half3 shadow = 1+(SHADOW_ATTENUATION(i)-1) * (SHADOW_ATTENUATION(i)-1) * (UNITY_LIGHTMODEL_AMBIENT.rgb-1);
				half4 col = tex2D(_MainTex, i.uv.xy) * i.color  * _Color;
				col.rgb *-  shadow;
				half rim = 1.0 - saturate(i.color.a);
				col.rgb += _RimColor * pow (rim, _RimPower);
				col.a = 1;
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
}
