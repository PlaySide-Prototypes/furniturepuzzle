﻿Shader "PlaySide/Unlit/Dissolve"
{
	Properties
	{
		_Color ("Main Color", Color) = (1, 1, 1,1)
		_MainTex ("Texture", 2D) = "white" {}
		_DissolveColor ("Dissolve Color", Color) = (1, 1, 1,1)
		_DissolveMask ("Dissolve Mask", 2D) = "white" {}
		_DissolveWidth ("Dissolve Width", Float) = 0.1
      	_Dissolve ("Dissolve Amount", Range(0.0, 1.0)) = 0.5
		_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
		[Enum(UnityEngine.Rendering.CullMode)] _CullMode ("Cull Mode", Float) = 2.0 // Back
	}
	SubShader
	{
		Tags { "Queue"="AlphaTest" "RenderType"="TransparentCutout" "IgnoreProjector"="True"}
		Pass
		{
			ZWrite On
			Alphatest Greater [_Cutoff]
			AlphaToMask True
			Cull [_CullMode]

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				float4 color : COLOR;
			};

			sampler2D _MainTex;
			sampler2D _DissolveMask;
			float4 _MainTex_ST;
			float4 _Color;
			float4 _DissolveColor;
			float _DissolveWidth;
			float _Dissolve;
			uniform float _Cutoff;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.color = v.color;
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv) * _Color;
				fixed mask = tex2D(_DissolveMask, i.uv);
				float fade = 1 - ((_Dissolve * 1.2) - 0.1);

				col.rgb *= i.color.rgb;
				col.a *= i.color.a;

				//Brighten dissolve border
				if(mask < fade + _DissolveWidth * 0.5)
					col.rgb += _DissolveColor/(1-fade);
				else if(mask < fade + _DissolveWidth)
					col.rgb += _DissolveColor * mask;

//				clip((mask - fade) - (col.a) );
//				col.a *= i.color.a;
				if(saturate((mask - fade)*col.a )< 0.01)
					col.a = 0;
				else
					col.a = 1;

				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);

				return col;
			}
			ENDCG
		}
	}
//	Fallback "Transparent/Cutout/Diffuse"
}
