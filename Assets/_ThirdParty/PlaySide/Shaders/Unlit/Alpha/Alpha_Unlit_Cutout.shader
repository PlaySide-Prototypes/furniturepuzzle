// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "PlaySide/Unlit/Alpha/Cutout" {
   Properties {
	  _MainTex ("Texture", 2D) = "white" {}
      [Enum(UnityEngine.Rendering.CullMode)] _CullMode ("Cull Mode", Float) = 2.0 // Back
      _Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
	}
	
	SubShader {
	  Tags { "Queue"="AlphaTest" "RenderType"="TransparentCutout" "IgnoreProjector"="True"}
	 Pass
		{
			ZWrite On
			Alphatest Greater [_Cutoff]
			AlphaToMask True
			Cull Back
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float4 _MainTex_ST;
			uniform float _Cutoff;
			 
			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR; 
				UNITY_FOG_COORDS(n)
			};

			v2f vert (appdata_full v)
			{				
				v2f o;
				o.pos = UnityObjectToClipPos (v.vertex); 
				o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
				o.color = v.color;
				UNITY_TRANSFER_FOG(o,o.pos);
				return o;
			}
			
			half4 frag (v2f i) : COLOR
			{
				half4 col = tex2D(_MainTex, i.uv.xy) * i.color;

				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);

				// Clip alpha
				clip (col.a - _Cutoff);

				return col;
			}
			ENDCG
		}
	}
	Fallback "Transparent/Cutout/Diffuse"
}
