// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "PlaySide/Unlit/Alpha/Dissolve Clip" {
   Properties {
	  _MainTex ("Texture", 2D) = "white" {}
	  _DissolveMask ("Dissolve Mask", 2D) = "white" {}
	  _DissolveColor ("Dissolve Color", Color) = (1, 1, 1,1)
      [Enum(UnityEngine.Rendering.BlendMode)] _SrcBlend ("Source Blend Mode", Float) = 5.0 // SrcAlpha
      [Enum(UnityEngine.Rendering.BlendMode)] _DstBlend ("Dest Blend Mode", Float) = 10.0 // OneMinusSrcAlpha
      [Enum(UnityEngine.Rendering.CullMode)] _CullMode ("Cull Mode", Float) = 2.0 // Back
      [Enum(UnityEngine.Rendering.CompareFunction)] _ZTest ("Z Test", Float) = 4.0 // LessEqual
	}
	
	SubShader {
	  Tags { "RenderType"="Transparent" "Queue" = "Transparent" "IgnoreProjector" = "True" }
	 Pass
		{
			ZTest [_ZTest]
			ZWrite Off
			AlphaTest Off
			Blend [_SrcBlend] [_DstBlend]
			Cull [_CullMod]
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			sampler2D _DissolveMask;
			float4 _DissolveColor;
			float4 _MainTex_ST;

			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR; 
			};

			v2f vert (appdata_full v)
			{				
				v2f o;
				o.pos = UnityObjectToClipPos (v.vertex); 
				o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
				o.color = v.color;
				return o;
			}
			
			half4 frag (v2f i) : COLOR
			{
				fixed4 tex 	= tex2D(_MainTex, i.uv.xy);
				fixed4 col = float4(1,1,1,1);
				fixed  mask = tex.a*i.color.a - tex2D(_DissolveMask, i.uv).r;

				col.rgb = tex.rgb * i.color.rgb;

				// Brighten dissolve border
				if(saturate(mask) < 0.1)
					col.rgb = _DissolveColor;

				//clip(mask - _Dissolve);
				if(mask < 0.01)
					col.a = 0;
				else
					col.a = 1;

				return col;
			}
			ENDCG
		}
	}
}
