Shader "PlaySide/Unlit/Alpha/Cubemap_Fresnel"
{
    Properties
    {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _FresnelTightness ("Fresnel Tightness", Float ) = 0.5
        _CubeMap ("CubeMap", Cube) = "_Skybox" {}
        _CubeIntensity ("Cubemap Intensity", Float ) = 1
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }

        Pass {
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal
            #pragma target 3.0

            uniform fixed4 _Color;
            uniform fixed _FresnelTightness;
            uniform samplerCUBE _CubeMap;
            uniform fixed _CubeIntensity;

            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR
            {
            	float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );

            	float4 col;
            	float3 emissive = texCUBE(_CubeMap,viewReflectDirection).rgb;
                fixed fresnel = pow(1.0-max(0,dot(normalDirection, viewDirection)),_FresnelTightness);
               
                col.rgb = saturate((1.0-(1.0-_Color.rgb) * (1.0-(emissive * _CubeIntensity))));
               	col.a = _Color.a * fresnel * fresnel * fresnel;

                return col;
            }
            ENDCG
        }
    }
}
