Shader "PlaySide/Unlit/Alpha/Cutout_Reflect"
{
    Properties
    {
        _Diffuse ("Diffuse", 2D) = "white" {}
        _Cubemap ("Cubemap", Cube) = "_Skybox" {}
        _FresnelStrength ("Fresnel Strength", Float ) = 1
        _HorizonAngle ("Horizon Angle", Float ) = 1
        _CubemapStrength ("Cubemap Strength", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
        [Enum(UnityEngine.Rendering.CullMode)] _CullMode ("Cull Mode", Float) = 2.0 // Back
    }
    SubShader {
        Tags {	"Queue"="AlphaTest" "RenderType"="TransparentCutout"	}
        Pass
        {
			Cull [_CullMode]

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"

            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0

            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform samplerCUBE _Cubemap;
            uniform float _FresnelStrength;
            uniform float _HorizonAngle;
            uniform float _CubemapStrength;

            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 color : COLOR;
            };

            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 color : COLOR;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.color = v.color;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }

            float4 frag(VertexOutput i) : COLOR
            {
            	float4 dif = tex2D(_Diffuse,TRANSFORM_TEX(i.uv0, _Diffuse));
                float4 col = dif * i.color;
                clip(col.a - 0.5);

                // Cubemap
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float reflMask = (pow(1.0-max(0,dot(normalDirection, viewDirection)),_HorizonAngle)*_FresnelStrength);

                // Add Cubemap overlay, mask by diffuse alpha
                col.rgb += ( ((texCUBE(_Cubemap,viewReflectDirection).rgb * _CubemapStrength) * reflMask) + (reflMask*0.03) ) * dif.a;

                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
