﻿Shader "PlaySide/Puppet/RimGlow" 
{
	Properties {
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	    _MaskTex ("Mask Texture", 2D) = "white" {} // R Tint Mask, G Rim mask, B unused, A unused
	    _TintColor ("Color", Color) = (1,1,1,1)
        _MaskColor ("Tint", Color) = (1,1,1,1)
        _LightColor ("Light Color", Color) = (1,1,1,0)
        _Light2Color ("Light2 Color", Color) = (1,1,1,0)
        _AddColor ("Additive Color", Color) = (1,1,1,0)
	}

	SubShader {
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		LOD 100
		
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha 
		
		Pass {  
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile_fog
				
				#include "UnityCG.cginc"

				struct appdata_t {
					float4 vertex : POSITION;
					float2 texcoord : TEXCOORD0;
				};

				struct v2f {
					float4 vertex : SV_POSITION;
					half2 texcoord : TEXCOORD0;
					UNITY_FOG_COORDS(1)
				};

				sampler2D _MainTex;
				float4 _MainTex_ST;
	            sampler2D _MaskTex;
	            float4 _LightColor;
	            float4 _Light2Color;
	            float4 _AddColor;
	            fixed4 _TintColor;
	            fixed4 _MaskColor;
				
				v2f vert (appdata_t v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
					UNITY_TRANSFER_FOG(o,o.vertex);
					return o;
				}
				
				fixed4 frag (v2f i) : SV_Target
				{
					fixed4 c = tex2D(_MainTex, i.texcoord);
					fixed4 m = tex2D(_MaskTex, i.texcoord);
					
					c.rgb *= _TintColor.rgb * (_MaskColor.rgb * m.r + (1-m.r) );

					// Add Light
	                c.rgb += (_LightColor.rgb * _LightColor.a * m.g) + (_Light2Color.rgb * _Light2Color.a * m.b);

	                // Add Flash
	                c.rgb += _AddColor.rgb * _AddColor.a * (c.a);
	                
	                // Add Cheeky Alphaf
	                c.a = _TintColor.a * c.a;



	                // Mask by original sprite alpha
					UNITY_APPLY_FOG(i.fogCoord, c);
					return c;
				}
			ENDCG
		}
	}
}
