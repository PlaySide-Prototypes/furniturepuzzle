﻿Shader "PlaySide/Puppet/RimGlow_Masked" 
{
	Properties {
		[PerRendererData] _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	    _MaskTex ("Mask Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)
        _RimColor ("Rim Color", Color) = (1,1,1,0)
        _FlashColor ("Flash Color", Color) = (1,1,1,0)
	}

	SubShader {

        Tags
        {
            "Queue"="Transparent+2"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }
 
        Cull Off
        Lighting Off
        ZWrite Off
        Fog { Mode Off }
        Blend One OneMinusSrcAlpha
		
		
		Pass {  

			Stencil
            {
                Ref 1
                Comp Equal
            }

			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile_fog
				
				#include "UnityCG.cginc"

				struct appdata_t {
					float4 vertex : POSITION;
					float2 texcoord : TEXCOORD0;
				};

				struct v2f {
					float4 vertex : SV_POSITION;
					half2 texcoord : TEXCOORD0;
				};

				sampler2D _MainTex;
				float4 _MainTex_ST;
	            sampler2D _MaskTex;
	            float4 _RimColor;
	            float4 _FlashColor;
	            fixed4 _Color;
				
				v2f vert (appdata_t v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
					return o;
				}
				
				fixed4 frag (v2f i) : SV_Target
				{
					fixed4 c = tex2D(_MainTex, i.texcoord) * _Color;
					fixed4 m = tex2D(_MaskTex, i.texcoord);

					// Add Rim
	                c.rgb += _RimColor.rgb * _RimColor.a * m.r;

	                // Add Flash
	                c.rgb += _FlashColor.rgb * _FlashColor.a * (c.a);
	                c.rgb *= c.a;
					return c;
				}
			ENDCG
		}
	}
}
