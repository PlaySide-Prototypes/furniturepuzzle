// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "PlaySide/VertexColor/Unlit_Gradient"
{
	Properties
	{
		_ColorA ("Tint Dark", Color) = (0,0,0,0)
		_ColorB ("Tint Light", Color) = (1,1,1,1)
		[Enum(UnityEngine.Rendering.CullMode)] _CullMode ("Cull Mode", Float) = 2.0 // Back
		[Enum(UnityEngine.Rendering.CompareFunction)] _ZTest ("Z Test", Float) = 4.0 // LessEqual
		[MaterialToggle] _ZWrite ("Z Write", Float) = 1
	}
	
	SubShader
	{
		Tags { "RenderType"="Opaque" "Queue" = "Geometry" }
		Pass
		{
			Cull [_CullMode]
			ZTest [_ZTest]
			ZWrite [_ZWrite]
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			//https://www.wikiwand.com/en/Relative_luminance
			const float3 _toLuminance = float3(0.2126, 0.7152, 0.0722);
			fixed4 _ColorA;
			fixed4 _ColorB;
//
//			struct appdata {
//				float4 position : POSITION;
//				float4 colour : COLOR;
//			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float4 color : COLOR;
				UNITY_FOG_COORDS(0)
			};

			v2f vert (appdata_full v)
			{				
				v2f o;
				o.pos = UnityObjectToClipPos (v.vertex);
				o.color = v.color;
				UNITY_TRANSFER_FOG(o,o.pos);
				return o;
			}
			
			half4 frag (v2f i) : COLOR
			{
//				float lum = dot(i.colour.rgb, _toLuminance);
				float lum = (i.color.r*0.2126 + i.color.g* 0.7152 + i.color.b*0.0722);
				half4 col = lerp(_ColorA, _ColorB, lum);
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
