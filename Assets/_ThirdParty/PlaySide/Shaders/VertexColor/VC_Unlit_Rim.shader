// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "PlaySide/VertexColor/Unlit_Rim"
{
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_RimColor ("RimColor", Color) = (0.5,0.5,0.5,1)
		_RimPower ("RimPower", Float ) = 0.5
		[Enum(UnityEngine.Rendering.CullMode)] _CullMode ("Cull Mode", Float) = 2.0 // Back
		[Enum(UnityEngine.Rendering.CompareFunction)] _ZTest ("Z Test", Float) = 4.0 // LessEqual
		[MaterialToggle] _ZWrite ("Z Write", Float) = 1
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "Queue" = "Geometry" }
		Pass
		{
			ZTest [_ZTest]
			ZWrite [_ZWrite]
			Cull [_CullMode]
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog
			#include "UnityCG.cginc"

			struct v2f {
				float4 pos : SV_POSITION;
				float4 color : COLOR;
				UNITY_FOG_COORDS(n)
			};

			float4 _Color;
			float4 _RimColor;
			float _RimPower;

			v2f vert (appdata_full v)
			{				
				v2f o;
				o.pos = UnityObjectToClipPos (v.vertex);
				o.color = v.color;
				o.color.a = dot(UnityObjectToWorldNormal(v.normal),  normalize(_WorldSpaceCameraPos.xyz - mul(unity_ObjectToWorld, v.vertex).xyz));
				UNITY_TRANSFER_FOG(o,o.pos);
				return o;
			}
			
			half4 frag (v2f i) : COLOR
			{
				half4 col = i.color * _Color;
				half rim = 1.0 - saturate(i.color.a);
				col.rgb += _RimColor * pow (rim, _RimPower);
				col.a = 1;
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
