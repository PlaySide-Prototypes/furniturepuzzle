﻿using UnityEngine;
using System.Collections.Generic;
[CreateAssetMenu(fileName = "New Level Container", menuName = "Levels/New Level Container", order = 2)]
public class LevelContainer : ScriptableObject
{
    [HideInInspector] public CustomLevelSkybox[] levelSkyboxes;

    [System.Serializable]
    public class CustomLevelSkybox
    {
        public SkyboxType skybox;
        public Material skyboxMaterial;
    }

	public List<Level> levels = new List<Level>();

}
