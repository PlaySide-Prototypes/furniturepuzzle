﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTransitionFader : MonoBehaviour
{
    [Header("Level Fade Animations")]
    public Animator levelFadeAnimation;

    [Header("Fade Time Variables")]
    [SerializeField]
    private float timeBeforeStartingFadeOut = 1.5f;

    public void PerformLevelTransitionFade()
    {
        StartCoroutine(DoLevelTransitionFade());
    } 

    IEnumerator DoLevelTransitionFade()
    {
        yield return new WaitForSeconds(timeBeforeStartingFadeOut);

		//levelFadeAnimation.gameObject.SetActive(true);
		levelFadeAnimation.SetTrigger("Fade");
    }
}
