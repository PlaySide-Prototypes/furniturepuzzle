﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLevel : MonoBehaviour
{
	private Level level;
	public Level Level { get { return level; } }

	public void Init(Level level)
	{
		this.level = level;
	}

	public virtual void StartLevel()
	{
		
	}
	

	
}
