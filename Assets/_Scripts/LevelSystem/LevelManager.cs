﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MoreMountains.NiceVibrations;

public class LevelManager : MonoBehaviour
{
    #region Singleton
    public static LevelManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of GameManager found!");
        }

        instance = this;

        levelTransitionFader = this.GetComponent<LevelTransitionFader>();


        if (PlayerDataManager.instance != null)
        {
            currentLevel = PlayerDataManager.instance.LoadPlayersCurrentLevel();
        }
    }
    #endregion

    [Header("Active Levels")]
    [SerializeField]
    private LevelContainer levelContainer;
	public LevelContainer LevelContainer { get { return levelContainer; } }


	public delegate void OnLevelLoaded(Level l);
	public OnLevelLoaded onLevelLoaded;

    [Header("Level Change Timers")]
    [SerializeField]
    private float timeBeforeLoadingNextLevel = 2.25f;
    [SerializeField]
    private float timeBeforeReloadingCurrentLevel = 2.25f;

	private int maxLevel;
	public int MaxLevel { get { return maxLevel; } set { maxLevel = value; } }

    private int currentLevel;
    private SkyboxType currentSkybox;

    // Gets reference in Awake.
    private LevelTransitionFader levelTransitionFader;

    private void Start()
    {
		maxLevel = PlayerDataManager.instance.LoadPlayersMaxLevel();
      // LoadLevelOnGameStart();
    }

    #region Getters
    // Public accessor to access what the current level index is without the risk of overwriting it.
    public int GetCurrentLevelIndex()
    {
        return currentLevel;
    }

	

	// Public accessor to access what the current level is without the risk of overwriting it.
	public Level GetCurrentLevel()
    {
        return levelContainer.levels[currentLevel];
    }
	// Public accessor to access what the current level is without the risk of overwriting it.
	public GameObject GetCurrentLevelObject()
	{
		return levelContainer.levels[currentLevel].SpawnedLevel;
	}
	#endregion

	#region Public Methods
	public void RetryCurrentLevel()
    {
        levelTransitionFader.PerformLevelTransitionFade();

        StartCoroutine(ReloadCurrentLevel());
    }

    public void GoToNextLevel()
    {
        levelTransitionFader.PerformLevelTransitionFade();

        StartCoroutine(LoadNextLevel());
    }

	public void GoToLevel(int index)
	{
		levelTransitionFader.PerformLevelTransitionFade();
		PanelManager.Instance.Transition(PanelID.HUD);
		StartCoroutine(LoadLevel(index));
	}

	public void AddCompletedLevel(int index)
	{
		if (index >= maxLevel)
			maxLevel = index + 1;
		else
			maxLevel++;
	}
    #endregion

    #region Private Working Methods
    private void LoadLevelOnGameStart()
    {
        currentLevel = PlayerDataManager.instance.LoadPlayersCurrentLevel();

		GoToLevel(currentLevel);
      
    }

    IEnumerator ReloadCurrentLevel()
    {
        yield return new WaitForSeconds(timeBeforeReloadingCurrentLevel);

        levelContainer.levels[currentLevel].DestroyThisLevel();

        EventManager.TriggerEvent(Events.ResetGame);

        levelContainer.levels[currentLevel].SpawnThisLevel();

		onLevelLoaded?.Invoke(GetCurrentLevel());
        //CheckCurrentSkybox();
    }

	public void UnloadLevel()
	{
		levelContainer.levels[currentLevel].DestroyThisLevel();
	}
    IEnumerator LoadNextLevel()
    {
        yield return new WaitForSeconds(timeBeforeLoadingNextLevel);

        int _currentDisplayLevel = PlayerPrefs.GetInt("CurrentDisplayLevel", 0);
        _currentDisplayLevel++;
        PlayerPrefs.SetInt("CurrentDisplayLevel", _currentDisplayLevel);

        // Destroy the old level.
        levelContainer.levels[currentLevel].DestroyThisLevel();

        // If this is the last level.
        if (currentLevel == levelContainer.levels.Count  - 1)
        {
            currentLevel = 0;

            EventManager.TriggerEvent(Events.ResetGame);

            // Load the new level.
            levelContainer.levels[currentLevel].SpawnThisLevel();
        }

        // If this is NOT the last level.
        else if (currentLevel != levelContainer.levels.Count - 1)
        {
            currentLevel++;
            
            EventManager.TriggerEvent(Events.ResetGame);

            // Load the new level.
            levelContainer.levels[currentLevel].SpawnThisLevel();
        }

		//CheckCurrentSkybox();
		onLevelLoaded?.Invoke(GetCurrentLevel());
		PlayerDataManager.instance.SaveAllData();
    }

	IEnumerator LoadLevel(int index)
	{
		yield return new WaitForSeconds(timeBeforeLoadingNextLevel);

		int _currentDisplayLevel = PlayerPrefs.GetInt("CurrentDisplayLevel", 0);
		_currentDisplayLevel = index;
		PlayerPrefs.SetInt("CurrentDisplayLevel", _currentDisplayLevel);

		if (currentLevel < levelContainer.levels.Count)
		{
			// Destroy the old level.
			levelContainer.levels[currentLevel].DestroyThisLevel();
		}

		// If this is the last level.
		if (currentLevel == levelContainer.levels.Count - 1)
		{
			currentLevel = 0;

			EventManager.TriggerEvent(Events.ResetGame);

			// Load the new level.
			levelContainer.levels[currentLevel].SpawnThisLevel();
		}

		// If this is NOT the last level.
		else if (currentLevel != levelContainer.levels.Count - 1)
		{
			currentLevel = index;

			EventManager.TriggerEvent(Events.ResetGame);

			// Load the new level.
			levelContainer.levels[currentLevel].SpawnThisLevel();
		}

		//CheckCurrentSkybox();

		onLevelLoaded?.Invoke(GetCurrentLevel());

		PlayerDataManager.instance.SaveAllData();
	}

	#endregion

	#region Skybox Adjustments

	void CheckCurrentSkybox()
    {
        // Check if the current skybox is the same as the one that is assinged in the level scirptable.
        if (levelContainer.levels[currentLevel].skybox == currentSkybox)
        {
            // All good we have the same one.
        }

        else
        {
            Debug.Log("Changing skybox...");

            currentSkybox = levelContainer.levels[currentLevel].skybox;

            RenderSettings.skybox = GetSkyboxMaterial(currentSkybox);

        }
    }

    // Returns the skybox material to be used as the current skybox.
    private Material GetSkyboxMaterial(SkyboxType _skybox)
    {
        foreach(LevelContainer.CustomLevelSkybox levelSkybox in levelContainer.levelSkyboxes)
        {
            if (levelSkybox.skybox == _skybox)
            {
                return levelSkybox.skyboxMaterial;
            }
        }

        Debug.LogError("Skybox " + _skybox + " not found!");
        return null;
    }

    #endregion
}
