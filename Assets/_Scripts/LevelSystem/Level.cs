﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level_", menuName = "Levels/New Level", order = 1)]
public class Level : ScriptableObject
{
    [HideInInspector] public bool isBonusLevel = false;
    [Space(10)]
    [HideInInspector] public SkyboxType skybox;

    [Space(10)]

    public GameObject levelPrefabToSpawn;

    private GameObject spawnedLevel;
	public GameObject SpawnedLevel { get { return spawnedLevel; } }

    public GameObject SpawnThisLevel()
    {
        if (levelPrefabToSpawn != null)
        {
            // Spawn in the level prefab and reset it's position.
            spawnedLevel = Instantiate(levelPrefabToSpawn, new Vector3(0, 0, 0), Quaternion.identity);
            spawnedLevel.transform.position = new Vector3(0, 0, 0);
			if(spawnedLevel.GetComponent<GameLevel>())
			{
				spawnedLevel.GetComponent<GameLevel>().Init(this);
			}
			return spawnedLevel;
        }

        else
        {
            Debug.LogError("Level Prefab has not been assigned for this level.");
        }
		return null;
    }

    public void DestroyThisLevel()
    {
        if (spawnedLevel != null)
        {
            Destroy(spawnedLevel);
        }
    }
}
