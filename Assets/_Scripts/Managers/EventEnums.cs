﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public enum Events
    {
        NONE = -1,
        ResetGame,
        StartGame,
        LevelComplete,
        LevelFailed,
        PlayerDataSaved,
        
        //--------
        TOTAL
    }