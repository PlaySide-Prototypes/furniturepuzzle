﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AudioManager
{
    public enum Sound
    {
        LevelComplete,
    }

    private static Dictionary<Sound, float> soundTimerDictionary;

    public static void Initialize()
    {
        //soundTimerDictionary = new Dictionary<Sound, float>();
        //soundTimerDictionary[Sound.PlayerAim] = 0;
    }

    public static void PlayAudio(Sound sound, Vector3 position)
    {
        if (CanPlaySound(sound))
        {
            GameObject soundGameObject = new GameObject("Sound");
            soundGameObject.transform.position = position;

            AudioSource audioSource = soundGameObject.AddComponent<AudioSource>();

            audioSource.clip = GetAudioClip(sound);
            audioSource.Play();

            audioSource.gameObject.AddComponent<DestroyObjectSoon>();
        }
    }

    public static void PlayAudio(Sound sound)
    {
        if (CanPlaySound(sound))
        {
            GameObject soundGameObject = new GameObject("Sound");

            AudioSource audioSource = soundGameObject.AddComponent<AudioSource>();

            audioSource.PlayOneShot(GetAudioClip(sound));

            audioSource.gameObject.AddComponent<DestroyObjectSoon>();
        }
    }

    private static bool CanPlaySound(Sound sound)
    {
        //if (GameManager.instance.canPlaySoundFX == true)
        //{
        //    return true;
        //}

        //else
        //{
        //    return false;
        //}

        return true;

        //switch(sound)
        //{
        //    // Any sound that doesn't have a defined state here will return true (the sound can be played).
        //    default:
        //        return true;
        //    // In the case of a sound that may be triggered from an Update method (naughty boy you are!) then it will do some checks to make sure that it's not ear rape.
        //    //case Sound.PlayerAim:
        //    //    if (soundTimerDictionary.ContainsKey(sound))
        //    //    {
        //    //        float lastTimePlayed = soundTimerDictionary[sound];
        //    //        float playerMoveTimerMax = .05f;
                    
        //    //        if (lastTimePlayed + playerMoveTimerMax < Time.time)
        //    //        {
        //    //            soundTimerDictionary[sound] = Time.time;
        //    //            return true;
        //    //        }

        //    //        else
        //    //        {
        //    //            return false;
        //    //        }
        //    //    }

        //    //    else
        //    //    {
        //    //        return true;
        //    //    }
        //    //break;
        //}
    }

    private static AudioClip GetAudioClip(Sound sound)
    {
        foreach (AudioCollection.SoundAudioClip soundAudioClip in AudioCollection.instance.soundAudioClipArray)
        {
            if (soundAudioClip.sound == sound)
            {
                return soundAudioClip.audioClip;
            }
        }

        Debug.LogError("Sound " + sound + " not found!");
        return null;
    }
}
