﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AdManager : MonoBehaviour
{
	// Iron Source game IDs
	const string iosID = "d91d97a9";
	const string androidID = "d91d97a9";

	public static int InterstitialCooldown = 60;

	static Action intersitialAction;
	static Action rewardedSuccessAction;
	static Action rewardedFailedAction;

	static bool watchedRewardedAd;

	static bool initialized = false;

	static bool waitForRewarded;

    public static DateTime lastInterstitital;
    public static bool watchedAdThisRound = false;

    public static void Init()
    {
        lastInterstitital = DateTime.Now;

        InitIronSourceEvents();

        IronSourceConfig.Instance.setClientSideCallbacks(true);

        if (!initialized)
        {
#if UNITY_IOS
			IronSource.Agent.init(iosID);
#else
            IronSource.Agent.init(androidID);
#endif

            initialized = true;
        }

        //Uncomment these for IronSource Logs
        IronSource.Agent.validateIntegration();
        IronSource.Agent.setAdaptersDebug(true);

        CheckForCredits();
        LoadInterstitial();

        ShowIntersitial(DismissAction);
    }

    private static void InitIronSourceEvents()
    {
        IronSourceEvents.onInterstitialAdClosedEvent += InterstitalComplete;
        IronSourceEvents.onInterstitialAdShowFailedEvent += InterstitialShowFailed;

        IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
        IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedShowFailed;

        IronSourceEvents.onOfferwallAdCreditedEvent += OnOfferwallAdCredited;
        IronSourceEvents.onGetOfferwallCreditsFailedEvent += GetOfferwallCreditsFailedEvent;
        IronSourceEvents.onOfferwallClosedEvent += OfferwallClosedEvent;
        IronSourceEvents.onOfferwallShowFailedEvent += OfferwallShowFailed;

    }

    #region Private Working Methods
    static void RewardedShowFailed(IronSourceError _error)
    {
        if (rewardedFailedAction != null)
        {
            rewardedFailedAction.Invoke();
            rewardedFailedAction = null;
        }
    }

    static void InterstitialShowFailed(IronSourceError _error)
    {
        if (intersitialAction != null)
        {
            intersitialAction.Invoke();
            intersitialAction = null;
        }
        LoadInterstitial();
    }

    static void OfferwallShowFailed(IronSourceError _error)
    {

    }

    private static void InterstitalComplete()
    {
        if (intersitialAction != null)
            intersitialAction.Invoke();

        intersitialAction = null;

        LoadInterstitial();
        lastInterstitital = DateTime.Now;
        watchedAdThisRound = true;
    }

    private static void RewardedVideoAdClosedEvent()
    {
        waitForRewarded = true;

        if (watchedRewardedAd)
        {
            if (rewardedSuccessAction != null)
            {
                rewardedSuccessAction.Invoke();
                rewardedSuccessAction = null;
            }

            lastInterstitital = DateTime.Now;
            watchedAdThisRound = true;
            watchedRewardedAd = false;
        }
        else
        {
            if (rewardedFailedAction != null)
            {
                rewardedFailedAction.Invoke();
                rewardedFailedAction = null;
            }
        }
    }

    private static void RewardedVideoAdRewardedEvent(IronSourcePlacement _placement)
    {
        watchedRewardedAd = true;

        if (waitForRewarded && rewardedSuccessAction != null)
        {
            //UIPopup.Close(); 
            rewardedSuccessAction.Invoke();
            rewardedSuccessAction = null;
            waitForRewarded = false;
        }
    }

    #endregion


    /// <summary>
    /// Shows an Interstitial with a Dismissed Action
    /// </summary>
    public static void ShowIntersitial(Action _dismissedAction)
    {
#if UNITY_EDITOR
        if (Application.isEditor)
        {
            _dismissedAction.Invoke();
            return;
        }
#endif

        // Interstitial Cooldown Disabled
        //if (!IsInterstitialReady())
        //{
        //	_dismissedAction.Invoke();
        //	return;
        //}

        if (IronSource.Agent.isInterstitialReady() == false)
        {
            _dismissedAction.Invoke();
            LoadInterstitial();
            return;
        }

        IronSource.Agent.showInterstitial();

        intersitialAction = _dismissedAction;
    }

    static void DismissAction()
    {

    }

    /// <summary>
    /// Shows an Incentivized with a Suceess Action and Failed Action
    /// </summary>
    public static void ShowIncentivized(string _placementName, Action _suceessAction, Action _failedAction)
    {
#if UNITY_EDITOR
        if (_suceessAction != null)
            _suceessAction.Invoke();
#else

		if (string.IsNullOrEmpty(_placementName))
			_placementName = "DefaultRewardedVideo";

		if (IronSource.Agent.isRewardedVideoAvailable() == false)
		{
			_failedAction.Invoke();
			return;
		}

		IronSource.Agent.showRewardedVideo(_placementName);

		rewardedSuccessAction = _suceessAction;
		rewardedFailedAction = _failedAction;

		waitForRewarded = false;
#endif
    }

    /// <summary>
    /// Load an interstitial from Iron Source Mediation
    /// </summary>
    public static void LoadInterstitial()
    {
        IronSource.Agent.loadInterstitial();
    }

    public static bool IsIncentivisedReady()
    {
#if UNITY_EDITOR
        return true;
#else
		return IronSource.Agent.isRewardedVideoAvailable();
#endif
    }

    public void ShowIntergrationDebug()
    {
        IronSource.Agent.validateIntegration();
    }

    public static void ShowOfferwall()
    {
#if UNITY_EDITOR
        return;
#else
		if (!IsOfferWallReady())
		{
			return;
		}

		IronSource.Agent.showOfferwall();
#endif
    }

    public static bool IsOfferWallReady()
    {
#if UNITY_EDITOR
        return true;
#else
		return IronSource.Agent.isOfferwallAvailable();
#endif
    }

    public static void OnOfferwallAdCredited(Dictionary<string, object> _credits)
    {
        int credits = Int32.Parse((string)_credits["credits"]);

        if (credits < 1)
            return;

        Debug.Log("[OFFERWALL] " + "I got OfferwallAdCreditedEvent, current credits = " + _credits["credits"] + "totalCredits = " + _credits["totalCredits"]);
    }

    public static void GetOfferwallCreditsFailedEvent(IronSourceError error)
    {
        Debug.Log("[OFFERWALL] " + error.getCode());
        Debug.Log("[OFFERWALL] " + error.getDescription());
    }

    private static void OfferwallClosedEvent()
    {
        CheckForCredits();
    }

    public static void ShowBannerAd()
    {
        IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.BOTTOM);
    }

    public static void RemoveBannerAd()
    {
        IronSource.Agent.destroyBanner();
    }

    /// <summary>
    /// Check If there's any unclaimed credits from IronSource Offerwall
    /// </summary>
    public static void CheckForCredits()
    {
        IronSource.Agent.getOfferwallCredits();
    }

    public void ShowInterstitialWithTimerReset()
    {
        //Show interstitial Ad
        Debug.Log("Interstitial ready: " + IronSource.Agent.isInterstitialReady());
        bool interstitialTime = System.DateTime.Now > AdManager.lastInterstitital.AddSeconds(AdManager.InterstitialCooldown);
        Debug.Log("Interstitial time: " + interstitialTime);
        Debug.Log(PlayerPrefs.GetString("InterstitialDate"));
        //if (interstitialTime)
        //{
        if (IronSource.Agent.isInterstitialReady() && PlayerPrefs.GetInt("noAds") == 0)
        {
            //InputBlocker.instance.SetActive(true);
            AdManager.ShowIntersitial(ResetInterstitialTimer);
        }
        //}
    }

    public void ResetInterstitialTimer()
	{
		//InputBlocker.instance.SetActive(false);
		PlayerPrefs.SetString("InterstitialDate", System.DateTime.Now.AddSeconds(AdManager.InterstitialCooldown).ToString());
	}
}
