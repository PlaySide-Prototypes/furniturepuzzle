﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDataManager : MonoBehaviour
{
    #region Singleton
    public static PlayerDataManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of PlayerDataManager found!");
        }

        instance = this;

        if (resetDataOnAwake)
        {
            ResetAllData();
        }
		maxLevel = LoadPlayersMaxLevel();
    }
    #endregion

    public bool resetDataOnAwake = false;

	public int maxLevel;
	
	public void SaveAllData()
    {
        PlayerPrefs.SetInt("CurrentLevel", LevelManager.instance.GetCurrentLevelIndex());
        PlayerPrefs.SetInt("TotalCoins", CoinManager.instance.totalCoins);
		PlayerPrefs.SetInt("MaxLevel", LevelManager.instance.MaxLevel);

		EventManager.TriggerEvent(Events.PlayerDataSaved);
    }

    public int LoadPlayersCurrentLevel()
    {
        return PlayerPrefs.GetInt("CurrentLevel", 0);
    }

	public int LoadPlayersMaxLevel()
	{
		return PlayerPrefs.GetInt("MaxLevel", 0);
	}
	public int LoadPlayersCurrentDisplayLevel()
    {
        return PlayerPrefs.GetInt("CurrentDisplayLevel", 0);
    }

    public int LoadPlayersTotalCoins()
    {
        return PlayerPrefs.GetInt("TotalCoins", 0);
    }

    public void ResetAllData()
    {
        PlayerPrefs.SetInt("CurrentLevel", 0);
        PlayerPrefs.SetInt("TotalCoins", 0);
		PlayerPrefs.SetInt("MaxLevel", 0);
		EventManager.TriggerEvent(Events.PlayerDataSaved);
    }
}
