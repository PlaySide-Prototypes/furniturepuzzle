﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;
using Facebook.Unity;

#if UNITY_PURCHASING
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;
#endif

public class AnalyticManager : MonoBehaviour
{


    void Start()
    {
        GameAnalytics.Initialize();
    }

    private void Awake()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(OnInitComplete);
        }
        else
        {
            FB.ActivateApp();
        }
    }

    // Facebook callback
    void OnInitComplete()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            //Failed to load app
            Debug.LogError("Facebook faild to load");
        }
    }

    private void OnApplicationPause(bool pauseStatus)
    {
        if (!pauseStatus && FB.IsInitialized)
        {
            FB.ActivateApp();
        }
    }


//	//-----------------------------------------------------------------------------
//	//								Tenjin Functions
//	//-----------------------------------------------------------------------------

//	const string tenjinKey = "6WZVWHLZ4SY4UHNHCYBZHQRNYQZUDA1Z";

//	/// <summary>
//	/// Init needs to be called in Awake to work correctly!
//	/// </summary>
//	public static void Init()
//	{
//		GameAnalytics.Initialize();

//		Connect();

//	}

//	public static void SendGameAnalyticEvent(string _name)
//	{
//		GameAnalytics.NewDesignEvent(_name);
//	}

//	public static void SendGameAnalyticEvent(string _name, float _value)
//	{
//		GameAnalytics.NewDesignEvent(_name, _value);
//	}

//	public static void SendGameAnalyticEventWithSessionTime(string _name)
//	{
//		float time = Mathf.Round(Time.time / 60);
//		GameAnalytics.NewDesignEvent(_name, time);
//	}

//	public static void SendGameAnalyticEventWithSessionTimeFTUE(string _name)
//	{
//		float time = Mathf.Round(Time.time / 60);
//		GameAnalytics.NewDesignEvent("FTUE:" + _name, time);
//	}

//	public static void SendProgressionEvent(GAProgressionStatus _status, string _levelType, int _score)
//	{
//		GameAnalytics.NewProgressionEvent(_status, _levelType, _score);
//	}

//	#if UNITY_PURCHASING
//	/// <summary> Wrapper for GA Business Event</summary>
//	public static void SendBusinessEvent(Product _product)
//	{
//		decimal costWithoutCurrency = _product.metadata.localizedPrice;
//		string currencyCode = _product.metadata.isoCurrencyCode;
//		string productID = _product.definition.id;

//		var wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(_product.receipt);
//		if (wrapper != null)
//		{
//			string store = (string)wrapper["Store"];
//			string payload = (string)wrapper["Payload"]; // For Apple this will be the base64 encoded ASN.1 receipt

//			int costInCents = decimal.ToInt32(costWithoutCurrency * 100);
//			double costDouble = decimal.ToDouble(costWithoutCurrency);

//#if UNITY_IOS

//			if (PlayerPrefs.GetInt(payload) != 1)
//			{
//				GameAnalytics.NewBusinessEventIOS(currencyCode, costInCents, Application.productName, productID, "Store", payload);

//				BaseTenjin instance = Tenjin.getInstance(tenjinKey);
//				instance.Transaction(productID, currencyCode, 1, costDouble, _product.transactionID, payload, null);		
//			}

//			PlayerPrefs.SetInt(payload, 1);
//			PlayerPrefs.Save();

//#elif UNITY_ANDROID

//			var gpDetails = (Dictionary<string, object>)MiniJson.JsonDecode(payload);
//			var gpJson    = (string)gpDetails["json"];
//			var gpSig     = (string)gpDetails["signature"];

//			if (PlayerPrefs.GetInt(payload) != 1)
//			{
//				GameAnalytics.NewBusinessEventGooglePlay(currencyCode, costInCents, Application.productName, productID, "Store", gpJson, gpSig);
//				BaseTenjin instance = Tenjin.getInstance(tenjinKey);
//				instance.Transaction(productID, currencyCode, 1, costDouble, null, gpJson, gpSig);
//			}

//			PlayerPrefs.SetInt(payload, 1);
//			PlayerPrefs.Save();
//#endif
//		}
//		else
//			Debug.LogError("Could not validate purchase, '" + _product.receipt + "' gave null JSON");
//	}
//#endif

//	public static void SendTenjinEvent(string _name)
//	{
//		BaseTenjin instance = Tenjin.getInstance(tenjinKey);
//		instance.SendEvent(_name);
//		Debug.Log(_name);
//	}

//	public static void SendTenjinEvent(string _name, string _amount)
//	{
//		BaseTenjin instance = Tenjin.getInstance(tenjinKey);
//		instance.SendEvent(_name, _amount);
//	}

//	public static void Connect()
//	{
//		BaseTenjin instance = Tenjin.getInstance(tenjinKey);
//		instance.Connect();
//	}
}
