﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;

public class CoinManager : MonoBehaviour
{
    #region Singleton
    public static CoinManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of CoinManager found!");
        }

        instance = this;
    }
    #endregion

    [Header("Coins")]
    public int totalCoins;
    public int coinsEarnedThisLevel;

    private void Start()
    {
        LoadCoins();
    }

    #region Save / Load Coins
    void SaveCoins()
    {
        PlayerPrefs.SetInt("TotalCoins", totalCoins);
    }

    void LoadCoins()
    {
        totalCoins = PlayerPrefs.GetInt("TotalCoins", 0);
	}
    #endregion

    #region Add / Remove Coins
    // On level complete, compiles all of the collected coins, saves them to total and clears collected.
    public void AddCurrentLevelCoinsToTotalCoins()
    {
        totalCoins += coinsEarnedThisLevel;

        SaveCoins();
        ResetCurrentLevelCoins();
    }

    // On level reset, resets the collected amount of coins and updates the UI to show this.
    public void ResetCurrentLevelCoins()
    {
        coinsEarnedThisLevel = 0;
    }

    public void GiveCoins(int coins)
    {
        coinsEarnedThisLevel += coins;

        MMVibrationManager.Haptic(HapticTypes.HeavyImpact);
    }

    public void GiveCoinsFromStore(int coins)
    {
        totalCoins += coins;

        MMVibrationManager.Haptic(HapticTypes.HeavyImpact);

        SaveCoins();

	}

    public void RemoveCoins(int coins)
    {
        totalCoins -= coins;

        MMVibrationManager.Haptic(HapticTypes.HeavyImpact);

        SaveCoins();
    }
    #endregion
}
