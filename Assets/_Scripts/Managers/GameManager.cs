﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;

public class GameManager : MonoBehaviour
{
    //#region Event Listener
    //private void OnEnable()
    //{
    //    EventManager.StartListening(Events.ResetGame, ResetGame);
    //    EventManager.StartListening(Events.StartGame, StartGame);
    //    EventManager.StartListening(Events.LevelComplete, LevelComplete);
    //    EventManager.StartListening(Events.LevelFailed, LevelFailed);
    //}

    //private void OnDisable()
    //{
    //    EventManager.StopListening(Events.ResetGame, ResetGame);
    //    EventManager.StopListening(Events.StartGame, StartGame);
    //    EventManager.StopListening(Events.LevelComplete, LevelComplete);
    //    EventManager.StopListening(Events.LevelFailed, LevelFailed);
    //}
    //#endregion

    #region Singleton
    public static GameManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of GameManager found!");
        }

        instance = this;

        AudioManager.Initialize();
    }
    #endregion

    // Player Settings
    [HideInInspector]
    public bool canPlaySoundFX;
    [HideInInspector]
    public bool canPlayHaptics;

    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 60;

        LoadPlayerSettings();
    }

    #region Game State Changes
    public void RetryCurrentLevel()
    {
        CoinManager.instance.ResetCurrentLevelCoins();

        LevelManager.instance.RetryCurrentLevel();
    }

    public void ProgressToNextLevel()
    {
        CoinManager.instance.AddCurrentLevelCoinsToTotalCoins();

        LevelManager.instance.GoToNextLevel();
    }
    #endregion

    #region Player Preference Settings
    public void SavePlayerSettings()
    {
        PlayerPrefsX.SetBool("canPlaySoundFX", canPlaySoundFX);
        PlayerPrefsX.SetBool("canPlayHaptics", canPlayHaptics);
    }

    public void LoadPlayerSettings()
    {
        canPlaySoundFX = PlayerPrefsX.GetBool("canPlaySoundFX", true);
        canPlayHaptics = PlayerPrefsX.GetBool("canPlayHaptics", true);
    }
    #endregion
}
