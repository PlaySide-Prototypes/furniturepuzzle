﻿using System.Collections;
using UnityEngine;

public class DestroyObjectSoon : MonoBehaviour
{
    #region Event Listener
    private void OnEnable()
    {
        EventManager.StartListening(Events.ResetGame, DestroyObject);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.ResetGame, DestroyObject);
    }
    #endregion

    public float timeToDestroy = 3;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartDestroyObject());
    }

    IEnumerator StartDestroyObject()
    {
        yield return new WaitForSeconds(timeToDestroy);

        DestroyObject();
    }

    void DestroyObject()
    {
        Destroy(this.gameObject);
    }
}
