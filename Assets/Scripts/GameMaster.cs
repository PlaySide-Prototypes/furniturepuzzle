﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
	public static GameMaster instance;

	private void Awake()
	{
		if(instance == null)
		{
			instance = this;
		}
	}

	public PNL_HUD panel_Hud;
	public static PNL_HUD Panel_HUD { get { return instance.panel_Hud; } }

	public Camera camera;
}
