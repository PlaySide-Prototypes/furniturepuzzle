﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FurnitureObject : MonoBehaviour
{

	public Vector2 size;

	[HideInInspector]
	public List<GridSlot> coveredSlots;

	[HideInInspector]
	public BoxCollider collider;

	[HideInInspector]
	public Vector3 targetPos;


	public void Start()
	{
		collider = GetComponent<BoxCollider>();
		SetupPrefab();
		targetPos = transform.position;

	}
	public void AddSlot(GridSlot s)
	{
		coveredSlots.Add(s);
		s.objectInSlot = this;
	}


	public void Update()
	{
		if(Vector3.Distance(transform.position, targetPos) > 0.01f)
		{
			transform.position = Vector3.Lerp(transform.position, targetPos, 0.2f);
		}

	
	}
	public void RemoveFromSlots()
	{
		foreach(GridSlot s in coveredSlots)
		{
			s.objectInSlot = null;
		}
		coveredSlots.Clear();
	}

	public void SetupPrefab()
	{
		collider.size = new Vector3(size.x, 1.0f, size.y);
	
		collider.center = new Vector3(0.5f * (size.x - 1), 0.5f, 0.5f * (size.y - 1));
	}


}
