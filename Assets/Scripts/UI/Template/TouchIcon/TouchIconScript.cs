﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchIconScript : MonoBehaviour
{

    public TouchType touchType;

    [Header("Sprites")]
    public Sprite touchDown;
    public Sprite touchUp;
    public Sprite touchSwipe;

    //renderer
    private Image handRen;

    //misc
    private Vector3 touchPos;


    private void Awake()
    {
        if (touchType == TouchType.Follow)
        {
            handRen = transform.Find("HandSprite").GetComponent<Image>();
        }
        else
        {
            handRen = GetComponent<Image>();
        }
    }

    private void Start()
    {
		Cursor.visible = false;
        switch (touchType)
        {
            case TouchType.Tap:
                handRen.sprite = touchUp;
                break;
            case TouchType.Follow:
                handRen.sprite = touchUp;
                break;
            case TouchType.Swipe:
                handRen.sprite = touchSwipe;
                break;
        }
    }

    void Update()
    {
        Vector3 touchPos = transform.position;
        touchPos = Input.mousePosition;

        if (Input.GetMouseButtonDown(0))
        {
            Pressed();       
        }
        if (Input.GetMouseButton(0))
        {
            Hold();
        }
        if (Input.GetMouseButtonUp(0))
        {
            Released();
        }


        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            touchPos = touch.position;
            switch (touch.phase)
            {
                case TouchPhase.Began: Pressed(); break;
                case TouchPhase.Moved:
                case TouchPhase.Stationary: Hold(); break;
                case TouchPhase.Ended: Released(); break;
            }
            
        }

        if (touchType == TouchType.Follow)
        {
            transform.position = Vector3.Lerp(transform.position, touchPos, Time.deltaTime * 20f);
        }
    }

    private void Pressed ()
    {
        switch (touchType)
        {
            case TouchType.Tap:
                handRen.sprite = touchDown;
                break;
            case TouchType.Follow:
                handRen.sprite = touchDown;
                break;
            case TouchType.Swipe:

                break;
        }
    }

    private void Hold ()
    {
        switch (touchType)
        {
            case TouchType.Tap:

                break;
            case TouchType.Follow:
                
                break;
            case TouchType.Swipe:

                break;
        }
    }

    private void Released ()
    {
        switch (touchType)
        {
            case TouchType.Tap:
                handRen.sprite = touchUp;
                break;
            case TouchType.Follow:
                handRen.sprite = touchUp;
                break;
            case TouchType.Swipe:

                break;
        }
    }

}

public enum TouchType { Tap, Follow, Swipe}
