﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridObject : MonoBehaviour
{
	public Color defaultColour, hoveredColour;

	public GridSlot slot;

	public void SetHover(bool value)
	{
		if(value)
		{
			GetComponentInChildren<Renderer>().material.SetColor("_Color", hoveredColour);
		}
		else
		{
			GetComponentInChildren<Renderer>().material.SetColor("_Color", defaultColour);
		}
	}
}
