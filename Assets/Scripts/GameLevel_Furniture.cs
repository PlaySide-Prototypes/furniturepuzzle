﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLevel_Furniture : MonoBehaviour
{
	public Vector2 gridSize = new Vector2(10, 10);
	public Vector2 targetGridPos;
	public FurnitureObject targetFurniture;

	[HideInInspector]
	public Dictionary<Vector2, GridSlot> grid;

	[HideInInspector]
	public List<GridObject> gridObjects;

	public GridObject gridPrefab;
	public Transform gridTransform;

	private LayerMask pickupLayer;
	private LayerMask raycastLayer;
	private LayerMask handLayer;

	[HideInInspector]
	public FurnitureObject selectedFurniture;

	public Transform furnitureTransform;
	[HideInInspector]
	public FurnitureObject[] furniture;

	public int currentMoves;

	public void Start()
	{
		//handanimations.instance.SetHandAnim("Point");
		pickupLayer = LayerMask.GetMask("Pickup");
		raycastLayer = LayerMask.GetMask("Raycast");
		handLayer = LayerMask.GetMask("Hand");
		GenerateGrid();
		furniture = furnitureTransform.GetComponentsInChildren<FurnitureObject>();
		foreach (FurnitureObject f in furniture)
		{
			GridObject o = GetClosestGridToObject(f);
			AddItemToGridSlot(f, o.slot.gridPos);
			f.targetPos = o.transform.position;
		}
		Cursor.visible = false;

	}
	public void GenerateGrid()
	{
		grid = new Dictionary<Vector2, GridSlot>();
		for (int i = 0; i < gridSize.y; i++)
		{
			for (int j = 0; j < gridSize.x; j++)
			{
				GridSlot s = new GridSlot(i, j);
				grid.Add(new Vector2(i, j), s);
				GridObject g = Instantiate(gridPrefab, gridTransform.position + new Vector3(i, 0.0f, j), Quaternion.identity, gridTransform);
				g.slot = s;
				s.gridObject = g;
				gridObjects.Add(g);
			}
		}
	}


	private GridObject hoveredGridSlot;

	public Transform hand;
	public Transform HandGraphic;
	public Vector3 handOffset;

	public Animator DoorAnim;
	public Transform doorHolder;

	private float timer;

	public void Update()
	{
		timer += Time.deltaTime;
		GameMaster.Panel_HUD.UpdateTime(timer);
		Ray ray = GameMaster.instance.camera.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;

		if (Physics.Raycast(ray, out hit, 100f, handLayer))
		{
			hand.transform.position = hit.point + handOffset;
		}
		//move hand to position
		if (Physics.Raycast(ray, out hit, 100f, raycastLayer))
		{
			if (selectedFurniture != null)
			{
				if (hoveredGridSlot == null)
				{
					hoveredGridSlot = hit.collider.GetComponent<GridObject>();
				}
				else if (hoveredGridSlot != null && hit.collider.GetComponent<GridObject>() != hoveredGridSlot)
				{
					hoveredGridSlot = hit.collider.GetComponent<GridObject>();
				}
			}
		}
		else
		{
			if (hoveredGridSlot != null)
			{
				hoveredGridSlot = null;
			}
		}

		if (Physics.Raycast(ray, out hit, 100f, pickupLayer))
		{
			if (Input.GetMouseButtonDown(0) && selectedFurniture == null)
			{
				if (hit.collider.GetComponent<FurnitureObject>())
				{
					handanimations.instance.SetHandAnim("GrabLarge");
					selectedFurniture = hit.collider.GetComponent<FurnitureObject>();
					offset = selectedFurniture.transform.position - hand.transform.position;
					startPos = hand.transform.position;
					handanimations.instance.targetPos = new Vector3(0, -0.1f, 0);
					selectedFurniture.GetComponent<Animator>().SetTrigger("Drag");
				}
			}
		}

		if (Input.GetMouseButtonUp(0))
		{
			if (selectedFurniture != null)
			{

				selectedFurniture.GetComponent<Animator>().SetTrigger("Drop");
				if (CanAddItemToGridSlot(selectedFurniture, GetClosestGridToObject(selectedFurniture).slot.gridPos))
				{
					if (GetClosestGridToObject(selectedFurniture).slot.gridObject != selectedFurniture.coveredSlots[0].gridObject)
					{
						currentMoves++;
						GameMaster.Panel_HUD.UpdateMoves(currentMoves);
					}
					RemovItemFromGridSlot(selectedFurniture);
					AddItemToGridSlot(selectedFurniture, GetClosestGridToObject(selectedFurniture).slot.gridPos);

				}

				handanimations.instance.targetPos = Vector3.zero;
				selectedFurniture = null;
				handanimations.instance.SetHandAnim("Point");
				DragX = false;
				DragY = false;

			}
		}

		if (Input.GetMouseButton(0))
		{
			if (selectedFurniture != null)
			{



				if (DragX)
				{

					float up = 1;
					float down = 1;
					for (int i = 0; i < selectedFurniture.size.y; i++)
					{
						Debug.DrawRay(new Vector3(selectedFurniture.transform.position.x - 0.5f, 0.5f, selectedFurniture.transform.position.z + i),
							selectedFurniture.transform.right * selectedFurniture.size.x);

						Debug.DrawRay(new Vector3(selectedFurniture.transform.position.x + selectedFurniture.size.x - 0.5f, 0.5f, selectedFurniture.transform.position.z + i),
							-selectedFurniture.transform.right * selectedFurniture.size.x);

						if (Physics.Raycast(new Vector3(selectedFurniture.transform.position.x - 0.5f, 0.5f, selectedFurniture.transform.position.z + i),
							selectedFurniture.transform.right, out hit, selectedFurniture.size.x, pickupLayer))
						{
							if (hit.collider != selectedFurniture.collider)
							{
								up = 0;
							}
						}
						if (Physics.Raycast(new Vector3(selectedFurniture.transform.position.x + selectedFurniture.size.x - 0.5f, 0.5f, selectedFurniture.transform.position.z + i),
							-selectedFurniture.transform.right, out hit, selectedFurniture.size.x, pickupLayer))
						{
							if (hit.collider != selectedFurniture.collider)
							{
								down = 0;

							}
						}
					}

					float currentX = selectedFurniture.transform.position.x;
					selectedFurniture.transform.position = new Vector3(hand.transform.position.x + offset.x, 0, selectedFurniture.transform.position.z);


					if (selectedFurniture.transform.position.x < currentX)
					{
						if (down == 0)
						{
							selectedFurniture.transform.position = new Vector3(currentX, 0, selectedFurniture.transform.position.z);
						}
					}
					if (selectedFurniture.transform.position.x > currentX)
					{
						if (up == 0)
						{
							selectedFurniture.transform.position = new Vector3(currentX, 0, selectedFurniture.transform.position.z);
						}
					}


					selectedFurniture.targetPos = selectedFurniture.transform.position;
				}
				if (DragY)
				{

					float up = 1;
					float down = 1;
					for (int i = 0; i < selectedFurniture.size.x; i++)
					{
						Debug.DrawRay(new Vector3(selectedFurniture.transform.position.x + i, 0.5f, selectedFurniture.transform.position.z - 0.5f),
							selectedFurniture.transform.forward * selectedFurniture.size.y);

						Debug.DrawRay(new Vector3(selectedFurniture.transform.position.x + i, 0.5f, selectedFurniture.transform.position.z + selectedFurniture.size.y - 0.5f),
							-selectedFurniture.transform.forward * selectedFurniture.size.y);

						if (Physics.Raycast(new Vector3(selectedFurniture.transform.position.x + i, 0.5f, selectedFurniture.transform.position.z - 0.5f),
							selectedFurniture.transform.forward, out hit, selectedFurniture.size.y, pickupLayer))
						{
							if (hit.collider != selectedFurniture.collider)
							{

								up = 0;
							}
						}
						if (Physics.Raycast(new Vector3(selectedFurniture.transform.position.x + i, 0.5f, selectedFurniture.transform.position.z + selectedFurniture.size.y - 0.5f),
							-selectedFurniture.transform.forward, out hit, selectedFurniture.size.y, pickupLayer))
						{
							if (hit.collider != selectedFurniture.collider)
							{
								down = 0;

							}
						}
					}

					float currentZ = selectedFurniture.transform.position.z;
					selectedFurniture.transform.position = new Vector3(selectedFurniture.transform.position.x, 0, hand.transform.position.z + offset.z);


					if (selectedFurniture.transform.position.z < currentZ)
					{
						if (down == 0)
						{
							selectedFurniture.transform.position = new Vector3(selectedFurniture.transform.position.x, 0, currentZ);
						
						}
					}
					if (selectedFurniture.transform.position.z > currentZ)
					{
						if (up == 0)
						{
							selectedFurniture.transform.position = new Vector3(selectedFurniture.transform.position.x, 0, currentZ);
						
						}
					}


					selectedFurniture.targetPos = selectedFurniture.transform.position;
				}

				if (!DragY && !DragX)
				{

					Vector3 difference = startPos - hand.transform.position;
					var distanceInX = Mathf.Abs(difference.x);
					var distanceInY = Mathf.Abs(difference.y);
					var distanceInZ = Mathf.Abs(difference.z);
					if (distanceInX > 0.1f)
					{
						DragX = true;
					}
					else if (distanceInZ > 0.1f)
					{
						DragY = true;
					}
				}

			}

		}
	}
	private Vector3 offset;
	bool DragX;
	bool DragY;
	Vector3 startPos;

	public GridObject GetClosestGridToObject(FurnitureObject f)
	{
		float tMin = Mathf.Infinity;
		GridObject closest = null;
		foreach (GridObject o in gridObjects)
		{
			if (Vector3.Distance(f.transform.position, o.transform.position) < tMin)
			{
				tMin = Vector3.Distance(f.transform.position, o.transform.position);
				closest = o;
			}
		}
		return closest;
	}
	public bool CanAddItemToGridSlot(FurnitureObject f, Vector2 pos)
	{

		GridSlot s = GetGridSlot(pos);
		if (s.objectInSlot != null && s.objectInSlot != f)
		{
			return false;
		}
		for (int i = 0; i < f.size.y; i++)
		{
			GridSlot slot = GetGridSlot(pos.x, pos.y + i);
			if (slot == null || slot.objectInSlot != null && slot.objectInSlot != f)
			{
				return false;
			}
		}
		for (int i = 0; i < f.size.x; i++)
		{
			GridSlot slot = GetGridSlot(pos.x + i, pos.y);
			if (slot == null || slot.objectInSlot != null && slot.objectInSlot != f)
			{
				return false;
			}
		}



		return true;
	}

	public bool SlotAvailable(FurnitureObject f, GridSlot slot)
	{
		if (slot == null || slot.objectInSlot != null && slot.objectInSlot != f)
		{
			return false;
		}
		return true;
	}
	public bool AddItemToGridSlot(FurnitureObject f, Vector2 pos)
	{
		if (CanAddItemToGridSlot(f, pos))
		{
			GridSlot s = GetGridSlot(pos);

			for (int i = 0; i < f.size.y; i++)
			{

				GridSlot slot = GetGridSlot(pos.x, pos.y + i);
				if (slot.gridPos == targetGridPos && f == targetFurniture)
				{
					CompleteLevel(f);
				}
				f.AddSlot(slot);

			}
			for (int i = 0; i < f.size.x; i++)
			{
				GridSlot slot = GetGridSlot(pos.x + i, pos.y);
				if (slot.gridPos == targetGridPos && f == targetFurniture)
				{
					CompleteLevel(f);
				}
				f.AddSlot(slot);

			}

			f.targetPos = s.gridObject.transform.position;

			if (s.gridPos == targetGridPos && f == targetFurniture)
			{
				CompleteLevel(f);
				
			}
			return true;
		}

		return false;
	}

	public void CompleteLevel(FurnitureObject f)
	{
		f.enabled = false;
	
		f.transform.SetParent(doorHolder.transform);
		f.transform.localPosition = Vector3.zero;
		DoorAnim.SetTrigger("Open");
		Debug.Log("YAY YOU WIN");
	}

	public void RemovItemFromGridSlot(FurnitureObject f)
	{
		f.RemoveFromSlots();
	}

	public GridSlot GetGridSlot(Vector2 pos)
	{
		GridSlot gridSlot = null;
		grid.TryGetValue(pos, out gridSlot);
		return gridSlot;
	}

	public GridSlot GetGridSlot(float x, float y)
	{
		GridSlot gridSlot = null;
		grid.TryGetValue(new Vector2(x, y), out gridSlot);
		return gridSlot;
	}

}

[System.Serializable]
public class GridSlot
{
	public Vector2 gridPos;

	public FurnitureObject objectInSlot;

	public GridObject gridObject;
	public GridSlot(Vector2 pos)
	{
		gridPos = pos;
	}
	public GridSlot(float x, float y)
	{
		gridPos = new Vector2(x, y);
	}
}


