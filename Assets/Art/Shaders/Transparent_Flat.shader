﻿Shader "Transparent_Flat" {
    Properties{
        _Color("Main Color", Color) = (1,1,1,1)
        _MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
    }

        SubShader{
            Tags {"RenderType" = "Transparent" "Queue" = "Transparent"}
            // Render into depth buffer only
            Pass {
                ColorMask 0
            }
        // Render normally
        Pass {
            ZWrite Off
            Blend SrcAlpha OneMinusSrcAlpha
            ColorMask RGB



            CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag
        #include "UnityCG.cginc"

        struct v2f {
            float4 pos : SV_POSITION;
            float3 uv : TEXCOORD0;
         };

        v2f vert(float4 v : POSITION, float3 n : NORMAL)
        {
            v2f o;
            o.pos = UnityObjectToClipPos(v);
            return o;
        }

        half4 _Color;
        half4 frag(v2f i) : SV_Target
        {
            return _Color;
        }

        ENDCG
            //Material {
            //    Diffuse[_Color]
            //    Emission[_Color]
            //    //Ambient[_Color]
            //}
            //Lighting On
            //SetTexture[_MainTex] {
            //    Combine texture * primary DOUBLE, texture * primary
            //}
        }
    }
}