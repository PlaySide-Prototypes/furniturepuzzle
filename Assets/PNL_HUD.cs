﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PNL_HUD : MonoBehaviour
{
	public TMPro.TextMeshProUGUI movesText;
	public TMPro.TextMeshProUGUI timerText;

	public void UpdateMoves(int moves)
	{
		movesText.text = moves.ToString();
	}
	public void UpdateTime(float time)
	{

		var minutes = time / 60; //Divide the guiTime by sixty to get the minutes.
		var seconds = time % 60;//Use the euclidean division for the seconds.
		//var fraction = (time * 100) % 100;

		//update the label value
		timerText.text = string.Format("{0:00} : {1:00}", minutes, seconds);
	}
}
